﻿Set @micNum = 116;

SELECT ses.id Into @sessionId
FROM sessions as ses
WHERE ses.isFinished = false
ORDER BY ses.StartDate DESC
LIMIT 1;

Select @sessionId ;

SELECT d.LastName, d.FirstName, d.SecondName, d.CardCode, sd.IsRegistered AS 'Registered', sd.IsCardRegistered as 'Present'
FROM sesdelegates as sd
INNER JOIN delegates as d ON sd.idDelegate = d.id
INNER JOIN seats ON sd.idSeat = seats.id

WHERE seats.MicNum = @micNum and sd.idSession = @sessionId;