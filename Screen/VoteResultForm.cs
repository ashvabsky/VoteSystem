﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class VoteResultForm : BaseScreenForm
    {
        public VoteResultForm(Controller c)
            : base(c)
        {
            InitializeComponent();
            tmrClose.Interval = 60000;
            //tmrClose.Start();
        }



        public override void UpdateData(Object o)
        {
            Platform.VoteInfoResult info = new Platform.VoteInfoResult();
            if (o is Platform.VoteInfoResult)
                info = (Platform.VoteInfoResult)o;

            lblVote_A.Text = info.VotedQnty_A.ToString();
            lblVote_B.Text = info.VotedQnty_B.ToString();
            lblVote_C.Text = info.VotedQnty_C.ToString();
            lblDelegatenonvoted.Text = (info.DelegateQnty - info.VotedQnty_A - info.VotedQnty_B - info.VotedQnty_C).ToString();
            if (info.IsResult)
            {
                lblResult.Text = "Решение  принято";
                lblResult.ForeColor = Color.Yellow;

            }
            else
            {
                lblResult.Text = "Решение не принято";
                lblResult.ForeColor = Color.Red;
            }


        }

        private void VoteResultForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblVote_A.Text = "";
            lblVote_B.Text = "";
            lblVote_C.Text = "";
            lblDelegatenonvoted.Text = "";
            lblResult.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}
