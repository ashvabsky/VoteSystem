﻿namespace Screen
{
    partial class VoteStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lblQntyDelegates = new System.Windows.Forms.Label();
            this.lblQntyVoted = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.lblCaption = new System.Windows.Forms.Label();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(148, 449);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(502, 68);
            this.label2.TabIndex = 4;
            this.label2.Text = "Проголосовало:";
            // 
            // lblQntyDelegates
            // 
            this.lblQntyDelegates.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyDelegates.AutoSize = true;
            this.lblQntyDelegates.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyDelegates.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyDelegates.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyDelegates.Location = new System.Drawing.Point(794, 551);
            this.lblQntyDelegates.Name = "lblQntyDelegates";
            this.lblQntyDelegates.Size = new System.Drawing.Size(101, 68);
            this.lblQntyDelegates.TabIndex = 10;
            this.lblQntyDelegates.Text = "50";
            // 
            // lblQntyVoted
            // 
            this.lblQntyVoted.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyVoted.AutoSize = true;
            this.lblQntyVoted.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyVoted.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyVoted.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyVoted.Location = new System.Drawing.Point(797, 449);
            this.lblQntyVoted.Name = "lblQntyVoted";
            this.lblQntyVoted.Size = new System.Drawing.Size(101, 68);
            this.lblQntyVoted.TabIndex = 16;
            this.lblQntyVoted.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(148, 551);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(522, 68);
            this.label6.TabIndex = 15;
            this.label6.Text = "Карт на пультах:";
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblTimeLeft.AutoSize = true;
            this.lblTimeLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeLeft.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTimeLeft.ForeColor = System.Drawing.Color.Gold;
            this.lblTimeLeft.Location = new System.Drawing.Point(748, 350);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(225, 68);
            this.lblTimeLeft.TabIndex = 18;
            this.lblTimeLeft.Text = "50 : 50";
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.BackColor = System.Drawing.Color.Transparent;
            this.lblCaption.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCaption.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Location = new System.Drawing.Point(192, 205);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(641, 77);
            this.lblCaption.TabIndex = 22;
            this.lblCaption.Text = "Идет голосование";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(160, 364);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.progressBarControl1.Properties.EndColor = System.Drawing.Color.Brown;
            this.progressBarControl1.Properties.LookAndFeel.SkinName = "Money Twins";
            this.progressBarControl1.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.progressBarControl1.Properties.StartColor = System.Drawing.Color.Brown;
            this.progressBarControl1.Size = new System.Drawing.Size(556, 43);
            this.progressBarControl1.TabIndex = 23;
            // 
            // VoteStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.lblCaption);
            this.Controls.Add(this.lblTimeLeft);
            this.Controls.Add(this.lblQntyVoted);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblQntyDelegates);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VoteStartForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.VoteStartForm_Load);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblQntyDelegates, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblQntyVoted, 0);
            this.Controls.SetChildIndex(this.lblTimeLeft, 0);
            this.Controls.SetChildIndex(this.lblCaption, 0);
            this.Controls.SetChildIndex(this.progressBarControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQntyDelegates;
        private System.Windows.Forms.Label lblQntyVoted;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTimeLeft;
        private System.Windows.Forms.Label lblCaption;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;

    }
}

