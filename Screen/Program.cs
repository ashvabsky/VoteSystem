﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;

namespace Screen
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DevExpress.UserSkins.BonusSkins.Register();
            DefaultLookAndFeel defaultLF = new DefaultLookAndFeel();
            defaultLF.LookAndFeel.UseDefaultLookAndFeel = true;

            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Caramel";
            DevExpress.LookAndFeel.UserLookAndFeel.Default.UseWindowsXPTheme = false;


            using (TitleForm mainform = new TitleForm())
            {
                Application.Run(mainform);

            }
        }
    }
}
