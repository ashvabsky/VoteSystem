﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Screen
{
    public partial class DelegateForm : BaseScreenForm
    {
        public DelegateForm(Controller c):base(c)
        {
            InitializeComponent();
            //tmrClose.Start();
        }


        public override void UpdateData(Object o)
        {
            Platform.DelegateInfo info = new Platform.DelegateInfo();
            if (o is Platform.DelegateInfo)
                info = (Platform.DelegateInfo)o;

            lblFIO.Text = info.FIO;
            lblFraction.Text = info.FractionName;
            lblParty.Text = info.PartyName;
            lblRegion.Text = info.RegionName;
            lblSeat.Text = "Ряд - " + info.RowNum + " , Место - " + info.SeatNum + " , № пульта - " + info.MicNum;
        }

        private void DelegateForm_Load(object sender, EventArgs e)
        {
            this.Left = this.Owner.Left;
            this.Top = this.Owner.Top;

            lblFIO.Text = "";
            lblFraction.Text = "";
            lblParty.Text = "";
            lblRegion.Text = "";
            lblSeat.Text = "";
        }


    }
}
