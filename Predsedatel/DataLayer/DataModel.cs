﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem;

namespace Predsedatel.DataLayer
{
    public class DataModel
    {
        static public XPCollection<SesDelegateObj> SesDelegates; // Привязка депутатов к определнной сессии и к месту в зале.
        static public XPCollection<SessionObj> Sessions; // Список сессий


        static public XPCollection<SesTaskObj> SesQuestions; // вопросы сессии, оригинал хранится на главной форме // XPCollection<SesQuestionObj>
        static public XPCollection<TaskKindObj> QuestKinds; // виды вопросов

        static public XPCollection<StatementObj> Statements; // выступающие депутаты
        static public XPCollection<DelegateStatementObj> stDelegates; // выступающие депутаты

        static public XPCollection<SettingsObj> Settings;
        static public SettingsObj TheSettings;
        static public SessionObj TheSession;


        public DataModel()
        {
        }

        static public void Init()
        {
            Sessions = new XPCollection<SessionObj>(Session.DefaultSession);
            Settings = new XPCollection<SettingsObj>(Session.DefaultSession);
            TheSettings = Settings[0] as SettingsObj;
        }


        static public int GetCardQnty()
        {
            int res = 0;
            using (XPCollection<SesDelegateObj> SesDelegates = new XPCollection<SesDelegateObj>(Session.DefaultSession))
            {
                CriteriaOperator criteria1 = CriteriaOperator.Parse("idSession = ? AND idSeat.MicNum > ? AND IsCardRegistered == ? AND idDelegate.idType.IsVoteRight = ?", TheSession.id, 0, true, true);
                SesDelegates.Criteria = criteria1;
                res = SesDelegates.Count;
            }
            return res;
        }

        static public XPCollection<SesTaskObj> GetTasks(ref int _iMimQNum)
        {
            CriteriaOperator criteria2 = CriteriaOperator.Parse("idSession.id = ? AND State == ? AND IsDel = ? AND IsHot = ?", TheSession.id, 0, false, false);

            XPCollection<SesTaskObj> tasks = new XPCollection<SesTaskObj>(criteria2);

            if (tasks.Count > 0)
                _iMimQNum = tasks.Min<SesTaskObj>(v => v.QueueNum);

            return tasks;
        }

        static public void LoadSession()
        {
            DateTime maxdate = DateTime.MinValue;
            TheSession = null;
            foreach (SessionObj ses in Sessions)
            {
                if (ses.IsFinished == false)
                {
                    if (ses.StartDate > maxdate)
                    {
                        maxdate = ses.StartDate;
                        TheSession = ses;
                    }
                }
            }

            if (TheSession == null)
            {
                int maxid = Sessions.Max<SessionObj>(v => v.id);
                TheSession = Sessions.Lookup(maxid);
            }

            if (TheSession == null)
            {
                string Err = "В базе данных нет ни одной сессии";
                Exception Ex = new Exception(Err);
                throw (Ex);
            }

            CriteriaOperator cr = new BinaryOperator(
            new OperandProperty("idSession"), new OperandValue(TheSession.id),
            BinaryOperatorType.Equal);

//          SesDelegates.Criteria = cr;
            CriteriaOperator cr2 = CriteriaOperator.Parse("idSession == ? AND IsDeleted == ?", TheSession.id, false);
            CriteriaOperator cr3 = CriteriaOperator.Parse("idSession == ? AND IsRegistered == ?", TheSession.id, true);
        }

    }
}
