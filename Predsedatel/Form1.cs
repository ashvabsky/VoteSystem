﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using Predsedatel.DataLayer;
using VoteSystem;
using DevExpress.Xpo;
using DevExpress.XtraGrid;
using DevExpress.Utils;

namespace Predsedatel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataModel.Init();
            DataModel.LoadSession();

            panelQuestions.Visible = DataModel.TheSettings.Pred_IsAgenda;
            if (DataModel.TheSettings.Pred_IsAgenda == false)
            {
                splitContainerLeft.SplitterPosition = splitContainerLeft.SplitterPosition + panelQuestions.Height;

            }

            ConditionsAdjustment();
        }

        private void InitDelegates()
        {
        }

        private void MainTabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            InitDelegates();
        }

        private void btnMainPage_Click(object sender, EventArgs e)
        {
        }

        private void btnQuorumPage_Click(object sender, EventArgs e)
        {
        }

        private void btnSesQuestions_Click(object sender, EventArgs e)
        {
        }

        private void btnStatements_Click(object sender, EventArgs e)
        {
        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }

        static int icnt = 0;
        int _iMinQNum = 0;

        private void tmrData_Tick(object sender, EventArgs e)
        {

            // Заполнение грида с вопросами
            if ((icnt % 6) == 0)
            {
                if (DataModel.TheSettings.Pred_IsAgenda == true)
                {
                    gridQuestions.DataSource = null;
                    gridDelegates.DataSource = null; // делегатов тоже надо обнулить, чобы избежать прооблем с DropCache
                    Session.DefaultSession.DropCache();


                    int minQNum = 0;
                    gridQuestions.DataSource = DataModel.GetTasks(ref minQNum);
                    if (_iMinQNum != minQNum)
                    {
                        _iMinQNum = minQNum;
                        ConditionsAdjustment();
                    }
                    gridQuestions.Update();
                }

                txtInfo.Text = DataModel.GetCardQnty().ToString();
            }

            icnt++;

            // Заполнение грида с записавшимися депутатами

            gridDelegates.DataSource = null;
            if (DataModel.TheSettings.Pred_IsAgenda == false)
            {
                Session.DefaultSession.DropCache();
            }
         

            CriteriaOperator statcriteria = CriteriaOperator.Parse("idSession.id = ? AND State == ?", DataModel.TheSession.id, 2);

            XPCollection<StatementObj> statems = new XPCollection<StatementObj>(statcriteria);

            statems.Load();


            CriteriaOperator criteria1 = CriteriaOperator.Parse("idStatement.id == ?", 0);
            if (statems.Count > 0)
            {
                int cnt = statems.Count;
                StatementObj currstatem = (StatementObj)statems[cnt-1];
                criteria1 = CriteriaOperator.Parse("(idStatement.id == ?) AND (State == 0 OR State == 2)", currstatem.id);

                XPCollection<DelegateStatementObj> stDelegates = new XPCollection<DelegateStatementObj>(criteria1);
                gridDelegates.DataSource = stDelegates;
                gridDelegates.Update();
                // обновить текстовые поля
            }

        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            if (DataModel.TheSettings.Pred_IsAgenda == false)
            {
                splitContainerLeft.SplitterPosition = this.Height - panelQuestions.Height;

            }
        }

        private void splitContainerControl2_Panel1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.None;
            }

        }

        private void gridDelegates_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.None;
           }

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            pictureEdit2.Focus();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (DataModel.TheSettings.Pred_IsAgenda == false)
            {
                splitContainerLeft.SplitterPosition = 1000;

            }

        }

        private void ConditionsAdjustment()
        {
            StyleFormatCondition cn;
            gridQuestionsView.FormatConditions.Clear();

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridQuestionsView.Columns["QueueNum"], null, _iMinQNum);
            cn.ApplyToRow = true;
            Font def = gridQuestionsView.Appearance.Row.Font;             
            Font f = new Font(def.FontFamily, def.Size, FontStyle.Bold);
            cn.Appearance.Font = f;
            cn.Appearance.ForeColor = Color.Purple;
            gridQuestionsView.FormatConditions.Add(cn);

        }

    
    }
}
