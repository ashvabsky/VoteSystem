﻿
SET @current_id = (SELECT questions.id
  FROM questions 
  INNER JOIN questions_results as res ON questions.idResult = res.id
  ORDER BY res.id DESC
  LIMIT 1);

Select @current_id ;

Select q.Name as 'Name', task.Description as 'Note', 
  voteresults.AnsQnty1 as 'Za', voteresults.AnsQnty2 as 'Vozd', voteresults.AnsQnty3 as 'Protiv', voteresults.AvailableQnty as 'Prisutstvie',  
  en_res.Name as 'Result'
FROM questions as q
INNER JOIN questions_results as res on q.idResult = res.id
INNER JOIN enresultvalues as en_res on res.idResultValue = en_res.id
INNER JOIN voteresults on res.idVoteResult = voteresults.id
INNER JOIN sestasks as task on q.idTask = task.id
WHERE q.id = @current_id

