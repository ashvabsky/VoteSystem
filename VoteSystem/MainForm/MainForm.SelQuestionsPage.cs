﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public interface ISelQuestionsPage
    {
        IMsgMethods GetShowInterface();
//      IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void Init();
        void InitControls(ref XPCollection<SesTaskObj> SesTasks);
        int NavigatorPosition { get; set; }
        void EnableButtons(bool IsVoted);

        void SelectPos (int id);

        XtraForm GetView();
    }

    partial class MainForm : ISelQuestionsPage
    {
        SelQuestionsCntrler _Controller_SQ;

#region реализация интерфейса ISelQuestionsPage
        IMsgMethods ISelQuestionsPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }
/*
        IQuestProperties ISelQuestionsPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }
*/

        void ISelQuestionsPage.Init()
        {
            _Controller_SQ = (SelQuestionsCntrler)_Controller.GetSubController("Select Questions Page Controller"); ;
            ConditionsAdjustment_SelQuest();
        }

        void ISelQuestionsPage.InitControls(ref XPCollection<SesTaskObj> SesTasks)
        {
            PropertiesControlQuest.DataSource = SesTasks;
            SelQuestGrid.DataSource = SesTasks;
            navigatorQuestions.DataSource = SesTasks;


            SesTasks.Reload();
            gridViewSelQuest.ExpandAllGroups();
        }

        int ISelQuestionsPage.NavigatorPosition
        {
            get
            {
                return navigatorQuestions.Position;
            }
            set
            {
                navigatorQuestions.Position = value;
            }
        }

        void ISelQuestionsPage.SelectPos (int id)
        {
            int handle = gridViewSelQuest.LocateByValue(0, gridViewSelQuest.Columns["id"], id);
            gridViewSelQuest.FocusedRowHandle = handle;
        }

/*
        IQuestProperties GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }
*/

        DevExpress.XtraEditors.XtraForm ISelQuestionsPage.GetView()
        {
            return this;
        }

        void ISelQuestionsPage.EnableButtons(bool IsVoted)
        {
/*
            navControlQuestions.Buttons.CustomButtons[1].Enabled = !IsVoted;
            navControlQuestions.Buttons.CustomButtons[2].Enabled = !IsVoted;
            navControlQuestions.Buttons.CustomButtons[3].Enabled = !IsVoted;
            navControlQuestions.Buttons.CustomButtons[4].Enabled = !IsVoted;
 */ 
        }

#endregion

        private void navControlQuestions_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    _Controller_SQ.AppendItem();

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = _messages.ShowQuestion("Вы уверены, что хотите удалить вопрос из повестки дня?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller_SQ.RemoveCurrentItem();
                    }

                }
                else if (e.Button.Tag.ToString() == "Edit")
                {
                    _Controller_SQ.EditCurrRecord();
                }
                else if (e.Button.Tag.ToString() == "Queue_Up" || e.Button.Tag.ToString() == "Queue_Down")
                {
                    _Controller_SQ.ChangeQueuePos(e.Button.Tag.ToString());
                }
            }
        }


        private void navigatorQuestions_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller_SQ != null)
                _Controller_SQ.UpdatePropData();
        }

        private void ConditionsAdjustment_SelQuest()
        {
            StyleFormatCondition cn;
/*
            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewDelegates.Columns["TaskState"], null, 1);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            cn.Appearance.ForeColor = Color.Navy;
            gridViewSelQuest.FormatConditions.Add(cn);

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewDelegates.Columns["TaskState"], null, 0);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            gridViewSelQuest.FormatConditions.Add(cn);
*/
            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewSelQuest.Columns["IsHot"], null, 1);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            cn.Appearance.ForeColor = Color.Purple;
            gridViewSelQuest.FormatConditions.Add(cn);

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridViewSelQuest.Columns["IsHot"], null, 0);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Regular);
            gridViewSelQuest.FormatConditions.Add(cn);
        }


        private void SelQuestGrid_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridViewSelQuest.CalcHitInfo(SelQuestGrid.PointToClient(MousePosition));
            if (!hi.InRow) return;

            _Controller_SQ.EditCurrRecord();
        }

        private void SelQuestGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                _Controller_SQ.EditCurrRecord();
            }
        }
   }
}