﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm, IDisposable
    {
        public MsgForm _msgForm;
        public IMsgMethods _messages;

        MainCntrler _Controller;
        bool _IsStatementApp = false;
        bool _IsDisposed = false;


        public MainForm()
        {
            InitializeComponent();

            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

            _Controller = new MainCntrler(this);

        }

        ~MainForm()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (DataModel.IsSpeechApp == false)
            {
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmDelegates"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmRegistration"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmQuestions"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgenda"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgendaItems"]);
            }
            else
            {
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmQuestions"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmDelegates"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmRegistration"]);
//              navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgenda"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmAgendaItems"]);
                navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmProtocol"]);

                ribbon.Visible = false;
//                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.Height = this.Height - ribbon.Height;
            }
            //          navBarGroup1.ItemLinks.Add(navBarControl1.Items["itmStatements"]);

            chkPanels.Checked = true;

            _Controller.OnMainFormShown();
        }

        public void SetStatementAppView()
        {
            this.Text = "Система управления выступлениями";
            navBarControl1.Items["itmRegistration"].Caption = "Расположение в зале";
//          navBarControl1.Items["itmAgendaItems"].LargeImageIndex = 6;
            navBarControl1.Items["itmQuestions"].Caption = "Выступления";
            navBarControl1.Items["itmQuestions"].LargeImageIndex = 6;

            barbtnQuestions.Enabled = false;

            _IsStatementApp = true;

        }

        private void barbtnDelegates_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowGlobalDelegates();
        }

        private void barbtnQuestions_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowGlobalQuestions();
        }

        private void navBarControl1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            ShowPage(e.Link.ItemName);
        }

        public void SetCaption(string caption)
        {
            this.Text = caption;
        }

        public void ShowPage(string PageName)
        {
            if (PageName != "itmProtocol")
            {
                pgSession.PageVisible = false;
                pgDelegates.PageVisible = false;
                pgSelQuestions.PageVisible = false;
                pgQuorumGraphic.PageVisible = false;
                pgQuestions.PageVisible = false;
                pgStatements.PageVisible = false;
            }

            if (PageName == "itmAgenda")
            {
                pgSession.PageVisible = true;
            }
            else if (PageName == "itmDelegates")
            {
                pgDelegates.PageVisible = true;
            }
            else if (PageName == "itmAgendaItems")
            {
                pgSelQuestions.PageVisible = true;
            }
            else if (PageName == "itmRegistration")
            {
                pgQuorumGraphic.PageVisible = true;
            }
            else if (PageName == "itmQuestions")
            {
                if (_IsStatementApp == false)
                    pgQuestions.PageVisible = true;
                else
                    pgStatements.PageVisible = true;
            }
            else if (PageName == "itmStatements")
            {
                pgStatements.PageVisible = true;
            }
            else if (PageName == "itmProtocol")
            {
                _Controller.PrintReport();
            }

        }

        public void EnableMonitor(bool IsEnabled)
        {
            chkPanels.Checked = IsEnabled;
        }

        public void EnableMonitorButtons(bool IsEnabled)
        {
//          barbtnClearMonitor.Enabled = IsEnabled;
            barbtnSplashToMonitor.Enabled = IsEnabled;
            barbtnInfoToMonitor.Enabled = IsEnabled;
            barbtnMonitorMsg.Enabled = IsEnabled;

            if (IsEnabled)
            {
                chkPanels.Caption = "Выключить";
            }
            else
            {
                chkPanels.Caption = "Включить";
            }
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == null)
                return;

            _Controller.InitPage(e.Page.Name);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pgDelegates.PageVisible = false;
            pgSession.PageVisible = false;
            pgQuorumGraphic.PageVisible = false;
            pgQuestions.PageVisible = false;
            pgSelQuestions.PageVisible = false;
            pgStatements.PageVisible = false;

            _Controller.Init(ref xpTasks/*, ref xpStatements*/);
            tmrGeneral.Start();
        }


        public void ClearMonitor()
        {
//          barbtnClearMonitor.Checked = true;
        }
            
        public void EnableInfoToMonitor(bool IsEnable)
        {
  //        ClearMonitor();
            barbtnInfoToMonitor.Enabled = IsEnable;
        }

        private void barbtnInfoToMonitor_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview();
        }

        private void barbtnExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void barbtnPredsedSendMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение для председателя";
            stf.ShowDialog();
        }

        private void barbtnMonitorMsg_ItemClick(object sender, ItemClickEventArgs e)
        {
            SendTextForm stf = new SendTextForm();
            stf.Text = "Текстовое сообщение на мониторы в зале";
            stf.ShowDialog();
        }


        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview();

        }

        private void chkPanels_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void chkPanels_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            bool isE = (bool)chkPanels.Checked;
            _Controller.EnableMonitors(isE);
        }

        private void btnSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowSettings();
        }

        private void barbtnSplashToMonitor_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowSplahToMonitors();
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            tmrGeneral.Stop();
           
            _Controller.OnClosing();
            Application.Exit();
        }


        private void tmrGeneral_Tick(object sender, EventArgs e)
        {
            tmrGeneral.Stop();
            if (_Controller.OnGeneralTimer() == true)
            {
                tmrGeneral.Start();
            }
        }

        private void btndrvStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.DriverStart();
        }

        private void btndrvStop_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.DriverStop();
        }

        private void btndrvHitMics_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.DriverMicHit();
        }

        private void barbtnReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.PrintReport();

        }

        private void barbtnMainMenu_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller.ShowStartMenu();
        }

        public void ShowHistoryMsg(string msg)
        {
            barHistoryList.Strings.Add(msg);
            if (msg.Length > 156)
                msg = msg.Substring(1, 156);
            barStaticText.Caption = msg;
        }

        private void ribbon_SizeChanged(object sender, EventArgs e)
        {
            int itemscount = 14;
            int borderwidth = 2;

            int width = ribbon.Width;
            width = (width - itemscount * borderwidth) / itemscount;
            barbtnMainMenu.LargeWidth = width;
            chkPanels.LargeWidth = width;
            barbtnSplashToMonitor.LargeWidth = width;
            barbtnInfoToMonitor.LargeWidth = width;
            barbtnPredsedSendMsg.LargeWidth = width;
            barbtnMonitorMsg.LargeWidth = width;
            barbtnDelegates.LargeWidth = width;
            barbtnQuestions.LargeWidth = width;
            btndrvStop.LargeWidth = width;
            btndrvStart.LargeWidth = width;
            btndrvHitMics.LargeWidth = width;
            barbtnReport.LargeWidth = width;
            btnSettings.LargeWidth = width;
            barbtnExit.LargeWidth = width;
        }

    }
}
