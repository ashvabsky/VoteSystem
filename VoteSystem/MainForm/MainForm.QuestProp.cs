﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
/*
    public interface IQuestProperties
    {
        void SetViewMode();
    }

    partial class MainForm : IQuestProperties
    {
        string _selectedVoteType = "";
        string _selectedVoteKind = "";
        string _selectedVoteQnty = "";
        string _selectedDecisionProcType = "";

        private void SetViewMode()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControlQuest.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                    foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowinin in rowin.ChildRows)
                    {
                        rowinin.Properties.ReadOnly = true;
                    }
                }
            }

        }


        # region IQuestProperties methods
        void IQuestProperties.SetViewMode()
        {
            this.SetViewMode();
        }



        private void btnPropQuestApply_Click(object sender, EventArgs e)
        {
        }

        private void btnPropQuestCancel_Click(object sender, EventArgs e)
        {
            // обновить поля
            _Controller_SQ.UpdatePropData();
        }

        #endregion


        private void repository_cmb_votetypes_SelectedIndexChanged(object sender, EventArgs e)
        {

            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteTypeObj vto = (VoteTypeObj)DataModel.VoteTypes[cmb.SelectedIndex];

            _selectedVoteType = vto.Name;
        }

        private void repository_cmb_VoteKinds_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteKindObj vko = (VoteKindObj)DataModel.VoteKinds[cmb.SelectedIndex];
            _selectedVoteKind = vko.Name;
        }

        private void repository_cmb_VoteQntyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
//          VoteQntyObj vqo = (VoteQntyObj)DataModel.VoteQntyTypes[cmb.SelectedIndex];
            _selectedVoteQnty = ""; //vqo.Name;

        }

        private void repository_cmb_DecisionProcTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
//          DecisionProcTypeObj dpt = (DecisionProcTypeObj)DataModel.DecisionProcTypes[cmb.SelectedIndex];
            _selectedDecisionProcType = ""; // dpt.Name;

        }

        private void repository_cedt_QuotaProcType_Present_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
        }

        private void repository_cedt_QuotaProcType_Total_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
        }


    }
 */ 
}