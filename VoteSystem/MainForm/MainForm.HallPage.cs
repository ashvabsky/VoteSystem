﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public interface IHallPage
    {
        IMsgMethods GetShowInterface();
//      void ShowView();

        void Init();
        void InitControls(bool IsStatementApp, SettingsHallObj hallsettings, SettingsObj cmnsettings);
        bool InitSeats(Dictionary<int, SesDelegateObj> SeatDelegatesList, Dictionary<int, SeatObj> SeatsList, bool IsDelegateSeatViewMode);
        void SetProperties(Dictionary<string, string> properties);
        void StartRegistration(int RegTime);
        void StartRegCards();
        void ShowCardChanges(bool show);
        void InitRegParams(int RegTime, int DelegatesQnty, int CardQnty, int RegQnty, int QuorumQnty);
        void StopRegistration();
        void StopRegCards();
        int ReqQnty{ set; }
        bool IsQuorum { set; }

        XtraForm GetView();
    }

    partial class MainForm : IHallPage
    {
        HallCntrler _Controller_H;

        int _CurrentImageNum = 0;

        DateTime _startregtime;
        int _RegTime;
        int _RegQnty = 0;

        int _MicNum = 0; // текущий номер микрфона

        #region реализация интерфейса IHallPage
        IMsgMethods IHallPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        void IHallPage.Init()
        {
            _Controller_H = (HallCntrler)_Controller.GetSubController("HallPage Controller");
        }

        void IHallPage.InitControls(bool IsStatementApp, SettingsHallObj hallsettings, SettingsObj cmnsettings)
        {
            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

            txtDelegateQnty.Properties.ReadOnly = true;

            txtRegQnty.Properties.ReadOnly = true;

            txtQuorumQnty.Properties.ReadOnly = true;

            lblQuorumFinished.Visible = false;

            if (IsStatementApp == true)
            {
                foreach (Control c in pnlQuorumButtons.Controls)
                {
                    c.Visible = false;
                }
                pnlHallSettings.Visible = false;
                pnlHall.Dock = DockStyle.Fill;
                
            }

            chkDelegateSeat.Checked = hallsettings.IsDelegateSeatViewMode;
            chkAutoCardMode.Checked = hallsettings.IsAutoCardMode;
            chkBackCardMode.Checked = hallsettings.IsBackCardMode;
            spinBackCardMode.Value = hallsettings.BackCardModeInterval;

            chkDelegateSeat.Enabled = cmnsettings.IsCardMode;
            chkAutoCardMode.Enabled = cmnsettings.IsCardMode;
            btnActAllCards.Enabled = cmnsettings.IsCardMode;
            btnCardStart.Enabled = cmnsettings.IsCardMode;
            btnTextOut.Enabled = cmnsettings.IsCardMode;
            tmrCardBackMode.Enabled = hallsettings.IsBackCardMode;


            if (cmnsettings.IsCardMode)
            {
                lblCardQnty.Text = "Кол-во карточек:";
            }
            else
            {
                lblCardQnty.Text = "Кол-во депутатов:";
            }

            btnActAllCards.Enabled = hallsettings.IsDelegateSeatViewMode;
            btnClearSeats.Enabled = hallsettings.IsDelegateSeatViewMode;
            btnTextOut.Enabled = hallsettings.IsDelegateSeatViewMode;

//          btnRegCancel.Enabled = true;
        }

        bool IHallPage.InitSeats(Dictionary<int, SesDelegateObj> SeatDelegatesList, Dictionary<int, SeatObj> SeatsList, bool IsDelegateSeatViewMode)
        {
            bool IsChanged = false;

            foreach (Control c in pnlHall.Controls)
            {
                if (c.Tag == null)
                    continue;
/*
                if (c.Tag.ToString() == "001")
                {
                    SeatComponent sc = (SeatComponent)c;
                    sc.SeatPicture.Image = imageCollection2.Images[2]; // серый человечек
                    continue;
                }
*/
                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();

                    int MicNum = Convert.ToInt32(s.Substring(3));
                    PictureEdit pic = (PictureEdit)((c is PictureEdit) ? c : ((SeatComponent)c).SeatPicture);

                    System.Drawing.Image img = null;
                    if (pic != null)
                        img = pic.Image;

                    if (SeatDelegatesList.ContainsKey(MicNum))
                    {
                        SesDelegateObj seatDelegate = SeatDelegatesList[MicNum];

                        if (seatDelegate.idDelegate.idType.CodeName != "Delegate_Active")
                        {
                            pic.Image = imageCollection2.Images[4];
                        }
                        else if (seatDelegate.IsCardRegistered)
                        {
                            if (seatDelegate.idSeat.PhysicalState == false || seatDelegate.idSeat.LogicalState == false)
                            {
                                pic.Image = imageCollection2.Images[2]; // серый человечек
                            }
                            else if (seatDelegate.IsRegistered == true)
                            {
                                pic.Image = imageCollection2.Images[0]; // зеленый человечек
                            }
                            else // если карточка есть, но делегат почему-то не зарегестрировался
                            {
                                pic.Image = imageCollection2.Images[2]; // серый человечек
                            }
                        }
                        else 
                        {
                            if (IsDelegateSeatViewMode == true)
                            {
                                if (seatDelegate.idSeat.PhysicalState == false)
                                {
                                    pic.Image = imageCollection2.Images[3]; // серый чел с красной окаймой
                                }
                                else if (seatDelegate.idSeat.LogicalState == false)
                                {
                                    pic.Image = imageCollection2.Images[3];
                                }
                                else
                                {
                                    pic.Image = imageCollection2.Images[3];
                                }
                            }
                            else
                            {
                                pic.Image = GetSeatImage(SeatsList, MicNum);
                            }
                        }

                        if (seatDelegate.SeatFixed == true)
                            pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
                        else
                        {
                            if (pic.BorderStyle == DevExpress.XtraEditors.Controls.BorderStyles.Simple)
                                pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
                        }

                    }
                    else
                    {
                        pic.Image = GetSeatImage(SeatsList, MicNum);
                    }

                    if (pic != null && img != null)
                    {
                        if (img.Equals(pic.Image) == false)
                            IsChanged = true;
                    }

                    pic.Update();

                }
            }

            return IsChanged;
        }

        private System.Drawing.Image GetSeatImage(Dictionary<int, SeatObj> SeatsList, int MicNum)
        {
            if (SeatsList.ContainsKey(MicNum))
            {
                if (SeatsList[MicNum].PhysicalState == false)
                {
                    return imageCollection2.Images[6]; // серый шар
                }
                else if (SeatsList[MicNum].LogicalState == false)
                {
                    return imageCollection2.Images[5]; // зеленый шар
                }
                else 
                {
                    if (SeatsList[MicNum].id == HallCntrler.ID_TRIBUNE)
                    {
                        return imageCollection2.Images[7]; // желтый шар
                    }
                    else
                        return imageCollection2.Images[1]; // зеленый шар
                }
            }
            else
            {
                return imageCollection2.Images[1]; // зеленый шар
            }
        }

        void IHallPage.SetProperties(Dictionary<string, string> properties)
        {
            PropertiesControl_QPage.Rows["row_q_FirstName"].Properties.Value = properties["FirstName"];
            PropertiesControl_QPage.Rows["row_q_SecondName"].Properties.Value = properties["SecondName"];
            PropertiesControl_QPage.Rows["row_q_LastName"].Properties.Value = properties["LastName"];

            PropertiesControl_QPage.Rows["row_q_PartyName"].Properties.Value = properties["PartyName"];
            PropertiesControl_QPage.Rows["row_q_Fraction"].Properties.Value = properties["FractionName"];
            PropertiesControl_QPage.Rows["row_q_RegionName"].Properties.Value = properties["RegionName"];
            PropertiesControl_QPage.Rows["row_q_RegionNum"].Properties.Value = properties["RegionNum"];

            PropertiesControl_QPage.Rows["row_q_MicNum"].Properties.Value = properties["MicNum"];
            PropertiesControl_QPage.Rows["row_q_RowNum"].Properties.Value = properties["RowNum"];
            PropertiesControl_QPage.Rows["row_q_SeatNum"].Properties.Value = properties["SeatNum"];
        }

        void IHallPage.StartRegistration(int RegTime)
        {
//          btnRegCancel.Enabled = false;
            picRegTime.Enabled = true;
            lblRegistration.Visible = true;
            lblRegistration.Enabled = true;
            lblRegistration.Text = "Идет регистрация...";
            progressBarRegistration.Enabled = true;
            progressBarRegistration.Visible = true;
//          btnRegCancel.Enabled = false;

//            btnRegStart.Text = "Завершить регистрацию";
//            btnRegStart.ImageIndex = 9;
            btnRegStart.Enabled = false;
            btnRegFinish.Enabled = true;

            _RegTime = RegTime;
            _startregtime = DateTime.Now;

            tmrRegStart.Start();
            tmrRotation.Start();
        }


        void IHallPage.ShowCardChanges(bool show)
        {
            if (show)
            {
                barProcesText.Visibility = BarItemVisibility.Always;
                barProcesText.Caption = "Произошли изменения в рассадке                                                      ";
            }
            else
            {
                barProcesText.Visibility = BarItemVisibility.Never;
            }
        }

        void IHallPage.StartRegCards()
        {
//          picRegTime.Enabled = true;
            lblRegistration.Visible = true;
            lblRegistration.Enabled = true;
            barProcesText.Visibility = BarItemVisibility.Always;
            barProcesText.Caption = "Идет идентификация карточек...                                                      ";
            picRegTime.Enabled = true;
            lblRegistration.Text = "Идет идентификация карточек...";
            progressBarRegistration.Enabled = true;
            progressBarRegistration.Visible = true;
            //          btnRegCancel.Enabled = false;

            //            btnRegStart.Text = "Завершить регистрацию";
            //            btnRegStart.ImageIndex = 9;
            btnRegStart.Enabled = false;
            btnRegFinish.Enabled = true;

            _startregtime = DateTime.Now;

            tmrRegCardStart.Start();
            tmrRotation.Start();
        }

        void IHallPage.InitRegParams(int RegTime, int DelegatesQnty, int CardQnty, int RegQnty, int QuorumQnty)
        {
            string stime = RegTime.ToString();
            if (stime.Length <= 1)
                stime = "0" + stime;
/*
            if (_Controller_H.RegStartMode == false && _Controller_H.RegCardMode == false)
                lblRegTime.Text = String.Format("{0} сек.", stime); ;
*/

            txtDelegateQnty.Text = DelegatesQnty.ToString();
            txtRegQnty.Text = RegQnty.ToString();
            txtQuorumQnty.Text = QuorumQnty.ToString();
            txtCardQnty.Text = CardQnty.ToString();

            if (RegQnty >= QuorumQnty)
                IsQuorum = true;
            else
                IsQuorum = false;

        }

        void IHallPage.StopRegistration()
        {
            if (tmrRegStart.Enabled == true)
            {
                tmrRegStart.Stop();
                tmrRotation.Stop();
            }

            progressBarRegistration.Position = 0;

            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];

            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            progressBarRegistration.Enabled = false;

//          btnRegStart.Text = "Начать регистрацию";
//          btnRegStart.ImageIndex = 0;
            btnRegStart.Enabled = true;
            btnRegFinish.Enabled = false;

//          btnRegCancel.Enabled = true;

        }

        void IHallPage.StopRegCards()
        {
            if (tmrRegCardStart.Enabled == true)
            {
                tmrRegCardStart.Stop();
                tmrRotation.Stop();
            }

            progressBarRegistration.Position = 0;

            _CurrentImageNum = 0;
            picRegTime.Image = TimerImages.Images[_CurrentImageNum];

            picRegTime.Enabled = false;
            lblRegistration.Enabled = false;
            barProcesText.Visibility = BarItemVisibility.Never;
            progressBarRegistration.Enabled = false;

            btnRegStart.Enabled = true;
            btnRegFinish.Enabled = false;
        }

        int IHallPage.ReqQnty
        {
            set { txtRegQnty.Text = value.ToString(); }
        }

        bool IHallPage.IsQuorum {set{IsQuorum = value;}}
        bool IsQuorum
        {
            set
            {
                if (value == true)
                {
                    lblQuorumFinished.Visible = true;
                }
                else
                {
                    lblQuorumFinished.Visible = false;
                }
            }

        }

        DevExpress.XtraEditors.XtraForm IHallPage.GetView()
        {
            return this;
        }

        #endregion



        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c is SeatComponent)
            {
                string ss = c.Tag.ToString();
                _MicNum = Convert.ToInt32(ss.Substring(3));
            }
            info_1.Visible = false;
            if (c.Tag == null)
            {
                _Controller_H.ShowDelegateProp(-1); // показать пустые поля
                return;
            }

            string s = c.Tag.ToString();
            _MicNum = Convert.ToInt32(s.Substring(3));

            int res = _Controller_H.ShowDelegateProp(_MicNum);
            if (res == -1)
                return;


            if (_MicNum > 0 && _Controller_H._TheDelegate != null)
            {

                info_1.Text = _Controller_H._TheDelegate.idDelegate.ShortName;

                int halfwidth = (info_1.Width - c.Width) / 2;
                info_1.Left = c.Left - halfwidth;
                info_1.Top = c.Top - info_1.Height;
                info_1.ForeColor = System.Drawing.Color.Purple;
                info_1.Visible = true;
            }
        }

        private void Seat_1_MouseEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c is SeatComponent)
            {
                string ss = c.Tag.ToString();
                _MicNum = Convert.ToInt32(ss.Substring(3));
            }
            info_1.Visible = false;
            if (c.Tag == null)
            {
                _Controller_H.ShowDelegateProp(-1); // показать пустые поля
                return;
            }

            string s = c.Tag.ToString();
            _MicNum = Convert.ToInt32(s.Substring(3));

            int res = _Controller_H.ShowDelegateProp(_MicNum);
            if (res == -1)
                return;


            if (_MicNum > 0 && _Controller_H._TheDelegate != null)
            {

                info_1.Text = _Controller_H._TheDelegate.idDelegate.ShortName;

                int halfwidth = (info_1.Width - c.Width) / 2;
                info_1.Left = c.Left - halfwidth;
                info_1.Top = c.Top - info_1.Height;
                info_1.ForeColor = System.Drawing.Color.Purple;
                info_1.Visible = true;
            }
        }

        private void Mic_DragDrop(object sender, DragEventArgs e)
        {
            Control c = (Control)sender;

            if (c is PictureEdit)
            {
                PictureEdit pic = (PictureEdit)c;
                pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            } else if (c is SeatComponent)
            {
                SeatComponent seat = (SeatComponent)c;
                seat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            }

            int DestSeatNum = 0;
            string tag = c.Tag.ToString();
            if (tag != "")
                DestSeatNum = Convert.ToInt32(tag.Substring(3));

            string[] formats = e.Data.GetFormats();
            bool b = e.Data.GetDataPresent("System.String");
            if (b == true)
            {
                string source = (string)e.Data.GetData("System.String");
                if (source != "")
                {
                    int SourceSeatNum = Convert.ToInt32(source.Substring(3));
                    _Controller_H.MoveDelegate(SourceSeatNum, DestSeatNum, false);
                }
            }
        }

        private void Mic_MouseDown(object sender, MouseEventArgs e)
        {
            if (_IsStatementApp == true)
                return;

            if (e.Button == MouseButtons.Left)
            {
                Control c = (Control)sender;

                string s = c.Tag.ToString();
                _MicNum = Convert.ToInt32(s.Substring(3));

                c.DoDragDrop(s, DragDropEffects.Move);
            }

        }

        private void Mic_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragOver2(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void Mic_DragEnter(object sender, DragEventArgs e)
        {

            PictureEdit pic = (PictureEdit)sender;
            if (pic != null)
            {
                pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            }
        }

        private void Mic_DragLeave(object sender, EventArgs e)
        {
            PictureEdit pic = (PictureEdit)sender;
            if (pic != null)
            {
                pic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            }
        }


        private void cntSwitherPhysical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _Controller_H.SetSwitchersValuePhysic(value);

        }

        private void cntSwitherLogical_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = (BarEditItem)sender;
            bool value = (bool)item.EditValue;
            _Controller_H.SetSwitchersValueLogic(value);
        }

        private void cntSeatFree_ItemPress(object sender, ItemClickEventArgs e)
        {
            _Controller_H.FreeCurrentSeat();
        }

        private void cntRegDelegate_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller_H.RegCurrentSeat();
        }

        private void cntRegCard_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller_H.RegCardCurrentSeat();

        }

        private void menuMicSeat_Popup(object sender, EventArgs e)
        {
            _Controller_H.SetPopupState(true);
            
            bool SwitcherPhysicalState = false;
            bool SwitcherLogicalState = false;
            bool SeatFree_Enable = true;
            bool SeatSelect_Enable = true;
            bool AllDisable = false;
            bool Register_Enable = true;
            bool RegisterCard_Enable = true;
            bool IsRegistered = false;
            bool IsCardRegistered = false;


            _Controller_H.GetPopupMenuStates(_MicNum, ref SwitcherPhysicalState, ref SwitcherLogicalState, ref SeatFree_Enable, ref SeatSelect_Enable, ref AllDisable, ref Register_Enable, ref IsRegistered, ref RegisterCard_Enable, ref IsCardRegistered);

            if (AllDisable || _IsStatementApp == true)
            {
                cntSwitherPhysical.Enabled = false;
                cntSwitherLogical.Enabled = false;
                cntSeatFree.Enabled = false;
                cntSeatSelect.Enabled = false;
                cntRegDelegate.Enabled = false;
            }
            else
            {
                cntSwitherPhysical.EditValue = SwitcherPhysicalState;
                cntSwitherLogical.EditValue = SwitcherLogicalState;
                cntSeatFree.Enabled = SeatFree_Enable;
                cntSeatSelect.Enabled = SeatSelect_Enable;
                cntRegDelegate.Enabled = Register_Enable;
                cntRegCard.Enabled = RegisterCard_Enable;

                if (IsRegistered)
                    cntRegDelegate.Caption = "Разрегистрировать депутата";
                else
                    cntRegDelegate.Caption = "Зарегистрировать депутата";

                if (IsCardRegistered)
                    cntRegCard.Caption = "Разрегистрировать карточку";
                else
                    cntRegCard.Caption = "Зарегистрировать карточку";
            }

            
        }

        private void menuMicSeat_CloseUp(object sender, EventArgs e)
        {
            _Controller_H.SetPopupState(false);
        }

        private void cntSeatSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller_H.Delegate_SetToSeat();
        }

        private void btnRegStart_Click(object sender, EventArgs e)
        {
            if (_Controller_H.RegStartMode == false)
                _Controller_H.Registration_PrepareToStart();

        }

        private void btnCardStart_Click(object sender, EventArgs e)
        {
            if (_Controller_H.IdentifyCards())
            {
//              _Controller_H.SaveToDBase();
                _Controller.HistoryAdd("СГ", "Определение карточек");
            }
        }

        private void btnRegFinish_Click(object sender, EventArgs e)
        {
            if (_Controller_H.RegStartMode == true)
                _Controller_H.Registration_Stop_Ask();
            else if (_Controller_H.RegCardMode == true)
                _Controller_H.RegCard_Stop();
        }


        private void tmrRegCardStart_Tick(object sender, EventArgs e)
        {
            OnRegCardModeTick();
        }

        private void tmrRegStart_Tick(object sender, EventArgs e)
        {
            {

                System.TimeSpan time = DateTime.Now - _startregtime;
                //          DateTime t = new DateTime(time.Ticks);

                int sec = _RegTime;
                int min = 0;
                int our = 0;
                if (sec >= 60)
                {
                    sec = (_RegTime % 60);
                    min = Convert.ToInt32(_RegTime / 60);
                    if (min >= 60)
                    {
                        min = (_RegTime % 60);
                        our = Convert.ToInt32(min / 60);
                    }
                }
                DateTime quorumTime = new DateTime(1900, 12, 13, our, min, sec);

                DateTime t = quorumTime.AddTicks(-time.Ticks);

                int QuorumSecs = quorumTime.Minute * 60 + quorumTime.Second;
                DateTime t2 = new DateTime(time.Ticks);
                int Secs = t2.Minute * 60 + t2.Second;

                int pos = 0;
                if (QuorumSecs != 0)
                    pos = (Secs * (100 + (300 / QuorumSecs))) / QuorumSecs;

                _Controller_H.OnRegProcess(t, pos);

                progressBarRegistration.Position = pos;
                lblRegTime.Text = t.ToString("mm:ss");

                if (t.Minute == 0 && t.Second == 0 || t.Day < quorumTime.Day)
                {
//                  pos = 100;
//                  _Controller_H.OnRegProcess(t.ToString("mm:ss"), pos);
                    _Controller_H.Registration_Stop();
                }
/*
                if (t.Minute == 0 && t.Second == 0 || t.Day < quorumTime.Day)
                {
                    pos = 100;
                    lblRegTime.Text = t.ToString("00:00");
                    lblRegistration.Text = "Идет подсчет результатов...";
                    progressBarRegistration.Position = pos;
                    tmrRegStop.Start();
                }
                else
                {
                    lblRegTime.Text = t.ToString("mm:ss");
                    progressBarRegistration.Position = pos;
                }
 */ 
            }
        }

        private void OnRegCardModeTick()
        {
/*
            if (_Controller_H.RegCardMode == false)
                return;
*/

            System.TimeSpan time = DateTime.Now - _startregtime;
            DateTime t = new DateTime(time.Ticks);

            lblRegTime.Text = t.ToString("mm:ss");

            _Controller_H.OnRegCardProcess();

//          progressBarRegistration.Position = pos;

        }

        private void tmrRotation_Tick(object sender, EventArgs e)
        {
            _CurrentImageNum++;
            if (_CurrentImageNum > TimerImages.Images.Count - 1)
                _CurrentImageNum = 0;

            picRegTime.Image = TimerImages.Images[_CurrentImageNum];
        }


        private void btnRegCancel_Click(object sender, EventArgs e)
        {
            _Controller_H.Registration_Clear_Ask();
        }

        private void btnTextOut_Click(object sender, EventArgs e)
        {
            _Controller_H.TextOut();
            _Controller.HistoryAdd("СГ", "Определение карточек и вывод имен на пульты");
        }

        private void btnActAllCards_Click(object sender, EventArgs e)
        {
            _Controller_H.ActivateAll();
        }
/*
        private void btnRegAllCards_Click(object sender, EventArgs e)
        {
            _Controller_H.RegistrateAll();
        }
*/
        private void chkDelegateSeat_CheckedChanged(object sender, EventArgs e)
        {
            bool IsDelegateSeatViewMode = chkDelegateSeat.Checked;
            _Controller_H.SetDelegateSeatViewMode(IsDelegateSeatViewMode);

            btnActAllCards.Enabled = IsDelegateSeatViewMode;
            btnClearSeats.Enabled = IsDelegateSeatViewMode;
            btnTextOut.Enabled = IsDelegateSeatViewMode;
        }

        private void chkAutoCardMode_CheckedChanged(object sender, EventArgs e)
        {
/*
            if (chkAutoCardMode.Checked == true && chkBackCardMode.Checked == true)
                chkBackCardMode.Checked = false;
*/
            _Controller_H.SetAutoCardMode(chkAutoCardMode.Checked);
        }

        private void chkBackCardMode_CheckedChanged(object sender, EventArgs e)
        {
/*
            if (chkBackCardMode.Checked == true && chkAutoCardMode.Checked == true)
                chkAutoCardMode.Checked = false;
 */ 

            _Controller_H.SetBackCardMode(chkBackCardMode.Checked);
            tmrCardBackMode.Enabled = chkBackCardMode.Checked;
        }

        private void tmrCardBackMode_Tick(object sender, EventArgs e)
        {
            _Controller_H.OnBackCardMode();
        }

        private void spinBackCardMode_EditValueChanged(object sender, EventArgs e)
        {
            tmrCardBackMode.Stop();
            int ticks = System.Convert.ToInt32(spinBackCardMode.Value);
            _Controller_H.SetBackCardModeInterval(ticks);
            if (ticks > 0)
                tmrCardBackMode.Interval = ticks * 1000;

            tmrCardBackMode.Start();
        }
/*
        private void btnClearRegistration_Click(object sender, EventArgs e)
        {
            _Controller_H.Registration_Clear_Ask();
        }
*/
        private void btnClearSeats_Click(object sender, EventArgs e)
        {
            _Controller_H.Seats_Clear_Ask();
        }

        private void cntSeatFix_ItemClick(object sender, ItemClickEventArgs e)
        {
            _Controller_H.Fix_Seat_Ask();
        }


        /*
                private void Mic_1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
                {
                    PictureEdit pic = (PictureEdit)sender;

                    if (pic != null)
                    {

        //              Form f = pic.FindForm();

                        // Cancel the drag if the mouse moves off the form. The screenOffset
                        // takes into account any desktop bands that may be at the top or left
                        // side of the screen.
                        Point screenOffset = SystemInformation.WorkingArea.Location;

                        if (((Control.MousePosition.X - screenOffset.X) < this.DesktopBounds.Left) ||
                            ((Control.MousePosition.X - screenOffset.X) > this.DesktopBounds.Right) ||
                            ((Control.MousePosition.Y - screenOffset.Y) < this.DesktopBounds.Top) ||
                            ((Control.MousePosition.Y - screenOffset.Y) > this.DesktopBounds.Bottom))
                        {

                            e.Action = DragAction.Cancel;
                        }
                    }
                }
        */
    }
}