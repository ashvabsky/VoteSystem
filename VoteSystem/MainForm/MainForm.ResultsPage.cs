﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;

namespace VoteSystem
{
    public interface IResultsPage
    {
        IMsgMethods GetShowInterface();
//      IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void Init();
        void InitControls(ref XPCollection Questions);
        int NavigatorPosition { get; set; }

        void ShowVoteResults(VoteResultObj vro);

    }

    partial class MainForm : IResultsPage
    {
        ResQuestionsCntrler _Controller_RQ;



        #region реализация интерфейса IResultsPage
        IMsgMethods IResultsPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }
/*
        IQuestProperties IResultsPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }
*/
        void IResultsPage.Init()
        {
            _Controller_RQ = (ResQuestionsCntrler)_Controller.GetSubController("ResultsPage Controller"); ;
        }

        void IResultsPage.InitControls(ref XPCollection ResQuestions)
        {
//        grpResQuestPropsIn.Controls.Remove(PropertiesControlQuest2);
//        grpResQuestPropsIn.Controls.Add(PropertiesControlQuest);
            //            cat_sq_Criteries.Expanded = false;

            PropertiesControlQuest2.DataSource = ResQuestions;
            ResQuestGrid.DataSource = ResQuestions;
        }


        int IResultsPage.NavigatorPosition
        {
            get
            {
                return navigatorResQuestions.Position;
            }
            set
            {
                navigatorResQuestions.Position = value;
            }
        }


        void IResultsPage.ShowVoteResults(VoteResultObj vro)
        {
            if (vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteDateTime.Visible = true;

                lblDecision.Text = vro.idResultValue.Name;
                lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty2.ToString();
                lblVoteAbstain.Text = vro.AnsQnty3.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;

                if (vro.AvailableQnty != 0)
                {
                    int votes_proc = (votes * 100) / vro.AvailableQnty;

                    int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                    int procAgainst = (vro.AnsQnty2 * 100) / vro.AvailableQnty;
                    int procAbstain = (vro.AnsQnty3 * 100) / vro.AvailableQnty;

                    lblProcAye.Text = procAye.ToString();
                    lblProcAgainst.Text = procAgainst.ToString();
                    lblProcAbstain.Text = procAbstain.ToString();

                    int notvoted = vro.AvailableQnty - votes;
                    int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                    lblNonVoteQnty.Text = notvoted.ToString();
                    lblNonProcQnty.Text = notvoted_proc.ToString();

                    lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                    lblProcQnty.Text = string.Format("{0}", votes_proc);
                }
                else
                {
                    lblProcAye.Text = "";
                    lblProcAgainst.Text = "";
                    lblProcAbstain.Text = "";
                    lblNonVoteQnty.Text = "";
                    lblNonProcQnty.Text = "";
                    lblVoteQnty.Text = "";
                    lblProcQnty.Text = "";

                }
            }
            else
            {
                lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }

        #endregion

        private void navControlResQuestions_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Edit")
                {
                    _Controller_RQ.EditCurrRecord();
                }
            }
        }

        private void navigatorResQuestions_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller_RQ != null)
                _Controller_RQ.OnSelectNewPosition();
        }



        private void btnQuestNewRead_Click(object sender, EventArgs e)
        {
            _Controller_RQ.CreateNewReading();
        }

        private void btnQuestVoteDetail_Click(object sender, EventArgs e)
        {
            _Controller_RQ.VoteDetailsRun();
        }

        private void ResQuestGrid_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = ResQuestGridView.CalcHitInfo(ResQuestGrid.PointToClient(MousePosition));
            if (!hi.InRow) return;

            _Controller_RQ.EditCurrRecord();
        }
   }
}