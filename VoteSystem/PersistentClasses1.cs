using System;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.delegates")]
    public class DelegateObj : XPLiteObject
    {
        int fID;
        [Key]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fLastName;
        [Size(150)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        public DelegateObj(Session session) : base(session) { }
        public DelegateObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
