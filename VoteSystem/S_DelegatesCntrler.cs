﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class S_DelegatesCntrler
    {
        public enum SeatState {Registered, NotRegistered, Disabled};
        SessionObj _TheSession;
        S_DelegateObj _TheDelegate;

        IQuorumPage _View;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController;

        NestedUnitOfWork _nuow;

        XPCollection<S_DelegateObj> _Delegates;
        XPCollection<SeatObj> _Seats;

        int _MicNum = -1;
        SeatObj _theSeat = null;

        Dictionary<int, S_DelegateObj> SeatDelegatesList = new Dictionary<int, S_DelegateObj>();
        Dictionary<int, SeatObj> SeatsList = new Dictionary<int, SeatObj>();

        RegStartForm _RegStartForm;

        bool _regstartMode = false;

        public S_DelegatesCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IQuorumPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public bool RegStartMode
        {
            get { return _regstartMode; }
        }

        public void Init()
        {
            _TheSession = DataModel.TheSession;
            _Delegates = DataModel.SesDelegates;
            _Seats = DataModel.Seats;
        }

        public void InitQuorumGraphicPage()
        {

            _View.InitControls();
            InitSeats();

            _mainController.CurrentActivity = MainCntrler.Activities.RegistrationInfo;
        }

        private void InitSeats()
        {
            _Delegates.Reload();
            SeatDelegatesList.Clear();

            foreach (S_DelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0)
                    SeatDelegatesList[d.idSeat.MicNum] = d;
            }

            foreach (SeatObj s in _Seats)
            {
                if (s.MicNum > 0)
                    SeatsList[s.MicNum] = s;
            }

            InitRegParams();
            _View.FillQuorumGraphicTabPage(SeatDelegatesList, SeatsList);
        }

        public void Save()
        {

        }

        public void ShowDelegateProp(int MicNum)
        {
            _MicNum = MicNum;

            if (SeatDelegatesList.ContainsKey(MicNum))
            {
                _TheDelegate = (S_DelegateObj)SeatDelegatesList[MicNum];
            }
            else
            {
                _TheDelegate = null;
            }

            ShowDelegateProp(_TheDelegate);
        }

        public void ShowDelegateProp(S_DelegateObj theDelegate)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            if (theDelegate != null)
            {
                properties["FirstName"] = theDelegate.idDelegate.FirstName;
                properties["SecondName"] = theDelegate.idDelegate.SecondName;
                properties["LastName"] = theDelegate.idDelegate.LastName;
                properties["PartyName"] = theDelegate.idDelegate.idParty.Name;
                properties["FractionName"] = theDelegate.idDelegate.idFraction.Name;
                properties["RegionName"] = theDelegate.idDelegate.idRegion.Name;
                properties["RegionNum"] = theDelegate.idDelegate.idRegion.Number;

                properties["MicNum"] = theDelegate.idSeat.MicNum.ToString();
                properties["RowNum"] = theDelegate.idSeat.RowNum.ToString();
                properties["SeatNum"] = theDelegate.idSeat.SeatNum.ToString();
            }
            else
            {
                properties["FirstName"] = "";
                properties["SecondName"] = "";
                properties["LastName"] = "";
                properties["PartyName"] = "";
                properties["FractionName"] = "";
                properties["RegionName"] = "";
                properties["RegionNum"] = "";

                properties["MicNum"] = "";
                properties["RowNum"] = "";
                properties["SeatNum"] = "";
            }

            _View.SetProperties(properties);
        }

        public void MoveDelegate(int SourceSeatNum, int DestSeatNum)
        {
            if (SourceSeatNum == DestSeatNum)
                return;

            S_DelegateObj sourceDelegate = null;
            S_DelegateObj destDelegate = null;

            _nuow = _Delegates.Session.BeginNestedUnitOfWork();

            if (SeatDelegatesList.ContainsKey(SourceSeatNum))
            {
                sourceDelegate = (S_DelegateObj)_nuow.GetNestedObject(SeatDelegatesList[SourceSeatNum]);
            }

            if (SeatDelegatesList.ContainsKey(DestSeatNum))
            {
                destDelegate = (S_DelegateObj)_nuow.GetNestedObject(SeatDelegatesList[DestSeatNum]);
            }

            if (destDelegate != null)
            {
                if (sourceDelegate != null)
                {

                    string msg = String.Format("Вы намерены поменять местами депутата \"{0}(место № {1})\" и депутата \"{2} (место № {3}\")?", sourceDelegate.idDelegate.FullName, sourceDelegate.idSeat.MicNum, destDelegate.idDelegate.FullName, destDelegate.idSeat.MicNum);
                    DialogResult dr = _msgForm.ShowWarningQuiestion(msg);
                    if (dr == DialogResult.Yes)
                    {
                        SeatObj seat = sourceDelegate.idSeat;
                        sourceDelegate.idSeat = destDelegate.idSeat;
                        destDelegate.idSeat = seat;

                        _nuow.CommitChanges();
                        _Delegates.Reload();

                        S_DelegateObj d = SeatDelegatesList[SourceSeatNum];
                        SeatDelegatesList[SourceSeatNum] = SeatDelegatesList[DestSeatNum];
                        SeatDelegatesList[DestSeatNum] = d;
                        InitSeats();
                    }
                }
            }
            else
            {
                if (sourceDelegate != null)
                {

                    CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", DestSeatNum);
                    _Seats.Filter = criteria1;
                    if (_Seats.Count != 0)
                    {
                        DialogResult dr = DialogResult.Yes;
                        if (dr == DialogResult.Yes)
                        {
                            SeatObj seat = (SeatObj)_nuow.GetNestedObject(_Seats[0]);
                            _Seats.Filter = null;

                            sourceDelegate.idSeat = seat;

                            _nuow.CommitChanges();

                            InitSeats();

                        }

                    }
                }
            }


        }


        public void GetPopupMenuStates(ref bool SwitcherPhysicalState, ref bool SwitcherLogicalState, ref bool SeatFree_Enable, ref bool SeatSelect_Enable, ref bool AllDisable)
        {
            // сначала необходимо определить выбранное микрофонное место
            CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", _MicNum);
            _Seats.Filter = criteria1;
            if (_Seats.Count != 0)
            {
                _theSeat = (SeatObj)_Seats[0];
                _Seats.Filter = null;
            }

            if (_theSeat == null) // исключительный случай - выбранное место отстуствует в списке всех мест, т.е. в базе данных
            {
                AllDisable = true;
                return;
            }

            SwitcherPhysicalState = _theSeat.OnPhysicalState;
            SwitcherLogicalState = _theSeat.OnLogicalState;

            if (_TheDelegate != null) // место занято?
            {
                SeatFree_Enable = true; // включить возможность освобождения места
                SeatSelect_Enable = false; // выключить возможность выбора депутата для данного места
            }
            else
            {
                SeatFree_Enable = false;
                SeatSelect_Enable = true;
            }
        }

        public void SetSwitchersValuePhysic(bool OnPhysic)
        {
            if (_theSeat == null)
                return;

            _theSeat.OnPhysicalState = OnPhysic;

            _theSeat.Save();

            InitSeats();
        }

        public void SetSwitchersValueLogic(bool OnLogic)
        {
            if (_theSeat == null)
                return;

            _theSeat.OnLogicalState = OnLogic;

            _theSeat.Save();

            InitSeats();
        }

        public void FreeCurrentSeat()
        {
            if (_theSeat == null)
                return;

            if (_TheDelegate == null)
                return;

            if (_TheDelegate.idSeat.id == _theSeat.id)
            {
                string msg = String.Format("Вы намерены освободить место № {0}, занятое  депутатом \"{1}\" ?", _theSeat.MicNum, _TheDelegate.idDelegate.FullName);
                DialogResult dr = _msgForm.ShowWarningQuiestion(msg);
                if (dr == DialogResult.Yes)
                {

                    // необходимо найти объект, выполняющий роль пустого места
                    SeatObj emptySeat = null;
                    CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", 0);
                    _Seats.Filter = criteria1;
                    if (_Seats.Count != 0)
                    {
                        emptySeat = (SeatObj)_Seats[0];
                        _Seats.Filter = null;

                        _TheDelegate.idSeat = emptySeat;
                        _TheDelegate.IsRegistered = false;
                        _TheDelegate.Save();
                        InitSeats();
                    }
                }
            }
        }

        public void Delegate_SetToSeat()
        {
            List<int> nonseatDelegates = new List<int>();
            foreach (S_DelegateObj o in _Delegates)
            {
                if (o.idSeat.MicNum <= 0)
                    nonseatDelegates.Add(o.idDelegate.id);
            }

            DelegatesCntrler d = (DelegatesCntrler)_mainController.GetSubController("Delegate List Controller");
            DelegateObj Delegate = d.GetSelectedItemFromList(nonseatDelegates);

            if (Delegate != null)
            {
                    CriteriaOperator criteria1 = CriteriaOperator.Parse("idDelegate.id == ?", Delegate.id);
                    _Delegates.Filter = criteria1;
                    if (_Delegates.Count != 0)
                    {
                        S_DelegateObj seatdelegate = (S_DelegateObj)_Delegates[0];
                        _Delegates.Filter = null;
                        seatdelegate.idSeat = _theSeat;
                        seatdelegate.Save();
                        InitSeats();
                    }
            }

        }

        public void Registration_PrepareToStart()
        {
            _RegStartForm = new RegStartForm(this);
            DialogResult dr = _RegStartForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                _TheSession.RegTime = _RegStartForm.RegTime;
                _TheSession.Quorum = _RegStartForm.QuorumQnty;
                _TheSession.Save();

                Registration_Clear();// отменить итоги регистрации
                int RegTime = _TheSession.RegTime;
                _View.StartRegistration(RegTime);

                _regstartMode = true;
            }
        }

        // очистить итоги регистрации
        public void Registration_Clear()
        {
            foreach (S_DelegateObj d in _Delegates)
            {
                d.IsRegistered = false;
                d.Save();
            }
            InitSeats();
        }

        public void Registration_Clear_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Отменить итоги регистрации?");
            if (dr == DialogResult.Yes)
            {
                Registration_Clear();
            }
        }

        public void OnRegStartFormLoad()
        {
            _RegStartForm.RegTime = _TheSession.RegTime;
            _RegStartForm.DelegatesQnty = GetSeatDelegatesQnty();
            _RegStartForm.QuorumQnty = _TheSession.Quorum;
        }

        private void InitRegParams()
        {
            int RegTime = _TheSession.RegTime;
            int DelegatesQnty = GetSeatDelegatesQnty();
            int RegQnty = GetRegDelegatesQnty();
            int QuorumQnty = _TheSession.Quorum;

            _View.InitRegParams(RegTime, DelegatesQnty, RegQnty, QuorumQnty);
        }

        public void Registration_Stop()
        {
            _View.StopRegistration();

            _regstartMode = false;

            MessageBox.Show("Регистрация завершена!");
        }

        public void Registration_Stop_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Завершить регистрацию?");
            if (dr == DialogResult.Yes)
            {
                Registration_Stop();
            }
        }

        public void OnRegProcess()
        {
            foreach (S_DelegateObj d in _Delegates)
            {
                if (d.IsRegistered != true && d.idSeat.MicNum > 0)
                {
                    d.IsRegistered = true;
                    d.Save();
                    break;
                }
            }

            InitSeats();

            int RegQnty = GetRegDelegatesQnty();
            _View.ReqQnty = RegQnty;

            if (RegQnty >= _TheSession.Quorum)
            {
                _View.IsQuorum = true;
            }

            if (RegQnty == GetSeatDelegatesQnty())
            {
                Registration_Stop();
            }
        }

        public int GetRegDelegatesQnty()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("IsRegistered == ?", true);
            _Delegates.Filter = criteria1;
            int res = _Delegates.Count;
            _Delegates.Filter = null;
            return res;
        }

        public int GetSeatDelegatesQnty()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("idSeat.MicNum > ?", 0);
            _Delegates.Filter = criteria1;
            int res = _Delegates.Count;
            _Delegates.Filter = null;
            return res;
        }

        public void ShowMonitorPreview(string action)
        {
            if (action == "RegPrepare")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.RegPrepare;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "RegStart")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.RegStart;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "RegResults")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.RegistrationInfo;
                _mainController.ShowMonitorPreview();
            }
        }
    }
}
