﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.seats")]
    public class SeatObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        int fMicNum;
        public int MicNum
        {
            get { return fMicNum; }
            set { SetPropertyValue<int>("MicNum", ref fMicNum, value); }
        }

        int fRowNum;
        public int RowNum
        {
            get { return fRowNum; }
            set { SetPropertyValue<int>("RowNum", ref fRowNum, value); }
        }

        int fSeatNum;
        public int SeatNum
        {
            get { return fSeatNum; }
            set { SetPropertyValue<int>("SeatNum", ref fSeatNum, value); }
        }


        bool fOnPhysicalState;
        public bool OnPhysicalState
        {
            get { return fOnPhysicalState; }
            set { SetPropertyValue<bool>("OnPhysicalState", ref fOnPhysicalState, value); }
        }

        bool fOnLogicalState;
        public bool OnLogicalState
        {
            get { return fOnLogicalState; }
            set { SetPropertyValue<bool>("OnLogicalState", ref fOnLogicalState, value); }
        }

        [Association("SeatObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }

        public SeatObj(Session session) : base(session) { }
        public SeatObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}
