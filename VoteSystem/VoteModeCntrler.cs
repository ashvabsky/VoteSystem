﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class VoteModeCntrler
    {
        public enum SeatStates {Answer1, Answer2, Answer3, Answer4, Answer5, NoAnswer, Disabled, None };

        IVoteModeForm _View; // главный интерфейс, с которым работаем
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController;
/*
        XPCollection<S_DelegateObj> _delegates;
        XPCollection<SeatObj> _seats;
        XPCollection<VoteDetailObj> _voteDetails;
        XPCollection<AnswerTypeObj> _answerTypes;
        XPCollection<VoteResultObj> _voteResults;
*/
        S_QuestionObj _TheQuestion;
        S_DelegateObj _selDelegate;
        VoteResultObj _TheVoteResult = null;

        Dictionary<int, SeatStates> _States = new Dictionary<int, SeatStates>();
        int _DelegatesQnty = 0;

        int _VoteTime = 0; // время отводимое на голосование

        bool _voteMode = false; // признак, указывает, идет ли процесс голосвания в данный момент


        public bool VoteMode
        {
            get { return _voteMode; }
        }

        public S_QuestionObj CurrentQuestion
        {
            get { return _TheQuestion; }
        }

        public VoteModeCntrler(MainCntrler controller)
        {
            _mainController = controller;
        }


        public void AssignView(IVoteModeForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void InitForm()
        {
            _View.ShowView();
        }

        public void Init(S_QuestionObj theQuestion)
        {
            _TheQuestion = theQuestion;

            _VoteTime = DataModel.TheSession.VoteTime;

            _States.Clear();

            CriteriaOperator criteria = CriteriaOperator.Parse("idSesQuestion.id = ?", _TheQuestion.id);
            DataModel.VoteDetails.Filter = criteria;

            CriteriaOperator criteria1 = CriteriaOperator.Parse("idSesQuestion == ?", _TheQuestion.id);

            DataModel.VoteResults.Filter = criteria1;
            if (DataModel.VoteResults.Count == 0)
            {
                DataModel.VoteResults.Filter = null;
                _TheVoteResult = null;
            }
            else
                _TheVoteResult = DataModel.VoteResults[0] as VoteResultObj;

            DataModel.VoteResults.Filter = null;
            _voteMode = false;
        }

        public void OnLoadForm()
        {
            _View.InitControls(_TheQuestion.idQuestion.Caption);

            ShowQuestProperties(_TheQuestion);
            ShowDelegateProp(-1);
            InitSeats();
            InitVoteParams();

            _View.ShowVoteResults(_TheVoteResult);
        }

        public SeatStates GetSeatState(int MicNum)
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", MicNum);

            DataModel.Seats.Filter = criteria1;
            if (DataModel.Seats.Count == 0)
            {
                DataModel.Seats.Filter = null;
                return SeatStates.None;
            }

            SeatObj seat = DataModel.Seats[0];
            DataModel.Seats.Filter = null;

            if (seat.OnPhysicalState == false)
                return SeatStates.None;

            if (seat.OnLogicalState == false)
                return SeatStates.Disabled;

            CriteriaOperator criteria2 = CriteriaOperator.Parse("idSeat.id == ?", seat.id);

            DataModel.SesDelegates.Filter = criteria2;
            if (DataModel.SesDelegates.Count == 0)
            {
                DataModel.SesDelegates.Filter = null;
                return SeatStates.None;
            }

            S_DelegateObj d = DataModel.SesDelegates[0];
            DataModel.SesDelegates.Filter = null;

            if (d.IsRegistered == false)
                return SeatStates.None;

            if (_States.ContainsKey(MicNum))
                return _States[MicNum];

            SeatStates res = SeatStates.NoAnswer;
            return res;
        }

        public void InitSeats()
        {
            _States.Clear();

            foreach (VoteDetailObj vdo in DataModel.VoteDetails)
            {
                if (!vdo.idSesDelegate.IsRegistered)
                {
                    _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Disabled;
                }
                else
                {
                    if (vdo.idAnswer.Name == "Answer1")
                        _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Answer1;
                    else if (vdo.idAnswer.Name == "Answer2")
                        _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Answer2;
                    else if (vdo.idAnswer.Name == "Answer3")
                        _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Answer3;
                    else if (vdo.idAnswer.Name == "Answer4")
                        _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Answer4;
                    else if (vdo.idAnswer.Name == "Answer5")
                        _States[vdo.idSesDelegate.idSeat.MicNum] = SeatStates.Answer5;

                }
            }

            _View.InitSeats();
        }

        private S_DelegateObj GetDelegate(int MicNum)
        {
            S_DelegateObj Delegate = null;

            CriteriaOperator criteria = CriteriaOperator.Parse("idSeat.MicNum == ?", MicNum);

            DataModel.SesDelegates.Filter = criteria;
            if (DataModel.SesDelegates.Count != 0)
            {
                Delegate = DataModel.SesDelegates[0];
            }

            DataModel.SesDelegates.Filter = null;

            return Delegate;
        }

        public void ShowDelegateProp(int MicNum)
        {

            S_DelegateObj Delegate = GetDelegate(MicNum);
            ShowDelegateProp(Delegate);
        }

        public void ShowDelegateProp(S_DelegateObj theDelegate)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            if (theDelegate != null && theDelegate.IsRegistered)
            {
                properties["FirstName"] = theDelegate.idDelegate.FirstName;
                properties["SecondName"] = theDelegate.idDelegate.SecondName;
                properties["LastName"] = theDelegate.idDelegate.LastName;
                properties["PartyName"] = theDelegate.idDelegate.idParty.Name;
                properties["FractionName"] = theDelegate.idDelegate.idFraction.Name;
                properties["RegionName"] = theDelegate.idDelegate.idRegion.Name;
                properties["RegionNum"] = theDelegate.idDelegate.idRegion.Number;

                properties["MicNum"] = theDelegate.idSeat.MicNum.ToString();
                properties["RowNum"] = theDelegate.idSeat.RowNum.ToString();
                properties["SeatNum"] = theDelegate.idSeat.SeatNum.ToString();
            }
            else
            {
                properties["FirstName"] = "";
                properties["SecondName"] = "";
                properties["LastName"] = "";
                properties["PartyName"] = "";
                properties["FractionName"] = "";
                properties["RegionName"] = "";
                properties["RegionNum"] = "";

                properties["MicNum"] = "";
                properties["RowNum"] = "";
                properties["SeatNum"] = "";
            }

            _View.SetDelegateProperties(properties);
        }

        public void ShowQuestProperties(S_QuestionObj _theQuestion)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties["Number"] = _theQuestion.id;
            properties["Caption"] = _theQuestion.idQuestion.Caption;
            properties["CrDate"] = _theQuestion.idQuestion.CrDate;
            properties["Notes"] = _theQuestion.Notes;

            properties["VoteType_Value"] = _theQuestion.idVoteType.Name;
            properties["VoteType_Name"] = _theQuestion.idVoteType.Name;
            properties["VoteKind_Value"] = _theQuestion.idVoteKind.Name;
            properties["VoteKind_Name"] = _theQuestion.idVoteKind.Name;

            if (_theQuestion.ReadNum == 1)
                properties["ReadNum"] = "первое чтение";
            else if (_theQuestion.ReadNum == 2)
                properties["ReadNum"] = "второе чтение";
            else if (_theQuestion.ReadNum == 3)
                properties["ReadNum"] = "третье чтение";

            properties["VoteQntyType_Value"] = _theQuestion.idVoteQnty.Value;
            properties["VoteQntyType_Name"] = _theQuestion.idVoteQnty.Name;

            properties["DecisionProc_Value"] = _theQuestion.DecisionProcValue;
            properties["DecisionProcType_Name"] = _theQuestion.idDecisionProcType.Name;
            properties["DecisionProcType_Value"] = _theQuestion.idDecisionProcType.Value;

            properties["QuotaProc_Value"] = _theQuestion.QuotaProcentValue;
            properties["QuotaProcType_Name"] = _theQuestion.idQuotaProcentType.Name;
            properties["QuotaProcType_Value"] = _theQuestion.idQuotaProcentType.Value;

            _View.SetQuestionProperties(properties);
        }

        public void Vote_PrepareToStart()
        {
            VoteStartForm _VoteStartForm;

            _VoteStartForm = new VoteStartForm(this);
            _VoteStartForm.VoteTime = _VoteTime;
            _DelegatesQnty = GetRegDelegatesQnty();
            _VoteStartForm.DelegatesQnty = _DelegatesQnty;

            DialogResult dr = _VoteStartForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                _VoteTime = _VoteStartForm.VoteTime;
                DataModel.TheSession.Save();

                Vote_Clear();// отменить итоги голосования, если они были
                InitVoteParams();
                _View.StartVoting(_VoteTime);

                _voteMode = true;
            }
        }

        // очистить итоги голосования
        public void Vote_Clear()
        {
            List<VoteDetailObj> delList = new List<VoteDetailObj>();
            foreach (VoteDetailObj vdo in DataModel.VoteDetails)
            {
                delList.Add(vdo);
            }

            foreach (VoteDetailObj vdo in delList)
            {
                DataModel.VoteDetails.Remove(vdo);
            }

            if (_TheVoteResult != null && _TheVoteResult.id != 0)
            {
                DataModel.VoteResults.Remove(_TheVoteResult);
            }

            InitVoteResult();
            InitSeats();
        }

        private void InitVoteResult()
        {
            _TheVoteResult = new VoteResultObj();
            int maxid = 0;
            if (DataModel.VoteResults.Count > 0)
                maxid = DataModel.VoteResults.Max<VoteResultObj>(v => v.id);

            _TheVoteResult.id = maxid + 1;
            _TheVoteResult.AvailableQnty = GetRegDelegatesQnty();
            _TheVoteResult.idResultValue = DataModel.QResTypes[0];
            _TheVoteResult.VoteDateTime = DateTime.Now;
            _TheVoteResult.idSesQuestion = _TheQuestion.id;
            _TheVoteResult.idQuestion = _TheQuestion.idQuestion.id;

            _TheQuestion.idResultValue = _TheVoteResult.idResultValue;

            _TheVoteResult.Save();
            _TheQuestion.Save();

            DataModel.VoteResults.Add(_TheVoteResult);

            InitSeats();
            _View.ShowVoteResults(null);
        }

        public void Vote_Clear_Ask()
        {
            if (_TheVoteResult != null)
            {
                DialogResult dr = _msgForm.ShowQuestion("Отменить итоги голосования?");
                if (dr == DialogResult.Yes)
                {
                    Vote_Clear();
                }
            }
        }

        private void InitVoteParams()
        {
            int delegateQnty = 0;
            if (_TheVoteResult != null)
            {
                _DelegatesQnty = _TheVoteResult.AvailableQnty;
            }
            else
                _DelegatesQnty = GetRegDelegatesQnty();


            _View.InitParams(_VoteTime, _DelegatesQnty);
        }

        public void Vote_Stop()
        {
            _View.StopVoting();

            if (_TheVoteResult == null)
                return;

            // рассчитать результат голосования
            _TheVoteResult.idResultValue = DataModel.QResTypes[1];

            _TheQuestion.idResultValue = _TheVoteResult.idResultValue;
            _TheQuestion.VoteDateTime = DateTime.Now;
 
             _TheVoteResult.Save();

             _TheQuestion.Save();

             _voteMode = false;

            MessageBox.Show("Голосование завершено!");
            _View.InitParams(_VoteTime, _DelegatesQnty);

            _View.ShowVoteResults(_TheVoteResult);

            _mainController.CurrentActivity = MainCntrler.Activities.QuestionResults;
            _mainController.ShowMonitorPreview();
        }

        public void Vote_Stop_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Завершить голосование?");
            if (dr == DialogResult.Yes)
            {
                Vote_Stop();
            }
        }

        public void OnVoteProcess()
        {
            CriteriaOperator oldcr = DataModel.VoteDetails.Filter;
            foreach (S_DelegateObj d in DataModel.SesDelegates)
            {
                if (d.idSeat.MicNum < 1 || d.idSeat.OnLogicalState == false)
                    continue;

                CriteriaOperator newcr = CriteriaOperator.Parse("idSesQuestion.id = ? AND idSesDelegate.id == ?", _TheQuestion.id, d.id);

                DataModel.VoteDetails.Filter = newcr;
                if (DataModel.VoteDetails.Count > 0)
                    continue;

                // имитация голосования депутатов
                VoteDetailObj vdo = new VoteDetailObj();
                AnswerTypeObj ato = DataModel.AnswerTypes[0];
                vdo.idAnswer = ato;
                vdo.idSesDelegate = d;
                vdo.idSesQuestion = _TheQuestion;

                DataModel.VoteDetails.Filter = null;
                int maxid = 0;
                if (DataModel.VoteDetails.Count > 0)
                    maxid = DataModel.VoteDetails.Max<VoteDetailObj>(v => v.id);

                vdo.id = maxid + 1;
                DataModel.VoteDetails.Add(vdo);
                vdo.Save();

                UpdateVoteResult(vdo);

                break;
            }


            DataModel.VoteDetails.Filter = oldcr;

            InitSeats();

            int VoteQnty = GetVoteDelegatesQnty();

            _View.ShowVoteResults(_TheVoteResult);

            if (VoteQnty == _DelegatesQnty) // если кол-во проголосовавших равно кол-ву голосующих делегатов, то голосвание закончено
            {
                Vote_Stop();
            }
        }

        public int GetRegDelegatesQnty()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("IsRegistered == ? AND idSeat.MicNum > 0 AND idSeat.OnLogicalState == true", true);
            DataModel.SesDelegates.Filter = criteria1;
            int res = DataModel.SesDelegates.Count;
            DataModel.SesDelegates.Filter = null;
            return res;
        }

        public int GetVoteDelegatesQnty()
        {
            return DataModel.VoteDetails.Count();
        }

        public void UpdateVoteResult(VoteDetailObj vdo)
        {
            if (_TheVoteResult.idSesQuestion == 0)
            {
                _TheVoteResult.idSesQuestion = _TheQuestion.id;
                _TheVoteResult.idQuestion = _TheQuestion.idQuestion.id;
                _TheVoteResult.idResultValue = DataModel.QResTypes[0];

                _TheVoteResult.AnsQnty1 = 0;
                _TheVoteResult.AnsQnty2 = 0;
                _TheVoteResult.AnsQnty3 = 0;
                _TheVoteResult.AnsQnty4 = 0;
                _TheVoteResult.AnsQnty5 = 0;
            }

            switch (vdo.idAnswer.Name)
            {
                case "Answer1": _TheVoteResult.AnsQnty1 = _TheVoteResult.AnsQnty1 + 1; break;
                case "Answer2": _TheVoteResult.AnsQnty2 = _TheVoteResult.AnsQnty2 + 1; break;
                case "Answer3": _TheVoteResult.AnsQnty3 = _TheVoteResult.AnsQnty3 + 1; break;
                case "Answer4": _TheVoteResult.AnsQnty4 = _TheVoteResult.AnsQnty4 + 1; break;
                case "Answer5": _TheVoteResult.AnsQnty5 = _TheVoteResult.AnsQnty5 + 1; break;
            }
        }

        public void ShowMonitorPreview(string action)
        {
            if (action == "VotePrepare")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.VotePrepare;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "VoteStart")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.VoteStart;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "VoteResults")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.QuestionResults;
                _mainController.ShowMonitorPreview();
            }
        }

    }
}
