﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.enresultvalues")]
    public class ResultValueObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        public ResultValueObj(Session session) : base(session) { }
        public ResultValueObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.enreadnums")]
    public class ReadNumObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        public ReadNumObj(Session session) : base(session) { }
        public ReadNumObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.enamendmenttypes")]
    public class AmendmentTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        public AmendmentTypeObj(Session session) : base(session) { }
        public AmendmentTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.questions")]
    public class QuestionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        SesTaskObj fidTask;
        public SesTaskObj idTask
        {
            get { return fidTask; }
            set { SetPropertyValue<SesTaskObj>("idTask", ref fidTask, value); }
        }

        TaskKindObj fidKind;
        //      [Association("VoteKindObj-QuestObj")]
        public TaskKindObj idKind
        {
            get { return fidKind; }
            set { SetPropertyValue<TaskKindObj>("idKind", ref fidKind, value); }
        }

        string fNotes;
        [Size(1024)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }

        string fName;
        [Size(1024)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        [NonPersistent]
        public string NameFull
        {
            get
            {
                if (Name != null)
                {
                    if (fidAmendmentType != null && (fidAmendmentType.CodeName != "AmendmentTable" && fAmendment > 0 || fidAmendmentType.CodeName == "AmendmentTable"))
                        return Name + ". " + AmendmentStr + ".";
                    else
                        return Name;
                }
                else
                    return "";
            }
        }

        QuestionResultObj fidResult;
        public QuestionResultObj idResult
        {
            get { return fidResult; }
            set { SetPropertyValue<QuestionResultObj>("idResult", ref fidResult, value); }
        }

        int fQueueNum;
        public int QueueNum
        {
            get { return fQueueNum; }
            set { SetPropertyValue<int>("QueueNum", ref fQueueNum, value); }
        }

        AmendmentTypeObj fidAmendmentType;
        public AmendmentTypeObj idAmendmentType
        {
            get { return fidAmendmentType; }
            set { SetPropertyValue<AmendmentTypeObj>("idAmendmentType", ref fidAmendmentType, value); }
        }

        int fAmendment;
        public int Amendment
        {
            get { return fAmendment; }
            set { SetPropertyValue<int>("Amendment", ref fAmendment, value); }
        }

        [NonPersistent]
        public string AmendmentStr
        {
            get {
                if (fidAmendmentType != null && fidAmendmentType.CodeName == "AmendmentTable")
                    return "Таблица поправок №" + fAmendment.ToString();
                else
                    return "Поправка №" + fAmendment.ToString(); ; 
            }
        }

        ReadNumObj fidReadNum;
        public ReadNumObj idReadNum
        {
            get { return fidReadNum; }
            set { SetPropertyValue<ReadNumObj>("idReadNum", ref fidReadNum, value); }
        }

        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        bool fIsDel;
        public bool IsDel
        {
            get { return fIsDel; }
            set { SetPropertyValue<bool>("IsDel", ref fIsDel, value); }
        }

        [NonPersistent]
        public string VoteStatus
        {
            get
            {
                if (idResult.IsResult == true)
                    return "голосование завершено";
                else
                    return "голосование ожидается";
            }
        }


        public QuestionObj(Session session) : base(session) 
        {
            idResult = new QuestionResultObj();
            this.idReadNum = DataLayer.DataModel._ReadNumDeafault;
            this.idAmendmentType = DataLayer.DataModel._AmendmentTypeDeafault;
        }
        public QuestionObj()
            : base(Session.DefaultSession)
        {
            idResult = new QuestionResultObj();
            this.idReadNum = DataLayer.DataModel._ReadNumDeafault;
            this.idAmendmentType = DataLayer.DataModel._AmendmentTypeDeafault;
        }

        public QuestionObj(SesTaskObj q)
            : base(Session.DefaultSession)
        {
            this.idTask = q;
            this.idSession = q.idSession;
            this.Notes = "";
            this.Name = q.Caption;
            this.idKind = q.idKind;
            this.idReadNum = DataLayer.DataModel._ReadNumDeafault;
            this.idAmendmentType = DataLayer.DataModel._AmendmentTypeDeafault;

            this.fCrDate = DateTime.Now;

            idResult = new QuestionResultObj();

        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.questions_results")]
    public class QuestionResultObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        ResultValueObj fidResultValue;
        public ResultValueObj idResultValue
        {
            get { return fidResultValue; }
            set { SetPropertyValue<ResultValueObj>("idResultValue", ref fidResultValue, value); }
        }

        int fidResultValueInt;
        public int idResultValueInt
        {
            get { return fidResultValueInt; }
            set { SetPropertyValue<int>("idResultValue", ref fidResultValueInt, value); }
        }

        VoteResultObj fidVoteResult;
        public VoteResultObj idVoteResult
        {
            get { return fidVoteResult; }
            set { SetPropertyValue<VoteResultObj>("idVoteResult", ref fidVoteResult, value); }
        }

        DateTime fVoteDateTime;
        public DateTime VoteDateTime
        {
            get { return fVoteDateTime; }
            set { SetPropertyValue<DateTime>("VoteDateTime", ref fVoteDateTime, value); }
        }

        [NonPersistent]
        public bool IsResult
        {
            get
            {
/*
                if (fidVoteResult.id == 0)
                    return false;

                if (fidVoteResult.idResultValue == null)
                    return false;
*/
                if (fidVoteResult.idResultValue.CodeName == "NoResult")
                    return false;
                else
                    return true;
            }
        }


        public QuestionResultObj(Session session) : base(session) { }
        public QuestionResultObj()
            : base(Session.DefaultSession)
        {
            this.idResultValueInt = 0;
            this.VoteDateTime = DateTime.Now;

        }

        public QuestionResultObj(SesTaskObj q)
            : base(Session.DefaultSession)
        {
            this.idResultValueInt = 0;
            this.VoteDateTime = DateTime.Now;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
