﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;

namespace VoteSystem.DataLayer
{
    public class DataModel
    {
        static public XPCollection<SesDelegateObj> SesDelegates;
        static public XPCollection<SesDelegateObj> SesDelegates_st;
        static public XPCollection<SesDelegateObj> RegDelegates;
        static private Session _statemSession = new Session();

        static public XPCollection<SeatObj> Seats;
        static public XPCollection<VoteDetailObj> VoteDetails;
        static public XPCollection<AnswerTypeObj> AnswerTypes;
        static public XPCollection<VoteResultObj> VoteResults;
        static public XPCollection<SessionObj> Sessions;
        static public XPCollection<SettingsObj> Settings;
        static public XPCollection<ResultValueObj> ResValues;

        static public XPCollection<DelegateStatementObj> StatemDetails;

        static public XPCollection<SesTaskObj> SesTasks; // вопросы сессии, оригинал хранится на главной форме // XPCollection<SesQuestionObj>
        static public XPCollection<StatementObj> SesStatements; // темы для выступлений в в данной сессии, оригинал хранится на главной форме // XPCollection<SesQuestionObj>

        static public XPCollection<QuestionObj> Questions; // вопросы сессии, оригинал хранится на главной форме // XPCollection<SesQuestionObj>
        static public XPCollection<VoteTypeObj> VoteTypes; // типы голосований
        static public XPCollection<VoteKindObj> VoteKinds; // виды голосований
        static public XPCollection<ReadNumObj> ReadNums; // виды чтений
        static public XPCollection<AmendmentTypeObj> AmendmentTypes; // виды поправок


        static public XPCollection<StatementObj> ResStatements; // список завершенных выступлений
        static public XPCollection<DelegateObj> Delegates; // список депутатов данного созыва
        static public XPCollection<DelegateTypeObj> DelegateTypes; // типы депутатов

        static public XPCollection<TaskKindObj> QuestKinds; // виды вопросов
        static public XPCollection<QntyTypeObj> QntyTypes; // виды количеств для определения результатов или кворума
        static public XPCollection<DelegateQntyTypeObj> DQntyTypes; // процентные доли соотвествующие видам количеств

        static public SessionObj TheSession;
        static public SettingsObj TheSettings;
        static public SettingsHallObj TheSettingsHall;
        static public SettingsReportObj TheSettingsReport;

        static public VoteResultObj VoteResultNull;

        static  int _DelegateQnty = -1; // количество депутатов, присутствующих в зале

        static public bool IsSpeechApp = false; // данный флаг указывает, является ли данное приложение программой для управления выступлениями

        static public int _ID_TRIBUNE = 1;
        static public ReadNumObj _ReadNumDeafault;
        static public AmendmentTypeObj _AmendmentTypeDeafault;
        public DataModel()
        {
        }

        static public void Init()
        {
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
//          DevExpress.Xpo.Helpers.SessionStateStack.SuppressCrossThreadFailuresDetection = true;

            Session.DefaultSession.OptimisticLockingReadBehavior = OptimisticLockingReadBehavior.ReloadObject;

            //--энумераторы---------------------------

            AmendmentTypes = new XPCollection<AmendmentTypeObj>(Session.DefaultSession);
            if (AmendmentTypes.Count == 0)
            {
                AmendmentTypeObj amendType1 = new AmendmentTypeObj(Session.DefaultSession);
                amendType1.id = 1; amendType1.CodeName = "AmendmentTable"; amendType1.Name = "Таблица поправок";
                AmendmentTypeObj amendType2 = new AmendmentTypeObj(Session.DefaultSession);
                amendType2.id = 2; amendType2.CodeName = "AmendmentNum"; amendType2.Name = "Поправка №";

                AmendmentTypes.Add(amendType1);
                AmendmentTypes.Add(amendType2);
      //          amendType1.Save(); amendType2.Save();
            }

            _AmendmentTypeDeafault = AmendmentTypes.Lookup(2);

            //----------------------------------------
            SesDelegates = new XPCollection<SesDelegateObj>(Session.DefaultSession);
            SesDelegates_st = new XPCollection<SesDelegateObj>(Session.DefaultSession); // _statemSession
//          SesDelegates_st = new XPCollection<SesDelegateObj>(SesDelegates);
            RegDelegates = new XPCollection<SesDelegateObj>(Session.DefaultSession);

            Seats = new XPCollection<SeatObj>(Session.DefaultSession);

            VoteDetails = new XPCollection<VoteDetailObj>(Session.DefaultSession);

            AnswerTypes = new XPCollection<AnswerTypeObj>(Session.DefaultSession);
            VoteResults = new XPCollection<VoteResultObj>(Session.DefaultSession);
            VoteResultNull = VoteResults.Lookup(0);

            Sessions = new XPCollection<SessionObj>(Session.DefaultSession);

            CriteriaOperator crSes = new BinaryOperator(
                new OperandProperty("IsDel"), new OperandValue(false),
                BinaryOperatorType.Equal);
            Sessions.Criteria = crSes;

            Sessions.Sorting.AddRange(new DevExpress.Xpo.SortProperty[] {
            new DevExpress.Xpo.SortProperty("StartDate", DevExpress.Xpo.DB.SortingDirection.Descending)});

            Settings = new XPCollection<SettingsObj>(Session.DefaultSession);
            XPCollection<SettingsHallObj> SettingsHall = new XPCollection<SettingsHallObj>(Session.DefaultSession);
            XPCollection<SettingsReportObj> SettingsReport = new XPCollection<SettingsReportObj>(Session.DefaultSession);

            ResValues = new XPCollection<ResultValueObj>(Session.DefaultSession);

            StatemDetails = new XPCollection<DelegateStatementObj>(Session.DefaultSession);

            SesTasks = new XPCollection<SesTaskObj>(Session.DefaultSession);
            Questions = new XPCollection<QuestionObj>(Session.DefaultSession);

            SesStatements = new XPCollection<StatementObj>(Session.DefaultSession);
            ResStatements = new XPCollection<StatementObj>(Session.DefaultSession);

            Delegates = new XPCollection<DelegateObj>(Session.DefaultSession);
            DelegateTypes = new XPCollection<DelegateTypeObj>(Session.DefaultSession);

            VoteTypes = new XPCollection<VoteTypeObj>(Session.DefaultSession);
            VoteKinds = new XPCollection<VoteKindObj>(Session.DefaultSession);
            ReadNums = new XPCollection<ReadNumObj>(Session.DefaultSession);
            _ReadNumDeafault = ReadNums.Lookup(0);


            QuestKinds = new XPCollection<TaskKindObj>(Session.DefaultSession);
            CriteriaOperator crQK = CriteriaOperator.Parse("id <> 0 AND IsDel = false");
            QuestKinds.Criteria = crQK;

            QntyTypes = new XPCollection<QntyTypeObj>(Session.DefaultSession);
            DQntyTypes = new XPCollection<DelegateQntyTypeObj>(Session.DefaultSession);

            CriteriaOperator crDQT = new BinaryOperator(
                new OperandProperty("id"), new OperandValue(0),
                BinaryOperatorType.NotEqual);
            DQntyTypes.Criteria = crDQT;

            TheSettings = Settings[0] as SettingsObj;
            TheSettingsHall = SettingsHall[0] as SettingsHallObj;
            TheSettingsReport = SettingsReport[0] as SettingsReportObj;

            VoteDetails.DeleteObjectOnRemove = true;
//            VoteDetails.Load();

            VoteResults.DeleteObjectOnRemove = true;

            SesDelegates.DeleteObjectOnRemove = true;
            SesTasks.DeleteObjectOnRemove = true;
            Questions.DeleteObjectOnRemove = true;
            Seats.DeleteObjectOnRemove = true;

            SesDelegates.Load();
            Seats.Load();

            AnswerTypes.Load();

            StatemDetails.DeleteObjectOnRemove = true;

        }

        static public bool _IsChangedSeating = false;

        static public bool CheckDBaseChanges(ref bool needRestart)
        {
            needRestart = false;
            Session newSession = new Session();

            XPCollection<SesDelegateObj> SesDelegatesNew;
            newSession.DropCache();
            newSession.OptimisticLockingReadBehavior = OptimisticLockingReadBehavior.ReloadObject;
            newSession.CacheBehavior = CacheBehavior.Strong;
            SesDelegatesNew = new XPCollection<SesDelegateObj>(newSession);
            SesDelegatesNew.Criteria = SesDelegates_st.Criteria;

            bool IsChanged = false;
            if (SesDelegates_st.Count != SesDelegatesNew.Count)
            {
                IsChanged = true;
                needRestart = true;
            }
            if (IsChanged == false)
            {
                foreach (SesDelegateObj sd in SesDelegates_st)
                {
                    SesDelegateObj sdnew = SesDelegatesNew.Lookup(sd.id);
                    if (sdnew == null)
                    {
                        IsChanged = true;
                        needRestart = true;
                        break;
                    }
                    if (sdnew.idSeat.MicNum != sd.idSeat.MicNum  || sdnew.IsCardRegistered != sd.IsCardRegistered)
                    {
                        IsChanged = true;
                        break;
                    }
                }

            }
            return IsChanged;
        }


        static public void Reload()
        {
/*
            _statemSession.DropCache();
            CriteriaOperator cr = SesDelegates_st.Criteria;
            SesDelegates_st = new XPCollection<SesDelegateObj>(_statemSession, true);
            SesDelegates_st.Criteria = cr;

            SesDelegates_st.Reload();
 */ 
        }

        static public void UpdateSesDelegatesST()
        {
//          SesDelegates_st.Reload();
        }

        static public void UpdateSesDelegates()
        {
            SesDelegates.Reload();
            RegDelegates.Reload();
        }

        static public void ReloadSesDelegates()
        {
            SesDelegates.Reload();
            
                        ArrayList arr = new ArrayList();
                        foreach (SesDelegateObj d in SesDelegates)
                        {
                            if (d.idDelegate.isActive == false)
                                arr.Add(d);
                        }

                        foreach (SesDelegateObj d in arr)
                        {
                            try
                            {
                                SesDelegates.Remove(d);
                            }
                            catch (DevExpress.Xpo.DB.Exceptions.SqlExecutionErrorException Ex)
                            {
                                SesDelegates.Reload();
                            }
                        }

            
            RegDelegates.Reload();
        }

        static public void LoadSession()
        {
            DateTime maxdate = DateTime.MinValue;
            TheSession = null;
            foreach (SessionObj ses in Sessions)
            {
                if (ses.IsFinished == false)
                {
                    if (ses.StartDate > maxdate)
                    {
                        maxdate = ses.StartDate;
                        TheSession = ses;
                    }
                }
            }

            if (TheSession == null)
            {
                int maxid = Sessions.Max<SessionObj>(v => v.id);
                TheSession = Sessions.Lookup(maxid);
            }

            if (TheSession == null)
            {
                const string Err = "В базе данных нет ни одной сессии";
                Exception Ex = new Exception(Err);
                throw (Ex);
            }

            CriteriaOperator cr = new BinaryOperator(
            new OperandProperty("idSession"), new OperandValue(TheSession.id),
            BinaryOperatorType.Equal);

            SesDelegates.Criteria = cr;
            SesDelegates_st.Criteria = cr;
            SesDelegates.Load();
            SesDelegates_st.Load();

            CriteriaOperator cr2 = CriteriaOperator.Parse("idSession.id == ? AND IsDel == ?", TheSession.id, false);

            Questions.Criteria = cr2;
            SesTasks.Criteria = cr2;

            CriteriaOperator crst = CriteriaOperator.Parse("idSession.id == ? AND IsDel == ?", TheSession.id, false);
            SesStatements.DeleteObjectOnRemove = true;
            SesStatements.Criteria = crst;
            ResStatements.Criteria = crst;

            CriteriaOperator cr3 = CriteriaOperator.Parse("idSession.id == ? AND IsRegistered == ?", TheSession.id, true);
            RegDelegates.Criteria = cr3;

            CriteriaOperator crVD = CriteriaOperator.Parse("idQuestion.idSession = ?", TheSession.id);
            VoteDetails.Criteria = crVD;

        }


        static public AnswerTypeObj AnswerTypes_GetByCodeName(string codename)
        {
            foreach (AnswerTypeObj ans in AnswerTypes)
            {
                if (ans.CodeName == codename)
                    return ans;
            }

            return new AnswerTypeObj();
        }

        static public TaskKindObj TaskKinds_GetByCodeName(string codename)
        {
            foreach (TaskKindObj t in QuestKinds)
            {
                if (t.CodeName == codename)
                    return t;
            }

            return null;
        }

        static public XPCollection<DelegateStatementObj> GetStatemDetails(StatementObj statement)
        {
            StatemDetails.Dispose();
            CriteriaOperator criteria = CriteriaOperator.Parse("idStatement.id = ?", statement.id);
            StatemDetails = new XPCollection<DelegateStatementObj>(Session.DefaultSession, criteria);
            StatemDetails.DeleteObjectOnRemove = true;

//          DataModel.StatemDetails.Filter = criteria;
            return StatemDetails;
        }

        static public XPCollection<StatementObj> GetStatemFromCollection(StatementObj statement)
        {
            DataModel.ResStatements.Reload();

            CriteriaOperator criteria = CriteriaOperator.Parse("id = ?", statement.id);
            XPCollection<StatementObj> statem = new XPCollection<StatementObj>();
            statem.Criteria = criteria;

            if (statem.Count == 0)
                DataModel.ResStatements.Add(statement);

            statem.Reload();

            return statem;
        }
/*
        static public StatementObj GetFinishedStatement(SesStatementObj sourceStatem)
        {
            StatementObj resultS = new StatementObj();
            CriteriaOperator oldCr = DataModel.ResStatements.Filter;
            CriteriaOperator cr = new BinaryOperator(
            new OperandProperty("idStatement.id"), new OperandValue(sourceStatem.id),
            BinaryOperatorType.Equal);


            DataModel.ResStatements.Filter = cr;

            if (DataModel.ResStatements.Count > 0)
            {
                resultS = (StatementObj)DataModel.ResStatements[0];
            }

            DataModel.ResStatements.Filter = oldCr;

            return resultS;
        }
*/


        static public SesDelegateObj SesDelegatesST_GetByMicNum(int MicNum)
        {
            SesDelegates_st.Filter = null;
            SesDelegateObj res = null;
            foreach (SesDelegateObj d in SesDelegates_st)
            {
                if (d.idSeat.MicNum == MicNum)
                    res = d;
            }

            return res;
        }

        static public SesDelegateObj SesDelegates_GetByMicNum(int MicNum)
        {
            SesDelegates.Filter = null;
            SesDelegateObj res = new SesDelegateObj();
            foreach (SesDelegateObj d in SesDelegates)
            {
                if (d.idSeat.MicNum == MicNum)
                    return d;
            }

            return res;
        }

        static public SesDelegateObj SesDelegates_GetByCard(string code)
        {
            SesDelegates.Filter = null;
            foreach (SesDelegateObj d in SesDelegates)
            {
                if (d.idDelegate.CardCode == code)
                    return d;
            }

            return null;
        }

        static public SesDelegateObj SesDelegatesST_GetByDelegateID(int id)
        {
            SesDelegates_st.Filter = null;
            SesDelegateObj res = null;
            foreach (SesDelegateObj d in SesDelegates_st)
            {
                if (d.idDelegate.id == id)
                    return d;
            }

            return res;
        }

        static public SesDelegateObj SesDelegates_GetByDelegateID(int id)
        {
            SesDelegates.Filter = null;
            SesDelegateObj res = null;
            foreach (SesDelegateObj d in SesDelegates)
            {
                if (d.idDelegate.id == id)
                    return d;
            }

            return res;
        }

        static public SesDelegateObj SesDelegates_AppendDelegate(DelegateObj Delegate)
        {
            SesDelegateObj sesd = new SesDelegateObj();

            sesd.idDelegate = Delegate;
            sesd.idSession = TheSession;
            sesd.IsCardRegistered = false;
            sesd.IsRegistered = false;
            sesd.LastRegTime = DateTime.MinValue;
            sesd.idSeat = Delegate.idSeat;

            SesDelegates.Add(sesd);

            return sesd;
        }

        // вычислить количество зарегестрировавшихся депутатов
        static public int GetRegDelegatesQnty()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("IsRegistered = ? AND idSeat.MicNum > 0 AND idSeat.LogicalState = true", true);
            DataModel.SesDelegates.Filter = criteria1;
            _DelegateQnty = DataModel.SesDelegates.Count;
            DataModel.SesDelegates.Filter = null;
            
            return _DelegateQnty;
        }

        static public int GetVoteDelegatesQnty()
        {
            CriteriaOperator criteria1 = CriteriaOperator.Parse("IsCardRegistered = ? AND idSeat.MicNum > 0 AND idSeat.LogicalState = true", true);
            DataModel.SesDelegates.Filter = criteria1;
            int qnty = DataModel.SesDelegates.Count;
            DataModel.SesDelegates.Filter = null;

            return qnty;
        }

        static public int GetDelegatesQnty_Total()
        {
//          CriteriaOperator criteria1 = CriteriaOperator.Parse("(idSeat.MicNum > 0) AND ( (idDelegate.idType.CodeName = 'Delegate_Active' AND IsCardRegistered = ?) OR (idDelegate.idType.CodeName <> 'Delegate_Active')", true);
            CriteriaOperator criteria1 = CriteriaOperator.Parse("idSeat.MicNum > 0");
            DataModel.SesDelegates.Filter = criteria1;
            int delegateQnty = DataModel.SesDelegates.Count;
/*
            CriteriaOperator criteria2 = CriteriaOperator.Parse("idSeat.MicNum > 0 AND idDelegate.idType.CodeName != 'Delegate_Active'");
            DataModel.SesDelegates.Filter = criteria2;
            delegateQnty = delegateQnty + DataModel.SesDelegates.Count;
            DataModel.SesDelegates.Filter = null;
*/
            return delegateQnty;
        }

        static public int RegDelegateQnty
        {
            get
            {
                if (_DelegateQnty == -1)
                    _DelegateQnty = GetRegDelegatesQnty();

                return _DelegateQnty;
            }
            set
            {
                _DelegateQnty = value;
            }
        }

        static public ResultValueObj ResValues_GetObject(string codename)
        {
            if (codename == "NoResult")
                return ResValues.Lookup(0);
            else
            {
                foreach (ResultValueObj rvo in ResValues)
                {
                    if (rvo.CodeName == codename)
                        return rvo;
                }

                return new ResultValueObj();
            }
        }

        static public DelegateObj GetTribuneDelegate()
        {
            DelegateObj d_tribune = DataModel.Delegates.Lookup(_ID_TRIBUNE);
            return d_tribune;
        }

        static public XPCollection<VoteDetailObj> GetVoteDetails(QuestionObj question)
        {
            int qid = -1;
            if (question != null)
                qid = question.id;

            CriteriaOperator criteria1 = CriteriaOperator.Parse("idQuestion.id = ?", qid);
            DataModel.VoteDetails.Filter = criteria1;

            DataModel.VoteDetails.Reload();

            return DataModel.VoteDetails;
        }

        static public void Sessions_AddNew(SessionObj ses, int maxid)
        {
            if (maxid <= 0)
                return;

            ses.id = maxid + 1;

            Sessions.Add(ses);
            ses.Save();
        }

        static public void Sessions_CreateSesDelegates(SessionObj ses, int maxid)
        {
            if (ses.id <= 0 || maxid <= 0)
                return;

            SesDelegates.Filter = null;
            XPCollection<SesDelegateObj> NewSesDelegates = new XPCollection<SesDelegateObj>();
            CriteriaOperator cr = new BinaryOperator(
            new OperandProperty("idSession"), new OperandValue(ses.id),
            BinaryOperatorType.Equal);

            NewSesDelegates.Criteria = cr;

            foreach (SesDelegateObj sesD in SesDelegates)
            {
                SesDelegateObj newSesD = new SesDelegateObj(sesD);
                maxid = maxid + 1;
                newSesD.id = maxid;
                newSesD.idSession = ses;

                if (sesD.idDelegate.idType.CodeName != "Delegate_Active" && sesD.IsCardRegistered == true)
                    newSesD.IsCardRegistered = sesD.IsCardRegistered;

                NewSesDelegates.Add(newSesD);
                newSesD.Save();
            }
        }

        static public void Sessions_SetActive(SessionObj ses)
        {
            if (ses != null && ses.id > 0)
            {
                foreach (SessionObj each_s in Sessions)
                {
                    if (each_s.id != ses.id)
                        each_s.IsFinished = true;
                    else
                        each_s.IsFinished = false;

                    each_s.Save();
                }
            }
        }

        static public bool Sessions_CanDelete(SessionObj ses)
        {
            if (ses != null && ses.id > 0)
            {
                XPCollection<QuestionObj> sesQuestions = new XPCollection<QuestionObj>();
                CriteriaOperator cr2 = CriteriaOperator.Parse("idSession.id == ? AND IsDeleted == ? AND idResult.idResultValue.CodeName != ?", ses.id, false, "NoResult");
                sesQuestions.Criteria = cr2;

                if (sesQuestions.Count > 0)
                    return false;


                return true;
            }

            return false;
        }

        static public void Sessions_Delete(SessionObj ses)
        {
            if (ses != null && ses.id > 0)
            {
                if (ses.IsFinished == false)
                {
                    Sessions_Finish(ses);
                }

                ses.IsDel = true;
                ses.Save();
                Sessions.Reload();
                TheSession.Reload();
            }

            return;
        }

        static public void Sessions_Finish(SessionObj ses)
        {
            if (ses != null && ses.id > 0)
            {
                if (ses.IsFinished == false)
                {
                    ses.FinishDate = DateTime.Now;
                    ses.IsFinished = true;
                    ses.Save();
                    Sessions.Reload();
                }
            }
        }
//----------------readnum procedures--------------

        static public ReadNumObj GetNextReadNum(ReadNumObj currNum)
        {
            int newid = currNum.id;
            if (newid < ReadNums.Count - 1)
            {
                newid++;
            }
            return ReadNums.Lookup(newid);
        }

//---------------sestasks procedures------------------
        static public SesTaskObj SesTasks_GetEmptyTask()
        {
            CriteriaOperator cr = CriteriaOperator.Parse("id == 0");
            XPCollection<SesTaskObj> tasks = new XPCollection<SesTaskObj>(cr);
            return tasks[0];
        }



        static public bool TryRemoveDelegate(DelegateObj removedDelegate)
        {
            foreach (SesDelegateObj sesd in SesDelegates)
            {
                if (sesd.idDelegate.id == removedDelegate.id)
                    return false;
            }
/*
            foreach (VoteDetailObj vdo in VoteDetails)
            {
                if (vdo.idDelegate.idDelegate.id == removedDelegate.id)
                    return false;
            }

            foreach (DelegateStatementObj dst in StatemDetails)
            {
                if (dst.idDelegate.id == removedDelegate.id)
                    return false;
            }
*/
            return true;
        }
    }
}
