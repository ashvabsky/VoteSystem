﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.settings_hall")]
    public class SettingsHallObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        bool fIsDelegateSeatViewMode;
        public bool IsDelegateSeatViewMode
        {
            get { return fIsDelegateSeatViewMode; }
            set { SetPropertyValue<bool>("IsDelegateSeatViewMode", ref fIsDelegateSeatViewMode, value); }
        }

        bool fIsAutoCardMode;
        public bool IsAutoCardMode
        {
            get { return fIsAutoCardMode; }
            set { SetPropertyValue<bool>("IsAutoCardMode", ref fIsAutoCardMode, value); }
        }

        bool fIsBackCardMode;
        public bool IsBackCardMode
        {
            get { return fIsBackCardMode; }
            set { SetPropertyValue<bool>("IsBackCardMode", ref fIsBackCardMode, value); }
        }

        int fBackCardModeInterval;
        public int BackCardModeInterval
        {
            get { return fBackCardModeInterval; }
            set { SetPropertyValue<int>("BackCardModeInterval", ref fBackCardModeInterval, value); }
        }

        bool fIsAutoNamesOutMode;
        public bool IsAutoNamesOutMode
        {
            get { return fIsAutoNamesOutMode; }
            set { SetPropertyValue<bool>("IsAutoNamesOutMode", ref fIsAutoNamesOutMode, value); }
        }

        public SettingsHallObj()
            : base(Session.DefaultSession) 
        {
        }
    }
}
