﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.statements")]
    public class StatementObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SesTaskObj fidTask;
        public SesTaskObj idTask
        {
            get { return fidTask; }
            set { SetPropertyValue<SesTaskObj>("idTask", ref fidTask, value); }
        }

        bool fIsDel;
        public bool IsDel
        {
            get { return fIsDel; }
            set { SetPropertyValue<bool>("IsDel", ref fIsDel, value); }
        }

        string fCaption;
        [Size(1024)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }

        string fNotes;
        [Size(1024)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }


        DateTime fDateTimeStart;
        public DateTime DateTimeStart
        {
            get { return fDateTimeStart; }
            set { SetPropertyValue<DateTime>("DateTimeStart", ref fDateTimeStart, value); }
        }

        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        int fSpeechTimeTotal;
        public int SpeechTimeTotal
        {
            get { return fSpeechTimeTotal; }
            set { SetPropertyValue<int>("SpeechTimeTotal", ref fSpeechTimeTotal, value); }
        }

        int fAvrgTime;
        public int AvrgTime
        {
            get { return fAvrgTime; }
            set { SetPropertyValue<int>("AvrgTime", ref fAvrgTime, value); }
        }

        int fDelegateQnty;
        public int DelegateQnty
        {
            get { return fDelegateQnty; }
            set { SetPropertyValue<int>("DelegateQnty", ref fDelegateQnty, value); }
        }

        int fState;
        public int State
        {
            get { return fState; }
            set { SetPropertyValue<int>("State", ref fState, value); }
        }

        int fQueueNum;
        public int QueueNum
        {
            get { return fQueueNum; }
            set { SetPropertyValue<int>("QueueNum", ref fQueueNum, value); }
        }

        int fThemeMaxTime;
        public int ThemeMaxTime
        {
            get { return fThemeMaxTime; }
            set { SetPropertyValue<int>("ThemeMaxTime", ref fThemeMaxTime, value); }
        }

        int fThemeMaxDelegates;
        public int ThemeMaxDelegates
        {
            get { return fThemeMaxDelegates; }
            set { SetPropertyValue<int>("ThemeMaxDelegates", ref fThemeMaxDelegates, value); }
        }

        bool fThemeMicOffMode;
        public bool ThemeMicOffMode
        {
            get { return fThemeMicOffMode; }
            set { SetPropertyValue<bool>("ThemeMicOffMode", ref fThemeMicOffMode, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        [NonPersistent]
        public string Status
        {
            get
            {
                if (State == 1)
                    return "выступления проводились";
                else if (State == 0)
                    return "выступления готовятся";
                else if (State == 2)
                    return "идут выступления ...";
                else
                    return "";
            }
        }


        [NonPersistent]
        public bool IsFinished
        {
            get
            {
                if (State == 1 && idTask.State == 3)
                    return true;
                else
                    return false;
            }
        }

        [NonPersistent]
        public string AvrgTime_F
        {
            get
            {
                Double d = fAvrgTime;
                d = d / 60;
                int i = System.Convert.ToInt32(d);
                string s = i.ToString();
                if (i == 0)
                {
                    i = System.Convert.ToInt32(fAvrgTime);
                    s = i.ToString();
                    s = s + " сек.";
                }
                else
                {
                    s = s + " мин.";
                }

                return s;
            }
        }

        [NonPersistent]
        public string SpeechTimeTotal_F
        {
            get
            {
                Double d = fSpeechTimeTotal;
                d = d / 60;
                return (Convert.ToInt32(d)).ToString() + " мин.";
/*
                string s = d.ToString();
                s = " " + s + " мин.";
  
                return s;
 */ 
            }
        }

        [NonPersistent]
        public string DelegateQnty_F
        {
            get
            {
                string s = DelegateQnty.ToString();
                if (DelegateQnty == 0)
                    s = "нет выступлений";
                else if (DelegateQnty == 1)
                    s = s+ " раз";
                else if (DelegateQnty > 1 && DelegateQnty < 5)
                    s = s + " раза";
                else
                    s = s + " раз";
                return s;
            }
        }

        public StatementObj(Session session) : base(session) 
        {
            fCrDate = DateTime.Now;
        }
        public StatementObj() : base(Session.DefaultSession) 
        {
            fCrDate = DateTime.Now;
        }
        public StatementObj(SesTaskObj task) : base(Session.DefaultSession) 
        {
            this.idTask = task;
            this.Caption = task.Caption;
            this.idSession = task.idSession;
            this.Notes = "";
            this.State = 0;
            this.SpeechTimeTotal = 0;
            this.AvrgTime = 0;
            this.DateTimeStart = DateTime.Now;
            this.DelegateQnty = 0;

            this.CrDate = DateTime.Now;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
