﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
namespace VoteSystem
{
    [Persistent("votesystem.sesdelegates")]
    public class SesDelegateObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        DelegateObj fidDelegate;
        public DelegateObj idDelegate
        {
            get { return fidDelegate; }
            set { SetPropertyValue<DelegateObj>("idDelegate", ref fidDelegate, value); }
        }

        SeatObj fidSeat;
        public SeatObj idSeat
        {
            get { return fidSeat; }
            set { SetPropertyValue<SeatObj>("idSeat", ref fidSeat, value); }
        }

        bool fIsRegistered;
        public bool IsRegistered
        {
            get { return fIsRegistered; }
            set { SetPropertyValue<bool>("IsRegistered", ref fIsRegistered, value); }
        }

        bool fIsCardRegistered;
        public bool IsCardRegistered
        {
            get { return fIsCardRegistered; }
            set { SetPropertyValue<bool>("IsCardRegistered", ref fIsCardRegistered, value); }
        }

        DateTime fLastRegTime;
        public DateTime LastRegTime
        {
            get { return fLastRegTime; }
            set { SetPropertyValue<DateTime>("LastRegTime", ref fLastRegTime, value); }
        }

        bool fSeatFixed;
        public bool SeatFixed
        {
            get { return fSeatFixed; }
            set { SetPropertyValue<bool>("SeatFixed", ref fSeatFixed, value); }
        }

        [NonPersistent]
        public string MicNum { get { return String.Format("{0}", fidSeat.MicNum.ToString()); } }

        [NonPersistent]
        public string RowNum { get { return String.Format("{0}", fidSeat.RowNum.ToString()); } }

        [NonPersistent]
        public string SeatNum { get { return String.Format("{0}", fidSeat.SeatNum.ToString()); } }

        public SesDelegateObj(Session session) : base(session) { }
        public SesDelegateObj() : base(Session.DefaultSession) { }
        public SesDelegateObj(SesDelegateObj source) : base(source.Session)
        {
            fidDelegate = source.idDelegate;
            fidSeat = source.fidSeat;
            fIsCardRegistered = false;
            fIsRegistered = false;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
