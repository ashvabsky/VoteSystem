﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.sesquestions")]
    public class SesQuestionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCaption;
        [Size(255)]
        public string Caption
        {
            get { return fCaption; }
            set { SetPropertyValue<string>("Caption", ref fCaption, value); }
        }

        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        SessionObj fidSession;
        public SessionObj idSession
        {
            get { return fidSession; }
            set { SetPropertyValue<SessionObj>("idSession", ref fidSession, value); }
        }

        QuestKindObj fidKind;
//      [Association("VoteKindObj-QuestObj")]
        public QuestKindObj idKind
        {
            get { return fidKind; }
            set { SetPropertyValue<QuestKindObj>("idKind", ref fidKind, value); }
        }

        DateTime fCrDate;
        public DateTime CrDate
        {
            get { return fCrDate; }
            set { SetPropertyValue<DateTime>("CrDate", ref fCrDate, value); }
        }

        int fQueueNum;
        public int QueueNum
        {
            get { return fQueueNum; }
            set { SetPropertyValue<int>("QueueNum", ref fQueueNum, value); }
        }

        int fReadNum;
        public int ReadNum
        {
            get { return fReadNum; }
            set { SetPropertyValue<int>("ReadNum", ref fReadNum, value); }
        }

        int fidBaseQuest;
        //      [Association("VoteKindObj-QuestObj")]
        public int idBaseQuest
        {
            get { return fidBaseQuest; }
            set { SetPropertyValue<int>("idBaseQuest", ref fidBaseQuest, value); }
        }

        bool fIsVoted;
        public bool IsVoted
        {
            get { return fIsVoted; }
            set { SetPropertyValue<bool>("IsVoted", ref fIsVoted, value); }
        }

        [NonPersistent]
        public string Status
        {
            get
            {
                if (fIsVoted == true)
                    return "голосование завершено";
                else if (fIsVoted == false)
                    return "готовятся к голосованию";
                else
                    return "";
            }
        }


        public SesQuestionObj(Session session) : base(session) { }
        public SesQuestionObj() : base(Session.DefaultSession) { }
        public SesQuestionObj(QuestionObj q) : base(Session.DefaultSession) 
        {
            this.Caption = q.idQuestion.Caption;
            this.CrDate = DateTime.Now;
            this.Description = q.idQuestion.Description;
            this.idBaseQuest = q.id;
            this.idKind = q.idQuestion.idKind;
            this.idSession = q.idSession;
        }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.refquestkinds")]
    public class QuestKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        VoteKindObj fidVoteKind;
        //      [Association("VoteKindObj-QuestObj")]
        public VoteKindObj idVoteKind
        {
            get { return fidVoteKind; }
            set { SetPropertyValue<VoteKindObj>("idVoteKind", ref fidVoteKind, value); }
        }

        VoteTypeObj fidVoteType;
        //      [Association("VoteKindObj-QuestObj")]
        public VoteTypeObj idVoteType
        {
            get { return fidVoteType; }
            set { SetPropertyValue<VoteTypeObj>("idVoteType", ref fidVoteType, value); }
        }

        QntyTypeObj fidQntyType;
        //      [Association("VoteKindObj-QuestObj")]
        public QntyTypeObj idQntyType
        {
            get { return fidQntyType; }
            set { SetPropertyValue<QntyTypeObj>("idQntyType", ref fidQntyType, value); }
        }

        public QuestKindObj(Session session) : base(session) { }
        public QuestKindObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotetypes")]
    public class VoteTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(150)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }
        
/*
        [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
        public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
*/

        public VoteTypeObj(Session session) : base(session) { }
        public VoteTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.envotekinds")]
    public class VoteKindObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(50)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        /*
                [Association("VoteTypeObj-QuestObj", typeof(VoteTypeObj)), Aggregated]
                public XPCollection<VoteTypeObj> Quests { get { return GetCollection<DelegateObj>("Quests"); } }
        */


        string fCodeName;
        [Size(150)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        public VoteKindObj(Session session) : base(session) { }
        public VoteKindObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [Persistent("votesystem.refqntytypes")]
    public class QntyTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        int fProcentValue;
        public int ProcentValue
        {
            get { return fProcentValue; }
            set { SetPropertyValue<int>("ProcentValue", ref fProcentValue, value); }
        }

        DelegateQntyTypeObj fidDelegateQntyType;
        //      [Association("VoteKindObj-QuestObj")]
        public DelegateQntyTypeObj idDelegateQntyType
        {
            get { return fidDelegateQntyType; }
            set { SetPropertyValue<DelegateQntyTypeObj>("idDelegateQntyType", ref fidDelegateQntyType, value); }
        }

        public QntyTypeObj(Session session) : base(session) { }
        public QntyTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    [Persistent("votesystem.endelegatesqntytype")]
    public class DelegateQntyTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        int fValueInt;
        public int ValueInt
        {
            get { return fValueInt; }
            set { SetPropertyValue<int>("ValueInt", ref fValueInt, value); }
        }

        public DelegateQntyTypeObj(Session session) : base(session) { }
        public DelegateQntyTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
