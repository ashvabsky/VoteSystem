﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;

namespace VoteSystem
{
    [Persistent("votesystem.enspeechtypes")]
    public class SpeechTypeObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }

        string fCodeName;
        [Size(50)]
        public string CodeName
        {
            get { return fCodeName; }
            set { SetPropertyValue<string>("CodeName", ref fCodeName, value); }
        }

        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        int fValue;
        public int Value
        {
            get { return fValue; }
            set { SetPropertyValue<int>("Value", ref fValue, value); }
        }

        public SpeechTypeObj(Session session) : base(session) { }
        public SpeechTypeObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    [Persistent("votesystem.delegates_statements")]
    public class DelegateStatementObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }


        DelegateObj fidDelegate;
        public DelegateObj idDelegate
        {
            get { return fidDelegate; }
            set { SetPropertyValue<DelegateObj>("idDelegate", ref fidDelegate, value); }
        }

        SeatObj fidSeat;
        public SeatObj idSeat
        {
            get { return fidSeat; }
            set { SetPropertyValue<SeatObj>("idSeat", ref fidSeat, value); }
        }

        StatementObj fidStatement;
        public StatementObj idStatement
        {
            get { return fidStatement; }
            set { SetPropertyValue<StatementObj>("idStatement", ref fidStatement, value); }
        }

        SpeechTypeObj fidSpeechType;
        public SpeechTypeObj idSpeechType
        {
            get { return fidSpeechType; }
            set { SetPropertyValue<SpeechTypeObj>("idSpeechType", ref fidSpeechType, value); }
        }

        int fState;
        public int State
        {
            get { return fState; }
            set { SetPropertyValue<int>("State", ref fState, value); }
        }

        int fSpeechDurationTotal;
        public int SpeechDurationTotal
        {
            get { return fSpeechDurationTotal; }
            set { SetPropertyValue<int>("SpeechDurationTotal", ref fSpeechDurationTotal, value); }
        }

        int fSpeechDurationLast;
        public int SpeechDurationLast
        {
            get { return fSpeechDurationLast; }
            set { SetPropertyValue<int>("SpeechDurationLast", ref fSpeechDurationLast, value); }
        }

        DateTime fSpeechTimeStart;
        public DateTime SpeechTimeStart
        {
            get { return fSpeechTimeStart; }
            set { SetPropertyValue<DateTime>("SpeechTimeStart", ref fSpeechTimeStart, value); }
        }

        int fQNum;
        public int QNum
        {
            get { return fQNum; }
            set { SetPropertyValue<int>("QNum", ref fQNum, value); }
        }

        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }

        [NonPersistent]
        public string Status
        {
            get
            {
                if (fState == 1)
                    return "III. Выступили";
                else if (fState == 2)
                    return "I. Выступает";
                else 
                    return "II. Готовятся";
            }
        }

        [NonPersistent]
        public string SpeechDurationTime_F
        {
            get
            {
                Double d = fSpeechDurationTotal;
                d = d / 60;
                int i = System.Convert.ToInt32(d);
                string s = i.ToString();
                if (i == 0)
                {
                    i = System.Convert.ToInt32(fSpeechDurationTotal);
                    s = i.ToString();
                    s = " " + s + " сек.";
                }
                else
                {
                    s = " " + s + " мин.";
                }

                return s;
            }
        }

        [NonPersistent]
        public string SpeechDurationLast_F
        {
            get
            {
                Double d = fSpeechDurationLast;
                d = d / 60;
                int i = System.Convert.ToInt32(d);
                string s = i.ToString();
                if (i == 0)
                {
                    i = System.Convert.ToInt32(fSpeechDurationLast);
                    s = i.ToString();
                    s = " " + s + " сек.";
                }
                else
                {
                    s = " " + s + " мин.";
                }

                return s;
            }
        }

        [NonPersistent]
        public bool State_F
        {
            get
            {
                if (fState == 1)
                    return true;
                else 
                    return false;
            }
        }

        [NonPersistent]
        public bool IsSpeech
        {
            get
            {
                if (fState == 2)
                    return true;
                else
                    return false;
            }

            set
            {
                if (value == true)
                    fState = 2;
                else
                    fState = 1;
            }
 
        }

        public DelegateStatementObj(Session session) : base(session) { }
        public DelegateStatementObj() : base(Session.DefaultSession) 
        {
            fState = 0;
            fNotes = "";
        }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
