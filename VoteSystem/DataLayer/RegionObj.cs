﻿using System;
using DevExpress.Xpo;
namespace VoteSystem
{
    [Persistent("votesystem.regions")]
    public class RegionObj : XPLiteObject
    {
        int fid;
        [Key]
        public int id
        {
            get { return fid; }
            set { SetPropertyValue<int>("id", ref fid, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }

        string fNumber;
        [Size(50)]
        public string Number
        {
            get { return fNumber; }
            set { SetPropertyValue<string>("Number", ref fNumber, value); }
        }

        bool fInUse;
        public bool InUse
        {
            get { return fInUse; }
            set { SetPropertyValue<bool>("InUse", ref fInUse, value); }
        }


        string fDescription;
        [Size(255)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        [Association("RegionObj-DelegateObj", typeof(DelegateObj)), Aggregated]
        public XPCollection<DelegateObj> Delegates { get { return GetCollection<DelegateObj>("Delegates"); } }


        public RegionObj(Session session) : base(session) { }
        public RegionObj() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
