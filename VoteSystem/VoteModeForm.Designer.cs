namespace VoteSystem
{
    partial class VoteModeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoteModeForm));
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties1 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties multiEditorRowProperties2 = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.grpSeates = new DevExpress.XtraEditors.GroupControl();
            this.label92 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.labelControl97 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit75 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit76 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl99 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit77 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit78 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit79 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit80 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit81 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit82 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl105 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit83 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl106 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit84 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit85 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit86 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit87 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit88 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit89 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit90 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit91 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit92 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit93 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit94 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit95 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl118 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit96 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl119 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit97 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl120 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit98 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl121 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit99 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl122 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit100 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit101 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit102 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit103 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit104 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit105 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit106 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit107 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl130 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit108 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit109 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit110 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit111 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl134 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit112 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit113 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl136 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit114 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl137 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit115 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl138 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit116 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl139 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit117 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl140 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit118 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl141 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit119 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl142 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit120 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit121 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit122 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit59 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit60 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit61 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit62 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit63 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit64 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit65 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit66 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit67 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit68 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit69 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit70 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit71 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl94 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit72 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl95 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit73 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl96 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit74 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit43 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit44 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit45 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit46 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit47 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit48 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit49 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit50 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit51 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit52 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit53 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit54 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit55 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit56 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit57 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit58 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit27 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit19 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit28 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit20 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit29 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit21 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit30 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit22 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit31 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit23 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit32 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit24 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit33 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit25 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit34 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit26 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit35 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit36 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit37 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit38 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit39 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit40 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit41 = new DevExpress.XtraEditors.PictureEdit();
            this.Mic_2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit42 = new DevExpress.XtraEditors.PictureEdit();
            this.Mic_1 = new DevExpress.XtraEditors.PictureEdit();
            this.grpVoteResults = new DevExpress.XtraEditors.GroupControl();
            this.lblVoteResTime = new System.Windows.Forms.Label();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lblVoteResDate = new System.Windows.Forms.Label();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.grpVoteResultsTable = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.lblProcAye = new System.Windows.Forms.Label();
            this.lblVoteAye = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.lblNonProcQnty = new System.Windows.Forms.Label();
            this.lblNonVoteQnty = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.lblProcAgainst = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lblProcAbstain = new System.Windows.Forms.Label();
            this.lblVoteAgainst = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lblVoteAbstain = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.domainUpDown5 = new System.Windows.Forms.DomainUpDown();
            this.label91 = new System.Windows.Forms.Label();
            this.lblDecision = new System.Windows.Forms.Label();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.PropertiesControlDelegate = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox17 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_LastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_FirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_SecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_Region = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_RegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_RegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_Info = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_Fraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_PartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.cat_Seat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_MicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_RowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_SeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.PropertiesControlQuest = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox18 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox19 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox20 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox21 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox22 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox23 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox24 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox25 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox26 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox27 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox28 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox29 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_sq_Number = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_Caption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_CrDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_Notes = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteKind = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_ReadNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.row_sq_DecisionProcValue = new DevExpress.XtraVerticalGrid.Rows.MultiEditorRow();
            this.row_sq_DecisionProcType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_VoteQnty = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentValue = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentType_Present = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.row_sq_QuotaProcentType_Total = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtProcQnty = new DevExpress.XtraEditors.TextEdit();
            this.lblProc = new System.Windows.Forms.Label();
            this.btnVoteFinish = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtDelegateQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoteQnty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.lblVoteTime = new DevExpress.XtraEditors.LabelControl();
            this.btnVoteCancel = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarVoting = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblVoting = new DevExpress.XtraEditors.LabelControl();
            this.picVoteTime = new DevExpress.XtraEditors.PictureEdit();
            this.btnVoteStart = new DevExpress.XtraEditors.SimpleButton();
            this.TimerImages = new DevExpress.Utils.ImageCollection(this.components);
            this.ManImages = new DevExpress.Utils.ImageCollection(this.components);
            this.tmrVoteStart = new System.Windows.Forms.Timer(this.components);
            this.tmrRotation = new System.Windows.Forms.Timer(this.components);
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSeates)).BeginInit();
            this.grpSeates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpVoteResults)).BeginInit();
            this.grpVoteResults.SuspendLayout();
            this.grpVoteResultsTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlDelegate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManImages)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.ribbonControl1);
            this.splitContainerControl2.Panel1.Controls.Add(this.grpSeates);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.grpVoteResults);
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1167, 693);
            this.splitContainerControl2.SplitterPosition = 809;
            this.splitContainerControl2.TabIndex = 34;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonKeyTip = "";
            this.ribbonControl1.ApplicationIcon = null;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.btnClose});
            this.ribbonControl1.LargeImages = this.RibbonImages;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.SelectedPage = this.ribbonPage1;
            this.ribbonControl1.ShowCategoryInCaption = false;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(805, 95);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "���������";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.LargeImageIndex = 0;
            this.barButtonItem1.LargeWidth = 100;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "����� ����������";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.LargeImageIndex = 10;
            this.barButtonItem2.LargeWidth = 95;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "�������� �����";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.LargeImageIndex = 11;
            this.barButtonItem3.LargeWidth = 95;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "����� ��������";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.LargeImageIndex = 9;
            this.barButtonItem4.LargeWidth = 95;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "�������� ���������";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.LargeImageIndex = 10;
            this.barButtonItem5.LargeWidth = 95;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "��������� �� ��������";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.LargeImageIndex = 1;
            this.barButtonItem6.LargeWidth = 95;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "������";
            this.barButtonItem7.Id = 7;
            this.barButtonItem7.LargeImageIndex = 15;
            this.barButtonItem7.LargeWidth = 100;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // btnClose
            // 
            this.btnClose.Caption = "�����";
            this.btnClose.Id = 8;
            this.btnClose.LargeImageIndex = 16;
            this.btnClose.LargeWidth = 100;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(48, 48);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.ribbonPage1.KeyTip = "";
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.KeyTip = "";
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "������������";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.KeyTip = "";
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "�������� � ����";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup4.KeyTip = "";
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "������";
            // 
            // grpSeates
            // 
            this.grpSeates.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.grpSeates.ContentImage = global::VoteSystem.Properties.Resources.Background2;
            this.grpSeates.ContentImageAlignement = System.Drawing.ContentAlignment.TopLeft;
            this.grpSeates.Controls.Add(this.label92);
            this.grpSeates.Controls.Add(this.label15);
            this.grpSeates.Controls.Add(this.label24);
            this.grpSeates.Controls.Add(this.label27);
            this.grpSeates.Controls.Add(this.label36);
            this.grpSeates.Controls.Add(this.label37);
            this.grpSeates.Controls.Add(this.label38);
            this.grpSeates.Controls.Add(this.labelControl97);
            this.grpSeates.Controls.Add(this.pictureEdit75);
            this.grpSeates.Controls.Add(this.labelControl98);
            this.grpSeates.Controls.Add(this.pictureEdit76);
            this.grpSeates.Controls.Add(this.labelControl99);
            this.grpSeates.Controls.Add(this.pictureEdit77);
            this.grpSeates.Controls.Add(this.labelControl100);
            this.grpSeates.Controls.Add(this.pictureEdit78);
            this.grpSeates.Controls.Add(this.labelControl101);
            this.grpSeates.Controls.Add(this.pictureEdit79);
            this.grpSeates.Controls.Add(this.labelControl102);
            this.grpSeates.Controls.Add(this.pictureEdit80);
            this.grpSeates.Controls.Add(this.labelControl103);
            this.grpSeates.Controls.Add(this.pictureEdit81);
            this.grpSeates.Controls.Add(this.labelControl104);
            this.grpSeates.Controls.Add(this.pictureEdit82);
            this.grpSeates.Controls.Add(this.labelControl105);
            this.grpSeates.Controls.Add(this.pictureEdit83);
            this.grpSeates.Controls.Add(this.labelControl106);
            this.grpSeates.Controls.Add(this.pictureEdit84);
            this.grpSeates.Controls.Add(this.labelControl107);
            this.grpSeates.Controls.Add(this.pictureEdit85);
            this.grpSeates.Controls.Add(this.labelControl108);
            this.grpSeates.Controls.Add(this.pictureEdit86);
            this.grpSeates.Controls.Add(this.labelControl109);
            this.grpSeates.Controls.Add(this.pictureEdit87);
            this.grpSeates.Controls.Add(this.labelControl110);
            this.grpSeates.Controls.Add(this.pictureEdit88);
            this.grpSeates.Controls.Add(this.labelControl111);
            this.grpSeates.Controls.Add(this.pictureEdit89);
            this.grpSeates.Controls.Add(this.labelControl112);
            this.grpSeates.Controls.Add(this.pictureEdit90);
            this.grpSeates.Controls.Add(this.labelControl113);
            this.grpSeates.Controls.Add(this.pictureEdit91);
            this.grpSeates.Controls.Add(this.labelControl114);
            this.grpSeates.Controls.Add(this.pictureEdit92);
            this.grpSeates.Controls.Add(this.labelControl115);
            this.grpSeates.Controls.Add(this.pictureEdit93);
            this.grpSeates.Controls.Add(this.labelControl116);
            this.grpSeates.Controls.Add(this.pictureEdit94);
            this.grpSeates.Controls.Add(this.labelControl117);
            this.grpSeates.Controls.Add(this.pictureEdit95);
            this.grpSeates.Controls.Add(this.labelControl118);
            this.grpSeates.Controls.Add(this.pictureEdit96);
            this.grpSeates.Controls.Add(this.labelControl119);
            this.grpSeates.Controls.Add(this.pictureEdit97);
            this.grpSeates.Controls.Add(this.labelControl120);
            this.grpSeates.Controls.Add(this.pictureEdit98);
            this.grpSeates.Controls.Add(this.labelControl121);
            this.grpSeates.Controls.Add(this.pictureEdit99);
            this.grpSeates.Controls.Add(this.labelControl122);
            this.grpSeates.Controls.Add(this.pictureEdit100);
            this.grpSeates.Controls.Add(this.labelControl123);
            this.grpSeates.Controls.Add(this.pictureEdit101);
            this.grpSeates.Controls.Add(this.labelControl124);
            this.grpSeates.Controls.Add(this.pictureEdit102);
            this.grpSeates.Controls.Add(this.labelControl125);
            this.grpSeates.Controls.Add(this.pictureEdit103);
            this.grpSeates.Controls.Add(this.labelControl126);
            this.grpSeates.Controls.Add(this.pictureEdit104);
            this.grpSeates.Controls.Add(this.labelControl127);
            this.grpSeates.Controls.Add(this.pictureEdit105);
            this.grpSeates.Controls.Add(this.labelControl128);
            this.grpSeates.Controls.Add(this.pictureEdit106);
            this.grpSeates.Controls.Add(this.labelControl129);
            this.grpSeates.Controls.Add(this.pictureEdit107);
            this.grpSeates.Controls.Add(this.labelControl130);
            this.grpSeates.Controls.Add(this.pictureEdit108);
            this.grpSeates.Controls.Add(this.labelControl131);
            this.grpSeates.Controls.Add(this.pictureEdit109);
            this.grpSeates.Controls.Add(this.labelControl132);
            this.grpSeates.Controls.Add(this.pictureEdit110);
            this.grpSeates.Controls.Add(this.labelControl133);
            this.grpSeates.Controls.Add(this.pictureEdit111);
            this.grpSeates.Controls.Add(this.labelControl134);
            this.grpSeates.Controls.Add(this.pictureEdit112);
            this.grpSeates.Controls.Add(this.labelControl135);
            this.grpSeates.Controls.Add(this.pictureEdit113);
            this.grpSeates.Controls.Add(this.labelControl136);
            this.grpSeates.Controls.Add(this.pictureEdit114);
            this.grpSeates.Controls.Add(this.labelControl137);
            this.grpSeates.Controls.Add(this.pictureEdit115);
            this.grpSeates.Controls.Add(this.labelControl138);
            this.grpSeates.Controls.Add(this.pictureEdit116);
            this.grpSeates.Controls.Add(this.labelControl139);
            this.grpSeates.Controls.Add(this.pictureEdit117);
            this.grpSeates.Controls.Add(this.labelControl140);
            this.grpSeates.Controls.Add(this.pictureEdit118);
            this.grpSeates.Controls.Add(this.labelControl141);
            this.grpSeates.Controls.Add(this.pictureEdit119);
            this.grpSeates.Controls.Add(this.labelControl142);
            this.grpSeates.Controls.Add(this.pictureEdit120);
            this.grpSeates.Controls.Add(this.labelControl143);
            this.grpSeates.Controls.Add(this.pictureEdit121);
            this.grpSeates.Controls.Add(this.labelControl144);
            this.grpSeates.Controls.Add(this.pictureEdit122);
            this.grpSeates.Controls.Add(this.labelControl81);
            this.grpSeates.Controls.Add(this.pictureEdit59);
            this.grpSeates.Controls.Add(this.labelControl82);
            this.grpSeates.Controls.Add(this.pictureEdit60);
            this.grpSeates.Controls.Add(this.labelControl83);
            this.grpSeates.Controls.Add(this.pictureEdit61);
            this.grpSeates.Controls.Add(this.labelControl84);
            this.grpSeates.Controls.Add(this.pictureEdit62);
            this.grpSeates.Controls.Add(this.labelControl85);
            this.grpSeates.Controls.Add(this.pictureEdit63);
            this.grpSeates.Controls.Add(this.labelControl86);
            this.grpSeates.Controls.Add(this.pictureEdit64);
            this.grpSeates.Controls.Add(this.labelControl87);
            this.grpSeates.Controls.Add(this.pictureEdit65);
            this.grpSeates.Controls.Add(this.labelControl88);
            this.grpSeates.Controls.Add(this.pictureEdit66);
            this.grpSeates.Controls.Add(this.labelControl89);
            this.grpSeates.Controls.Add(this.pictureEdit67);
            this.grpSeates.Controls.Add(this.labelControl90);
            this.grpSeates.Controls.Add(this.pictureEdit68);
            this.grpSeates.Controls.Add(this.labelControl91);
            this.grpSeates.Controls.Add(this.pictureEdit69);
            this.grpSeates.Controls.Add(this.labelControl92);
            this.grpSeates.Controls.Add(this.pictureEdit70);
            this.grpSeates.Controls.Add(this.labelControl93);
            this.grpSeates.Controls.Add(this.pictureEdit71);
            this.grpSeates.Controls.Add(this.labelControl94);
            this.grpSeates.Controls.Add(this.pictureEdit72);
            this.grpSeates.Controls.Add(this.labelControl95);
            this.grpSeates.Controls.Add(this.pictureEdit73);
            this.grpSeates.Controls.Add(this.labelControl96);
            this.grpSeates.Controls.Add(this.pictureEdit74);
            this.grpSeates.Controls.Add(this.labelControl65);
            this.grpSeates.Controls.Add(this.pictureEdit43);
            this.grpSeates.Controls.Add(this.labelControl66);
            this.grpSeates.Controls.Add(this.pictureEdit44);
            this.grpSeates.Controls.Add(this.labelControl67);
            this.grpSeates.Controls.Add(this.pictureEdit45);
            this.grpSeates.Controls.Add(this.labelControl68);
            this.grpSeates.Controls.Add(this.pictureEdit46);
            this.grpSeates.Controls.Add(this.labelControl69);
            this.grpSeates.Controls.Add(this.pictureEdit47);
            this.grpSeates.Controls.Add(this.labelControl70);
            this.grpSeates.Controls.Add(this.pictureEdit48);
            this.grpSeates.Controls.Add(this.labelControl71);
            this.grpSeates.Controls.Add(this.pictureEdit49);
            this.grpSeates.Controls.Add(this.labelControl72);
            this.grpSeates.Controls.Add(this.pictureEdit50);
            this.grpSeates.Controls.Add(this.labelControl73);
            this.grpSeates.Controls.Add(this.pictureEdit51);
            this.grpSeates.Controls.Add(this.labelControl74);
            this.grpSeates.Controls.Add(this.pictureEdit52);
            this.grpSeates.Controls.Add(this.labelControl75);
            this.grpSeates.Controls.Add(this.pictureEdit53);
            this.grpSeates.Controls.Add(this.labelControl76);
            this.grpSeates.Controls.Add(this.pictureEdit54);
            this.grpSeates.Controls.Add(this.labelControl77);
            this.grpSeates.Controls.Add(this.pictureEdit55);
            this.grpSeates.Controls.Add(this.labelControl78);
            this.grpSeates.Controls.Add(this.pictureEdit56);
            this.grpSeates.Controls.Add(this.labelControl79);
            this.grpSeates.Controls.Add(this.pictureEdit57);
            this.grpSeates.Controls.Add(this.labelControl80);
            this.grpSeates.Controls.Add(this.pictureEdit58);
            this.grpSeates.Controls.Add(this.labelControl49);
            this.grpSeates.Controls.Add(this.labelControl41);
            this.grpSeates.Controls.Add(this.pictureEdit27);
            this.grpSeates.Controls.Add(this.pictureEdit19);
            this.grpSeates.Controls.Add(this.labelControl50);
            this.grpSeates.Controls.Add(this.labelControl42);
            this.grpSeates.Controls.Add(this.pictureEdit28);
            this.grpSeates.Controls.Add(this.pictureEdit20);
            this.grpSeates.Controls.Add(this.labelControl51);
            this.grpSeates.Controls.Add(this.labelControl43);
            this.grpSeates.Controls.Add(this.pictureEdit29);
            this.grpSeates.Controls.Add(this.pictureEdit21);
            this.grpSeates.Controls.Add(this.labelControl52);
            this.grpSeates.Controls.Add(this.labelControl44);
            this.grpSeates.Controls.Add(this.pictureEdit30);
            this.grpSeates.Controls.Add(this.pictureEdit22);
            this.grpSeates.Controls.Add(this.labelControl53);
            this.grpSeates.Controls.Add(this.labelControl45);
            this.grpSeates.Controls.Add(this.pictureEdit31);
            this.grpSeates.Controls.Add(this.pictureEdit23);
            this.grpSeates.Controls.Add(this.labelControl54);
            this.grpSeates.Controls.Add(this.labelControl46);
            this.grpSeates.Controls.Add(this.pictureEdit32);
            this.grpSeates.Controls.Add(this.pictureEdit24);
            this.grpSeates.Controls.Add(this.labelControl55);
            this.grpSeates.Controls.Add(this.labelControl47);
            this.grpSeates.Controls.Add(this.pictureEdit33);
            this.grpSeates.Controls.Add(this.pictureEdit25);
            this.grpSeates.Controls.Add(this.labelControl56);
            this.grpSeates.Controls.Add(this.labelControl48);
            this.grpSeates.Controls.Add(this.pictureEdit34);
            this.grpSeates.Controls.Add(this.pictureEdit26);
            this.grpSeates.Controls.Add(this.labelControl57);
            this.grpSeates.Controls.Add(this.labelControl8);
            this.grpSeates.Controls.Add(this.pictureEdit35);
            this.grpSeates.Controls.Add(this.pictureEdit1);
            this.grpSeates.Controls.Add(this.labelControl58);
            this.grpSeates.Controls.Add(this.labelControl9);
            this.grpSeates.Controls.Add(this.pictureEdit36);
            this.grpSeates.Controls.Add(this.pictureEdit2);
            this.grpSeates.Controls.Add(this.labelControl59);
            this.grpSeates.Controls.Add(this.labelControl10);
            this.grpSeates.Controls.Add(this.pictureEdit37);
            this.grpSeates.Controls.Add(this.pictureEdit3);
            this.grpSeates.Controls.Add(this.labelControl60);
            this.grpSeates.Controls.Add(this.labelControl11);
            this.grpSeates.Controls.Add(this.pictureEdit38);
            this.grpSeates.Controls.Add(this.pictureEdit4);
            this.grpSeates.Controls.Add(this.labelControl61);
            this.grpSeates.Controls.Add(this.labelControl12);
            this.grpSeates.Controls.Add(this.pictureEdit39);
            this.grpSeates.Controls.Add(this.pictureEdit5);
            this.grpSeates.Controls.Add(this.labelControl62);
            this.grpSeates.Controls.Add(this.labelControl13);
            this.grpSeates.Controls.Add(this.pictureEdit40);
            this.grpSeates.Controls.Add(this.pictureEdit6);
            this.grpSeates.Controls.Add(this.labelControl63);
            this.grpSeates.Controls.Add(this.labelControl14);
            this.grpSeates.Controls.Add(this.pictureEdit41);
            this.grpSeates.Controls.Add(this.Mic_2);
            this.grpSeates.Controls.Add(this.labelControl64);
            this.grpSeates.Controls.Add(this.labelControl15);
            this.grpSeates.Controls.Add(this.pictureEdit42);
            this.grpSeates.Controls.Add(this.Mic_1);
            this.grpSeates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpSeates.Location = new System.Drawing.Point(0, 95);
            this.grpSeates.Name = "grpSeates";
            this.grpSeates.Size = new System.Drawing.Size(805, 594);
            this.grpSeates.TabIndex = 0;
            this.grpSeates.Text = "�����������";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(18, 442);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(43, 16);
            this.label92.TabIndex = 277;
            this.label92.Text = "��� 6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(18, 377);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 16);
            this.label15.TabIndex = 276;
            this.label15.Text = "��� 5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(18, 318);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 16);
            this.label24.TabIndex = 275;
            this.label24.Text = "��� 4";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(18, 259);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 274;
            this.label27.Text = "��� 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(17, 195);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 16);
            this.label36.TabIndex = 273;
            this.label36.Text = "��� 2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(18, 135);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 272;
            this.label37.Text = "��� 1";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(5, 51);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(81, 16);
            this.label38.TabIndex = 271;
            this.label38.Text = "���������";
            // 
            // labelControl97
            // 
            this.labelControl97.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl97.Appearance.Options.UseFont = true;
            this.labelControl97.Location = new System.Drawing.Point(715, 460);
            this.labelControl97.Name = "labelControl97";
            this.labelControl97.Size = new System.Drawing.Size(18, 15);
            this.labelControl97.TabIndex = 270;
            this.labelControl97.Text = "112";
            // 
            // pictureEdit75
            // 
            this.pictureEdit75.AllowDrop = true;
            this.pictureEdit75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit75.Location = new System.Drawing.Point(714, 431);
            this.pictureEdit75.Name = "pictureEdit75";
            this.pictureEdit75.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit75.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit75.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit75.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit75.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit75.Properties.UseParentBackground = true;
            this.pictureEdit75.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit75.TabIndex = 269;
            this.pictureEdit75.Tag = "Mic112";
            this.pictureEdit75.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl98
            // 
            this.labelControl98.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl98.Appearance.Options.UseFont = true;
            this.labelControl98.Location = new System.Drawing.Point(675, 460);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(18, 15);
            this.labelControl98.TabIndex = 268;
            this.labelControl98.Text = "111";
            // 
            // pictureEdit76
            // 
            this.pictureEdit76.AllowDrop = true;
            this.pictureEdit76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit76.Location = new System.Drawing.Point(675, 431);
            this.pictureEdit76.Name = "pictureEdit76";
            this.pictureEdit76.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit76.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit76.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit76.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit76.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit76.Properties.UseParentBackground = true;
            this.pictureEdit76.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit76.TabIndex = 267;
            this.pictureEdit76.Tag = "Mic111";
            this.pictureEdit76.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl99
            // 
            this.labelControl99.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl99.Appearance.Options.UseFont = true;
            this.labelControl99.Location = new System.Drawing.Point(637, 459);
            this.labelControl99.Name = "labelControl99";
            this.labelControl99.Size = new System.Drawing.Size(18, 15);
            this.labelControl99.TabIndex = 266;
            this.labelControl99.Text = "110";
            // 
            // pictureEdit77
            // 
            this.pictureEdit77.AllowDrop = true;
            this.pictureEdit77.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit77.Location = new System.Drawing.Point(636, 431);
            this.pictureEdit77.Name = "pictureEdit77";
            this.pictureEdit77.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit77.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit77.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit77.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit77.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit77.Properties.UseParentBackground = true;
            this.pictureEdit77.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit77.TabIndex = 265;
            this.pictureEdit77.Tag = "Mic110";
            this.pictureEdit77.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl100
            // 
            this.labelControl100.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl100.Appearance.Options.UseFont = true;
            this.labelControl100.Location = new System.Drawing.Point(598, 460);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(18, 15);
            this.labelControl100.TabIndex = 264;
            this.labelControl100.Text = "109";
            // 
            // pictureEdit78
            // 
            this.pictureEdit78.AllowDrop = true;
            this.pictureEdit78.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit78.Location = new System.Drawing.Point(597, 431);
            this.pictureEdit78.Name = "pictureEdit78";
            this.pictureEdit78.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit78.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit78.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit78.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit78.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit78.Properties.UseParentBackground = true;
            this.pictureEdit78.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit78.TabIndex = 263;
            this.pictureEdit78.Tag = "Mic109";
            this.pictureEdit78.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl101.Appearance.Options.UseFont = true;
            this.labelControl101.Location = new System.Drawing.Point(559, 459);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(18, 15);
            this.labelControl101.TabIndex = 262;
            this.labelControl101.Text = "108";
            // 
            // pictureEdit79
            // 
            this.pictureEdit79.AllowDrop = true;
            this.pictureEdit79.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit79.Location = new System.Drawing.Point(558, 431);
            this.pictureEdit79.Name = "pictureEdit79";
            this.pictureEdit79.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit79.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit79.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit79.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit79.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit79.Properties.UseParentBackground = true;
            this.pictureEdit79.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit79.TabIndex = 261;
            this.pictureEdit79.Tag = "Mic108";
            this.pictureEdit79.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl102
            // 
            this.labelControl102.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl102.Appearance.Options.UseFont = true;
            this.labelControl102.Location = new System.Drawing.Point(519, 460);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(18, 15);
            this.labelControl102.TabIndex = 260;
            this.labelControl102.Text = "107";
            // 
            // pictureEdit80
            // 
            this.pictureEdit80.AllowDrop = true;
            this.pictureEdit80.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit80.Location = new System.Drawing.Point(519, 431);
            this.pictureEdit80.Name = "pictureEdit80";
            this.pictureEdit80.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit80.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit80.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit80.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit80.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit80.Properties.UseParentBackground = true;
            this.pictureEdit80.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit80.TabIndex = 259;
            this.pictureEdit80.Tag = "Mic107";
            this.pictureEdit80.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl103.Appearance.Options.UseFont = true;
            this.labelControl103.Location = new System.Drawing.Point(481, 460);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(18, 15);
            this.labelControl103.TabIndex = 258;
            this.labelControl103.Text = "106";
            // 
            // pictureEdit81
            // 
            this.pictureEdit81.AllowDrop = true;
            this.pictureEdit81.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit81.Location = new System.Drawing.Point(480, 431);
            this.pictureEdit81.Name = "pictureEdit81";
            this.pictureEdit81.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit81.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit81.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit81.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit81.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit81.Properties.UseParentBackground = true;
            this.pictureEdit81.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit81.TabIndex = 257;
            this.pictureEdit81.Tag = "Mic106";
            this.pictureEdit81.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl104.Appearance.Options.UseFont = true;
            this.labelControl104.Location = new System.Drawing.Point(442, 459);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(18, 15);
            this.labelControl104.TabIndex = 256;
            this.labelControl104.Text = "105";
            // 
            // pictureEdit82
            // 
            this.pictureEdit82.AllowDrop = true;
            this.pictureEdit82.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit82.Location = new System.Drawing.Point(441, 431);
            this.pictureEdit82.Name = "pictureEdit82";
            this.pictureEdit82.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit82.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit82.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit82.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit82.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit82.Properties.UseParentBackground = true;
            this.pictureEdit82.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit82.TabIndex = 255;
            this.pictureEdit82.Tag = "Mic105";
            this.pictureEdit82.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl105
            // 
            this.labelControl105.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl105.Appearance.Options.UseFont = true;
            this.labelControl105.Location = new System.Drawing.Point(383, 460);
            this.labelControl105.Name = "labelControl105";
            this.labelControl105.Size = new System.Drawing.Size(18, 15);
            this.labelControl105.TabIndex = 254;
            this.labelControl105.Text = "104";
            // 
            // pictureEdit83
            // 
            this.pictureEdit83.AllowDrop = true;
            this.pictureEdit83.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit83.Location = new System.Drawing.Point(382, 431);
            this.pictureEdit83.Name = "pictureEdit83";
            this.pictureEdit83.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit83.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit83.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit83.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit83.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit83.Properties.UseParentBackground = true;
            this.pictureEdit83.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit83.TabIndex = 253;
            this.pictureEdit83.Tag = "Mic104";
            this.pictureEdit83.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl106
            // 
            this.labelControl106.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl106.Appearance.Options.UseFont = true;
            this.labelControl106.Location = new System.Drawing.Point(343, 460);
            this.labelControl106.Name = "labelControl106";
            this.labelControl106.Size = new System.Drawing.Size(18, 15);
            this.labelControl106.TabIndex = 252;
            this.labelControl106.Text = "103";
            // 
            // pictureEdit84
            // 
            this.pictureEdit84.AllowDrop = true;
            this.pictureEdit84.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit84.Location = new System.Drawing.Point(343, 431);
            this.pictureEdit84.Name = "pictureEdit84";
            this.pictureEdit84.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit84.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit84.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit84.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit84.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit84.Properties.UseParentBackground = true;
            this.pictureEdit84.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit84.TabIndex = 251;
            this.pictureEdit84.Tag = "Mic103";
            this.pictureEdit84.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl107
            // 
            this.labelControl107.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl107.Appearance.Options.UseFont = true;
            this.labelControl107.Location = new System.Drawing.Point(305, 459);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(18, 15);
            this.labelControl107.TabIndex = 250;
            this.labelControl107.Text = "102";
            // 
            // pictureEdit85
            // 
            this.pictureEdit85.AllowDrop = true;
            this.pictureEdit85.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit85.Location = new System.Drawing.Point(304, 431);
            this.pictureEdit85.Name = "pictureEdit85";
            this.pictureEdit85.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit85.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit85.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit85.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit85.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit85.Properties.UseParentBackground = true;
            this.pictureEdit85.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit85.TabIndex = 249;
            this.pictureEdit85.Tag = "Mic102";
            this.pictureEdit85.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl108
            // 
            this.labelControl108.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl108.Appearance.Options.UseFont = true;
            this.labelControl108.Location = new System.Drawing.Point(266, 460);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(18, 15);
            this.labelControl108.TabIndex = 248;
            this.labelControl108.Text = "101";
            // 
            // pictureEdit86
            // 
            this.pictureEdit86.AllowDrop = true;
            this.pictureEdit86.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit86.Location = new System.Drawing.Point(265, 431);
            this.pictureEdit86.Name = "pictureEdit86";
            this.pictureEdit86.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit86.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit86.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit86.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit86.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit86.Properties.UseParentBackground = true;
            this.pictureEdit86.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit86.TabIndex = 247;
            this.pictureEdit86.Tag = "Mic101";
            this.pictureEdit86.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl109
            // 
            this.labelControl109.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl109.Appearance.Options.UseFont = true;
            this.labelControl109.Location = new System.Drawing.Point(227, 459);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(18, 15);
            this.labelControl109.TabIndex = 246;
            this.labelControl109.Text = "100";
            // 
            // pictureEdit87
            // 
            this.pictureEdit87.AllowDrop = true;
            this.pictureEdit87.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit87.Location = new System.Drawing.Point(226, 431);
            this.pictureEdit87.Name = "pictureEdit87";
            this.pictureEdit87.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit87.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit87.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit87.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit87.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit87.Properties.UseParentBackground = true;
            this.pictureEdit87.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit87.TabIndex = 245;
            this.pictureEdit87.Tag = "Mic100";
            this.pictureEdit87.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl110.Appearance.Options.UseFont = true;
            this.labelControl110.Location = new System.Drawing.Point(190, 460);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(12, 15);
            this.labelControl110.TabIndex = 244;
            this.labelControl110.Text = "99";
            // 
            // pictureEdit88
            // 
            this.pictureEdit88.AllowDrop = true;
            this.pictureEdit88.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit88.Location = new System.Drawing.Point(187, 431);
            this.pictureEdit88.Name = "pictureEdit88";
            this.pictureEdit88.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit88.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit88.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit88.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit88.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit88.Properties.UseParentBackground = true;
            this.pictureEdit88.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit88.TabIndex = 243;
            this.pictureEdit88.Tag = "Mic099";
            this.pictureEdit88.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl111.Appearance.Options.UseFont = true;
            this.labelControl111.Location = new System.Drawing.Point(152, 460);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(12, 15);
            this.labelControl111.TabIndex = 242;
            this.labelControl111.Text = "98";
            // 
            // pictureEdit89
            // 
            this.pictureEdit89.AllowDrop = true;
            this.pictureEdit89.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit89.Location = new System.Drawing.Point(148, 431);
            this.pictureEdit89.Name = "pictureEdit89";
            this.pictureEdit89.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit89.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit89.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit89.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit89.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit89.Properties.UseParentBackground = true;
            this.pictureEdit89.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit89.TabIndex = 241;
            this.pictureEdit89.Tag = "Mic098";
            this.pictureEdit89.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl112.Appearance.Options.UseFont = true;
            this.labelControl112.Location = new System.Drawing.Point(113, 459);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(12, 15);
            this.labelControl112.TabIndex = 240;
            this.labelControl112.Text = "97";
            // 
            // pictureEdit90
            // 
            this.pictureEdit90.AllowDrop = true;
            this.pictureEdit90.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit90.Location = new System.Drawing.Point(109, 431);
            this.pictureEdit90.Name = "pictureEdit90";
            this.pictureEdit90.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit90.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit90.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit90.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit90.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit90.Properties.UseParentBackground = true;
            this.pictureEdit90.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit90.TabIndex = 239;
            this.pictureEdit90.Tag = "Mic097";
            this.pictureEdit90.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl113.Appearance.Options.UseFont = true;
            this.labelControl113.Location = new System.Drawing.Point(718, 396);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(12, 15);
            this.labelControl113.TabIndex = 238;
            this.labelControl113.Text = "96";
            // 
            // pictureEdit91
            // 
            this.pictureEdit91.AllowDrop = true;
            this.pictureEdit91.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit91.Location = new System.Drawing.Point(714, 367);
            this.pictureEdit91.Name = "pictureEdit91";
            this.pictureEdit91.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit91.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit91.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit91.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit91.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit91.Properties.UseParentBackground = true;
            this.pictureEdit91.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit91.TabIndex = 237;
            this.pictureEdit91.Tag = "Mic096";
            this.pictureEdit91.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl114.Appearance.Options.UseFont = true;
            this.labelControl114.Location = new System.Drawing.Point(678, 396);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(12, 15);
            this.labelControl114.TabIndex = 236;
            this.labelControl114.Text = "95";
            // 
            // pictureEdit92
            // 
            this.pictureEdit92.AllowDrop = true;
            this.pictureEdit92.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit92.Location = new System.Drawing.Point(675, 367);
            this.pictureEdit92.Name = "pictureEdit92";
            this.pictureEdit92.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit92.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit92.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit92.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit92.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit92.Properties.UseParentBackground = true;
            this.pictureEdit92.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit92.TabIndex = 235;
            this.pictureEdit92.Tag = "Mic095";
            this.pictureEdit92.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl115.Appearance.Options.UseFont = true;
            this.labelControl115.Location = new System.Drawing.Point(640, 395);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(12, 15);
            this.labelControl115.TabIndex = 234;
            this.labelControl115.Text = "94";
            // 
            // pictureEdit93
            // 
            this.pictureEdit93.AllowDrop = true;
            this.pictureEdit93.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit93.Location = new System.Drawing.Point(636, 367);
            this.pictureEdit93.Name = "pictureEdit93";
            this.pictureEdit93.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit93.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit93.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit93.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit93.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit93.Properties.UseParentBackground = true;
            this.pictureEdit93.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit93.TabIndex = 233;
            this.pictureEdit93.Tag = "Mic094";
            this.pictureEdit93.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl116.Appearance.Options.UseFont = true;
            this.labelControl116.Location = new System.Drawing.Point(601, 396);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(12, 15);
            this.labelControl116.TabIndex = 232;
            this.labelControl116.Text = "93";
            // 
            // pictureEdit94
            // 
            this.pictureEdit94.AllowDrop = true;
            this.pictureEdit94.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit94.Location = new System.Drawing.Point(597, 367);
            this.pictureEdit94.Name = "pictureEdit94";
            this.pictureEdit94.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit94.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit94.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit94.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit94.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit94.Properties.UseParentBackground = true;
            this.pictureEdit94.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit94.TabIndex = 231;
            this.pictureEdit94.Tag = "Mic093";
            this.pictureEdit94.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl117.Appearance.Options.UseFont = true;
            this.labelControl117.Location = new System.Drawing.Point(562, 395);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(12, 15);
            this.labelControl117.TabIndex = 230;
            this.labelControl117.Text = "92";
            // 
            // pictureEdit95
            // 
            this.pictureEdit95.AllowDrop = true;
            this.pictureEdit95.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit95.Location = new System.Drawing.Point(558, 367);
            this.pictureEdit95.Name = "pictureEdit95";
            this.pictureEdit95.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit95.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit95.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit95.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit95.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit95.Properties.UseParentBackground = true;
            this.pictureEdit95.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit95.TabIndex = 229;
            this.pictureEdit95.Tag = "Mic092";
            this.pictureEdit95.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl118
            // 
            this.labelControl118.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl118.Appearance.Options.UseFont = true;
            this.labelControl118.Location = new System.Drawing.Point(522, 396);
            this.labelControl118.Name = "labelControl118";
            this.labelControl118.Size = new System.Drawing.Size(12, 15);
            this.labelControl118.TabIndex = 228;
            this.labelControl118.Text = "91";
            // 
            // pictureEdit96
            // 
            this.pictureEdit96.AllowDrop = true;
            this.pictureEdit96.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit96.Location = new System.Drawing.Point(519, 367);
            this.pictureEdit96.Name = "pictureEdit96";
            this.pictureEdit96.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit96.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit96.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit96.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit96.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit96.Properties.UseParentBackground = true;
            this.pictureEdit96.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit96.TabIndex = 227;
            this.pictureEdit96.Tag = "Mic091";
            this.pictureEdit96.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl119
            // 
            this.labelControl119.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl119.Appearance.Options.UseFont = true;
            this.labelControl119.Location = new System.Drawing.Point(484, 396);
            this.labelControl119.Name = "labelControl119";
            this.labelControl119.Size = new System.Drawing.Size(12, 15);
            this.labelControl119.TabIndex = 226;
            this.labelControl119.Text = "90";
            // 
            // pictureEdit97
            // 
            this.pictureEdit97.AllowDrop = true;
            this.pictureEdit97.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit97.Location = new System.Drawing.Point(480, 367);
            this.pictureEdit97.Name = "pictureEdit97";
            this.pictureEdit97.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit97.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit97.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit97.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit97.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit97.Properties.UseParentBackground = true;
            this.pictureEdit97.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit97.TabIndex = 225;
            this.pictureEdit97.Tag = "Mic090";
            this.pictureEdit97.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl120
            // 
            this.labelControl120.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl120.Appearance.Options.UseFont = true;
            this.labelControl120.Location = new System.Drawing.Point(445, 395);
            this.labelControl120.Name = "labelControl120";
            this.labelControl120.Size = new System.Drawing.Size(12, 15);
            this.labelControl120.TabIndex = 224;
            this.labelControl120.Text = "89";
            // 
            // pictureEdit98
            // 
            this.pictureEdit98.AllowDrop = true;
            this.pictureEdit98.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit98.Location = new System.Drawing.Point(441, 367);
            this.pictureEdit98.Name = "pictureEdit98";
            this.pictureEdit98.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit98.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit98.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit98.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit98.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit98.Properties.UseParentBackground = true;
            this.pictureEdit98.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit98.TabIndex = 223;
            this.pictureEdit98.Tag = "Mic089";
            this.pictureEdit98.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl121
            // 
            this.labelControl121.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl121.Appearance.Options.UseFont = true;
            this.labelControl121.Location = new System.Drawing.Point(386, 396);
            this.labelControl121.Name = "labelControl121";
            this.labelControl121.Size = new System.Drawing.Size(12, 15);
            this.labelControl121.TabIndex = 222;
            this.labelControl121.Text = "88";
            // 
            // pictureEdit99
            // 
            this.pictureEdit99.AllowDrop = true;
            this.pictureEdit99.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit99.Location = new System.Drawing.Point(382, 367);
            this.pictureEdit99.Name = "pictureEdit99";
            this.pictureEdit99.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit99.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit99.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit99.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit99.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit99.Properties.UseParentBackground = true;
            this.pictureEdit99.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit99.TabIndex = 221;
            this.pictureEdit99.Tag = "Mic088";
            this.pictureEdit99.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl122
            // 
            this.labelControl122.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl122.Appearance.Options.UseFont = true;
            this.labelControl122.Location = new System.Drawing.Point(346, 396);
            this.labelControl122.Name = "labelControl122";
            this.labelControl122.Size = new System.Drawing.Size(12, 15);
            this.labelControl122.TabIndex = 220;
            this.labelControl122.Text = "87";
            // 
            // pictureEdit100
            // 
            this.pictureEdit100.AllowDrop = true;
            this.pictureEdit100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit100.Location = new System.Drawing.Point(343, 367);
            this.pictureEdit100.Name = "pictureEdit100";
            this.pictureEdit100.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit100.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit100.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit100.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit100.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit100.Properties.UseParentBackground = true;
            this.pictureEdit100.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit100.TabIndex = 219;
            this.pictureEdit100.Tag = "Mic087";
            this.pictureEdit100.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl123
            // 
            this.labelControl123.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl123.Appearance.Options.UseFont = true;
            this.labelControl123.Location = new System.Drawing.Point(308, 395);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(12, 15);
            this.labelControl123.TabIndex = 218;
            this.labelControl123.Text = "86";
            // 
            // pictureEdit101
            // 
            this.pictureEdit101.AllowDrop = true;
            this.pictureEdit101.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit101.Location = new System.Drawing.Point(304, 367);
            this.pictureEdit101.Name = "pictureEdit101";
            this.pictureEdit101.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit101.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit101.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit101.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit101.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit101.Properties.UseParentBackground = true;
            this.pictureEdit101.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit101.TabIndex = 217;
            this.pictureEdit101.Tag = "Mic086";
            this.pictureEdit101.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl124
            // 
            this.labelControl124.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl124.Appearance.Options.UseFont = true;
            this.labelControl124.Location = new System.Drawing.Point(269, 396);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(12, 15);
            this.labelControl124.TabIndex = 216;
            this.labelControl124.Text = "85";
            // 
            // pictureEdit102
            // 
            this.pictureEdit102.AllowDrop = true;
            this.pictureEdit102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit102.Location = new System.Drawing.Point(265, 367);
            this.pictureEdit102.Name = "pictureEdit102";
            this.pictureEdit102.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit102.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit102.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit102.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit102.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit102.Properties.UseParentBackground = true;
            this.pictureEdit102.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit102.TabIndex = 215;
            this.pictureEdit102.Tag = "Mic085";
            this.pictureEdit102.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl125
            // 
            this.labelControl125.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl125.Appearance.Options.UseFont = true;
            this.labelControl125.Location = new System.Drawing.Point(230, 395);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(12, 15);
            this.labelControl125.TabIndex = 214;
            this.labelControl125.Text = "84";
            // 
            // pictureEdit103
            // 
            this.pictureEdit103.AllowDrop = true;
            this.pictureEdit103.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit103.Location = new System.Drawing.Point(226, 367);
            this.pictureEdit103.Name = "pictureEdit103";
            this.pictureEdit103.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit103.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit103.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit103.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit103.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit103.Properties.UseParentBackground = true;
            this.pictureEdit103.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit103.TabIndex = 213;
            this.pictureEdit103.Tag = "Mic084";
            this.pictureEdit103.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl126
            // 
            this.labelControl126.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl126.Appearance.Options.UseFont = true;
            this.labelControl126.Location = new System.Drawing.Point(190, 396);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(12, 15);
            this.labelControl126.TabIndex = 212;
            this.labelControl126.Text = "83";
            // 
            // pictureEdit104
            // 
            this.pictureEdit104.AllowDrop = true;
            this.pictureEdit104.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit104.Location = new System.Drawing.Point(187, 367);
            this.pictureEdit104.Name = "pictureEdit104";
            this.pictureEdit104.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit104.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit104.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit104.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit104.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit104.Properties.UseParentBackground = true;
            this.pictureEdit104.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit104.TabIndex = 211;
            this.pictureEdit104.Tag = "Mic083";
            this.pictureEdit104.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl127
            // 
            this.labelControl127.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl127.Appearance.Options.UseFont = true;
            this.labelControl127.Location = new System.Drawing.Point(152, 396);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(12, 15);
            this.labelControl127.TabIndex = 210;
            this.labelControl127.Text = "82";
            // 
            // pictureEdit105
            // 
            this.pictureEdit105.AllowDrop = true;
            this.pictureEdit105.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit105.Location = new System.Drawing.Point(148, 367);
            this.pictureEdit105.Name = "pictureEdit105";
            this.pictureEdit105.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit105.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit105.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit105.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit105.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit105.Properties.UseParentBackground = true;
            this.pictureEdit105.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit105.TabIndex = 209;
            this.pictureEdit105.Tag = "Mic082";
            this.pictureEdit105.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl128
            // 
            this.labelControl128.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl128.Appearance.Options.UseFont = true;
            this.labelControl128.Location = new System.Drawing.Point(113, 395);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(12, 15);
            this.labelControl128.TabIndex = 208;
            this.labelControl128.Text = "81";
            // 
            // pictureEdit106
            // 
            this.pictureEdit106.AllowDrop = true;
            this.pictureEdit106.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit106.Location = new System.Drawing.Point(109, 367);
            this.pictureEdit106.Name = "pictureEdit106";
            this.pictureEdit106.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit106.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit106.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit106.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit106.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit106.Properties.UseParentBackground = true;
            this.pictureEdit106.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit106.TabIndex = 207;
            this.pictureEdit106.Tag = "Mic081";
            this.pictureEdit106.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl129
            // 
            this.labelControl129.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl129.Appearance.Options.UseFont = true;
            this.labelControl129.Location = new System.Drawing.Point(718, 337);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(12, 15);
            this.labelControl129.TabIndex = 206;
            this.labelControl129.Text = "80";
            // 
            // pictureEdit107
            // 
            this.pictureEdit107.AllowDrop = true;
            this.pictureEdit107.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit107.Location = new System.Drawing.Point(714, 308);
            this.pictureEdit107.Name = "pictureEdit107";
            this.pictureEdit107.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit107.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit107.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit107.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit107.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit107.Properties.UseParentBackground = true;
            this.pictureEdit107.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit107.TabIndex = 205;
            this.pictureEdit107.Tag = "Mic080";
            this.pictureEdit107.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl130
            // 
            this.labelControl130.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl130.Appearance.Options.UseFont = true;
            this.labelControl130.Location = new System.Drawing.Point(678, 337);
            this.labelControl130.Name = "labelControl130";
            this.labelControl130.Size = new System.Drawing.Size(12, 15);
            this.labelControl130.TabIndex = 204;
            this.labelControl130.Text = "79";
            // 
            // pictureEdit108
            // 
            this.pictureEdit108.AllowDrop = true;
            this.pictureEdit108.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit108.Location = new System.Drawing.Point(675, 308);
            this.pictureEdit108.Name = "pictureEdit108";
            this.pictureEdit108.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit108.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit108.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit108.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit108.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit108.Properties.UseParentBackground = true;
            this.pictureEdit108.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit108.TabIndex = 203;
            this.pictureEdit108.Tag = "Mic079";
            this.pictureEdit108.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl131
            // 
            this.labelControl131.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl131.Appearance.Options.UseFont = true;
            this.labelControl131.Location = new System.Drawing.Point(640, 336);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(12, 15);
            this.labelControl131.TabIndex = 202;
            this.labelControl131.Text = "78";
            // 
            // pictureEdit109
            // 
            this.pictureEdit109.AllowDrop = true;
            this.pictureEdit109.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit109.Location = new System.Drawing.Point(636, 308);
            this.pictureEdit109.Name = "pictureEdit109";
            this.pictureEdit109.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit109.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit109.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit109.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit109.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit109.Properties.UseParentBackground = true;
            this.pictureEdit109.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit109.TabIndex = 201;
            this.pictureEdit109.Tag = "Mic078";
            this.pictureEdit109.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl132.Appearance.Options.UseFont = true;
            this.labelControl132.Location = new System.Drawing.Point(601, 337);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(12, 15);
            this.labelControl132.TabIndex = 200;
            this.labelControl132.Text = "77";
            // 
            // pictureEdit110
            // 
            this.pictureEdit110.AllowDrop = true;
            this.pictureEdit110.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit110.Location = new System.Drawing.Point(597, 308);
            this.pictureEdit110.Name = "pictureEdit110";
            this.pictureEdit110.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit110.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit110.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit110.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit110.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit110.Properties.UseParentBackground = true;
            this.pictureEdit110.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit110.TabIndex = 199;
            this.pictureEdit110.Tag = "Mic077";
            this.pictureEdit110.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl133
            // 
            this.labelControl133.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl133.Appearance.Options.UseFont = true;
            this.labelControl133.Location = new System.Drawing.Point(562, 336);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(12, 15);
            this.labelControl133.TabIndex = 198;
            this.labelControl133.Text = "76";
            // 
            // pictureEdit111
            // 
            this.pictureEdit111.AllowDrop = true;
            this.pictureEdit111.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit111.Location = new System.Drawing.Point(558, 308);
            this.pictureEdit111.Name = "pictureEdit111";
            this.pictureEdit111.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit111.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit111.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit111.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit111.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit111.Properties.UseParentBackground = true;
            this.pictureEdit111.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit111.TabIndex = 197;
            this.pictureEdit111.Tag = "Mic076";
            this.pictureEdit111.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl134
            // 
            this.labelControl134.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl134.Appearance.Options.UseFont = true;
            this.labelControl134.Location = new System.Drawing.Point(522, 337);
            this.labelControl134.Name = "labelControl134";
            this.labelControl134.Size = new System.Drawing.Size(12, 15);
            this.labelControl134.TabIndex = 196;
            this.labelControl134.Text = "75";
            // 
            // pictureEdit112
            // 
            this.pictureEdit112.AllowDrop = true;
            this.pictureEdit112.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit112.Location = new System.Drawing.Point(519, 308);
            this.pictureEdit112.Name = "pictureEdit112";
            this.pictureEdit112.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit112.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit112.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit112.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit112.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit112.Properties.UseParentBackground = true;
            this.pictureEdit112.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit112.TabIndex = 195;
            this.pictureEdit112.Tag = "Mic075";
            this.pictureEdit112.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl135
            // 
            this.labelControl135.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl135.Appearance.Options.UseFont = true;
            this.labelControl135.Location = new System.Drawing.Point(484, 337);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(12, 15);
            this.labelControl135.TabIndex = 194;
            this.labelControl135.Text = "74";
            // 
            // pictureEdit113
            // 
            this.pictureEdit113.AllowDrop = true;
            this.pictureEdit113.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit113.Location = new System.Drawing.Point(480, 308);
            this.pictureEdit113.Name = "pictureEdit113";
            this.pictureEdit113.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit113.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit113.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit113.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit113.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit113.Properties.UseParentBackground = true;
            this.pictureEdit113.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit113.TabIndex = 193;
            this.pictureEdit113.Tag = "Mic074";
            this.pictureEdit113.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl136
            // 
            this.labelControl136.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl136.Appearance.Options.UseFont = true;
            this.labelControl136.Location = new System.Drawing.Point(445, 336);
            this.labelControl136.Name = "labelControl136";
            this.labelControl136.Size = new System.Drawing.Size(12, 15);
            this.labelControl136.TabIndex = 192;
            this.labelControl136.Text = "73";
            // 
            // pictureEdit114
            // 
            this.pictureEdit114.AllowDrop = true;
            this.pictureEdit114.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit114.Location = new System.Drawing.Point(441, 308);
            this.pictureEdit114.Name = "pictureEdit114";
            this.pictureEdit114.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit114.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit114.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit114.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit114.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit114.Properties.UseParentBackground = true;
            this.pictureEdit114.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit114.TabIndex = 191;
            this.pictureEdit114.Tag = "Mic073";
            this.pictureEdit114.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl137
            // 
            this.labelControl137.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl137.Appearance.Options.UseFont = true;
            this.labelControl137.Location = new System.Drawing.Point(386, 337);
            this.labelControl137.Name = "labelControl137";
            this.labelControl137.Size = new System.Drawing.Size(12, 15);
            this.labelControl137.TabIndex = 190;
            this.labelControl137.Text = "72";
            // 
            // pictureEdit115
            // 
            this.pictureEdit115.AllowDrop = true;
            this.pictureEdit115.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit115.Location = new System.Drawing.Point(382, 308);
            this.pictureEdit115.Name = "pictureEdit115";
            this.pictureEdit115.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit115.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit115.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit115.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit115.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit115.Properties.UseParentBackground = true;
            this.pictureEdit115.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit115.TabIndex = 189;
            this.pictureEdit115.Tag = "Mic072";
            this.pictureEdit115.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl138
            // 
            this.labelControl138.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl138.Appearance.Options.UseFont = true;
            this.labelControl138.Location = new System.Drawing.Point(346, 337);
            this.labelControl138.Name = "labelControl138";
            this.labelControl138.Size = new System.Drawing.Size(12, 15);
            this.labelControl138.TabIndex = 188;
            this.labelControl138.Text = "71";
            // 
            // pictureEdit116
            // 
            this.pictureEdit116.AllowDrop = true;
            this.pictureEdit116.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit116.Location = new System.Drawing.Point(343, 308);
            this.pictureEdit116.Name = "pictureEdit116";
            this.pictureEdit116.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit116.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit116.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit116.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit116.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit116.Properties.UseParentBackground = true;
            this.pictureEdit116.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit116.TabIndex = 187;
            this.pictureEdit116.Tag = "Mic071";
            this.pictureEdit116.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl139
            // 
            this.labelControl139.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl139.Appearance.Options.UseFont = true;
            this.labelControl139.Location = new System.Drawing.Point(308, 336);
            this.labelControl139.Name = "labelControl139";
            this.labelControl139.Size = new System.Drawing.Size(12, 15);
            this.labelControl139.TabIndex = 186;
            this.labelControl139.Text = "70";
            // 
            // pictureEdit117
            // 
            this.pictureEdit117.AllowDrop = true;
            this.pictureEdit117.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit117.Location = new System.Drawing.Point(304, 308);
            this.pictureEdit117.Name = "pictureEdit117";
            this.pictureEdit117.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit117.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit117.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit117.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit117.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit117.Properties.UseParentBackground = true;
            this.pictureEdit117.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit117.TabIndex = 185;
            this.pictureEdit117.Tag = "Mic070";
            this.pictureEdit117.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl140
            // 
            this.labelControl140.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl140.Appearance.Options.UseFont = true;
            this.labelControl140.Location = new System.Drawing.Point(269, 337);
            this.labelControl140.Name = "labelControl140";
            this.labelControl140.Size = new System.Drawing.Size(12, 15);
            this.labelControl140.TabIndex = 184;
            this.labelControl140.Text = "69";
            // 
            // pictureEdit118
            // 
            this.pictureEdit118.AllowDrop = true;
            this.pictureEdit118.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit118.Location = new System.Drawing.Point(265, 308);
            this.pictureEdit118.Name = "pictureEdit118";
            this.pictureEdit118.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit118.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit118.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit118.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit118.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit118.Properties.UseParentBackground = true;
            this.pictureEdit118.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit118.TabIndex = 183;
            this.pictureEdit118.Tag = "Mic069";
            this.pictureEdit118.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl141
            // 
            this.labelControl141.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl141.Appearance.Options.UseFont = true;
            this.labelControl141.Location = new System.Drawing.Point(230, 336);
            this.labelControl141.Name = "labelControl141";
            this.labelControl141.Size = new System.Drawing.Size(12, 15);
            this.labelControl141.TabIndex = 182;
            this.labelControl141.Text = "68";
            // 
            // pictureEdit119
            // 
            this.pictureEdit119.AllowDrop = true;
            this.pictureEdit119.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit119.Location = new System.Drawing.Point(226, 308);
            this.pictureEdit119.Name = "pictureEdit119";
            this.pictureEdit119.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit119.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit119.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit119.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit119.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit119.Properties.UseParentBackground = true;
            this.pictureEdit119.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit119.TabIndex = 181;
            this.pictureEdit119.Tag = "Mic068";
            this.pictureEdit119.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl142
            // 
            this.labelControl142.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl142.Appearance.Options.UseFont = true;
            this.labelControl142.Location = new System.Drawing.Point(190, 337);
            this.labelControl142.Name = "labelControl142";
            this.labelControl142.Size = new System.Drawing.Size(12, 15);
            this.labelControl142.TabIndex = 180;
            this.labelControl142.Text = "67";
            // 
            // pictureEdit120
            // 
            this.pictureEdit120.AllowDrop = true;
            this.pictureEdit120.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit120.Location = new System.Drawing.Point(187, 308);
            this.pictureEdit120.Name = "pictureEdit120";
            this.pictureEdit120.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit120.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit120.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit120.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit120.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit120.Properties.UseParentBackground = true;
            this.pictureEdit120.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit120.TabIndex = 179;
            this.pictureEdit120.Tag = "Mic067";
            this.pictureEdit120.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl143
            // 
            this.labelControl143.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl143.Appearance.Options.UseFont = true;
            this.labelControl143.Location = new System.Drawing.Point(152, 337);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(12, 15);
            this.labelControl143.TabIndex = 178;
            this.labelControl143.Text = "66";
            // 
            // pictureEdit121
            // 
            this.pictureEdit121.AllowDrop = true;
            this.pictureEdit121.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit121.Location = new System.Drawing.Point(148, 308);
            this.pictureEdit121.Name = "pictureEdit121";
            this.pictureEdit121.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit121.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit121.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit121.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit121.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit121.Properties.UseParentBackground = true;
            this.pictureEdit121.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit121.TabIndex = 177;
            this.pictureEdit121.Tag = "Mic066";
            this.pictureEdit121.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl144
            // 
            this.labelControl144.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl144.Appearance.Options.UseFont = true;
            this.labelControl144.Location = new System.Drawing.Point(113, 336);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(12, 15);
            this.labelControl144.TabIndex = 176;
            this.labelControl144.Text = "65";
            // 
            // pictureEdit122
            // 
            this.pictureEdit122.AllowDrop = true;
            this.pictureEdit122.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit122.Location = new System.Drawing.Point(109, 308);
            this.pictureEdit122.Name = "pictureEdit122";
            this.pictureEdit122.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit122.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit122.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit122.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit122.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit122.Properties.UseParentBackground = true;
            this.pictureEdit122.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit122.TabIndex = 175;
            this.pictureEdit122.Tag = "Mic065";
            this.pictureEdit122.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl81.Appearance.Options.UseFont = true;
            this.labelControl81.Location = new System.Drawing.Point(718, 279);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(12, 15);
            this.labelControl81.TabIndex = 174;
            this.labelControl81.Text = "64";
            // 
            // pictureEdit59
            // 
            this.pictureEdit59.AllowDrop = true;
            this.pictureEdit59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit59.Location = new System.Drawing.Point(714, 250);
            this.pictureEdit59.Name = "pictureEdit59";
            this.pictureEdit59.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit59.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit59.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit59.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit59.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit59.Properties.UseParentBackground = true;
            this.pictureEdit59.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit59.TabIndex = 173;
            this.pictureEdit59.Tag = "Mic064";
            this.pictureEdit59.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl82.Appearance.Options.UseFont = true;
            this.labelControl82.Location = new System.Drawing.Point(678, 279);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(12, 15);
            this.labelControl82.TabIndex = 172;
            this.labelControl82.Text = "63";
            // 
            // pictureEdit60
            // 
            this.pictureEdit60.AllowDrop = true;
            this.pictureEdit60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit60.Location = new System.Drawing.Point(675, 250);
            this.pictureEdit60.Name = "pictureEdit60";
            this.pictureEdit60.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit60.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit60.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit60.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit60.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit60.Properties.UseParentBackground = true;
            this.pictureEdit60.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit60.TabIndex = 171;
            this.pictureEdit60.Tag = "Mic063";
            this.pictureEdit60.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl83.Appearance.Options.UseFont = true;
            this.labelControl83.Location = new System.Drawing.Point(640, 278);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(12, 15);
            this.labelControl83.TabIndex = 170;
            this.labelControl83.Text = "62";
            // 
            // pictureEdit61
            // 
            this.pictureEdit61.AllowDrop = true;
            this.pictureEdit61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit61.Location = new System.Drawing.Point(636, 250);
            this.pictureEdit61.Name = "pictureEdit61";
            this.pictureEdit61.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit61.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit61.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit61.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit61.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit61.Properties.UseParentBackground = true;
            this.pictureEdit61.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit61.TabIndex = 169;
            this.pictureEdit61.Tag = "Mic062";
            this.pictureEdit61.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl84.Appearance.Options.UseFont = true;
            this.labelControl84.Location = new System.Drawing.Point(601, 279);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(12, 15);
            this.labelControl84.TabIndex = 168;
            this.labelControl84.Text = "61";
            // 
            // pictureEdit62
            // 
            this.pictureEdit62.AllowDrop = true;
            this.pictureEdit62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit62.Location = new System.Drawing.Point(597, 250);
            this.pictureEdit62.Name = "pictureEdit62";
            this.pictureEdit62.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit62.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit62.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit62.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit62.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit62.Properties.UseParentBackground = true;
            this.pictureEdit62.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit62.TabIndex = 167;
            this.pictureEdit62.Tag = "Mic061";
            this.pictureEdit62.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl85.Appearance.Options.UseFont = true;
            this.labelControl85.Location = new System.Drawing.Point(562, 278);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(12, 15);
            this.labelControl85.TabIndex = 166;
            this.labelControl85.Text = "60";
            // 
            // pictureEdit63
            // 
            this.pictureEdit63.AllowDrop = true;
            this.pictureEdit63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit63.Location = new System.Drawing.Point(558, 250);
            this.pictureEdit63.Name = "pictureEdit63";
            this.pictureEdit63.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit63.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit63.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit63.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit63.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit63.Properties.UseParentBackground = true;
            this.pictureEdit63.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit63.TabIndex = 165;
            this.pictureEdit63.Tag = "Mic060";
            this.pictureEdit63.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl86.Appearance.Options.UseFont = true;
            this.labelControl86.Location = new System.Drawing.Point(522, 279);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(12, 15);
            this.labelControl86.TabIndex = 164;
            this.labelControl86.Text = "59";
            // 
            // pictureEdit64
            // 
            this.pictureEdit64.AllowDrop = true;
            this.pictureEdit64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit64.Location = new System.Drawing.Point(519, 250);
            this.pictureEdit64.Name = "pictureEdit64";
            this.pictureEdit64.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit64.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit64.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit64.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit64.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit64.Properties.UseParentBackground = true;
            this.pictureEdit64.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit64.TabIndex = 163;
            this.pictureEdit64.Tag = "Mic059";
            this.pictureEdit64.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl87
            // 
            this.labelControl87.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl87.Appearance.Options.UseFont = true;
            this.labelControl87.Location = new System.Drawing.Point(484, 279);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(12, 15);
            this.labelControl87.TabIndex = 162;
            this.labelControl87.Text = "58";
            // 
            // pictureEdit65
            // 
            this.pictureEdit65.AllowDrop = true;
            this.pictureEdit65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit65.Location = new System.Drawing.Point(480, 250);
            this.pictureEdit65.Name = "pictureEdit65";
            this.pictureEdit65.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit65.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit65.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit65.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit65.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit65.Properties.UseParentBackground = true;
            this.pictureEdit65.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit65.TabIndex = 161;
            this.pictureEdit65.Tag = "Mic058";
            this.pictureEdit65.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl88
            // 
            this.labelControl88.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl88.Appearance.Options.UseFont = true;
            this.labelControl88.Location = new System.Drawing.Point(445, 278);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(12, 15);
            this.labelControl88.TabIndex = 160;
            this.labelControl88.Text = "57";
            // 
            // pictureEdit66
            // 
            this.pictureEdit66.AllowDrop = true;
            this.pictureEdit66.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit66.Location = new System.Drawing.Point(441, 250);
            this.pictureEdit66.Name = "pictureEdit66";
            this.pictureEdit66.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit66.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit66.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit66.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit66.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit66.Properties.UseParentBackground = true;
            this.pictureEdit66.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit66.TabIndex = 159;
            this.pictureEdit66.Tag = "Mic057";
            this.pictureEdit66.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl89
            // 
            this.labelControl89.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl89.Appearance.Options.UseFont = true;
            this.labelControl89.Location = new System.Drawing.Point(386, 279);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(12, 15);
            this.labelControl89.TabIndex = 158;
            this.labelControl89.Text = "56";
            // 
            // pictureEdit67
            // 
            this.pictureEdit67.AllowDrop = true;
            this.pictureEdit67.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit67.Location = new System.Drawing.Point(382, 250);
            this.pictureEdit67.Name = "pictureEdit67";
            this.pictureEdit67.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit67.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit67.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit67.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit67.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit67.Properties.UseParentBackground = true;
            this.pictureEdit67.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit67.TabIndex = 157;
            this.pictureEdit67.Tag = "Mic056";
            this.pictureEdit67.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl90
            // 
            this.labelControl90.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl90.Appearance.Options.UseFont = true;
            this.labelControl90.Location = new System.Drawing.Point(346, 279);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(12, 15);
            this.labelControl90.TabIndex = 156;
            this.labelControl90.Text = "55";
            // 
            // pictureEdit68
            // 
            this.pictureEdit68.AllowDrop = true;
            this.pictureEdit68.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit68.Location = new System.Drawing.Point(343, 250);
            this.pictureEdit68.Name = "pictureEdit68";
            this.pictureEdit68.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit68.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit68.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit68.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit68.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit68.Properties.UseParentBackground = true;
            this.pictureEdit68.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit68.TabIndex = 155;
            this.pictureEdit68.Tag = "Mic055";
            this.pictureEdit68.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl91
            // 
            this.labelControl91.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl91.Appearance.Options.UseFont = true;
            this.labelControl91.Location = new System.Drawing.Point(308, 278);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(12, 15);
            this.labelControl91.TabIndex = 154;
            this.labelControl91.Text = "54";
            // 
            // pictureEdit69
            // 
            this.pictureEdit69.AllowDrop = true;
            this.pictureEdit69.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit69.Location = new System.Drawing.Point(304, 250);
            this.pictureEdit69.Name = "pictureEdit69";
            this.pictureEdit69.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit69.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit69.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit69.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit69.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit69.Properties.UseParentBackground = true;
            this.pictureEdit69.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit69.TabIndex = 153;
            this.pictureEdit69.Tag = "Mic054";
            this.pictureEdit69.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl92
            // 
            this.labelControl92.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl92.Appearance.Options.UseFont = true;
            this.labelControl92.Location = new System.Drawing.Point(269, 279);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(12, 15);
            this.labelControl92.TabIndex = 152;
            this.labelControl92.Text = "53";
            // 
            // pictureEdit70
            // 
            this.pictureEdit70.AllowDrop = true;
            this.pictureEdit70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit70.Location = new System.Drawing.Point(265, 250);
            this.pictureEdit70.Name = "pictureEdit70";
            this.pictureEdit70.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit70.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit70.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit70.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit70.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit70.Properties.UseParentBackground = true;
            this.pictureEdit70.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit70.TabIndex = 151;
            this.pictureEdit70.Tag = "Mic053";
            this.pictureEdit70.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl93
            // 
            this.labelControl93.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl93.Appearance.Options.UseFont = true;
            this.labelControl93.Location = new System.Drawing.Point(230, 278);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(12, 15);
            this.labelControl93.TabIndex = 150;
            this.labelControl93.Text = "52";
            // 
            // pictureEdit71
            // 
            this.pictureEdit71.AllowDrop = true;
            this.pictureEdit71.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit71.Location = new System.Drawing.Point(226, 250);
            this.pictureEdit71.Name = "pictureEdit71";
            this.pictureEdit71.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit71.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit71.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit71.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit71.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit71.Properties.UseParentBackground = true;
            this.pictureEdit71.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit71.TabIndex = 149;
            this.pictureEdit71.Tag = "Mic052";
            this.pictureEdit71.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl94
            // 
            this.labelControl94.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl94.Appearance.Options.UseFont = true;
            this.labelControl94.Location = new System.Drawing.Point(190, 279);
            this.labelControl94.Name = "labelControl94";
            this.labelControl94.Size = new System.Drawing.Size(12, 15);
            this.labelControl94.TabIndex = 148;
            this.labelControl94.Text = "51";
            // 
            // pictureEdit72
            // 
            this.pictureEdit72.AllowDrop = true;
            this.pictureEdit72.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit72.Location = new System.Drawing.Point(187, 250);
            this.pictureEdit72.Name = "pictureEdit72";
            this.pictureEdit72.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit72.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit72.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit72.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit72.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit72.Properties.UseParentBackground = true;
            this.pictureEdit72.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit72.TabIndex = 147;
            this.pictureEdit72.Tag = "Mic051";
            this.pictureEdit72.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl95
            // 
            this.labelControl95.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl95.Appearance.Options.UseFont = true;
            this.labelControl95.Location = new System.Drawing.Point(152, 279);
            this.labelControl95.Name = "labelControl95";
            this.labelControl95.Size = new System.Drawing.Size(12, 15);
            this.labelControl95.TabIndex = 146;
            this.labelControl95.Text = "50";
            // 
            // pictureEdit73
            // 
            this.pictureEdit73.AllowDrop = true;
            this.pictureEdit73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit73.Location = new System.Drawing.Point(148, 250);
            this.pictureEdit73.Name = "pictureEdit73";
            this.pictureEdit73.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit73.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit73.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit73.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit73.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit73.Properties.UseParentBackground = true;
            this.pictureEdit73.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit73.TabIndex = 145;
            this.pictureEdit73.Tag = "Mic050";
            this.pictureEdit73.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl96
            // 
            this.labelControl96.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl96.Appearance.Options.UseFont = true;
            this.labelControl96.Location = new System.Drawing.Point(113, 278);
            this.labelControl96.Name = "labelControl96";
            this.labelControl96.Size = new System.Drawing.Size(12, 15);
            this.labelControl96.TabIndex = 144;
            this.labelControl96.Text = "49";
            // 
            // pictureEdit74
            // 
            this.pictureEdit74.AllowDrop = true;
            this.pictureEdit74.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit74.Location = new System.Drawing.Point(109, 250);
            this.pictureEdit74.Name = "pictureEdit74";
            this.pictureEdit74.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit74.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit74.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit74.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit74.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit74.Properties.UseParentBackground = true;
            this.pictureEdit74.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit74.TabIndex = 143;
            this.pictureEdit74.Tag = "Mic049";
            this.pictureEdit74.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl65.Appearance.Options.UseFont = true;
            this.labelControl65.Location = new System.Drawing.Point(718, 215);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(12, 15);
            this.labelControl65.TabIndex = 142;
            this.labelControl65.Text = "48";
            // 
            // pictureEdit43
            // 
            this.pictureEdit43.AllowDrop = true;
            this.pictureEdit43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit43.Location = new System.Drawing.Point(714, 186);
            this.pictureEdit43.Name = "pictureEdit43";
            this.pictureEdit43.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit43.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit43.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit43.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit43.Properties.UseParentBackground = true;
            this.pictureEdit43.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit43.TabIndex = 141;
            this.pictureEdit43.Tag = "Mic048";
            this.pictureEdit43.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl66.Appearance.Options.UseFont = true;
            this.labelControl66.Location = new System.Drawing.Point(678, 215);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(12, 15);
            this.labelControl66.TabIndex = 140;
            this.labelControl66.Text = "47";
            // 
            // pictureEdit44
            // 
            this.pictureEdit44.AllowDrop = true;
            this.pictureEdit44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit44.Location = new System.Drawing.Point(675, 186);
            this.pictureEdit44.Name = "pictureEdit44";
            this.pictureEdit44.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit44.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit44.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit44.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit44.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit44.Properties.UseParentBackground = true;
            this.pictureEdit44.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit44.TabIndex = 139;
            this.pictureEdit44.Tag = "Mic047";
            this.pictureEdit44.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl67.Appearance.Options.UseFont = true;
            this.labelControl67.Location = new System.Drawing.Point(640, 214);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(12, 15);
            this.labelControl67.TabIndex = 138;
            this.labelControl67.Text = "46";
            // 
            // pictureEdit45
            // 
            this.pictureEdit45.AllowDrop = true;
            this.pictureEdit45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit45.Location = new System.Drawing.Point(636, 186);
            this.pictureEdit45.Name = "pictureEdit45";
            this.pictureEdit45.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit45.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit45.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit45.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit45.Properties.UseParentBackground = true;
            this.pictureEdit45.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit45.TabIndex = 137;
            this.pictureEdit45.Tag = "Mic046";
            this.pictureEdit45.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl68.Appearance.Options.UseFont = true;
            this.labelControl68.Location = new System.Drawing.Point(601, 215);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(12, 15);
            this.labelControl68.TabIndex = 136;
            this.labelControl68.Text = "45";
            // 
            // pictureEdit46
            // 
            this.pictureEdit46.AllowDrop = true;
            this.pictureEdit46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit46.Location = new System.Drawing.Point(597, 186);
            this.pictureEdit46.Name = "pictureEdit46";
            this.pictureEdit46.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit46.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit46.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit46.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit46.Properties.UseParentBackground = true;
            this.pictureEdit46.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit46.TabIndex = 135;
            this.pictureEdit46.Tag = "Mic045";
            this.pictureEdit46.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl69.Appearance.Options.UseFont = true;
            this.labelControl69.Location = new System.Drawing.Point(562, 214);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(12, 15);
            this.labelControl69.TabIndex = 134;
            this.labelControl69.Text = "44";
            // 
            // pictureEdit47
            // 
            this.pictureEdit47.AllowDrop = true;
            this.pictureEdit47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit47.Location = new System.Drawing.Point(558, 186);
            this.pictureEdit47.Name = "pictureEdit47";
            this.pictureEdit47.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit47.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit47.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit47.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit47.Properties.UseParentBackground = true;
            this.pictureEdit47.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit47.TabIndex = 133;
            this.pictureEdit47.Tag = "Mic044";
            this.pictureEdit47.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl70
            // 
            this.labelControl70.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl70.Appearance.Options.UseFont = true;
            this.labelControl70.Location = new System.Drawing.Point(522, 215);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(12, 15);
            this.labelControl70.TabIndex = 132;
            this.labelControl70.Text = "43";
            // 
            // pictureEdit48
            // 
            this.pictureEdit48.AllowDrop = true;
            this.pictureEdit48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit48.Location = new System.Drawing.Point(519, 186);
            this.pictureEdit48.Name = "pictureEdit48";
            this.pictureEdit48.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit48.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit48.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit48.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit48.Properties.UseParentBackground = true;
            this.pictureEdit48.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit48.TabIndex = 131;
            this.pictureEdit48.Tag = "Mic043";
            this.pictureEdit48.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl71
            // 
            this.labelControl71.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl71.Appearance.Options.UseFont = true;
            this.labelControl71.Location = new System.Drawing.Point(484, 215);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(12, 15);
            this.labelControl71.TabIndex = 130;
            this.labelControl71.Text = "42";
            // 
            // pictureEdit49
            // 
            this.pictureEdit49.AllowDrop = true;
            this.pictureEdit49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit49.Location = new System.Drawing.Point(480, 186);
            this.pictureEdit49.Name = "pictureEdit49";
            this.pictureEdit49.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit49.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit49.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit49.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit49.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit49.Properties.UseParentBackground = true;
            this.pictureEdit49.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit49.TabIndex = 129;
            this.pictureEdit49.Tag = "Mic042";
            this.pictureEdit49.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl72.Appearance.Options.UseFont = true;
            this.labelControl72.Location = new System.Drawing.Point(445, 214);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(12, 15);
            this.labelControl72.TabIndex = 128;
            this.labelControl72.Text = "41";
            // 
            // pictureEdit50
            // 
            this.pictureEdit50.AllowDrop = true;
            this.pictureEdit50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit50.Location = new System.Drawing.Point(441, 186);
            this.pictureEdit50.Name = "pictureEdit50";
            this.pictureEdit50.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit50.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit50.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit50.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit50.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit50.Properties.UseParentBackground = true;
            this.pictureEdit50.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit50.TabIndex = 127;
            this.pictureEdit50.Tag = "Mic041";
            this.pictureEdit50.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl73.Appearance.Options.UseFont = true;
            this.labelControl73.Location = new System.Drawing.Point(386, 215);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(12, 15);
            this.labelControl73.TabIndex = 126;
            this.labelControl73.Text = "40";
            // 
            // pictureEdit51
            // 
            this.pictureEdit51.AllowDrop = true;
            this.pictureEdit51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit51.Location = new System.Drawing.Point(382, 186);
            this.pictureEdit51.Name = "pictureEdit51";
            this.pictureEdit51.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit51.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit51.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit51.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit51.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit51.Properties.UseParentBackground = true;
            this.pictureEdit51.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit51.TabIndex = 125;
            this.pictureEdit51.Tag = "Mic040";
            this.pictureEdit51.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl74.Appearance.Options.UseFont = true;
            this.labelControl74.Location = new System.Drawing.Point(346, 215);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(12, 15);
            this.labelControl74.TabIndex = 124;
            this.labelControl74.Text = "39";
            // 
            // pictureEdit52
            // 
            this.pictureEdit52.AllowDrop = true;
            this.pictureEdit52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit52.Location = new System.Drawing.Point(343, 186);
            this.pictureEdit52.Name = "pictureEdit52";
            this.pictureEdit52.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit52.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit52.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit52.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit52.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit52.Properties.UseParentBackground = true;
            this.pictureEdit52.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit52.TabIndex = 123;
            this.pictureEdit52.Tag = "Mic039";
            this.pictureEdit52.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl75
            // 
            this.labelControl75.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl75.Appearance.Options.UseFont = true;
            this.labelControl75.Location = new System.Drawing.Point(308, 214);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(12, 15);
            this.labelControl75.TabIndex = 122;
            this.labelControl75.Text = "38";
            // 
            // pictureEdit53
            // 
            this.pictureEdit53.AllowDrop = true;
            this.pictureEdit53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit53.Location = new System.Drawing.Point(304, 186);
            this.pictureEdit53.Name = "pictureEdit53";
            this.pictureEdit53.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit53.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit53.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit53.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit53.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit53.Properties.UseParentBackground = true;
            this.pictureEdit53.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit53.TabIndex = 121;
            this.pictureEdit53.Tag = "Mic038";
            this.pictureEdit53.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl76
            // 
            this.labelControl76.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl76.Appearance.Options.UseFont = true;
            this.labelControl76.Location = new System.Drawing.Point(269, 215);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(12, 15);
            this.labelControl76.TabIndex = 120;
            this.labelControl76.Text = "37";
            // 
            // pictureEdit54
            // 
            this.pictureEdit54.AllowDrop = true;
            this.pictureEdit54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit54.Location = new System.Drawing.Point(265, 186);
            this.pictureEdit54.Name = "pictureEdit54";
            this.pictureEdit54.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit54.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit54.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit54.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit54.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit54.Properties.UseParentBackground = true;
            this.pictureEdit54.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit54.TabIndex = 119;
            this.pictureEdit54.Tag = "Mic037";
            this.pictureEdit54.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl77
            // 
            this.labelControl77.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl77.Appearance.Options.UseFont = true;
            this.labelControl77.Location = new System.Drawing.Point(230, 214);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(12, 15);
            this.labelControl77.TabIndex = 118;
            this.labelControl77.Text = "36";
            // 
            // pictureEdit55
            // 
            this.pictureEdit55.AllowDrop = true;
            this.pictureEdit55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit55.Location = new System.Drawing.Point(226, 186);
            this.pictureEdit55.Name = "pictureEdit55";
            this.pictureEdit55.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit55.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit55.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit55.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit55.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit55.Properties.UseParentBackground = true;
            this.pictureEdit55.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit55.TabIndex = 117;
            this.pictureEdit55.Tag = "Mic036";
            this.pictureEdit55.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl78
            // 
            this.labelControl78.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl78.Appearance.Options.UseFont = true;
            this.labelControl78.Location = new System.Drawing.Point(190, 215);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(12, 15);
            this.labelControl78.TabIndex = 116;
            this.labelControl78.Text = "35";
            // 
            // pictureEdit56
            // 
            this.pictureEdit56.AllowDrop = true;
            this.pictureEdit56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit56.Location = new System.Drawing.Point(187, 186);
            this.pictureEdit56.Name = "pictureEdit56";
            this.pictureEdit56.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit56.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit56.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit56.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit56.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit56.Properties.UseParentBackground = true;
            this.pictureEdit56.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit56.TabIndex = 115;
            this.pictureEdit56.Tag = "Mic035";
            this.pictureEdit56.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl79
            // 
            this.labelControl79.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl79.Appearance.Options.UseFont = true;
            this.labelControl79.Location = new System.Drawing.Point(152, 215);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(12, 15);
            this.labelControl79.TabIndex = 114;
            this.labelControl79.Text = "34";
            // 
            // pictureEdit57
            // 
            this.pictureEdit57.AllowDrop = true;
            this.pictureEdit57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit57.Location = new System.Drawing.Point(148, 186);
            this.pictureEdit57.Name = "pictureEdit57";
            this.pictureEdit57.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit57.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit57.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit57.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit57.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit57.Properties.UseParentBackground = true;
            this.pictureEdit57.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit57.TabIndex = 113;
            this.pictureEdit57.Tag = "Mic034";
            this.pictureEdit57.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl80.Appearance.Options.UseFont = true;
            this.labelControl80.Location = new System.Drawing.Point(113, 214);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(12, 15);
            this.labelControl80.TabIndex = 112;
            this.labelControl80.Text = "33";
            // 
            // pictureEdit58
            // 
            this.pictureEdit58.AllowDrop = true;
            this.pictureEdit58.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit58.Location = new System.Drawing.Point(109, 186);
            this.pictureEdit58.Name = "pictureEdit58";
            this.pictureEdit58.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit58.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit58.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit58.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit58.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit58.Properties.UseParentBackground = true;
            this.pictureEdit58.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit58.TabIndex = 111;
            this.pictureEdit58.Tag = "Mic033";
            this.pictureEdit58.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.Location = new System.Drawing.Point(718, 156);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(12, 15);
            this.labelControl49.TabIndex = 110;
            this.labelControl49.Text = "32";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(725, 64);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(12, 15);
            this.labelControl41.TabIndex = 78;
            this.labelControl41.Text = "16";
            // 
            // pictureEdit27
            // 
            this.pictureEdit27.AllowDrop = true;
            this.pictureEdit27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit27.Location = new System.Drawing.Point(714, 127);
            this.pictureEdit27.Name = "pictureEdit27";
            this.pictureEdit27.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit27.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit27.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit27.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit27.Properties.UseParentBackground = true;
            this.pictureEdit27.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit27.TabIndex = 109;
            this.pictureEdit27.Tag = "Mic032";
            this.pictureEdit27.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit19
            // 
            this.pictureEdit19.AllowDrop = true;
            this.pictureEdit19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit19.Location = new System.Drawing.Point(714, 35);
            this.pictureEdit19.Name = "pictureEdit19";
            this.pictureEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit19.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit19.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit19.Properties.UseParentBackground = true;
            this.pictureEdit19.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit19.TabIndex = 77;
            this.pictureEdit19.Tag = "Mic016";
            this.pictureEdit19.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.Location = new System.Drawing.Point(678, 156);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(12, 15);
            this.labelControl50.TabIndex = 108;
            this.labelControl50.Text = "31";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(685, 64);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(12, 15);
            this.labelControl42.TabIndex = 76;
            this.labelControl42.Text = "15";
            // 
            // pictureEdit28
            // 
            this.pictureEdit28.AllowDrop = true;
            this.pictureEdit28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit28.Location = new System.Drawing.Point(675, 127);
            this.pictureEdit28.Name = "pictureEdit28";
            this.pictureEdit28.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit28.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit28.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit28.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit28.Properties.UseParentBackground = true;
            this.pictureEdit28.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit28.TabIndex = 107;
            this.pictureEdit28.Tag = "Mic031";
            this.pictureEdit28.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit20
            // 
            this.pictureEdit20.AllowDrop = true;
            this.pictureEdit20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit20.Location = new System.Drawing.Point(675, 35);
            this.pictureEdit20.Name = "pictureEdit20";
            this.pictureEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit20.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit20.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit20.Properties.UseParentBackground = true;
            this.pictureEdit20.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit20.TabIndex = 75;
            this.pictureEdit20.Tag = "Mic015";
            this.pictureEdit20.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.Location = new System.Drawing.Point(640, 155);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(12, 15);
            this.labelControl51.TabIndex = 106;
            this.labelControl51.Text = "30";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.Location = new System.Drawing.Point(647, 63);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(12, 15);
            this.labelControl43.TabIndex = 74;
            this.labelControl43.Text = "14";
            // 
            // pictureEdit29
            // 
            this.pictureEdit29.AllowDrop = true;
            this.pictureEdit29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit29.Location = new System.Drawing.Point(636, 127);
            this.pictureEdit29.Name = "pictureEdit29";
            this.pictureEdit29.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit29.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit29.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit29.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit29.Properties.UseParentBackground = true;
            this.pictureEdit29.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit29.TabIndex = 105;
            this.pictureEdit29.Tag = "Mic030";
            this.pictureEdit29.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit21
            // 
            this.pictureEdit21.AllowDrop = true;
            this.pictureEdit21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit21.Location = new System.Drawing.Point(636, 35);
            this.pictureEdit21.Name = "pictureEdit21";
            this.pictureEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit21.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit21.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit21.Properties.UseParentBackground = true;
            this.pictureEdit21.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit21.TabIndex = 73;
            this.pictureEdit21.Tag = "Mic014";
            this.pictureEdit21.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.Location = new System.Drawing.Point(601, 156);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(12, 15);
            this.labelControl52.TabIndex = 104;
            this.labelControl52.Text = "29";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.Location = new System.Drawing.Point(608, 64);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(12, 15);
            this.labelControl44.TabIndex = 72;
            this.labelControl44.Text = "13";
            // 
            // pictureEdit30
            // 
            this.pictureEdit30.AllowDrop = true;
            this.pictureEdit30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit30.Location = new System.Drawing.Point(597, 127);
            this.pictureEdit30.Name = "pictureEdit30";
            this.pictureEdit30.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit30.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit30.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit30.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit30.Properties.UseParentBackground = true;
            this.pictureEdit30.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit30.TabIndex = 103;
            this.pictureEdit30.Tag = "Mic029";
            this.pictureEdit30.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit22
            // 
            this.pictureEdit22.AllowDrop = true;
            this.pictureEdit22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit22.Location = new System.Drawing.Point(597, 35);
            this.pictureEdit22.Name = "pictureEdit22";
            this.pictureEdit22.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit22.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit22.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit22.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit22.Properties.UseParentBackground = true;
            this.pictureEdit22.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit22.TabIndex = 71;
            this.pictureEdit22.Tag = "Mic013";
            this.pictureEdit22.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.Location = new System.Drawing.Point(562, 155);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(12, 15);
            this.labelControl53.TabIndex = 102;
            this.labelControl53.Text = "28";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(569, 63);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(12, 15);
            this.labelControl45.TabIndex = 70;
            this.labelControl45.Text = "12";
            // 
            // pictureEdit31
            // 
            this.pictureEdit31.AllowDrop = true;
            this.pictureEdit31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit31.Location = new System.Drawing.Point(558, 127);
            this.pictureEdit31.Name = "pictureEdit31";
            this.pictureEdit31.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit31.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit31.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit31.Properties.UseParentBackground = true;
            this.pictureEdit31.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit31.TabIndex = 101;
            this.pictureEdit31.Tag = "Mic028";
            this.pictureEdit31.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit23
            // 
            this.pictureEdit23.AllowDrop = true;
            this.pictureEdit23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit23.Location = new System.Drawing.Point(558, 35);
            this.pictureEdit23.Name = "pictureEdit23";
            this.pictureEdit23.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit23.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit23.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit23.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit23.Properties.UseParentBackground = true;
            this.pictureEdit23.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit23.TabIndex = 69;
            this.pictureEdit23.Tag = "Mic012";
            this.pictureEdit23.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.Location = new System.Drawing.Point(522, 156);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(12, 15);
            this.labelControl54.TabIndex = 100;
            this.labelControl54.Text = "27";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.Location = new System.Drawing.Point(529, 64);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(12, 15);
            this.labelControl46.TabIndex = 68;
            this.labelControl46.Text = "11";
            // 
            // pictureEdit32
            // 
            this.pictureEdit32.AllowDrop = true;
            this.pictureEdit32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit32.Location = new System.Drawing.Point(519, 127);
            this.pictureEdit32.Name = "pictureEdit32";
            this.pictureEdit32.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit32.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit32.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit32.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit32.Properties.UseParentBackground = true;
            this.pictureEdit32.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit32.TabIndex = 99;
            this.pictureEdit32.Tag = "Mic027";
            this.pictureEdit32.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit24
            // 
            this.pictureEdit24.AllowDrop = true;
            this.pictureEdit24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit24.Location = new System.Drawing.Point(519, 35);
            this.pictureEdit24.Name = "pictureEdit24";
            this.pictureEdit24.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit24.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit24.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit24.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit24.Properties.UseParentBackground = true;
            this.pictureEdit24.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit24.TabIndex = 67;
            this.pictureEdit24.Tag = "Mic011";
            this.pictureEdit24.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.Location = new System.Drawing.Point(484, 156);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(12, 15);
            this.labelControl55.TabIndex = 98;
            this.labelControl55.Text = "26";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.Location = new System.Drawing.Point(491, 64);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(12, 15);
            this.labelControl47.TabIndex = 66;
            this.labelControl47.Text = "10";
            // 
            // pictureEdit33
            // 
            this.pictureEdit33.AllowDrop = true;
            this.pictureEdit33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit33.Location = new System.Drawing.Point(480, 127);
            this.pictureEdit33.Name = "pictureEdit33";
            this.pictureEdit33.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit33.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit33.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit33.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit33.Properties.UseParentBackground = true;
            this.pictureEdit33.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit33.TabIndex = 97;
            this.pictureEdit33.Tag = "Mic026";
            this.pictureEdit33.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit25
            // 
            this.pictureEdit25.AllowDrop = true;
            this.pictureEdit25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit25.Location = new System.Drawing.Point(480, 35);
            this.pictureEdit25.Name = "pictureEdit25";
            this.pictureEdit25.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit25.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit25.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit25.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit25.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit25.Properties.UseParentBackground = true;
            this.pictureEdit25.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit25.TabIndex = 65;
            this.pictureEdit25.Tag = "Mic010";
            this.pictureEdit25.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.Location = new System.Drawing.Point(445, 155);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(12, 15);
            this.labelControl56.TabIndex = 96;
            this.labelControl56.Text = "25";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.Location = new System.Drawing.Point(452, 63);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(6, 15);
            this.labelControl48.TabIndex = 64;
            this.labelControl48.Text = "9";
            // 
            // pictureEdit34
            // 
            this.pictureEdit34.AllowDrop = true;
            this.pictureEdit34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit34.Location = new System.Drawing.Point(441, 127);
            this.pictureEdit34.Name = "pictureEdit34";
            this.pictureEdit34.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit34.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit34.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit34.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit34.Properties.UseParentBackground = true;
            this.pictureEdit34.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit34.TabIndex = 95;
            this.pictureEdit34.Tag = "Mic025";
            this.pictureEdit34.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit26
            // 
            this.pictureEdit26.AllowDrop = true;
            this.pictureEdit26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit26.Location = new System.Drawing.Point(441, 35);
            this.pictureEdit26.Name = "pictureEdit26";
            this.pictureEdit26.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit26.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit26.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit26.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit26.Properties.UseParentBackground = true;
            this.pictureEdit26.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit26.TabIndex = 63;
            this.pictureEdit26.Tag = "Mic009";
            this.pictureEdit26.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.Location = new System.Drawing.Point(386, 156);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(12, 15);
            this.labelControl57.TabIndex = 94;
            this.labelControl57.Text = "24";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(393, 64);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(6, 15);
            this.labelControl8.TabIndex = 62;
            this.labelControl8.Text = "8";
            // 
            // pictureEdit35
            // 
            this.pictureEdit35.AllowDrop = true;
            this.pictureEdit35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit35.Location = new System.Drawing.Point(382, 127);
            this.pictureEdit35.Name = "pictureEdit35";
            this.pictureEdit35.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit35.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit35.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit35.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit35.Properties.UseParentBackground = true;
            this.pictureEdit35.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit35.TabIndex = 93;
            this.pictureEdit35.Tag = "Mic024";
            this.pictureEdit35.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.AllowDrop = true;
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit1.Location = new System.Drawing.Point(382, 35);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Properties.UseParentBackground = true;
            this.pictureEdit1.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit1.TabIndex = 61;
            this.pictureEdit1.Tag = "Mic008";
            this.pictureEdit1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.Location = new System.Drawing.Point(346, 156);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(12, 15);
            this.labelControl58.TabIndex = 92;
            this.labelControl58.Text = "23";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(353, 64);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 15);
            this.labelControl9.TabIndex = 60;
            this.labelControl9.Text = "7";
            // 
            // pictureEdit36
            // 
            this.pictureEdit36.AllowDrop = true;
            this.pictureEdit36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit36.Location = new System.Drawing.Point(343, 127);
            this.pictureEdit36.Name = "pictureEdit36";
            this.pictureEdit36.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit36.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit36.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit36.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit36.Properties.UseParentBackground = true;
            this.pictureEdit36.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit36.TabIndex = 91;
            this.pictureEdit36.Tag = "Mic023";
            this.pictureEdit36.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.AllowDrop = true;
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit2.Location = new System.Drawing.Point(343, 35);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Properties.UseParentBackground = true;
            this.pictureEdit2.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit2.TabIndex = 59;
            this.pictureEdit2.Tag = "Mic007";
            this.pictureEdit2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.Location = new System.Drawing.Point(308, 155);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(12, 15);
            this.labelControl59.TabIndex = 90;
            this.labelControl59.Text = "22";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(315, 63);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 15);
            this.labelControl10.TabIndex = 58;
            this.labelControl10.Text = "6";
            // 
            // pictureEdit37
            // 
            this.pictureEdit37.AllowDrop = true;
            this.pictureEdit37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit37.Location = new System.Drawing.Point(304, 127);
            this.pictureEdit37.Name = "pictureEdit37";
            this.pictureEdit37.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit37.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit37.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit37.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit37.Properties.UseParentBackground = true;
            this.pictureEdit37.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit37.TabIndex = 89;
            this.pictureEdit37.Tag = "Mic022";
            this.pictureEdit37.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.AllowDrop = true;
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit3.Location = new System.Drawing.Point(304, 35);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit3.Properties.UseParentBackground = true;
            this.pictureEdit3.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit3.TabIndex = 57;
            this.pictureEdit3.Tag = "Mic006";
            this.pictureEdit3.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl60.Appearance.Options.UseFont = true;
            this.labelControl60.Location = new System.Drawing.Point(269, 156);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(12, 15);
            this.labelControl60.TabIndex = 88;
            this.labelControl60.Text = "21";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(276, 64);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 15);
            this.labelControl11.TabIndex = 56;
            this.labelControl11.Text = "5";
            // 
            // pictureEdit38
            // 
            this.pictureEdit38.AllowDrop = true;
            this.pictureEdit38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit38.Location = new System.Drawing.Point(265, 127);
            this.pictureEdit38.Name = "pictureEdit38";
            this.pictureEdit38.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit38.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit38.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit38.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit38.Properties.UseParentBackground = true;
            this.pictureEdit38.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit38.TabIndex = 87;
            this.pictureEdit38.Tag = "Mic021";
            this.pictureEdit38.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.AllowDrop = true;
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit4.Location = new System.Drawing.Point(265, 35);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit4.Properties.UseParentBackground = true;
            this.pictureEdit4.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit4.TabIndex = 55;
            this.pictureEdit4.Tag = "Mic005";
            this.pictureEdit4.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.Location = new System.Drawing.Point(230, 155);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(12, 15);
            this.labelControl61.TabIndex = 86;
            this.labelControl61.Text = "20";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(237, 63);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 15);
            this.labelControl12.TabIndex = 54;
            this.labelControl12.Text = "4";
            // 
            // pictureEdit39
            // 
            this.pictureEdit39.AllowDrop = true;
            this.pictureEdit39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit39.Location = new System.Drawing.Point(226, 127);
            this.pictureEdit39.Name = "pictureEdit39";
            this.pictureEdit39.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit39.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit39.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit39.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit39.Properties.UseParentBackground = true;
            this.pictureEdit39.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit39.TabIndex = 85;
            this.pictureEdit39.Tag = "Mic020";
            this.pictureEdit39.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.AllowDrop = true;
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit5.Location = new System.Drawing.Point(226, 35);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit5.Properties.UseParentBackground = true;
            this.pictureEdit5.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit5.TabIndex = 53;
            this.pictureEdit5.Tag = "Mic004";
            this.pictureEdit5.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl62.Appearance.Options.UseFont = true;
            this.labelControl62.Location = new System.Drawing.Point(190, 156);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(12, 15);
            this.labelControl62.TabIndex = 84;
            this.labelControl62.Text = "19";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(197, 64);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 15);
            this.labelControl13.TabIndex = 52;
            this.labelControl13.Text = "3";
            // 
            // pictureEdit40
            // 
            this.pictureEdit40.AllowDrop = true;
            this.pictureEdit40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit40.Location = new System.Drawing.Point(187, 127);
            this.pictureEdit40.Name = "pictureEdit40";
            this.pictureEdit40.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit40.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit40.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit40.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit40.Properties.UseParentBackground = true;
            this.pictureEdit40.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit40.TabIndex = 83;
            this.pictureEdit40.Tag = "Mic019";
            this.pictureEdit40.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.AllowDrop = true;
            this.pictureEdit6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit6.Location = new System.Drawing.Point(187, 35);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit6.Properties.UseParentBackground = true;
            this.pictureEdit6.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit6.TabIndex = 51;
            this.pictureEdit6.Tag = "Mic003";
            this.pictureEdit6.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.Location = new System.Drawing.Point(152, 156);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(12, 15);
            this.labelControl63.TabIndex = 82;
            this.labelControl63.Text = "18";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(154, 64);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 15);
            this.labelControl14.TabIndex = 50;
            this.labelControl14.Text = "2";
            // 
            // pictureEdit41
            // 
            this.pictureEdit41.AllowDrop = true;
            this.pictureEdit41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit41.Location = new System.Drawing.Point(148, 127);
            this.pictureEdit41.Name = "pictureEdit41";
            this.pictureEdit41.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit41.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit41.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit41.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit41.Properties.UseParentBackground = true;
            this.pictureEdit41.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit41.TabIndex = 81;
            this.pictureEdit41.Tag = "Mic018";
            this.pictureEdit41.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // Mic_2
            // 
            this.Mic_2.AllowDrop = true;
            this.Mic_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_2.Location = new System.Drawing.Point(148, 35);
            this.Mic_2.Name = "Mic_2";
            this.Mic_2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_2.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_2.Properties.UseParentBackground = true;
            this.Mic_2.Size = new System.Drawing.Size(28, 32);
            this.Mic_2.TabIndex = 49;
            this.Mic_2.Tag = "Mic002";
            this.Mic_2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.Location = new System.Drawing.Point(113, 155);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(12, 15);
            this.labelControl64.TabIndex = 80;
            this.labelControl64.Text = "17";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(120, 63);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 15);
            this.labelControl15.TabIndex = 48;
            this.labelControl15.Text = "1";
            // 
            // pictureEdit42
            // 
            this.pictureEdit42.AllowDrop = true;
            this.pictureEdit42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit42.Location = new System.Drawing.Point(109, 127);
            this.pictureEdit42.Name = "pictureEdit42";
            this.pictureEdit42.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit42.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit42.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit42.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit42.Properties.UseParentBackground = true;
            this.pictureEdit42.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit42.TabIndex = 79;
            this.pictureEdit42.Tag = "Mic017";
            this.pictureEdit42.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // Mic_1
            // 
            this.Mic_1.AllowDrop = true;
            this.Mic_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_1.EditValue = global::VoteSystem.Properties.Resources.User;
            this.Mic_1.Location = new System.Drawing.Point(109, 35);
            this.Mic_1.Name = "Mic_1";
            this.Mic_1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_1.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_1.Properties.UseParentBackground = true;
            this.Mic_1.Size = new System.Drawing.Size(28, 32);
            this.Mic_1.TabIndex = 47;
            this.Mic_1.Tag = "Mic001";
            this.Mic_1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // grpVoteResults
            // 
            this.grpVoteResults.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.grpVoteResults.Controls.Add(this.lblVoteResTime);
            this.grpVoteResults.Controls.Add(this.labelControl16);
            this.grpVoteResults.Controls.Add(this.lblVoteResDate);
            this.grpVoteResults.Controls.Add(this.labelControl22);
            this.grpVoteResults.Controls.Add(this.grpVoteResultsTable);
            this.grpVoteResults.Controls.Add(this.lblDecision);
            this.grpVoteResults.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpVoteResults.Location = new System.Drawing.Point(0, 445);
            this.grpVoteResults.Name = "grpVoteResults";
            this.grpVoteResults.Size = new System.Drawing.Size(348, 244);
            this.grpVoteResults.TabIndex = 11;
            this.grpVoteResults.Text = "����� �����������";
            // 
            // lblVoteResTime
            // 
            this.lblVoteResTime.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteResTime.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteResTime.Location = new System.Drawing.Point(236, 41);
            this.lblVoteResTime.Name = "lblVoteResTime";
            this.lblVoteResTime.Size = new System.Drawing.Size(67, 13);
            this.lblVoteResTime.TabIndex = 229;
            this.lblVoteResTime.Text = "16:30";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(193, 40);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(34, 13);
            this.labelControl16.TabIndex = 228;
            this.labelControl16.Text = "�����:";
            // 
            // lblVoteResDate
            // 
            this.lblVoteResDate.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteResDate.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteResDate.Location = new System.Drawing.Point(236, 24);
            this.lblVoteResDate.Name = "lblVoteResDate";
            this.lblVoteResDate.Size = new System.Drawing.Size(67, 13);
            this.lblVoteResDate.TabIndex = 227;
            this.lblVoteResDate.Text = "16:30";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(34, 40);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(48, 13);
            this.labelControl22.TabIndex = 215;
            this.labelControl22.Text = "�������:";
            // 
            // grpVoteResultsTable
            // 
            this.grpVoteResultsTable.Controls.Add(this.label107);
            this.grpVoteResultsTable.Controls.Add(this.label106);
            this.grpVoteResultsTable.Controls.Add(this.label105);
            this.grpVoteResultsTable.Controls.Add(this.label104);
            this.grpVoteResultsTable.Controls.Add(this.label103);
            this.grpVoteResultsTable.Controls.Add(this.lblProcAye);
            this.grpVoteResultsTable.Controls.Add(this.lblVoteAye);
            this.grpVoteResultsTable.Controls.Add(this.label101);
            this.grpVoteResultsTable.Controls.Add(this.lblNonProcQnty);
            this.grpVoteResultsTable.Controls.Add(this.lblNonVoteQnty);
            this.grpVoteResultsTable.Controls.Add(this.label95);
            this.grpVoteResultsTable.Controls.Add(this.lblProcAgainst);
            this.grpVoteResultsTable.Controls.Add(this.label79);
            this.grpVoteResultsTable.Controls.Add(this.label80);
            this.grpVoteResultsTable.Controls.Add(this.lblProcAbstain);
            this.grpVoteResultsTable.Controls.Add(this.lblVoteAgainst);
            this.grpVoteResultsTable.Controls.Add(this.label83);
            this.grpVoteResultsTable.Controls.Add(this.label84);
            this.grpVoteResultsTable.Controls.Add(this.label85);
            this.grpVoteResultsTable.Controls.Add(this.label86);
            this.grpVoteResultsTable.Controls.Add(this.label87);
            this.grpVoteResultsTable.Controls.Add(this.label88);
            this.grpVoteResultsTable.Controls.Add(this.lblVoteAbstain);
            this.grpVoteResultsTable.Controls.Add(this.label90);
            this.grpVoteResultsTable.Controls.Add(this.domainUpDown5);
            this.grpVoteResultsTable.Controls.Add(this.label91);
            this.grpVoteResultsTable.Location = new System.Drawing.Point(30, 57);
            this.grpVoteResultsTable.Name = "grpVoteResultsTable";
            this.grpVoteResultsTable.Size = new System.Drawing.Size(301, 171);
            this.grpVoteResultsTable.TabIndex = 203;
            this.grpVoteResultsTable.TabStop = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Enabled = false;
            this.label107.ForeColor = System.Drawing.Color.DimGray;
            this.label107.Location = new System.Drawing.Point(6, 127);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(17, 13);
            this.label107.TabIndex = 184;
            this.label107.Text = "5.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Enabled = false;
            this.label106.ForeColor = System.Drawing.Color.DimGray;
            this.label106.Location = new System.Drawing.Point(6, 106);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(17, 13);
            this.label106.TabIndex = 183;
            this.label106.Text = "4.";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 84);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(17, 13);
            this.label105.TabIndex = 182;
            this.label105.Text = "3.";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 62);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 13);
            this.label104.TabIndex = 181;
            this.label104.Text = "2.";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 39);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(17, 13);
            this.label103.TabIndex = 180;
            this.label103.Text = "1.";
            // 
            // lblProcAye
            // 
            this.lblProcAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblProcAye.Location = new System.Drawing.Point(223, 39);
            this.lblProcAye.Name = "lblProcAye";
            this.lblProcAye.Size = new System.Drawing.Size(46, 17);
            this.lblProcAye.TabIndex = 179;
            this.lblProcAye.Text = "8";
            this.lblProcAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAye
            // 
            this.lblVoteAye.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAye.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblVoteAye.Location = new System.Drawing.Point(124, 39);
            this.lblVoteAye.Name = "lblVoteAye";
            this.lblVoteAye.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAye.TabIndex = 178;
            this.lblVoteAye.Text = "8";
            this.lblVoteAye.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.ForeColor = System.Drawing.Color.DarkGreen;
            this.label101.Location = new System.Drawing.Point(44, 40);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(23, 14);
            this.label101.TabIndex = 177;
            this.label101.Text = "��";
            // 
            // lblNonProcQnty
            // 
            this.lblNonProcQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonProcQnty.Location = new System.Drawing.Point(223, 149);
            this.lblNonProcQnty.Name = "lblNonProcQnty";
            this.lblNonProcQnty.Size = new System.Drawing.Size(46, 17);
            this.lblNonProcQnty.TabIndex = 176;
            this.lblNonProcQnty.Text = "8";
            this.lblNonProcQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblNonVoteQnty
            // 
            this.lblNonVoteQnty.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNonVoteQnty.Location = new System.Drawing.Point(141, 150);
            this.lblNonVoteQnty.Name = "lblNonVoteQnty";
            this.lblNonVoteQnty.Size = new System.Drawing.Size(10, 16);
            this.lblNonVoteQnty.TabIndex = 175;
            this.lblNonVoteQnty.Text = "8";
            this.lblNonVoteQnty.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.ForeColor = System.Drawing.Color.Purple;
            this.label95.Location = new System.Drawing.Point(44, 150);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(94, 14);
            this.label95.TabIndex = 174;
            this.label95.Text = "�� ����������";
            // 
            // lblProcAgainst
            // 
            this.lblProcAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblProcAgainst.Location = new System.Drawing.Point(223, 61);
            this.lblProcAgainst.Name = "lblProcAgainst";
            this.lblProcAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblProcAgainst.TabIndex = 173;
            this.lblProcAgainst.Text = "8";
            this.lblProcAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(223, 127);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(46, 17);
            this.label79.TabIndex = 172;
            this.label79.Text = "9";
            this.label79.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label79.Visible = false;
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(223, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(46, 17);
            this.label80.TabIndex = 171;
            this.label80.Text = "8";
            this.label80.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label80.Visible = false;
            // 
            // lblProcAbstain
            // 
            this.lblProcAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblProcAbstain.Location = new System.Drawing.Point(223, 83);
            this.lblProcAbstain.Name = "lblProcAbstain";
            this.lblProcAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblProcAbstain.TabIndex = 170;
            this.lblProcAbstain.Text = "8";
            this.lblProcAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVoteAgainst
            // 
            this.lblVoteAgainst.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAgainst.ForeColor = System.Drawing.Color.Maroon;
            this.lblVoteAgainst.Location = new System.Drawing.Point(124, 61);
            this.lblVoteAgainst.Name = "lblVoteAgainst";
            this.lblVoteAgainst.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAgainst.TabIndex = 169;
            this.lblVoteAgainst.Text = "8";
            this.lblVoteAgainst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(124, 127);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 17);
            this.label83.TabIndex = 168;
            this.label83.Text = "9";
            this.label83.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label83.Visible = false;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label84.Location = new System.Drawing.Point(124, 105);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(46, 17);
            this.label84.TabIndex = 167;
            this.label84.Text = "8";
            this.label84.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label84.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label85.Location = new System.Drawing.Point(223, 15);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(62, 14);
            this.label85.TabIndex = 166;
            this.label85.Text = "�������:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label86.Location = new System.Drawing.Point(124, 15);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 14);
            this.label86.TabIndex = 165;
            this.label86.Text = "������:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(44, 127);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(48, 13);
            this.label87.TabIndex = 164;
            this.label87.Text = "����� 5";
            this.label87.Visible = false;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(44, 106);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 163;
            this.label88.Text = "����� 4";
            this.label88.Visible = false;
            // 
            // lblVoteAbstain
            // 
            this.lblVoteAbstain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteAbstain.ForeColor = System.Drawing.Color.Orange;
            this.lblVoteAbstain.Location = new System.Drawing.Point(124, 83);
            this.lblVoteAbstain.Name = "lblVoteAbstain";
            this.lblVoteAbstain.Size = new System.Drawing.Size(46, 17);
            this.lblVoteAbstain.TabIndex = 155;
            this.lblVoteAbstain.Text = "8";
            this.lblVoteAbstain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.ForeColor = System.Drawing.Color.Maroon;
            this.label90.Location = new System.Drawing.Point(44, 62);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(57, 14);
            this.label90.TabIndex = 153;
            this.label90.Text = "������";
            // 
            // domainUpDown5
            // 
            this.domainUpDown5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.domainUpDown5.Location = new System.Drawing.Point(80, 400);
            this.domainUpDown5.Name = "domainUpDown5";
            this.domainUpDown5.Size = new System.Drawing.Size(77, 20);
            this.domainUpDown5.TabIndex = 145;
            this.domainUpDown5.Text = "2";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Georgia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.ForeColor = System.Drawing.Color.Orange;
            this.label91.Location = new System.Drawing.Point(44, 84);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(57, 14);
            this.label91.TabIndex = 139;
            this.label91.Text = "������.";
            // 
            // lblDecision
            // 
            this.lblDecision.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblDecision.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblDecision.Location = new System.Drawing.Point(90, 39);
            this.lblDecision.Name = "lblDecision";
            this.lblDecision.Size = new System.Drawing.Size(113, 22);
            this.lblDecision.TabIndex = 200;
            this.lblDecision.Text = "�� �������";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(348, 689);
            this.xtraTabControl1.TabIndex = 10;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            this.xtraTabControl1.Text = "�������������� �������";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.PropertiesControlDelegate);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(339, 658);
            this.xtraTabPage1.Text = "�������������� ��������";
            // 
            // PropertiesControlDelegate
            // 
            this.PropertiesControlDelegate.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlDelegate.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlDelegate.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlDelegate.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesControlDelegate.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlDelegate.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControlDelegate.Name = "PropertiesControlDelegate";
            this.PropertiesControlDelegate.RecordWidth = 123;
            this.PropertiesControlDelegate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2,
            this.repositoryItemGridLookUpEdit3,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemComboBox17});
            this.PropertiesControlDelegate.RowHeaderWidth = 77;
            this.PropertiesControlDelegate.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.cat_Region,
            this.cat_Info,
            this.cat_Seat});
            this.PropertiesControlDelegate.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlDelegate.Size = new System.Drawing.Size(339, 658);
            this.PropertiesControlDelegate.TabIndex = 11;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemGridLookUpEdit3
            // 
            this.repositoryItemGridLookUpEdit3.AutoHeight = false;
            this.repositoryItemGridLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit3.Name = "repositoryItemGridLookUpEdit3";
            this.repositoryItemGridLookUpEdit3.View = this.gridView8;
            // 
            // gridView8
            // 
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox7.ImmediatePopup = true;
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            this.repositoryItemComboBox7.Sorted = true;
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // repositoryItemComboBox17
            // 
            this.repositoryItemComboBox17.AutoHeight = false;
            this.repositoryItemComboBox17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox17.ImmediatePopup = true;
            this.repositoryItemComboBox17.Name = "repositoryItemComboBox17";
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_LastName,
            this.row_FirstName,
            this.row_SecondName});
            this.categoryRow1.Height = 18;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "���";
            // 
            // row_LastName
            // 
            this.row_LastName.Height = 27;
            this.row_LastName.Name = "row_LastName";
            this.row_LastName.Properties.Caption = "�������";
            this.row_LastName.Properties.FieldName = "LastName";
            this.row_LastName.Properties.ReadOnly = true;
            this.row_LastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_LastName.Properties.Value = "1111";
            // 
            // row_FirstName
            // 
            this.row_FirstName.Height = 20;
            this.row_FirstName.Name = "row_FirstName";
            this.row_FirstName.Properties.Caption = "���";
            this.row_FirstName.Properties.FieldName = "FirstName";
            this.row_FirstName.Properties.ReadOnly = true;
            this.row_FirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_SecondName
            // 
            this.row_SecondName.Height = 19;
            this.row_SecondName.Name = "row_SecondName";
            this.row_SecondName.Properties.Caption = "��������";
            this.row_SecondName.Properties.FieldName = "SecondName";
            this.row_SecondName.Properties.ReadOnly = true;
            // 
            // cat_Region
            // 
            this.cat_Region.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_RegionName,
            this.row_RegionNum});
            this.cat_Region.Height = 19;
            this.cat_Region.Name = "cat_Region";
            this.cat_Region.Properties.Caption = "������";
            // 
            // row_RegionName
            // 
            this.row_RegionName.Height = 20;
            this.row_RegionName.Name = "row_RegionName";
            this.row_RegionName.Properties.Caption = "������";
            this.row_RegionName.Properties.FieldName = "RegionName";
            this.row_RegionName.Properties.ReadOnly = true;
            this.row_RegionName.Properties.RowEdit = this.repositoryItemComboBox7;
            this.row_RegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_RegionNum
            // 
            this.row_RegionNum.Height = 20;
            this.row_RegionNum.Name = "row_RegionNum";
            this.row_RegionNum.Properties.Caption = "� �������";
            this.row_RegionNum.Properties.FieldName = "RegionNum";
            this.row_RegionNum.Properties.ReadOnly = true;
            this.row_RegionNum.Properties.RowEdit = this.repositoryItemComboBox8;
            this.row_RegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // cat_Info
            // 
            this.cat_Info.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_Fraction,
            this.row_PartyName});
            this.cat_Info.Name = "cat_Info";
            this.cat_Info.Properties.Caption = "�������������� ����������";
            // 
            // row_Fraction
            // 
            this.row_Fraction.Height = 25;
            this.row_Fraction.Name = "row_Fraction";
            this.row_Fraction.Properties.Caption = "�������";
            this.row_Fraction.Properties.FieldName = "Fraction";
            this.row_Fraction.Properties.ReadOnly = true;
            this.row_Fraction.Properties.RowEdit = this.repositoryItemComboBox17;
            this.row_Fraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_PartyName
            // 
            this.row_PartyName.Height = 23;
            this.row_PartyName.Name = "row_PartyName";
            this.row_PartyName.Properties.Caption = "������";
            this.row_PartyName.Properties.FieldName = "idParty.Name";
            this.row_PartyName.Properties.ReadOnly = true;
            this.row_PartyName.Properties.RowEdit = this.repositoryItemComboBox1;
            this.row_PartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.row_PartyName.Properties.Value = "������ 1";
            // 
            // cat_Seat
            // 
            this.cat_Seat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_MicNum,
            this.row_RowNum,
            this.row_SeatNum});
            this.cat_Seat.Name = "cat_Seat";
            // 
            // row_MicNum
            // 
            this.row_MicNum.Height = 16;
            this.row_MicNum.Name = "row_MicNum";
            this.row_MicNum.Properties.Caption = "�������� �";
            this.row_MicNum.Properties.ReadOnly = true;
            this.row_MicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_RowNum
            // 
            this.row_RowNum.Name = "row_RowNum";
            this.row_RowNum.Properties.Caption = "��� �";
            this.row_RowNum.Properties.ReadOnly = true;
            this.row_RowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_SeatNum
            // 
            this.row_SeatNum.Name = "row_SeatNum";
            this.row_SeatNum.Properties.Caption = "����� �";
            this.row_SeatNum.Properties.ReadOnly = true;
            this.row_SeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.PropertiesControlQuest);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(339, 658);
            this.xtraTabPage2.Text = "�������������� �������";
            // 
            // PropertiesControlQuest
            // 
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertiesControlQuest.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControlQuest.Name = "PropertiesControlQuest";
            this.PropertiesControlQuest.RecordWidth = 97;
            this.PropertiesControlQuest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemGridLookUpEdit4,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox18,
            this.repositoryItemComboBox19,
            this.repositoryItemComboBox20,
            this.repositoryItemComboBox21,
            this.repositoryItemComboBox22,
            this.repositoryItemDateEdit2,
            this.repositoryItemComboBox23,
            this.repositoryItemComboBox24,
            this.repositoryItemComboBox25,
            this.repositoryItemComboBox26,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4,
            this.repositoryItemDateEdit3,
            this.repositoryItemMemoEdit1,
            this.repositoryItemComboBox27,
            this.repositoryItemSpinEdit2,
            this.repositoryItemComboBox28,
            this.repositoryItemComboBox29});
            this.PropertiesControlQuest.RowHeaderWidth = 103;
            this.PropertiesControlQuest.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow2,
            this.categoryRow3});
            this.PropertiesControlQuest.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest.Size = new System.Drawing.Size(339, 658);
            this.PropertiesControlQuest.TabIndex = 13;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemGridLookUpEdit4
            // 
            this.repositoryItemGridLookUpEdit4.AutoHeight = false;
            this.repositoryItemGridLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit4.Name = "repositoryItemGridLookUpEdit4";
            this.repositoryItemGridLookUpEdit4.View = this.gridView9;
            // 
            // gridView9
            // 
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox3.ImmediatePopup = true;
            this.repositoryItemComboBox3.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox18
            // 
            this.repositoryItemComboBox18.AutoHeight = false;
            this.repositoryItemComboBox18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox18.ImmediatePopup = true;
            this.repositoryItemComboBox18.Name = "repositoryItemComboBox18";
            this.repositoryItemComboBox18.Sorted = true;
            // 
            // repositoryItemComboBox19
            // 
            this.repositoryItemComboBox19.AutoHeight = false;
            this.repositoryItemComboBox19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox19.Name = "repositoryItemComboBox19";
            // 
            // repositoryItemComboBox20
            // 
            this.repositoryItemComboBox20.AutoHeight = false;
            this.repositoryItemComboBox20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox20.ImmediatePopup = true;
            this.repositoryItemComboBox20.Name = "repositoryItemComboBox20";
            // 
            // repositoryItemComboBox21
            // 
            this.repositoryItemComboBox21.AutoHeight = false;
            this.repositoryItemComboBox21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox21.Name = "repositoryItemComboBox21";
            // 
            // repositoryItemComboBox22
            // 
            this.repositoryItemComboBox22.AutoHeight = false;
            this.repositoryItemComboBox22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox22.Name = "repositoryItemComboBox22";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            this.repositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox23
            // 
            this.repositoryItemComboBox23.AutoHeight = false;
            this.repositoryItemComboBox23.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox23.Name = "repositoryItemComboBox23";
            // 
            // repositoryItemComboBox24
            // 
            this.repositoryItemComboBox24.AutoHeight = false;
            this.repositoryItemComboBox24.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox24.Name = "repositoryItemComboBox24";
            // 
            // repositoryItemComboBox25
            // 
            this.repositoryItemComboBox25.AutoHeight = false;
            this.repositoryItemComboBox25.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox25.Name = "repositoryItemComboBox25";
            // 
            // repositoryItemComboBox26
            // 
            this.repositoryItemComboBox26.AutoHeight = false;
            this.repositoryItemComboBox26.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox26.Name = "repositoryItemComboBox26";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            this.repositoryItemDateEdit3.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // repositoryItemComboBox27
            // 
            this.repositoryItemComboBox27.AutoHeight = false;
            this.repositoryItemComboBox27.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox27.Name = "repositoryItemComboBox27";
            // 
            // repositoryItemComboBox28
            // 
            this.repositoryItemComboBox28.AutoHeight = false;
            this.repositoryItemComboBox28.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox28.Name = "repositoryItemComboBox28";
            // 
            // repositoryItemComboBox29
            // 
            this.repositoryItemComboBox29.AutoHeight = false;
            this.repositoryItemComboBox29.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox29.Name = "repositoryItemComboBox29";
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_Number,
            this.row_sq_Caption,
            this.row_sq_CrDate,
            this.row_sq_Notes,
            this.row_sq_VoteType,
            this.row_sq_VoteKind,
            this.row_sq_ReadNum});
            this.categoryRow2.Height = 19;
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "��������������";
            // 
            // row_sq_Number
            // 
            this.row_sq_Number.Height = 28;
            this.row_sq_Number.Name = "row_sq_Number";
            this.row_sq_Number.Properties.Caption = "� �������";
            this.row_sq_Number.Properties.ReadOnly = true;
            this.row_sq_Number.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_Caption
            // 
            this.row_sq_Caption.Height = 47;
            this.row_sq_Caption.Name = "row_sq_Caption";
            this.row_sq_Caption.Properties.Caption = "���������";
            this.row_sq_Caption.Properties.ReadOnly = true;
            this.row_sq_Caption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_CrDate
            // 
            this.row_sq_CrDate.Height = 28;
            this.row_sq_CrDate.Name = "row_sq_CrDate";
            this.row_sq_CrDate.Properties.Caption = "����/����� ��������";
            this.row_sq_CrDate.Properties.ReadOnly = true;
            this.row_sq_CrDate.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            // 
            // row_sq_Notes
            // 
            this.row_sq_Notes.Height = 80;
            this.row_sq_Notes.Name = "row_sq_Notes";
            this.row_sq_Notes.Properties.Caption = "�������";
            this.row_sq_Notes.Properties.ReadOnly = true;
            this.row_sq_Notes.Properties.RowEdit = this.repositoryItemMemoEdit1;
            this.row_sq_Notes.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_VoteType
            // 
            this.row_sq_VoteType.Name = "row_sq_VoteType";
            this.row_sq_VoteType.Properties.Caption = "��� �����������";
            this.row_sq_VoteType.Properties.ReadOnly = true;
            this.row_sq_VoteType.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_VoteKind
            // 
            this.row_sq_VoteKind.Name = "row_sq_VoteKind";
            this.row_sq_VoteKind.Properties.Caption = "��� �����������";
            this.row_sq_VoteKind.Properties.ReadOnly = true;
            this.row_sq_VoteKind.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_ReadNum
            // 
            this.row_sq_ReadNum.Height = 25;
            this.row_sq_ReadNum.Name = "row_sq_ReadNum";
            this.row_sq_ReadNum.Properties.Caption = "������";
            this.row_sq_ReadNum.Properties.ReadOnly = true;
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_DecisionProcValue,
            this.row_sq_QuotaProcentValue});
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "�������� �������� �������";
            // 
            // row_sq_DecisionProcValue
            // 
            this.row_sq_DecisionProcValue.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_DecisionProcType,
            this.row_sq_VoteQnty});
            this.row_sq_DecisionProcValue.Name = "row_sq_DecisionProcValue";
            multiEditorRowProperties1.Caption = "����� �������� �������";
            multiEditorRowProperties1.CellWidth = 15;
            multiEditorRowProperties1.ReadOnly = true;
            multiEditorRowProperties1.RowEdit = this.repositoryItemSpinEdit2;
            multiEditorRowProperties1.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            multiEditorRowProperties1.Width = 117;
            multiEditorRowProperties2.ReadOnly = true;
            multiEditorRowProperties2.UnboundType = DevExpress.Data.UnboundColumnType.String;
            multiEditorRowProperties2.Value = "%";
            multiEditorRowProperties2.Width = 15;
            this.row_sq_DecisionProcValue.PropertiesCollection.AddRange(new DevExpress.XtraVerticalGrid.Rows.MultiEditorRowProperties[] {
            multiEditorRowProperties1,
            multiEditorRowProperties2});
            // 
            // row_sq_DecisionProcType
            // 
            this.row_sq_DecisionProcType.Name = "row_sq_DecisionProcType";
            this.row_sq_DecisionProcType.Properties.Caption = "�� ����� ���������";
            this.row_sq_DecisionProcType.Properties.ReadOnly = true;
            this.row_sq_DecisionProcType.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_VoteQnty
            // 
            this.row_sq_VoteQnty.Name = "row_sq_VoteQnty";
            this.row_sq_VoteQnty.Properties.Caption = "����� �������";
            this.row_sq_VoteQnty.Properties.ReadOnly = true;
            this.row_sq_VoteQnty.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // row_sq_QuotaProcentValue
            // 
            this.row_sq_QuotaProcentValue.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.row_sq_QuotaProcentType_Present,
            this.row_sq_QuotaProcentType_Total});
            this.row_sq_QuotaProcentValue.Name = "row_sq_QuotaProcentValue";
            this.row_sq_QuotaProcentValue.Properties.Caption = "����� �� ������";
            this.row_sq_QuotaProcentValue.Properties.ReadOnly = true;
            this.row_sq_QuotaProcentValue.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // row_sq_QuotaProcentType_Present
            // 
            this.row_sq_QuotaProcentType_Present.Name = "row_sq_QuotaProcentType_Present";
            this.row_sq_QuotaProcentType_Present.Properties.Caption = "�� ��������������";
            this.row_sq_QuotaProcentType_Present.Properties.ReadOnly = true;
            this.row_sq_QuotaProcentType_Present.Properties.RowEdit = this.repositoryItemCheckEdit3;
            this.row_sq_QuotaProcentType_Present.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            // 
            // row_sq_QuotaProcentType_Total
            // 
            this.row_sq_QuotaProcentType_Total.Name = "row_sq_QuotaProcentType_Total";
            this.row_sq_QuotaProcentType_Total.Properties.Caption = "�� ������ ������";
            this.row_sq_QuotaProcentType_Total.Properties.ReadOnly = true;
            this.row_sq_QuotaProcentType_Total.Properties.RowEdit = this.repositoryItemCheckEdit4;
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.txtProcQnty);
            this.panelControl2.Controls.Add(this.lblProc);
            this.panelControl2.Controls.Add(this.btnVoteFinish);
            this.panelControl2.Controls.Add(this.labelControl25);
            this.panelControl2.Controls.Add(this.txtDelegateQnty);
            this.panelControl2.Controls.Add(this.labelControl23);
            this.panelControl2.Controls.Add(this.txtVoteQnty);
            this.panelControl2.Controls.Add(this.labelControl36);
            this.panelControl2.Controls.Add(this.lblVoteTime);
            this.panelControl2.Controls.Add(this.btnVoteCancel);
            this.panelControl2.Controls.Add(this.progressBarVoting);
            this.panelControl2.Controls.Add(this.lblVoting);
            this.panelControl2.Controls.Add(this.picVoteTime);
            this.panelControl2.Controls.Add(this.btnVoteStart);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 697);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1167, 89);
            this.panelControl2.TabIndex = 33;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(702, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(23, 13);
            this.labelControl1.TabIndex = 260;
            this.labelControl1.Text = "���.";
            // 
            // txtProcQnty
            // 
            this.txtProcQnty.Location = new System.Drawing.Point(737, 15);
            this.txtProcQnty.Name = "txtProcQnty";
            this.txtProcQnty.Size = new System.Drawing.Size(49, 20);
            this.txtProcQnty.TabIndex = 259;
            // 
            // lblProc
            // 
            this.lblProc.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProc.Location = new System.Drawing.Point(788, 19);
            this.lblProc.Name = "lblProc";
            this.lblProc.Size = new System.Drawing.Size(19, 16);
            this.lblProc.TabIndex = 258;
            this.lblProc.Text = "%";
            this.lblProc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnVoteFinish
            // 
            this.btnVoteFinish.ImageIndex = 8;
            this.btnVoteFinish.ImageList = this.ButtonImages;
            this.btnVoteFinish.Location = new System.Drawing.Point(59, 48);
            this.btnVoteFinish.Name = "btnVoteFinish";
            this.btnVoteFinish.Size = new System.Drawing.Size(172, 31);
            this.btnVoteFinish.TabIndex = 257;
            this.btnVoteFinish.Text = "��������� �����������";
            this.btnVoteFinish.Click += new System.EventHandler(this.btnVoteFinish_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(702, 66);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(23, 13);
            this.labelControl25.TabIndex = 256;
            this.labelControl25.Text = "���.";
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.Location = new System.Drawing.Point(647, 60);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Size = new System.Drawing.Size(49, 20);
            this.txtDelegateQnty.TabIndex = 255;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(664, 41);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(11, 13);
            this.labelControl23.TabIndex = 254;
            this.labelControl23.Text = "��";
            // 
            // txtVoteQnty
            // 
            this.txtVoteQnty.Location = new System.Drawing.Point(647, 15);
            this.txtVoteQnty.Name = "txtVoteQnty";
            this.txtVoteQnty.Size = new System.Drawing.Size(49, 20);
            this.txtVoteQnty.TabIndex = 253;
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(548, 18);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(81, 13);
            this.labelControl36.TabIndex = 252;
            this.labelControl36.Text = "�������������:";
            // 
            // lblVoteTime
            // 
            this.lblVoteTime.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteTime.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblVoteTime.Appearance.Options.UseFont = true;
            this.lblVoteTime.Appearance.Options.UseForeColor = true;
            this.lblVoteTime.Location = new System.Drawing.Point(299, 37);
            this.lblVoteTime.Name = "lblVoteTime";
            this.lblVoteTime.Size = new System.Drawing.Size(42, 15);
            this.lblVoteTime.TabIndex = 243;
            this.lblVoteTime.Text = "02 : 30";
            // 
            // btnVoteCancel
            // 
            this.btnVoteCancel.ImageIndex = 12;
            this.btnVoteCancel.ImageList = this.ButtonImages;
            this.btnVoteCancel.Location = new System.Drawing.Point(872, 27);
            this.btnVoteCancel.Name = "btnVoteCancel";
            this.btnVoteCancel.Size = new System.Drawing.Size(194, 38);
            this.btnVoteCancel.TabIndex = 241;
            this.btnVoteCancel.Text = "�������� �����  �����������";
            this.btnVoteCancel.Click += new System.EventHandler(this.btnVoteCancel_Click);
            // 
            // progressBarVoting
            // 
            this.progressBarVoting.Location = new System.Drawing.Point(355, 49);
            this.progressBarVoting.Name = "progressBarVoting";
            this.progressBarVoting.Size = new System.Drawing.Size(144, 18);
            this.progressBarVoting.TabIndex = 240;
            // 
            // lblVoting
            // 
            this.lblVoting.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoting.Appearance.Options.UseFont = true;
            this.lblVoting.Location = new System.Drawing.Point(355, 23);
            this.lblVoting.Name = "lblVoting";
            this.lblVoting.Size = new System.Drawing.Size(105, 13);
            this.lblVoting.TabIndex = 229;
            this.lblVoting.Text = "���� �����������...";
            // 
            // picVoteTime
            // 
            this.picVoteTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picVoteTime.EditValue = ((object)(resources.GetObject("picVoteTime.EditValue")));
            this.picVoteTime.Location = new System.Drawing.Point(259, 27);
            this.picVoteTime.Name = "picVoteTime";
            this.picVoteTime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picVoteTime.Properties.Appearance.Options.UseBackColor = true;
            this.picVoteTime.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picVoteTime.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picVoteTime.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picVoteTime.Properties.UseParentBackground = true;
            this.picVoteTime.Size = new System.Drawing.Size(35, 47);
            this.picVoteTime.TabIndex = 228;
            // 
            // btnVoteStart
            // 
            this.btnVoteStart.ImageIndex = 0;
            this.btnVoteStart.ImageList = this.ButtonImages;
            this.btnVoteStart.Location = new System.Drawing.Point(59, 6);
            this.btnVoteStart.Name = "btnVoteStart";
            this.btnVoteStart.Size = new System.Drawing.Size(172, 38);
            this.btnVoteStart.TabIndex = 230;
            this.btnVoteStart.Text = "������ �����������";
            this.btnVoteStart.Click += new System.EventHandler(this.btnVoteStart_Click);
            // 
            // TimerImages
            // 
            this.TimerImages.ImageSize = new System.Drawing.Size(96, 96);
            this.TimerImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("TimerImages.ImageStream")));
            // 
            // ManImages
            // 
            this.ManImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ManImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ManImages.ImageStream")));
            // 
            // tmrVoteStart
            // 
            this.tmrVoteStart.Interval = 1000;
            this.tmrVoteStart.Tick += new System.EventHandler(this.tmrVoteStart_Tick);
            // 
            // tmrRotation
            // 
            this.tmrRotation.Interval = 500;
            this.tmrRotation.Tick += new System.EventHandler(this.tmrRotation_Tick);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.KeyTip = "";
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "�������� � ����";
            // 
            // VoteModeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 786);
            this.Controls.Add(this.splitContainerControl2);
            this.Controls.Add(this.panelControl2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VoteModeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "����� �����������";
            this.Load += new System.EventHandler(this.VoteModeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpSeates)).EndInit();
            this.grpSeates.ResumeLayout(false);
            this.grpSeates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpVoteResults)).EndInit();
            this.grpVoteResults.ResumeLayout(false);
            this.grpVoteResults.PerformLayout();
            this.grpVoteResultsTable.ResumeLayout(false);
            this.grpVoteResultsTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlDelegate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox17)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProcQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimerImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManImages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.GroupControl grpSeates;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LabelControl labelControl97;
        private DevExpress.XtraEditors.PictureEdit pictureEdit75;
        private DevExpress.XtraEditors.LabelControl labelControl98;
        private DevExpress.XtraEditors.PictureEdit pictureEdit76;
        private DevExpress.XtraEditors.LabelControl labelControl99;
        private DevExpress.XtraEditors.PictureEdit pictureEdit77;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.PictureEdit pictureEdit78;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.PictureEdit pictureEdit79;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.PictureEdit pictureEdit80;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.PictureEdit pictureEdit81;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.PictureEdit pictureEdit82;
        private DevExpress.XtraEditors.LabelControl labelControl105;
        private DevExpress.XtraEditors.PictureEdit pictureEdit83;
        private DevExpress.XtraEditors.LabelControl labelControl106;
        private DevExpress.XtraEditors.PictureEdit pictureEdit84;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraEditors.PictureEdit pictureEdit85;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.PictureEdit pictureEdit86;
        private DevExpress.XtraEditors.LabelControl labelControl109;
        private DevExpress.XtraEditors.PictureEdit pictureEdit87;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.PictureEdit pictureEdit88;
        private DevExpress.XtraEditors.LabelControl labelControl111;
        private DevExpress.XtraEditors.PictureEdit pictureEdit89;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.PictureEdit pictureEdit90;
        private DevExpress.XtraEditors.LabelControl labelControl113;
        private DevExpress.XtraEditors.PictureEdit pictureEdit91;
        private DevExpress.XtraEditors.LabelControl labelControl114;
        private DevExpress.XtraEditors.PictureEdit pictureEdit92;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.PictureEdit pictureEdit93;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.PictureEdit pictureEdit94;
        private DevExpress.XtraEditors.LabelControl labelControl117;
        private DevExpress.XtraEditors.PictureEdit pictureEdit95;
        private DevExpress.XtraEditors.LabelControl labelControl118;
        private DevExpress.XtraEditors.PictureEdit pictureEdit96;
        private DevExpress.XtraEditors.LabelControl labelControl119;
        private DevExpress.XtraEditors.PictureEdit pictureEdit97;
        private DevExpress.XtraEditors.LabelControl labelControl120;
        private DevExpress.XtraEditors.PictureEdit pictureEdit98;
        private DevExpress.XtraEditors.LabelControl labelControl121;
        private DevExpress.XtraEditors.PictureEdit pictureEdit99;
        private DevExpress.XtraEditors.LabelControl labelControl122;
        private DevExpress.XtraEditors.PictureEdit pictureEdit100;
        private DevExpress.XtraEditors.LabelControl labelControl123;
        private DevExpress.XtraEditors.PictureEdit pictureEdit101;
        private DevExpress.XtraEditors.LabelControl labelControl124;
        private DevExpress.XtraEditors.PictureEdit pictureEdit102;
        private DevExpress.XtraEditors.LabelControl labelControl125;
        private DevExpress.XtraEditors.PictureEdit pictureEdit103;
        private DevExpress.XtraEditors.LabelControl labelControl126;
        private DevExpress.XtraEditors.PictureEdit pictureEdit104;
        private DevExpress.XtraEditors.LabelControl labelControl127;
        private DevExpress.XtraEditors.PictureEdit pictureEdit105;
        private DevExpress.XtraEditors.LabelControl labelControl128;
        private DevExpress.XtraEditors.PictureEdit pictureEdit106;
        private DevExpress.XtraEditors.LabelControl labelControl129;
        private DevExpress.XtraEditors.PictureEdit pictureEdit107;
        private DevExpress.XtraEditors.LabelControl labelControl130;
        private DevExpress.XtraEditors.PictureEdit pictureEdit108;
        private DevExpress.XtraEditors.LabelControl labelControl131;
        private DevExpress.XtraEditors.PictureEdit pictureEdit109;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.PictureEdit pictureEdit110;
        private DevExpress.XtraEditors.LabelControl labelControl133;
        private DevExpress.XtraEditors.PictureEdit pictureEdit111;
        private DevExpress.XtraEditors.LabelControl labelControl134;
        private DevExpress.XtraEditors.PictureEdit pictureEdit112;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.PictureEdit pictureEdit113;
        private DevExpress.XtraEditors.LabelControl labelControl136;
        private DevExpress.XtraEditors.PictureEdit pictureEdit114;
        private DevExpress.XtraEditors.LabelControl labelControl137;
        private DevExpress.XtraEditors.PictureEdit pictureEdit115;
        private DevExpress.XtraEditors.LabelControl labelControl138;
        private DevExpress.XtraEditors.PictureEdit pictureEdit116;
        private DevExpress.XtraEditors.LabelControl labelControl139;
        private DevExpress.XtraEditors.PictureEdit pictureEdit117;
        private DevExpress.XtraEditors.LabelControl labelControl140;
        private DevExpress.XtraEditors.PictureEdit pictureEdit118;
        private DevExpress.XtraEditors.LabelControl labelControl141;
        private DevExpress.XtraEditors.PictureEdit pictureEdit119;
        private DevExpress.XtraEditors.LabelControl labelControl142;
        private DevExpress.XtraEditors.PictureEdit pictureEdit120;
        private DevExpress.XtraEditors.LabelControl labelControl143;
        private DevExpress.XtraEditors.PictureEdit pictureEdit121;
        private DevExpress.XtraEditors.LabelControl labelControl144;
        private DevExpress.XtraEditors.PictureEdit pictureEdit122;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.PictureEdit pictureEdit59;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.PictureEdit pictureEdit60;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.PictureEdit pictureEdit61;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.PictureEdit pictureEdit62;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.PictureEdit pictureEdit63;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.PictureEdit pictureEdit64;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.PictureEdit pictureEdit65;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.PictureEdit pictureEdit66;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.PictureEdit pictureEdit67;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.PictureEdit pictureEdit68;
        private DevExpress.XtraEditors.LabelControl labelControl91;
        private DevExpress.XtraEditors.PictureEdit pictureEdit69;
        private DevExpress.XtraEditors.LabelControl labelControl92;
        private DevExpress.XtraEditors.PictureEdit pictureEdit70;
        private DevExpress.XtraEditors.LabelControl labelControl93;
        private DevExpress.XtraEditors.PictureEdit pictureEdit71;
        private DevExpress.XtraEditors.LabelControl labelControl94;
        private DevExpress.XtraEditors.PictureEdit pictureEdit72;
        private DevExpress.XtraEditors.LabelControl labelControl95;
        private DevExpress.XtraEditors.PictureEdit pictureEdit73;
        private DevExpress.XtraEditors.LabelControl labelControl96;
        private DevExpress.XtraEditors.PictureEdit pictureEdit74;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.PictureEdit pictureEdit43;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.PictureEdit pictureEdit44;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.PictureEdit pictureEdit45;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.PictureEdit pictureEdit46;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.PictureEdit pictureEdit47;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.PictureEdit pictureEdit48;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.PictureEdit pictureEdit49;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.PictureEdit pictureEdit50;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.PictureEdit pictureEdit51;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.PictureEdit pictureEdit52;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.PictureEdit pictureEdit53;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.PictureEdit pictureEdit54;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.PictureEdit pictureEdit55;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.PictureEdit pictureEdit56;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.PictureEdit pictureEdit57;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.PictureEdit pictureEdit58;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.PictureEdit pictureEdit27;
        private DevExpress.XtraEditors.PictureEdit pictureEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.PictureEdit pictureEdit28;
        private DevExpress.XtraEditors.PictureEdit pictureEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.PictureEdit pictureEdit29;
        private DevExpress.XtraEditors.PictureEdit pictureEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.PictureEdit pictureEdit30;
        private DevExpress.XtraEditors.PictureEdit pictureEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.PictureEdit pictureEdit31;
        private DevExpress.XtraEditors.PictureEdit pictureEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.PictureEdit pictureEdit32;
        private DevExpress.XtraEditors.PictureEdit pictureEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.PictureEdit pictureEdit33;
        private DevExpress.XtraEditors.PictureEdit pictureEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.PictureEdit pictureEdit34;
        private DevExpress.XtraEditors.PictureEdit pictureEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit35;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit36;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit37;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit38;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit39;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PictureEdit pictureEdit40;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PictureEdit pictureEdit41;
        private DevExpress.XtraEditors.PictureEdit Mic_2;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PictureEdit pictureEdit42;
        private DevExpress.XtraEditors.PictureEdit Mic_1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl lblVoteTime;
        private DevExpress.XtraEditors.SimpleButton btnVoteCancel;
        private DevExpress.XtraEditors.ProgressBarControl progressBarVoting;
        private DevExpress.XtraEditors.LabelControl lblVoting;
        private DevExpress.XtraEditors.PictureEdit picVoteTime;
        private DevExpress.XtraEditors.SimpleButton btnVoteStart;
        private DevExpress.XtraEditors.GroupControl grpVoteResults;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private System.Windows.Forms.Label lblVoteResDate;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private System.Windows.Forms.GroupBox grpVoteResultsTable;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lblProcAye;
        private System.Windows.Forms.Label lblVoteAye;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblNonProcQnty;
        private System.Windows.Forms.Label lblNonVoteQnty;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lblProcAgainst;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lblProcAbstain;
        private System.Windows.Forms.Label lblVoteAgainst;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblVoteAbstain;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.DomainUpDown domainUpDown5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label lblDecision;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlDelegate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox17;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_LastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_FirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_SecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_Region;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_RegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_RegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_Info;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_Fraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_PartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow cat_Seat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_MicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_RowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_SeatNum;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox18;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox19;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox20;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox21;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox22;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox23;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox24;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox25;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox26;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox27;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox28;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox29;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Number;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Caption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_CrDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_Notes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteKind;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_ReadNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.MultiEditorRow row_sq_DecisionProcValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_DecisionProcType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_VoteQnty;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentValue;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentType_Present;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow row_sq_QuotaProcentType_Total;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtDelegateQnty;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtVoteQnty;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.Utils.ImageCollection TimerImages;
        private DevExpress.Utils.ImageCollection ManImages;
        private System.Windows.Forms.Timer tmrVoteStart;
        private System.Windows.Forms.Timer tmrRotation;
        private DevExpress.XtraEditors.SimpleButton btnVoteFinish;
        private System.Windows.Forms.Label lblProc;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtProcQnty;
        private System.Windows.Forms.Label lblVoteResTime;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
    }
}