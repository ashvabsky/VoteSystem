namespace VoteSystem
{
    partial class RegStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegStartForm));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtRegTime = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.chkRegProccess = new DevExpress.XtraEditors.CheckEdit();
            this.chkTotalsToScreen = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtQuorumQnty = new DevExpress.XtraEditors.SpinEdit();
            this.txtDelegateQnty = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnMonitorRegPrepare = new DevExpress.XtraEditors.CheckButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegProccess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTotalsToScreen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(31, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(119, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "����� �� �����������:";
            // 
            // txtRegTime
            // 
            this.txtRegTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtRegTime.Location = new System.Drawing.Point(182, 17);
            this.txtRegTime.Name = "txtRegTime";
            this.txtRegTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRegTime.Properties.IsFloatValue = false;
            this.txtRegTime.Properties.Mask.EditMask = "00";
            this.txtRegTime.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtRegTime.Size = new System.Drawing.Size(61, 20);
            this.txtRegTime.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(249, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(22, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "���.";
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnStart.ImageIndex = 5;
            this.btnStart.ImageList = this.ButtonImages;
            this.btnStart.Location = new System.Drawing.Point(22, 289);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(154, 37);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "������ �����������";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 2;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(182, 289);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 37);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "��������";
            // 
            // chkRegProccess
            // 
            this.chkRegProccess.EditValue = true;
            this.chkRegProccess.Location = new System.Drawing.Point(5, 33);
            this.chkRegProccess.Name = "chkRegProccess";
            this.chkRegProccess.Properties.Caption = "���������� ��� �����������";
            this.chkRegProccess.Size = new System.Drawing.Size(248, 19);
            this.chkRegProccess.TabIndex = 8;
            this.chkRegProccess.CheckedChanged += new System.EventHandler(this.chkRegProccess_CheckedChanged);
            // 
            // chkTotalsToScreen
            // 
            this.chkTotalsToScreen.EditValue = true;
            this.chkTotalsToScreen.Location = new System.Drawing.Point(5, 59);
            this.chkTotalsToScreen.Name = "chkTotalsToScreen";
            this.chkTotalsToScreen.Properties.Caption = "������� �������� ���������";
            this.chkTotalsToScreen.Size = new System.Drawing.Size(242, 19);
            this.chkTotalsToScreen.TabIndex = 9;
            this.chkTotalsToScreen.CheckedChanged += new System.EventHandler(this.chkTotalsToScreen_CheckedChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(31, 60);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "���������� ����:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(31, 100);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(131, 13);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "���������� ��� �������:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(249, 104);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(22, 13);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "���.";
            // 
            // txtQuorumQnty
            // 
            this.txtQuorumQnty.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtQuorumQnty.Location = new System.Drawing.Point(182, 97);
            this.txtQuorumQnty.Name = "txtQuorumQnty";
            this.txtQuorumQnty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtQuorumQnty.Properties.IsFloatValue = false;
            this.txtQuorumQnty.Properties.Mask.EditMask = "000";
            this.txtQuorumQnty.Size = new System.Drawing.Size(61, 20);
            this.txtQuorumQnty.TabIndex = 16;
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDelegateQnty.Location = new System.Drawing.Point(182, 57);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDelegateQnty.Properties.IsFloatValue = false;
            this.txtDelegateQnty.Properties.Mask.EditMask = "000";
            this.txtDelegateQnty.Properties.ReadOnly = true;
            this.txtDelegateQnty.Size = new System.Drawing.Size(61, 20);
            this.txtDelegateQnty.TabIndex = 18;
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl1.Controls.Add(this.btnMonitorRegPrepare);
            this.groupControl1.Controls.Add(this.chkTotalsToScreen);
            this.groupControl1.Controls.Add(this.chkRegProccess);
            this.groupControl1.Location = new System.Drawing.Point(22, 141);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(249, 133);
            this.groupControl1.TabIndex = 19;
            this.groupControl1.Text = "����� �� ��������";
            // 
            // btnMonitorRegPrepare
            // 
            this.btnMonitorRegPrepare.ImageIndex = 6;
            this.btnMonitorRegPrepare.ImageList = this.ButtonImages;
            this.btnMonitorRegPrepare.Location = new System.Drawing.Point(4, 87);
            this.btnMonitorRegPrepare.Name = "btnMonitorRegPrepare";
            this.btnMonitorRegPrepare.Size = new System.Drawing.Size(240, 34);
            this.btnMonitorRegPrepare.TabIndex = 10;
            this.btnMonitorRegPrepare.Text = "������� ����������� �� �������";
            this.btnMonitorRegPrepare.CheckedChanged += new System.EventHandler(this.btnMonitorRegPrepare_CheckedChanged);
            // 
            // RegStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 338);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.txtDelegateQnty);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.txtQuorumQnty);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtRegTime);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RegStartForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����������";
            this.Load += new System.EventHandler(this.RegStartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegProccess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTotalsToScreen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit txtRegTime;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.CheckEdit chkRegProccess;
        private DevExpress.XtraEditors.CheckEdit chkTotalsToScreen;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit txtQuorumQnty;
        private DevExpress.XtraEditors.SpinEdit txtDelegateQnty;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckButton btnMonitorRegPrepare;
        private DevExpress.Utils.ImageCollection ButtonImages;
    }
}