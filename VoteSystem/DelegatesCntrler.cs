﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;

namespace VoteSystem
{
    public class DelegatesCntrler
    {
        public enum DelegatesFormMode
        {
            Manage,
            Select,
            SeatSelect // выбора депутатов без мест
        }

        DelegateObj _theDelegate;
        NestedUnitOfWork _nuow;

        IDelegatesForm _View;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        XPCollection _Parties;
        XPCollection _Delegates;
        XPCollection _Fractions;
        XPCollection _Regions;
        XPCollection _Seats;

        DelegatesFormMode _Mode = DelegatesFormMode.Manage;
        List<int> _filterArray;

        MainCntrler _mainController;

        public DelegatesFormMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public DelegatesCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IDelegatesForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }


        public void Init(ref XPCollection Delegates, ref XPCollection Parties, ref XPCollection Fractions, 
            ref XPCollection Regions, ref XPCollection Seats)
        {
            _Parties = Parties;
            _Delegates = Delegates;
            _Regions = Regions;
            _Fractions = Fractions;
            _Seats = Seats;
        }

        public void EditRecord(int pos)
        {
            _theDelegate = null;

            if (pos <= -1)
                return;

            _nuow = _Delegates.Session.BeginNestedUnitOfWork();

            _theDelegate = _nuow.GetNestedObject(_Delegates[pos]) as DelegateObj;

            _nuow.BeginTransaction();
            _View.Properties_StartEdit();
        }

        public void DataPositionChanged(int pos)
        {
            _theDelegate = null;

            if (pos < 0)
                return;

            if (pos >= _Delegates.Count)
                return;

            _theDelegate = _Delegates[pos] as DelegateObj;

            ShowRecProperties(_theDelegate); 
        }

        public void ShowRecProperties(DelegateObj theDelegate)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties["FirstName"] = theDelegate.FirstName;
            properties["SecondName"] = theDelegate.SecondName;
            properties["LastName"] = theDelegate.LastName;
            properties["PartyName"] = theDelegate.idParty.Name;
            properties["FractionName"] = theDelegate.idFraction.Name;
            properties["RegionName"] = theDelegate.idRegion.Name;
            properties["RegionNum"] = theDelegate.idRegion.Number;

            properties["MicNum"] = theDelegate.idSeat.MicNum.ToString();
            properties["RowNum"] = theDelegate.idSeat.RowNum.ToString();
            properties["SeatNum"] = theDelegate.idSeat.SeatNum.ToString();

            _View.SetProperties(properties);
        }

        public void ApplyChanges(Dictionary<string, string> properties)
        {
            _theDelegate.LastName = properties["LastName"];
            _theDelegate.FirstName = properties["FirstName"];
            _theDelegate.SecondName = properties["SecondName"];

//          CriteriaOperator criteria = CriteriaOperator.Parse("Name != 'PartyName'");

            // партии
            CriteriaOperator criteria1 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(properties["PartyName"]),
                BinaryOperatorType.Equal);

            _Parties.Filter = criteria1;
            if (_Parties.Count == 0)
                _Parties.Filter = null;

            PartyObj party = _nuow.GetNestedObject(_Parties[0]) as PartyObj;

            _theDelegate.idParty = party;

            // фракции
            CriteriaOperator criteria2 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(properties["FractionName"]),
                BinaryOperatorType.Equal);

            _Fractions.Filter = criteria2;
            if (_Fractions.Count == 0)
                _Fractions.Filter = null;

            FractionObj fraction = _nuow.GetNestedObject(_Fractions[0]) as FractionObj;

            _theDelegate.idFraction = fraction;

            // регионы
            CriteriaOperator criteria3 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(properties["RegionName"]),
                BinaryOperatorType.Equal);

            _Regions.Filter = criteria3;
            if (_Regions.Count == 0)
                _Regions.Filter = null;

            RegionObj region = _nuow.GetNestedObject(_Regions[0]) as RegionObj;

            _theDelegate.idRegion = region;

            // место в зале

            CriteriaOperator criteria4 = new BinaryOperator(
                new OperandProperty("MicNum"), new OperandValue(properties["MicNum"]),
                BinaryOperatorType.Equal);

            _Seats.Filter = criteria4;
            if (_Seats.Count == 0)
                _Seats.Filter = null;

            SeatObj seat = _nuow.GetNestedObject(_Seats[0]) as SeatObj;

            _theDelegate.idSeat = seat;

            _nuow.CommitChanges();

            _View.Properties_FinishEdit();
        }

        public void CancelChanges()
        {
            _nuow.RollbackTransaction();
            _View.Properties_FinishEdit();
        }

        public int AddNewDelegate()
        {
            DelegateObj newDelegate = new DelegateObj();

            PartyObj party = (PartyObj)_Parties[0];
            FractionObj fraction = (FractionObj)_Fractions[0];
            RegionObj region = (RegionObj)_Regions[0];
            SeatObj seat = (SeatObj)_Seats[0];

            newDelegate.idParty = party;
            newDelegate.idFraction = fraction;
            newDelegate.idRegion = region;
            newDelegate.idSeat = seat;

            newDelegate.LastName = "Новый";
            newDelegate.FirstName = "";
            newDelegate.SecondName = "";

            int maxid = 0;
            _Delegates.Filter = null;
            if (_Delegates.Count > 0)
            {
                DelegateObj last = (DelegateObj)_Delegates[_Delegates.Count - 1];
                maxid = last.id;
            }

            newDelegate.id = maxid + 1;
            if (Mode == DelegatesFormMode.Select)
            {
                if (_filterArray != null)
                {
                    _filterArray.Add(newDelegate.id);
                    CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
                    _Delegates.Filter = cr;
                }
            }

            int i = _Delegates.Add(newDelegate);
            newDelegate.Save();
            return i;
        }

        public void RemoveDelegate()
        {
            if (_theDelegate == null)
                return;

//          DelegateObj o = _Delegates[dataNavigator1.Position] as DelegateObj;
            _Delegates.Remove(_theDelegate);
        }

        public string GetRegionNumByValue(string RegionName)
        {
            // регионы
            CriteriaOperator criteria3 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(RegionName),
                BinaryOperatorType.Equal);

            _Regions.Filter = criteria3;
            if (_Regions.Count == 0)
                _Regions.Filter = null;

            RegionObj region = _nuow.GetNestedObject(_Regions[0]) as RegionObj;
            return region.Number;
        }

        public DelegateObj GetSelectedItem(List<int> filterlist)
        {
//          _View = new DelegatesForm(this);

            List<int> list = new List<int>();
            foreach (DelegateObj o in _Delegates )
            {
                list.Add(o.id);
            }

            _filterArray = new List<int>(list.Except<int>(filterlist).ToArray<int>());

            CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
            _Delegates.Filter = cr;
            _Mode = DelegatesFormMode.Select;

            DialogResult dr = _View.ShowView();
            if (dr == DialogResult.OK)
            {
                return _theDelegate;
            }

            return null;
        }

        public void ShowView()
        {
            _View.ShowView();
        }

        public DelegateObj GetSelectedItemFromList(List<int> selectedList)
        {
//          _View = new DelegatesForm(this);


            CriteriaOperator cr = new InOperator("id", selectedList.ToArray());
            _Delegates.Filter = cr;
            _Mode = DelegatesFormMode.SeatSelect; // включить режим выбора депутатов без мест

            DialogResult dr = _View.ShowView();
            if (dr == DialogResult.OK)
            {
                return _theDelegate;
            }

            return null;
        }

    }
}
