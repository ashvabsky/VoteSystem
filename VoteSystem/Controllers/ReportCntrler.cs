﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using VoteSystem.DataLayer;
using Excel = Microsoft.Office.Interop.Excel;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public class ReportCntrler
    {

        IReportForm _View;
        IReportQuest _QView;
        IReportThemes _TView;
        IReportDelegate _DView;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        public XPCollection<QuestionObj> _Questions; // вопросы сессии
        public XPCollection<StatementObj> _statements; // темы сессии
        public XPCollection<TaskKindObj> _Kinds; // список видов вопросов 

        public XPCollection<ReportKindObj> _reports; // список видов вопросов 
        public ReportKindObj _theReport; // текущий выбранный отчет из списка

        public List<TaskKindObj> _selKinds = new List<TaskKindObj>();

        public QuestionObj _TheQuestion; // вопрос по которому выводится отчет , если вызвана форма QView
        public StatementObj _TheStatement; // тема выступлений, по которой выводится отчет , если вызвана форма TView
        public DelegateObj _TheDelegate; // Депутат, по которому получаем отчет
        public SessionObj _TheSession; // Сессия, по которой получаем отчет по депутату
        public XPCollection<DelegateObj> _delegateList; // Список депутатов, по которым получаем отчет

        public string _ResultFile;
        MainCntrler _mainController;

        private Excel.Application m_objExcel;
        private Excel.Workbook m_objBook;
        private Excel.Worksheet m_objSheet;
        private Excel.Range m_objRng;
        private string m_fldName;

        private string _reportFile = "";

        public ReportCntrler(MainCntrler mcontroller)
        {
            _mainController = mcontroller;
            _reports = new XPCollection<ReportKindObj>();
            _ResultFile = "";
        }

        public void AssignView(IReportForm viewInterface, IReportQuest viewReportQuest, IReportThemes themereportView, IReportDelegate delegateReportView )
        {
            _View = viewInterface;
            _QView = viewReportQuest;
            _TView = themereportView;
            _DView = delegateReportView;
            _msgForm = _View.GetShowInterface();
        }

        public void Init()
        {
        }

        public ReportKindObj GetReportByCodeName(string CodeName)
        {
            ReportKindObj res = null;
            foreach (ReportKindObj r in _reports)
            {
                if (r.CodeName == CodeName)
                {
                    res = r;
                    break;
                }
            }

            return res;
        }

        public void ShowView()
        {
            CriteriaOperator cr = CriteriaOperator.Parse("CodeName = 'report_quest_table' OR CodeName = 'report_questions' OR CodeName = 'report_quest_delegates' OR CodeName = 'report_theme' OR CodeName = 'report_registration'");
            _reports.Criteria = cr;

            _theReport = GetReportByCodeName("report_quest_table");

            _ResultFile = GenerateResultFile();
            _View.ShowView();
        }


        public void ShowReport_Q(QuestionObj q)
        {
            _TheQuestion = q;
//          _QView.ShowView();

            CriteriaOperator cr = CriteriaOperator.Parse("CodeName = 'report_question' OR CodeName = 'report_question_delegates'");
            _reports.Criteria = cr;

            _theReport = GetReportByCodeName("report_question");

            _ResultFile = Q_GenerateResultFile();

            _View.ShowView();
        }

        public void ShowReport_T()
        {
            _TheStatement = null;

//          _TView.ShowView();
            CriteriaOperator cr = CriteriaOperator.Parse("CodeName = 'report_theme'");
            _reports.Criteria = cr;

            _ResultFile = GenerateResultFile_T();

            _theReport = GetReportByCodeName("report_theme");

            _View.ShowView();
        }

        public void ShowReport_T(StatementObj s)
        {
            _TheStatement = s;

            CriteriaOperator cr = CriteriaOperator.Parse("CodeName = 'report_theme'");
            _reports.Criteria = cr;

            _theReport = GetReportByCodeName("report_theme");
            _ResultFile = GenerateResultFile_T();

            _View.ShowView();
//          _TView.ShowView();
        }


        public void ShowReport_D(DelegateObj d, SessionObj s, XPCollection<DelegateObj> delegateList)
        {
            _TheDelegate = d;
            _TheSession = s;
            _delegateList = delegateList;

            _DView.ShowView();
        }

        public void ShowReport_D2(bool IsCurrSession)
        {
            if (IsCurrSession)
            {
                _TheSession = DataModel.TheSession;
            }
            else
            {
                _TheSession = null;
            }

            CriteriaOperator cr = CriteriaOperator.Parse("CodeName = 'report_delegate'");
            _reports.Criteria = cr;

            _theReport = GetReportByCodeName("report_delegate");
            _ResultFile = GenerateResultFile_D();

            _View.ShowView();
        }

        public string GenerateResultFile()
        {
            string s = "отчет_" + DateTime.Now.ToString("dd_MM_yy") + ".xls";
            return s;
        }

        public string GenerateResultFile_T()
        {
            string s = "отчет_выступления_" + DateTime.Now.ToString("dd_MM_yy") + ".xls";
            if (_TheStatement != null)
            {
                s = string.Format("тема_{0}", _TheStatement.Caption);
                s = s.Replace("\"", "");
                if (s.Length > 200)
                    s = s.Substring(0, 200);
                s = s + ".xls";
            }

            return s;
        }

        public string Q_GenerateResultFile()
        {
            string s = string.Format("вопрос_{0}", _TheQuestion.Name);
            s = s.Replace("\"", "");
            if (s.Length > 200)
                s = s.Substring(0, 200);

            s = s + ".xls";

            return s;
        }

        public string GenerateResultFile_D()
        {
            string s = "отчет_депутат_" + DateTime.Now.ToString("dd_MM_yy") + ".xls";
            if (_TheDelegate != null)
            {
                s = string.Format("отчет_депутат_{0}", _TheDelegate.FullName);
                s = s.Replace("\"", "");
                if (s.Length > 200)
                    s = s.Substring(0, 200);
                s = s + ".xls";
            }

            return s;
        }

        public void OnInitForm()
        {

            _View.InitControls(DataModel.TheSettingsReport, _theReport, _reports);
        }

        public void OnSelectReport(string codename)
        {
            _theReport = GetReportByCodeName(codename);

            if (_theReport != null)
                _View.InitControls(DataModel.TheSettingsReport, _theReport, _reports);
        }

        public void Q_OnInitForm()
        {

            _QView.InitControls(DataModel.TheSettingsReport);
        }

        public void T_OnInitForm()
        {

            _TView.InitControls(DataModel.TheSettingsReport);
        }

        public void D_OnInitForm()
        {

            _DView.InitControls(DataModel.TheSettingsReport);
        }


        private void CreateExcelApp()
        {
            m_objExcel = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            m_objExcel = new Excel.Application();
            m_objExcel.ScreenUpdating = true;
            m_objExcel.Caption = "Протокол голосований";
        }

        private bool InitExelApp(string TemplateFile)
        {
            if (m_objExcel != null)
            {
                try
                {
                    m_objExcel.Visible = false;
                    m_objBook.Close(false, Type.Missing, Type.Missing);
                    //                  m_objExcel.Quit();
                }
                catch (Exception ex)
                {
                    CreateExcelApp();
                }
            }
            else
                CreateExcelApp();


            try
            {
                m_objBook = (m_objExcel.Workbooks.Open(TemplateFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                dynamic activeSheet = m_objBook.ActiveSheet;
                m_objSheet = activeSheet;
            }
            catch (Exception ex)
            {
                string err = "Невозможно открыть шаблон для отчета.\n";
                err = err + ex.Message;
                _msgForm.ShowError(err);
                return false;
            }


            System.IO.FileInfo f;
            try
            {
                f = new System.IO.FileInfo(_reportFile);
                if (f.Directory.Exists == false)
                {
                    string err = "Не правильно указана папка для сохранения результатов.\n";
                    _msgForm.ShowError(err);
                    return false;
                }
            }
            catch (Exception ex)
            {
                string err = "Ошибка в определении имени файла отчета.\n";
                err = err + ex.Message;
                _msgForm.ShowError(err);
                return false;
            }

            try
            {
                f.Delete();
            }
            catch (Exception ex)
            {
                string err = "Невозможно удалить файл отчета.\n";
                err = err + ex.Message;
                _msgForm.ShowError(err);
                return false;
            }

            /*
                            f = new System.IO.FileInfo("C:\\Разработка\\Системы голосования\\TestExcel\\report2.xls");
                            f.CopyTo("C:\\Разработка\\Системы голосования\\TestExcel\\report.xls");
                            //Start Excel and get Application object.
             */


            m_objExcel.ScreenUpdating = true;

            //Get a new workbook.
            //              string CurrDirectory = Logic.CurrPath;


            m_objExcel.DefaultSaveFormat = Excel.XlFileFormat.xlOpenXMLWorkbook;

            return true;
        }

        public void PrintElem(string name, string value)
        {
            m_fldName = name;
            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (m_objRng != null)
                m_objRng.Cells[1, 1] = value;
            m_fldName = "";
        }

        int m_RegQnty = 0;
        public void PrintReport()
        {
            if (!InitExelApp(_theReport.TemplateFile))
                return;

            try
            {
                m_fldName = "";

                m_RegQnty = (DataModel.TheSession.RegQnty != 0 ? DataModel.TheSession.RegQnty : DataModel.GetRegDelegatesQnty());

                switch (_theReport.CodeName)
                {
                    case "report_quest_table": PrintReportTable(); break;
                    case "report_questions": PrintReportQuestions(); break;
                    case "report_registration": PrintReportRegistration(); break;
                    case "report_quest_delegates": PrintReportQuestionsDelegates(); break;
                    case "report_theme": T_PrintReport(); break;

                    case "report_question": PrintReportQuestion(); break;
                    case "report_question_delegates": PrintReportQuestion_Delegates(); break;

                    case "report_delegate": PrintReportDelegates(); break;
                        

                }

                m_objBook.SaveCopyAs(_reportFile);

                m_objBook.Close(false, Type.Missing, Type.Missing);

                m_objBook = m_objExcel.Workbooks.Open(_reportFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                m_objSheet = m_objBook.ActiveSheet;

                m_objExcel.Visible = true;
            }
            catch (Exception theException)
            {
                string errMsg = "";
                if (m_fldName != "")
                {
                    errMsg = "Неверный формат файла шаблона.\nВ шаблоне отчета отсутствует поле " + m_fldName + "\n";
                }

                errMsg = errMsg + "Excell error: ";
                errMsg = errMsg + theException.Message;

                _msgForm.ShowError(errMsg);
            }

            finally
            {
            }
        }

        public void PrintReportTable()
        {

            CriteriaOperator cr2 = CriteriaOperator.Parse("idTask.State >= 1");

            _Questions = new XPCollection<QuestionObj>(DataModel.Questions, cr2);

            Excel.Range newRange;
            Excel.Range origRange;

            m_fldName = "<finish_section>";
            Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (finishRow != null)
                finishRow.Clear();

            m_fldName = "";

            if (_Questions == null || _Questions.Count == 0)
                throw (new Exception("Невозможно создать отчет. Отсутствует список вопросов"));

            int itemCnt = _Questions.Count;


            // дата и время
            PrintElem("<дата>", DataModel.TheSession.RegDate.ToString("dd.MM.yyyy H:mm"));
            PrintElem("<повестка дня>", DataModel.TheSession.Code);
            PrintElem("<кол-во участников>", m_RegQnty.ToString());

           // заполняем таблицу результатов
            m_fldName = "<table_row>";
            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (m_objRng != null)
                m_objRng.Clear();

            m_fldName = "";

            foreach (QuestionObj Item in _Questions)
            {

                m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                origRange.Copy(destRange);

                //                      newRange = m_objRng2.get_Offset(1, 0);


                m_objRng.Cells[0, 2] = Item.QueueNum; // № вопроса
                m_objRng.Cells[0, 3] = Item.Name; // название вопроса
                m_objRng.Cells[0, 4] = Item.idKind.Name; // вид вопроса
                m_objRng.Cells[0, 5] = Item.idResult.idVoteResult.AnsQnty1.ToString(); // кол-во за
                m_objRng.Cells[0, 6] = Item.idResult.idVoteResult.AnsQnty3.ToString(); // кол-во против
                m_objRng.Cells[0, 7] = Item.idResult.idVoteResult.AnsQnty2.ToString(); // кол-во возд
                int n = Item.idResult.idVoteResult.AvailableQnty - Item.idResult.idVoteResult.AnsQnty1 - Item.idResult.idVoteResult.AnsQnty2 - Item.idResult.idVoteResult.AnsQnty3;
                m_objRng.Cells[0, 8] = n.ToString(); // не голосовали
                m_objRng.Cells[0, 9] = Item.idResult.idResultValue.Name; // решение
                m_objRng.Cells[0, 10] = Item.idResult.VoteDateTime.ToString("dd.MM.yyyy H:mm"); // решение
            }
        }

        private void PrintReportQuestions()
        {
            CriteriaOperator cr2 = CriteriaOperator.Parse("idTask.State >= 1");

            _Questions = new XPCollection<QuestionObj>(DataModel.Questions, cr2);

/*
            // убираем исключаемые вопросы
            List<QuestionObj> excluded = new List<QuestionObj>();
            foreach (QuestionObj q in _Questions)
            {
                if (_selKinds.Contains(q.idKind))
                    excluded.Add(q);
            }

            foreach (QuestionObj q in excluded)
            {
                _Questions.Remove(q);
            }
*/

                Excel.Range newRange;
                Excel.Range origRange;

                m_fldName = "<finish_section>";
                Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (finishRow != null)
                    finishRow.Clear();

                m_fldName = "";

                if (_Questions == null || _Questions.Count == 0)
                    throw (new Exception("Невозможно создать отчет. Отсутствует список вопросов"));

                int itemCnt = _Questions.Count;

                for (int i = 1; i <= itemCnt; i++)
                {
                    m_fldName = "<item_section_begin>";
                    m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    m_fldName = "<item_section_end>";
                    Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    if (i < itemCnt)
                    {
                        origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng2.Cells[1, 1]).Rows;
                        newRange = m_objRng2.get_Offset(1, 0);

                        for (int y = 1; y <= origRange.Rows.Count; y++)
                        {
                            finishRow.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        }


                        origRange.Copy(newRange.EntireRow);
                    }

                    m_objRng.Clear();
                    m_objRng2.Clear();
                }


                // дата и время
//              PrintElem("<дата>", DateTime.Now.ToString("dd.MM.yyyy H:mm"));

                // название повестки
                PrintElem("<повестка дня>", DataModel.TheSession.Code);
                PrintElem("<кол-во участников>", m_RegQnty.ToString());
                if (DataModel.TheSession.IsQuorum == true)
                    PrintElem("<наличие кворума>", "есть");
                else
                    PrintElem("<наличие кворума>", "нет");

                PrintElem("<кол-во вопросов>", itemCnt.ToString());

                foreach(QuestionObj Item in _Questions)
                {
/*
                    if (_selKinds.Contains(Item.idTask.idKind))
                        continue; // пропускаем исключаемые вопросы
*/
                    PrintElem("<вопрос>", Item.Name);
                    PrintElem("<дата и время>", Item.idResult.idVoteResult.VoteDateTime.ToString("dd.MM.yyyy H:mm"));
                    PrintElem("<вид вопроса>", Item.idKind.Name);
                    PrintElem("<ответ за>", Item.idResult.idVoteResult.AnsQnty1.ToString());
                    PrintElem("<ответ против>", Item.idResult.idVoteResult.AnsQnty3.ToString());
                    PrintElem("<ответ воздержался>", Item.idResult.idVoteResult.AnsQnty2.ToString());

                    int n = Item.idResult.idVoteResult.AvailableQnty - Item.idResult.idVoteResult.AnsQnty1 - Item.idResult.idVoteResult.AnsQnty2 - Item.idResult.idVoteResult.AnsQnty3;
                    PrintElem("<не голосовали>", n.ToString());
                    PrintElem("<принятие решения>", Item.idResult.idResultValue.Name);

                    // заполняем таблицу результатов
                    m_fldName = "<table_row>";
                    m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                    if (m_objRng != null)
                        m_objRng.Clear();

                }
        }


        private void PrintReportQuestionsDelegates()
        {

            CriteriaOperator cr2 = CriteriaOperator.Parse("idTask.State >= 1");

            _Questions = new XPCollection<QuestionObj>(DataModel.Questions, cr2);

            /*
                        // убираем исключаемые вопросы
                        List<QuestionObj> excluded = new List<QuestionObj>();
                        foreach (QuestionObj q in _Questions)
                        {
                            if (_selKinds.Contains(q.idKind))
                                excluded.Add(q);
                        }

                        foreach (QuestionObj q in excluded)
                        {
                            _Questions.Remove(q);
                        }
            */

            Excel.Range newRange;
            Excel.Range origRange;

            m_fldName = "<finish_section>";
            Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (finishRow != null)
                finishRow.Clear();

            m_fldName = "";

            if (_Questions == null || _Questions.Count == 0)
                throw (new Exception("Невозможно создать отчет. Отсутствует список вопросов"));

            int itemCnt = _Questions.Count;

            for (int i = 1; i <= itemCnt; i++)
            {
                m_fldName = "<item_section_begin>";
                m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                m_fldName = "<item_section_end>";
                Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                if (i < itemCnt)
                {
                    origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng2.Cells[1, 1]).Rows;
                    newRange = m_objRng2.get_Offset(1, 0);

                    for (int y = 1; y <= origRange.Rows.Count; y++)
                    {
                        finishRow.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                    }


                    origRange.Copy(newRange.EntireRow);
                }

                m_objRng.Clear();
                m_objRng2.Clear();
            }


            // дата и время
            PrintElem("<дата>", DataModel.TheSession.RegDate.ToString("dd.MM.yyyy H:mm"));

            // название повестки
            PrintElem("<повестка дня>", DataModel.TheSession.Code);
            PrintElem("<кол-во участников>", m_RegQnty.ToString());
            if (DataModel.TheSession.IsQuorum == true)
                PrintElem("<наличие кворума>", "есть");
            else
                PrintElem("<наличие кворума>", "нет");

            PrintElem("<кол-во вопросов>", itemCnt.ToString());

            foreach (QuestionObj Item in _Questions)
            {
                /*
                                    if (_selKinds.Contains(Item.idTask.idKind))
                                        continue; // пропускаем исключаемые вопросы
                */
                PrintElem("<вопрос>", Item.Name);
                PrintElem("<дата и время>", Item.idResult.VoteDateTime.ToString("dd.MM.yyyy H:mm"));
                PrintElem("<вид вопроса>", Item.idKind.Name);
                PrintElem("<ответ за>", Item.idResult.idVoteResult.AnsQnty1.ToString());
                PrintElem("<ответ против>", Item.idResult.idVoteResult.AnsQnty3.ToString());
                PrintElem("<ответ воздержался>", Item.idResult.idVoteResult.AnsQnty2.ToString());

                int n = Item.idResult.idVoteResult.AvailableQnty - Item.idResult.idVoteResult.AnsQnty1 - Item.idResult.idVoteResult.AnsQnty2 - Item.idResult.idVoteResult.AnsQnty3;
                PrintElem("<не голосовали>", n.ToString());
                PrintElem("<принятие решения>", Item.idResult.idResultValue.Name);

                // заполняем таблицу результатов
                m_fldName = "<table_row>";
                m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (m_objRng != null)
                    m_objRng.Clear();

                // вывести список результатов по депутатам
                XPCollection<VoteDetailObj> votedetails = DataModel.GetVoteDetails(Item);

                foreach (VoteDetailObj vdo in votedetails)
                {
                    /*
                                            if (DataModel.TheSettingsReport.Report_ShowNonVotes == false)
                                            {
                                                if (vdo.idAnswer.CodeName == "None")
                                                    continue;
                                            }
                     */
                    //                      origRange = m_objRng.EntireRow;

                    m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                    origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                    Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                    origRange.Copy(destRange);

                    //                      newRange = m_objRng2.get_Offset(1, 0);


                    m_objRng.Cells[0, 2] = vdo.idSeat.MicNum;
                    m_objRng.Cells[0, 3] = vdo.idDelegate.idDelegate.FullName;
                    m_objRng.Cells[0, 4] = vdo.idAnswer.Name;

                }
            }
        }


        public void PrintReportQuestion()
        {
                Excel.Range newRange;
                Excel.Range origRange;

                m_fldName = "<item_section_begin>";
                m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                m_fldName = "<item_section_end>";
                Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                if (m_objRng != null)
                    m_objRng.Clear();
                if (m_objRng2 != null)
                    m_objRng2.Clear();

                m_fldName = "<finish_section>";
                Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (finishRow != null)
                    finishRow.Clear();

                m_fldName = "";
                int itemCnt = 1;

                if (_TheQuestion == null || _TheQuestion.id <= 0)
                    throw (new Exception("Невозможно создать отчет. Не указан вопрос, по которому выводится отчет"));

                PrintElem("<дата>", DataModel.TheSession.RegDate.ToString("dd.MM.yyyy"));
                PrintElem("<повестка дня>", DataModel.TheSession.Code);
                PrintElem("<кол-во участников>", m_RegQnty.ToString());

                if (DataModel.TheSession.IsQuorum == true)
                    PrintElem("<наличие кворума>", "есть");
                else
                    PrintElem("<наличие кворума>", "нет");

                PrintElem("<кол-во вопросов>", itemCnt.ToString());

                QuestionObj Item = _TheQuestion;
                {
                    PrintElem("<вопрос>", Item.Name);
                    PrintElem("<вид вопроса>", Item.idKind.Name);
                    PrintElem("<ответ за>", Item.idResult.idVoteResult.AnsQnty1.ToString());
                    PrintElem("<ответ против>", Item.idResult.idVoteResult.AnsQnty3.ToString());
                    PrintElem("<ответ воздержался>", Item.idResult.idVoteResult.AnsQnty2.ToString());
                    int n = Item.idResult.idVoteResult.AvailableQnty - Item.idResult.idVoteResult.AnsQnty1 - Item.idResult.idVoteResult.AnsQnty2 - Item.idResult.idVoteResult.AnsQnty3;
                    PrintElem("<не голосовали>", n.ToString());
                    PrintElem("<принятие решения>", Item.idResult.idResultValue.Name);
                    

                    // заполняем таблицу результатов
                    m_fldName = "<table_row>";
                    m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                    if (m_objRng != null)
                        m_objRng.Clear();

                    m_fldName = "";
                }

        }

        public void PrintReportQuestion_Delegates()
        {
            Excel.Range newRange;
            Excel.Range origRange;

            m_fldName = "<item_section_begin>";
            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

            m_fldName = "<item_section_end>";
            Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

            if (m_objRng != null)
                m_objRng.Clear();
            if (m_objRng2 != null)
                m_objRng2.Clear();

            m_fldName = "<finish_section>";
            Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (finishRow != null)
                finishRow.Clear();

            m_fldName = "";
            int itemCnt = 1;

            if (_TheQuestion == null || _TheQuestion.id <= 0)
                throw (new Exception("Невозможно создать отчет. Не указан вопрос, по которому выводится отчет"));

            PrintElem("<дата>", DataModel.TheSession.RegDate.ToString("dd.MM.yyyy"));
            PrintElem("<повестка дня>", DataModel.TheSession.Code);
            PrintElem("<кол-во участников>", m_RegQnty.ToString());

            if (DataModel.TheSession.IsQuorum == true)
                PrintElem("<наличие кворума>", "есть");
            else
                PrintElem("<наличие кворума>", "нет");

            PrintElem("<кол-во вопросов>", itemCnt.ToString());

            QuestionObj Item = _TheQuestion;
            {
                PrintElem("<вопрос>", Item.Name);
                PrintElem("<вид вопроса>", Item.idKind.Name);
                PrintElem("<ответ за>", Item.idResult.idVoteResult.AnsQnty1.ToString());
                PrintElem("<ответ против>", Item.idResult.idVoteResult.AnsQnty3.ToString());
                PrintElem("<ответ воздержался>", Item.idResult.idVoteResult.AnsQnty2.ToString());
                int n = Item.idResult.idVoteResult.AvailableQnty - Item.idResult.idVoteResult.AnsQnty1 - Item.idResult.idVoteResult.AnsQnty2 - Item.idResult.idVoteResult.AnsQnty3;
                PrintElem("<не голосовали>", n.ToString());
                PrintElem("<принятие решения>", Item.idResult.idResultValue.Name);


                // заполняем таблицу результатов
                m_fldName = "<table_row>";
                m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (m_objRng != null)
                    m_objRng.Clear();

                m_fldName = "";

                // вывести список результатов по депутатам
                    XPCollection<VoteDetailObj> votedetails = DataModel.GetVoteDetails(Item);

                    foreach (VoteDetailObj vdo in votedetails)
                    {
                        if (DataModel.TheSettingsReport.Report_ShowNonVotes == false)
                        {
                            if (vdo.idAnswer.CodeName == "None")
                                continue;
                        }
                        //                      origRange = m_objRng.EntireRow;

                        m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                        Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                        origRange.Copy(destRange);

                        //                      newRange = m_objRng2.get_Offset(1, 0);


                        m_objRng.Cells[0, 2] = vdo.idSeat.MicNum;
                        m_objRng.Cells[0, 3] = vdo.idDelegate.idDelegate.FullName;
                        m_objRng.Cells[0, 4] = vdo.idAnswer.Name;

                    }
            }

        }

        public void PrintReportRegistration()
        {
            Excel.Range newRange;
            Excel.Range origRange;

            m_fldName = "<item_section_begin>";
            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

            m_fldName = "<item_section_end>";
            Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

            if (m_objRng != null)
                m_objRng.Clear();
            if (m_objRng2 != null)
                m_objRng2.Clear();

            m_fldName = "<finish_section>";
            Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (finishRow != null)
                finishRow.Clear();

            m_fldName = "";
            int itemCnt = 1;

            // вывести список результатов по депутатам
            XPCollection<SesDelegateObj> sesdelegates = new XPCollection<SesDelegateObj>(DataModel.SesDelegates);

            SortingCollection s = new SortingCollection();
            SortProperty sp1 = new SortProperty("idDelegate.idFraction.Name", DevExpress.Xpo.DB.SortingDirection.Ascending);
            SortProperty sp2 = new SortProperty("idDelegate.FullName", DevExpress.Xpo.DB.SortingDirection.Ascending);
            s.Add(sp1);
            s.Add(sp2);

            sesdelegates.Sorting.Add(s);

            CriteriaOperator cr;
            cr = CriteriaOperator.Parse("IsRegistered = ?", true);
            sesdelegates.Filter = cr;
            int regqnty = sesdelegates.Count;
            sesdelegates.Filter = null;

            if (DataModel.TheSession.RegDate > DateTime.MinValue)
                PrintElem("<дата>", DataModel.TheSession.RegDate.ToString("dd.MM.yyyy H:mm"));
            else
                PrintElem("<дата>", DataModel.TheSession.StartDate.ToString("dd.MM.yyyy H:mm"));

            PrintElem("<сессия>", DataModel.TheSession.Code);
            PrintElem("<кол-во участников>", m_RegQnty.ToString());
            if (DataModel.TheSession.IsQuorum == true)
                PrintElem("<наличие кворума>", "есть");
            else
                PrintElem("<наличие кворума>", "нет");
            PrintElem("<кол-во вопросов>", itemCnt.ToString());



            // заполняем таблицу результатов
            m_fldName = "<table_row>";
            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
            if (m_objRng != null)
                m_objRng.Clear();

            m_fldName = "";


            foreach (SesDelegateObj sd in sesdelegates)
            {
                if (sd.idDelegate.idType.IsVoteRight == false)
                    continue;

                m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                origRange.Copy(destRange);

                //                      newRange = m_objRng2.get_Offset(1, 0);


                m_objRng.Cells[0, 2] = sd.idDelegate.idFraction.Name;
                m_objRng.Cells[0, 3] = sd.idDelegate.FullName;
                string res = "";
                if (sd.IsRegistered == true)
                    res = "есть";
                else
                    res = "нет";

                m_objRng.Cells[0, 4] = res;

            }

        }



        private void T_PrintReport()
        {
            CriteriaOperator cr2;
            cr2 = CriteriaOperator.Parse("State == 1 OR State == 2");
            _statements = new XPCollection<StatementObj>(DataModel.SesStatements, cr2);

                Excel.Range newRange;
                Excel.Range origRange;

                m_fldName = "<finish_section>";
                Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (finishRow != null)
                    finishRow.Clear();


                if (_statements == null || _statements.Count == 0)
                    throw (new Exception("Невозможно создать отчет. Отсутствует список тем"));

                int itemCnt = 1;
                if (_TheStatement == null)
                {
                    itemCnt = _statements.Count;
                }

                for (int i = 1; i <= itemCnt; i++)
                {
                    m_fldName = "<item_section_begin>";
                    m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    m_fldName = "<item_section_end>";
                    Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    if (i < itemCnt)
                    {
                        origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng2.Cells[1, 1]).Rows;
                        newRange = m_objRng2.get_Offset(1, 0);

                        for (int y = 1; y <= origRange.Rows.Count; y++)
                        {
                            finishRow.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        }


                        origRange.Copy(newRange.EntireRow);
                    }

                    m_objRng.Clear();
                    m_objRng2.Clear();
                }


                PrintElem("<дата>", DateTime.Now.ToString("dd.MM.yyyy H:mm"));
                PrintElem("<повестка дня>", DataModel.TheSession.Code);
                PrintElem("<кол-во участников>", DataModel.GetDelegatesQnty_Total().ToString());
                PrintElem("<кол-во тем>", itemCnt.ToString());

                if (_TheStatement != null)
                {
                    CriteriaOperator cr = CriteriaOperator.Parse("id = ?", _TheStatement.id);
                    _statements.Filter = cr;
                }

                foreach (StatementObj Item in _statements)
                {
                    PrintElem("<тема>", Item.Caption);
                    PrintElem("<ппд>", Item.idTask.CaptionItem);
                    PrintElem("<тема_начало>", Item.DateTimeStart.ToLongDateString() + " " + Item.DateTimeStart.ToShortTimeString());
                    PrintElem("<кол-во_выступили>", Item.DelegateQnty_F);
                    PrintElem("<время_общее>", Item.SpeechTimeTotal_F);
                    PrintElem("<время_среднее>", Item.AvrgTime_F);

                    int n = 0;

                    CriteriaOperator criteria = CriteriaOperator.Parse("idStatement.id = ?", Item.id);

                    using (XPCollection<DelegateStatementObj> stdetails = new XPCollection<DelegateStatementObj>(Session.DefaultSession, criteria))
                    {
                        SortingCollection s = new SortingCollection();
                        SortProperty sp1 = new SortProperty("State", DevExpress.Xpo.DB.SortingDirection.Descending);
                        SortProperty sp2 = new SortProperty("QNum", DevExpress.Xpo.DB.SortingDirection.Ascending);
                        s.Add(sp1);
                        s.Add(sp2);
                        stdetails.Sorting.Add(s);

                        foreach (DelegateStatementObj st in stdetails)
                        {
                            if (st.State == 0)
                                n = n + 1;
                        }

                        PrintElem("<кол-во_не выступили>", n.ToString());


                        // заполняем таблицу результатов
                        m_fldName = "<table_row>";
                        m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                        if (m_objRng != null)
                            m_objRng.Clear();

                        m_fldName = "";

                        if (DataModel.TheSettingsReport.Report_ShowDetails_T == true)
                        {

                            foreach (DelegateStatementObj detail in stdetails)
                            {
                                if (DataModel.TheSettingsReport.Report_ShowNonSpeeches == false)
                                {
                                    if (detail.State == 0)
                                        continue;
                                }
                                //                      origRange = m_objRng.EntireRow;

                                m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                                origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                                Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                                origRange.Copy(destRange);

                                //                      newRange = m_objRng2.get_Offset(1, 0);


                                m_objRng.Cells[0, 2] = detail.idSeat.MicNum;
                                m_objRng.Cells[0, 3] = detail.idDelegate.FullName;
                                m_objRng.Cells[0, 4] = detail.SpeechDurationTime_F;

                            }
                        }
                    }
                }
        }




        public void PrintReportDelegates()
        {
            if (_TheDelegate == null && _delegateList == null)
                return;

            if (_TheDelegate != null)
            {
                _delegateList = new XPCollection<DelegateObj>(Session.DefaultSession, false);
                _delegateList.Add(_TheDelegate);
            }


            CriteriaOperator cr = null;
            XPCollection<SessionObj> sessions;

            if (_TheSession != null)
            {
                cr = CriteriaOperator.Parse("id = ?", _TheSession.id);
                sessions = new XPCollection<SessionObj>(Session.DefaultSession, false);
                sessions.Add(_TheSession);
            }
            else
            {
                sessions = new XPCollection<SessionObj>(Session.DefaultSession);
                sessions.Criteria = DataModel.Sessions.Criteria;
            }

                Excel.Range newRange;
                Excel.Range origRange;

                m_fldName = "<finish_section>";
                Excel.Range finishRow = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                if (finishRow != null)
                    finishRow.Clear();

                m_fldName = "";


                int itemCnt = sessions.Count * _delegateList.Count;

                for (int i = 1; i <= itemCnt; i++)
                {
                    m_fldName = "<item_section_begin>";
                    m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    m_fldName = "<item_section_end>";
                    Excel.Range m_objRng2 = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);

                    if (i < itemCnt)
                    {
                        origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng2.Cells[1, 1]).Rows;
                        newRange = m_objRng2.get_Offset(1, 0);

                        for (int y = 1; y <= origRange.Rows.Count; y++)
                        {
                            finishRow.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                        }


                        origRange.Copy(newRange.EntireRow);
                    }

                    m_objRng.Clear();
                    m_objRng2.Clear();
                }


                PrintElem("<дата>", DateTime.Now.ToString("dd.MM.yyyy H:mm"));

                DelegateObj dlgt = _TheDelegate;
                if (_TheDelegate == null)
                    dlgt = _delegateList[0];


                string name = "";
                if (_TheDelegate != null)
                    name = _TheDelegate.FullName; //"Сессия №28";
                else
                    name = "Отчет по группе депутатов "; //"Сессия №28";

                PrintElem("<депутат>", name);
                PrintElem("<фракция>", dlgt.idFraction.Name);

                m_fldName = "";


                foreach (DelegateObj dlg in _delegateList)
                {
                    itemCnt = 0;
                    foreach (SessionObj ses in sessions)
                    {
                        PrintElem("<депутат>", dlg.FullName);
                        PrintElem("<сессия>", ses.Code);
                        PrintElem("<сессия_начало>", ses.StartDate.ToString());

                        string s = "";
                        if ((ses.IsActive == false))
                            s = ses.FinishDate.ToString();
                        else
                            s = "";

                        PrintElem("<сессия_окончание>", s);

                        m_fldName = "";

                        int n = 0;
                        CriteriaOperator votecr;
                        votecr = CriteriaOperator.Parse("idDelegate.idDelegate.id = ? AND idQuestion.idSession.id = ?", dlgt.id, ses.id);

                        using (XPCollection<VoteDetailObj> votedetails = new XPCollection<VoteDetailObj>(Session.DefaultSession, votecr))
                        {
                            /*
                                                    SortingCollection s = new SortingCollection();
                                                    SortProperty sp1 = new SortProperty("State", DevExpress.Xpo.DB.SortingDirection.Descending);
                                                    SortProperty sp2 = new SortProperty("QNum", DevExpress.Xpo.DB.SortingDirection.Ascending);
                                                    s.Add(sp1);
                                                    s.Add(sp2);
                                                    stdetails.Sorting.Add(s);
                            */
                            // заполняем таблицу результатов
                            m_fldName = "<table_row>";
                            m_objRng = m_objSheet.Cells.Find(m_fldName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Excel.XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
                            if (m_objRng != null)
                                m_objRng.Clear();

                            m_fldName = "";

                            //                      if (DataModel.TheSettingsReport.Report_ShowDetails_T == true)
                            {

                                foreach (VoteDetailObj detail in votedetails)
                                {
                                    itemCnt++;

                                    m_objRng.EntireRow.Insert(Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing);
                                    origRange = m_objSheet.get_Range(m_objRng.Cells[1, 1], m_objRng.Cells[1, 4]);
                                    Excel.Range destRange = m_objSheet.get_Range(m_objRng.Cells[0, 1], m_objRng.Cells[0, 4]);
                                    origRange.Copy(destRange);

                                    //                      newRange = m_objRng2.get_Offset(1, 0);


                                    m_objRng.Cells[0, 2] = detail.idQuestion.idTask.ItemNum;
                                    m_objRng.Cells[0, 3] = detail.idQuestion.idKind.Name + ": " + detail.idQuestion.Name;
                                    m_objRng.Cells[0, 4] = detail.idAnswer.Name;
                                }
                            }
                        }
                    }
                }

                PrintElem("<кол-во_вопросов>", itemCnt.ToString());

                m_fldName = "";


        }

        public void OnClosing()
        {
            if (m_objExcel != null)
            {
                try
                {
                    m_objBook.Close(false, Type.Missing, Type.Missing);
                }
                catch
                {

                }
                finally
                {
                    m_objExcel.Quit();
                }
            }

            _Questions = null;
        }

        public void SaveParams(string txtResultPath, string txtReportFile)
        {
            SettingsReportObj settings = DataModel.TheSettingsReport;

            settings.ReportPath = txtResultPath;
            settings.Save();

            _reportFile = txtResultPath + txtReportFile;
            if (_reportFile.Length > 210)
            {
                _reportFile.Replace(".xls", "");
                _reportFile = _reportFile.Substring(0, 210);
                _reportFile = _reportFile + ".xls";
            }
        }

        public void SaveTemplateFile(string templateFile)
        {
            if (_theReport != null)
            {
                _theReport.TemplateFile = templateFile;
                _theReport.Save();
            }
        }

        public void Q_SaveParams(string txtTemplateFile, string txtResultPath, bool chkShowDetails, bool chkShowNonVotes, string txtReportFile)
        {
            SettingsReportObj settings = DataModel.TheSettingsReport;

            settings.TemplateFile_Q = txtTemplateFile;
            settings.ReportPath = txtResultPath;
            settings.Report_ShowDetails = chkShowDetails;
            settings.Report_ShowNonVotes = chkShowNonVotes;

            settings.Save();

            _reportFile = txtResultPath + txtReportFile;
            if (_reportFile.Length > 210)
            {
                _reportFile.Replace(".xls", "");
                _reportFile = _reportFile.Substring(0, 210);
                _reportFile = _reportFile + ".xls";
            }

        }

        public void T_SaveParams(string txtTemplateFile, string txtResultPath, bool chkShowDetails, bool chkShowNonSpeeches, string txtReportFile)
        {
            SettingsReportObj settings = DataModel.TheSettingsReport;

            settings.TemplateFile_T = txtTemplateFile;
            settings.ReportPath = txtResultPath;
            settings.Report_ShowDetails_T = chkShowDetails;
            settings.Report_ShowNonSpeeches = chkShowNonSpeeches;

            settings.Save();

            _reportFile = txtResultPath + txtReportFile;
            if (_reportFile.Length > 210)
            {
                _reportFile.Replace(".xls", "");
                _reportFile = _reportFile.Substring(0, 210);
                _reportFile = _reportFile + ".xls";
            }

        }
    }
}
