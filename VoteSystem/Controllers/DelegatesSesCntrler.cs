﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    // субконтроллер для управления страничкой главного окна со списком депутатов сессии (далее, основная страничка)
    public class DelegatesSesCntrler
    {

#region private секция данных
        SessionObj _TheSession;         // данные о текущей сессии
        SesDelegateObj _theDelegate;     // текущий (выбранный в данный момент) депутат из списка

        IDelegatesPage _View;           // интерфейс для управления основной страничкой 
        IMsgMethods _msgForm;           // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController;    // главный контроллер

        XPCollection<SesDelegateObj> _Delegates;        // список делегатов, присутствующих на форме

#endregion private секция данных

#region основные методы контроллеров
        public DelegatesSesCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IDelegatesPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void Init(ref XPCollection<SesDelegateObj> SesDelegates )
        {
            _TheSession = DataModel.TheSession;
            _Delegates = SesDelegates;
            _View.Init();
        }

#endregion основные методы контроллеров

#region public methods
        // Инициализировать основную страничку. 
        // Вызывается при переходе на основную страничку
        public void InitPage()  
        {
            DataModel.ReloadSesDelegates();
            _View.InitControls(ref _Delegates, DataModel.IsSpeechApp);
            UpdatePropData();
        }


#endregion public methods

#region public MainForm.DelegatesPage methods
        // вывести информацию о текущем депутате в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;
            _mainController.CurrentActivity = MainCntrler.Activities.None;

            _theDelegate = null;

            if (pos < 0)
                return;

            if (pos >= _Delegates.Count)
                return;

            _theDelegate = _Delegates[pos] as SesDelegateObj;

            // подготовить информацию для вывода на мониторы
            _mainController.CurrentActivity = MainCntrler.Activities.SelectDelegate;
             Platform.DelegateInfo info = new Platform.DelegateInfo();
             info.FIO = _theDelegate.idDelegate.FullName;
             info.FractionName = _theDelegate.idDelegate.idFraction.Name;
             info.PartyName = _theDelegate.idDelegate.idParty.Name;
             info.RegionName = _theDelegate.idDelegate.idRegion.Name;
             info.MicNum = _theDelegate.idSeat.MicNum;
             info.RowNum = _theDelegate.idSeat.RowNum;
             info.SeatNum = _theDelegate.idSeat.SeatNum;
             _mainController._MonitorInfo = info;

        }

        // удалить текущего депутата из списка
        public void RemoveDelegate()
        {
            if (_theDelegate == null)
                return;

            _theDelegate.idDelegate.isActive = false;
            _theDelegate.idDelegate.Save();

            try
            {
                _Delegates.Remove(_theDelegate);
            }
            catch
            {
                _msgForm.ShowError("Не удалось удалить запись!\n Нельзя исключить депутата, который участвовал в процедуре голосования в данной сессии.");
            }

            DataModel.ReloadSesDelegates(); // перезагрузить список, для актуальности
            UpdatePropData();
        }

        // удалить текущих депутатов из списка
        public void RemoveDelegates()
        {
            if (_theDelegate == null)
                return;

            _theDelegate.idDelegate.isActive = false;
            _theDelegate.idDelegate.Save();

            List<int> IDs = new List<int>();

            foreach (SesDelegateObj d in _Delegates)
            {
                IDs.Add(d.id);
            }

            foreach (int id in IDs)
            {
                SesDelegateObj d = _Delegates.Lookup(id);
                try
                {
                    _Delegates.Remove(d);
                }
                catch
                {
                    string err = string.Format("Не удалось удалить участника {0}!\n Нельзя исключить участника, который голосовал в данном заседании.", d.idDelegate.FullName);
                    _msgForm.ShowError(err);
                }
            }

            DataModel.ReloadSesDelegates(); // перезагрузить список, для актуальности
            UpdatePropData();
        }

        // добавляет депутата в список депутатов сессии  из глобального списка депутатов
        public void AppendDelegate()
        {

            List<int> arr = new List<int>();
//          List<int> arr_Max = new List<int>();
            foreach (SesDelegateObj o in DataModel.SesDelegates)
            {
                arr.Add(o.idDelegate.id);
//              arr_Max.Add(o.id);
            }

            int idMax = 0;
//          if (arr_Max.Count > 0)
//              idMax = arr_Max.Max();

            idMax = _mainController.GetMaxID("SesDelegates");

            // взять контроллер, отвечающий за глобальный список депутатов
            DelegatesCntrler d = (DelegatesCntrler)_mainController.GetSubController("Delegate List Controller");
            d.Mode = DelegatesCntrler.DelegatesFormMode.Select;

            bool IsOk = d.SelectItems(arr); // выбрать депутата из глобального списка

            if (IsOk == true)
            {
                int new_index = 0; // номер из списка для добавляемого депутата
                DelegateObj Delegate = d.GetFirstSelItem();
                while (Delegate != null)
                {
                    if (Delegate.isActive == true)
                    {
                        SesDelegateObj S_Delegate = CreateSesDelegate(Delegate, idMax);
                        if (S_Delegate != null)
                        {
                            Delegate.isActive = true;
                            Delegate.Save();

                            _Delegates.Add(S_Delegate);
                            S_Delegate.Save();
                            new_index = _Delegates.Count - 1;
                            idMax = S_Delegate.id;
                        }
                    }
                    Delegate = d.GetNextSelItem();
                }
                

                DataModel.ReloadSesDelegates(); // актуализировать список депутатов в модели данных

                if (new_index > 0)
                    _View.NavigatorPosition = new_index; // выбрать вновь добавленного депутата из списка
            }
        }

        public SesDelegateObj CreateSesDelegate(DelegateObj Delegate, int maxID)
        {
            if (Delegate != null)
            {

                SesDelegateObj S_Delegate = new SesDelegateObj(Session.DefaultSession);

                S_Delegate.idDelegate = Delegate;
                S_Delegate.idSession = _TheSession;
                S_Delegate.idSeat = Delegate.idSeat;
                S_Delegate.IsRegistered = false;
                S_Delegate.IsCardRegistered = true;
                S_Delegate.LastRegTime = DateTime.MinValue;

                if (maxID < 0)
                    maxID = _mainController.GetMaxID("SesDelegates");

                S_Delegate.id = maxID + 1;


                return S_Delegate;
            }

            return null;

        }
#endregion public MainForm.DelegatesPage methods

#region private methods


#endregion private methods


    }
}
