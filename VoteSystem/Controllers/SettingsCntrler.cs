﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public class SettingsCntrler
    {

        ISettingsForm _View;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму
        ISettingsPortionsForm _portionsView;
        ISettingsQKindsForm   _qkindsView;

        XPCollection<TaskKindObj> _QKinds; // виды вопросов
        XPCollection<QntyTypeObj> _QTypes; // виды числа голосов
        XPCollection<DelegateQntyTypeObj> _DQTypes; // процентные доли в соотвествие с видом числа голосов

        MainCntrler _mainController;


        protected int _lastSelectedItem = -1;
        QntyTypeObj _lastQntyTypeObj_pv = null;
        QntyTypeObj _currQntyTypeObj = null;

        bool _changed = false;

        public QntyTypeObj QntyTypeCurrent
        {
            get { return _currQntyTypeObj; }
        }

        public bool IsCardMode
        {
            get { return DataModel.TheSettings.IsCardMode; }
        }

        public SettingsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(ISettingsForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            _portionsView = _View.CreatePortionsView();
        }

        public void Init()
        {
            _QKinds = DataModel.QuestKinds;


            _QTypes = DataModel.QntyTypes;
            _DQTypes = DataModel.DQntyTypes;
        }

        public void ShowView()
        {
            _View.ShowView();
        }

        public void ShowPortionsView()
        {
            _portionsView.ShowView();
        }

        public void OnInitPortionsForm()
        {
            _changed = false;
            _lastQntyTypeObj_pv = null;

            CriteriaOperator crQK = new BinaryOperator(
                new OperandProperty("CodeName"), new OperandValue("Vote_%"),
                BinaryOperatorType.Like);

            _QTypes.Filter = crQK;

            _portionsView.InitControls(ref _QTypes, ref _DQTypes);

            TaskKindObj q = _QKinds[0];
//            foreach(QuestKindObj q in _QKinds)
            {
//                q.Session.BeginTransaction();
                _QTypes.Session.BeginTransaction();
            }

            
        }

        public void OnInitQuorumPage()
        {
            CriteriaOperator crQK = new BinaryOperator(
                new OperandProperty("CodeName"), new OperandValue("Quorum_%"),
                BinaryOperatorType.Like);

              _QTypes.Filter = crQK;

              _currQntyTypeObj = DataModel.TheSettings.idQuorumQnty;
              _View.InitQuorumPageControls(_currQntyTypeObj, DataModel.TheSettings);
        }


        public void PortionsView_CheckDelegateQntyTypeChanged(ref QntyTypeObj currItem, ref DelegateQntyTypeObj newDQTypeValue, int newProcentValue)
        {

            if (_lastQntyTypeObj_pv != null)
            {
                if (_lastQntyTypeObj_pv.id == currItem.id)
                    return;

                if (newDQTypeValue != null)
                {

                    if (newDQTypeValue.id != _lastQntyTypeObj_pv.idDelegateQntyType.id)
                    {
                        _lastQntyTypeObj_pv.idDelegateQntyType = newDQTypeValue;
                        _changed = true;
                        _lastQntyTypeObj_pv.Save();
                    }
                }

                if (_lastQntyTypeObj_pv.ProcentValue != newProcentValue)
                {
                    _lastQntyTypeObj_pv.ProcentValue = newProcentValue;
                    _changed = true;
                    _lastQntyTypeObj_pv.Save();
                }

            }


            _lastQntyTypeObj_pv = currItem;
        }

        public bool PortionsView_CheckDelegateQntyChanged()
        {
            TaskKindObj q = _QKinds[0];
            if (_changed == true)
            {
                DialogResult dr = _View.GetShowInterface().ShowQuestion("Изменились текущие настройки долей. Сохранить изменения?");
                if (dr != DialogResult.Yes)
                {
                    //            foreach(QuestKindObj q in _QKinds)
                    {
                        _QTypes.Session.RollbackTransaction();
                    }
                }

                if (dr == DialogResult.Cancel)
                    return false;

            }

            //            foreach(QuestKindObj q in _QKinds)
            {
                if (_QTypes.Session.InTransaction)
                    _QTypes.Session.CommitTransaction();
            }
            return true;

        }

//---------------------

        public void OnInitQKindsForm()
        {

            _qkindsView.InitControls(ref _QKinds, "");
        }

        public void ShowQKinds()
        {
            _qkindsView.ShowView();
        }

//----------------------
        public void OnInitForm()
        {
            CriteriaOperator crQK = new BinaryOperator(
                new OperandProperty("CodeName"), new OperandValue("Quorum_%"),
                BinaryOperatorType.Like);

            _QTypes.Filter = crQK;

            CriteriaOperator crQT = new BinaryOperator(
                new OperandProperty("CodeName"), new OperandValue("Vote_%"),
                BinaryOperatorType.Like);

            _currQntyTypeObj = DataModel.TheSettings.idQuorumQnty;
            _View.InitControls(ref _QKinds, ref _QTypes, crQT, ref _DQTypes, _currQntyTypeObj, DataModel.TheSettings);
        }


        public void OnSelValueChanged_ListQuantTypes(ref QntyTypeObj currItem)
        {
            _currQntyTypeObj = currItem;
        }

        public void UpdateQntyTypeValues_QuorumSettings(DelegateQntyTypeObj newDQTypeValue, int newProcentValue)
        {

            if (_currQntyTypeObj != null)
            {
                if (newDQTypeValue != null)
                {

                    if (newDQTypeValue.id != _currQntyTypeObj.idDelegateQntyType.id)
                    {
                        _currQntyTypeObj.idDelegateQntyType = newDQTypeValue;
                        _changed = true;
                        _currQntyTypeObj.Save();
                    }
                }

                if (_currQntyTypeObj.ProcentValue != newProcentValue && newProcentValue != -1)
                {
                    _currQntyTypeObj.ProcentValue = newProcentValue;
                    _changed = true;
                    _currQntyTypeObj.Save();
                }

            }
        }

        public void UpdateQntyType_QuorumSettings()
        {
            if (_currQntyTypeObj != null && DataModel.TheSettings.idQuorumQnty.id != _currQntyTypeObj.id)
            {
                DataModel.TheSettings.idQuorumQnty = _currQntyTypeObj;
                DataModel.TheSettings.Save();
            }

        }

        public void UpdateAssignedQnty( int assignedQnty)
        {
            if (assignedQnty > 0 && assignedQnty != DataModel.TheSettings.AssignedQnty)
            {
                DataModel.TheSettings.AssignedQnty = assignedQnty;
                DataModel.TheSettings.Save();
            }
        }

        public void UpdateSelectedQnty(int selectedQnty)
        {
            if (selectedQnty > 0 && selectedQnty != DataModel.TheSettings.SelectedQnty)
            {
                DataModel.TheSettings.SelectedQnty = selectedQnty;
                DataModel.TheSettings.Save();
            }
        }

        public void TKinds_InsertNewItem()
        {
            TaskKindObj tk = new TaskKindObj(Session.DefaultSession);

            XPCollection<TaskKindObj> qkinds = new XPCollection<TaskKindObj>();
            int maxid = qkinds.Max<TaskKindObj>(v => v.id);

            tk.id = maxid + 1;
            tk.Name = "Новый вид";
            tk.CodeName = "TypeNew";
            _QKinds.Add(tk);
            tk.Save();
        }

        public void TKinds_RemoveItem(TaskKindObj tk)
        {
            tk.IsDel = true;
            tk.Save();
            DataModel.QuestKinds.Reload();
        }

        public void SetCardMode(bool cardMode)
        {
            if (cardMode == false)
            {
                DataModel.TheSettingsHall.IsDelegateSeatViewMode = true;
                DataModel.TheSettingsHall.IsAutoCardMode = false;
                DataModel.TheSettingsHall.IsAutoNamesOutMode = false;
                DataModel.TheSettingsHall.IsBackCardMode = false;
                HallCntrler hall = (HallCntrler)_mainController.GetSubController("HallPage Controller");
                hall.ActivateAll();
            }
            else
            {
                DataModel.TheSettingsHall.IsDelegateSeatViewMode = false;
                DataModel.TheSettingsHall.IsAutoCardMode = true;
                DataModel.TheSettingsHall.IsBackCardMode = false;
                DataModel.TheSettingsHall.IsAutoNamesOutMode = true;

            }

            DataModel.TheSettings.IsCardMode = cardMode;
            DataModel.TheSettingsHall.Save();
            DataModel.TheSettings.Save();
        }

        public void SetPath(string type, string path)
        {
            switch (type)
            {
                case "docs": DataModel.TheSettings.Path_Doc = path; break;
                case "excel": DataModel.TheSettings.Path_Excel = path; break;
                case "dbase": DataModel.TheSettings.Path_DBase = path; break;
            }

            DataModel.TheSettings.Save();
        }

        public void SetPredsedatelViewAgenda(bool viewAgenda)
        {
            DataModel.TheSettings.Pred_IsAgenda = viewAgenda;
            DataModel.TheSettings.Save();

        }
    }
}
