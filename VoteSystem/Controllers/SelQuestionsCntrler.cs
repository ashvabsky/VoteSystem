﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;

namespace VoteSystem
{
    /*
     * Контроллер предназначен для управления списком вопросов для голосования
     * на страничке главного окна MainForm.SelQuestionsPage (далее, основная страничка)
     */
    public class SelQuestionsCntrler
    {
        #region private секция данных

        SessionObj _TheSession;         // данные о текущей сессии
        ISelQuestionsPage _View;        // интерфейс для управления основной страничкой 

        MainCntrler _mainController;    // главный контроллер
        //      IQuestProperties _questSheetView; // интерфейс для управления панелью свойств на форме
        IMsgMethods _msgForm;           // интерфейс, для вывода сообщений на форму

        SesTaskObj _theTask;     // текущий (выбранный или выделенный в данный момент) вопрос из списка
        NestedUnitOfWork _nuow;

        XPCollection<SesTaskObj> _Tasks;        // вопросы сессии 

        XPCollection<VoteTypeObj> _VoteTypes; // типы голосваний 
        XPCollection<VoteKindObj> _VoteKinds; // виды голосваний

        #endregion private секция данных

        #region основные методы
        public SelQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(ISelQuestionsPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
            //          _questSheetView = _View.GetQuestPropertiesInterface();
        }

        // первоначальная инициализация
        public void Init(ref XPCollection<SesTaskObj> session_questions)
        {
            _TheSession = DataModel.TheSession;
            session_questions.Criteria = DataModel.SesTasks.Criteria;

            _Tasks = session_questions;

          CriteriaOperator cr = CriteriaOperator.Parse("IsHot == ?", false);
          _Tasks.Filter = cr;

            _VoteTypes = DataModel.VoteTypes;
            _VoteKinds = DataModel.VoteKinds;

            _View.Init();
        }

        // инициализация странички при ее активации
        public void InitPage()
        {
            _Tasks.Reload();

            _View.InitControls(ref _Tasks);

            UpdatePropData();
        }
        #endregion основные методы

        #region public methods, which is called from the view

        // вывести информацию о текущем вопросе в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;

            _theTask = null;

            if (pos < 0)
                return;

            if (pos >= _Tasks.Count)
                return;

            _theTask = _Tasks[pos] as SesTaskObj;

            Platform.QuestionInfo info = new Platform.QuestionInfo();
            info.Caption = _theTask.Caption;
            info.Description = _theTask.Description;
            info.Type = _theTask.idKind.Name;
            _mainController.CurrentActivity = MainCntrler.Activities.SelectQuestion;
            _mainController._MonitorInfo = info;


            bool IsVoted = false;
            if (_theTask.State >= 1)
                IsVoted = true;

            _View.EnableButtons(IsVoted);
        }

        // пользователь пытается удалить текущий вопрос
        public void RemoveCurrentItem()
        {
            if (_theTask == null)
                return;

            RemoveItem(ref _theTask);
            UpdatePropData();
        }


        // добавляет вопрос из глобального списка в список для голосвания
        public void AppendItem()
        {

            int idMax = GetMaxID();

            if (idMax == -1)
                return;

            int new_index = -1;

            Session ses = _Tasks.Session;
            TaskKindObj kind;
            if (_theTask != null)
            {
                //                ses = _theQuestion.Session;
                kind = _theTask.idKind;
                //              ses = Session.DefaultSession; //_theQuestion.Session;
            }
            else
            {
                kind = (TaskKindObj)DataModel.QuestKinds[1];
                //              ses = Session.DefaultSession;
            }

            SesTaskObj newTask = new SesTaskObj(ses);

            newTask.idSession = DataModel.Sessions.Lookup(_TheSession.id);
            newTask.id = idMax + 1;
            newTask.idKind = DataModel.QuestKinds.Lookup(kind.id);
            newTask.Description = "";
            newTask.Caption = "";

            newTask.QueueNum = GetMaxQueue() + 1;


            TaskEditForm d = new TaskEditForm(newTask);
            DialogResult dr = d.ShowDialog(_View.GetView());

            if (dr == DialogResult.OK)
            {
                _Tasks.Add(newTask);
                newTask.Save();
                SetPosition(newTask);
            }

        }

        // пользователь пытается отредактировать текущий вопрос
        public void EditCurrRecord()
        {
            int Pos = _View.NavigatorPosition;
            EditRecord(Pos);
        }


        // пользователь пытается изменить номер очередности вопроса
        public void ChangeQueuePos(string command)
        {
            int Pos = _View.NavigatorPosition;

            _theTask = null;

            if (Pos <= -1)
                return;

            //          _nuow = _Questions.Session.BeginNestedUnitOfWork();

            //            _theQuestion = _nuow.GetNestedObject(_Questions[Pos]) as SesQuestionObj;

            _theTask = (SesTaskObj)_Tasks[Pos];

            int newQNum = 0;
            int step = 0;

            int minQueueNum = GetMinQueue(-1);

            if (command == "Queue_Up")
            {
                if (_theTask.QueueNum > minQueueNum + 1)
                {
                    newQNum = _theTask.QueueNum - 1;
                    step = 1;
                }
            }
            else if (command == "Queue_Down")
            {
                newQNum = _theTask.QueueNum + 1;
                step = -1;
            }

            if (newQNum > 0 && newQNum <= GetMaxQueue())
            {

                foreach (SesTaskObj q in _Tasks)
                {
                    if (q.QueueNum == newQNum)
                    {
                        q.QueueNum = q.QueueNum + step;
                        q.Save();
                    }
                }

                _theTask.QueueNum = newQNum;
                _theTask.Save();
                SetPosition(_theTask);
                //                _View.SelectPos(_theTask.id);
            }
        }


        #endregion public methods, which is called from the view


        #region public methods
        public SesTaskObj CurrentTask
        {
            get { return _theTask; }
        }
        /*
                // Добавляет новое чтение вопроса. При создании нового чтения вопроса, фактически создается новый вопрос, 
                // который надо добавить в список вопросов для голосования
                public int AddNewVoteQuestion(SesTaskObj q)
                {

                    int idMax = GetMaxID();
                    int qNum = GetMaxQueue() + 1;
                    int new_index = -1;
                    if (idMax > 0)
                    {
                        q.id = idMax + 1;
                        q.QueueNum = qNum;
                        new_index = _Tasks.Add(q);
                        q.Save();
                    }
                    return new_index;
                }
        */
        // программно выбрать вопрос из списка и отобразить его свойства
        public void SetCurrentPos(int currPos)
        {
            _View.NavigatorPosition = currPos;
            UpdatePropData();
        }

        // найти заданный вопрос в списке и выделить его
        public void SetPosition(SesTaskObj selTask)
        {
            _Tasks.Reload();
            int currPos = _Tasks.IndexOf(selTask);

            _View.NavigatorPosition = currPos;
        }


        #endregion  public methods

        #region private methods


        // просто удаляет вопрос из списка
        public void RemoveItem(ref SesTaskObj q)
        {
            SesTaskObj task = q;
            bool isDeleted = false;

            try
            {
                _Tasks.Remove(q);
                isDeleted = true;
            }
            catch
            {
                DialogResult dr = _msgForm.ShowQuestion("На выбранный пункт повестки дня ссылается вопрос для голосования или тема для выступлений!\nВы уверены, что хотите исключить данный пункт из списка?");
                if (dr == DialogResult.Yes)
                {
                    task.IsDel = true;
                    task.Save();
                    isDeleted = true;
                }
            }
            finally
            {
                _Tasks.Reload();
            }

            if (isDeleted == true)
            {
                foreach (SesTaskObj eachQ in _Tasks)
                {
                    if (eachQ.QueueNum > task.QueueNum)
                    {
                        eachQ.QueueNum--;
                        eachQ.Save();
                    }
                }
            }
        }

        // подготавливает выбранный вопрос для редактирования через панель свойств
        private void EditRecord(int pos)
        {
            _theTask = null;

            if (pos <= -1)
                return;

            _nuow = _Tasks.Session.BeginNestedUnitOfWork();

            _theTask = _nuow.GetNestedObject(_Tasks[pos]) as SesTaskObj;

            TaskEditForm qedt = new TaskEditForm(_theTask);
            if (_theTask.State >= 1)
                qedt._readMode = true;

            qedt.ShowDialog();

            //            _Questions.Reload();

            //          _nuow.BeginTransaction();
            //          _questSheetView.PropertiesQuestions_StartEdit();
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxID()
        {

            int idMax = _mainController.GetMaxID("SesTasks");

            return idMax;
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxQueue()
        {

            int Max = 0;
            if (_Tasks.Count > 0)
                Max = _Tasks.Max<SesTaskObj>(v => v.QueueNum);

            /*
                        List<int> arr_Max = new List<int>();
                        foreach (SesTaskObj o in DataModel.SesTasks)
                        {
                            arr_Max.Add(o.QueueNum);
                        }

                        int Max = 0;
                        if (arr_Max.Count > 0)
                            Max = arr_Max.Max();
            */
            return Max;
        }

        // получить минимальный идентификатор вопроса из списка
        private int GetMinQueue(int excludedID)
        {
            int minQNum = 0; // max voted Queue Num 
            /*
                        foreach (SesTaskObj eachQ in DataModel.SesTasks)
                        {
                            if (eachQ.State == 2 && eachQ.id != excludedID)
                            {
                                if (minQNum < eachQ.QueueNum)
                                    minQNum = eachQ.QueueNum;
                            }
                        }
            */
            return minQNum;
        }

        #endregion private methods

    }
}
