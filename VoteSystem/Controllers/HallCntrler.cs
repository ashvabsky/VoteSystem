﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;
using System.Threading;

namespace VoteSystem
{
    // субконтроллер для управления страничкой главного окна MainForm.HallPage (далее, основная страничка)
    // страничка отвечает за рассадку депутатов по залу, регистрацию и определение кворума
    public class HallCntrler
    {
        /* энумератор состояний пульта депутата
         * Registered - депутат зарегистрировался, NotRegistered - депутат не зарегистрировался, Disabled - пульт отключен
         */
        public enum SeatState {Registered, NotRegistered, Disabled};

#region private секция данных

        SessionObj _TheSession;         // данные о текущей сессии
        public SesDelegateObj _TheDelegate;    // текущий (выбранный или выделенный в данный момент) депутат в зале

        IHallPage _View;                // интерфейс для управления основной страничкой
        IMsgMethods _msgForm;           // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController;    // главный контроллер

        NestedUnitOfWork _nuow;

        XPCollection<SesDelegateObj> _Delegates; // список делегатов, присутствующих на форме
        XPCollection<SeatObj> _Seats;   // пульты на местах

        int _MicNum = -1;               // номер текущего пульта
        SeatObj _theSeat = null;        // текущий пульт

        // сопоставление номера пульта и привязанного к этоу пульту депутата
        Dictionary<int, SesDelegateObj> _SeatDelegatesList = new Dictionary<int, SesDelegateObj>();
        
        // сопоставление номера пульта и xpo-объекта со всей информацией о пульте
        Dictionary<int, SeatObj> _SeatsList = new Dictionary<int, SeatObj>();

        RegStartForm _RegStartForm;     // форма для запуска процесса регистрации и определения квормуа

        bool _regstartProcess = false;// Флаг - указует, запущен ли процесс регистрации
        int _сardQnty; // кол-во зарегестрированных карточек депутатов
        int _regQnty; // кол-во зарегистрированных депутатов
        int _quorumQnty; // кол-во для кворума

        bool _IsregInfoToScreen = true; // флаг указывает автоматически выводить информацию на мониторы
        bool _Isquorum = false; // флаг, есть ли кворум
//      bool _IsCardProcess = false;
        bool _IsTextOutAutoProcess = false; // запущено ли определение карточек и вывод текста в авторежиме
        int  _TextOutAutoProcessMode = 1; //0 - выкл, 1 - вкл, 2 - режим паузы - временно выключает процесс определения карточек

        SeatObj _emptySeat;
        SeatObj _tribuneSeat;
        static public int ID_TRIBUNE = 6;


        private bool _IsPopupMnuOpen = false;

        Platform.ProgressFormAuto _autoProgressForm = new Platform.ProgressFormAuto();

#endregion private секция данных

#region основные методы контроллера
        public HallCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IHallPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void Init()
        {
            _TheSession = DataModel.TheSession;
            _Delegates = DataModel.SesDelegates;
            _Seats = DataModel.Seats;

                    _emptySeat = null;
                    _tribuneSeat = _Seats.Lookup(ID_TRIBUNE);
                    CriteriaOperator criteria1 = CriteriaOperator.Parse("MicNum == ?", 0);
                    _Seats.Filter = criteria1;
                    if (_Seats.Count != 0)
                    {
                        _emptySeat = (SeatObj)_Seats[0];
                        _Seats.Filter = null;
                    }

            LoadSeatsInfo();
            InitSeatsList();

            _regQnty = DataModel.RegDelegateQnty;
            _quorumQnty = CalcQuorumQnty();

            if (DataModel.IsSpeechApp == false)
            {
                ConfHelper.OnProcessCard = OnProccessCard; //ProccessCardNumbers;
                ConfHelper.OnFinishProcessText = FinishProccessText;
                ConfHelper.OnFinishIdentifyCards = FinishIdentifyCards;
            }
            else
            {
                ConfHelper.OnProcessCard = OnProccessCard; //ProccessCardNumbers;
                ConfHelper.OnFinishProcessText = null;
                ConfHelper.OnFinishIdentifyCards = FinishIdentifyCards;
            }

            _View.Init();

            _autoProgressForm = new Platform.ProgressFormAuto();
            _autoProgressForm.Owner = _View.GetView();
            _autoProgressForm.ProgressDone = identifyDone;
            _autoProgressForm.MaxTime = 8000;
            _autoProgressForm.Text = "Идентификация карточек";
            _autoProgressForm.ProgressText = "Идет процедура определения карточек.";
            //          _autoProgressForm.ProgressFinishText = "Процедура определения карточек завершена.";
            _autoProgressForm.ProgressFailedText = "Ошибка идентификации карточек!";

  //        _autoProgressForm.Show();

        }

        public void OnClosing()
        {
//          _autoProgressForm.Close();
            _autoProgressForm.Dispose();

        }
#endregion основные методы контроллера

#region public MainForm.HallPage methods and properties

        public bool RegStartMode
        {
            get { return _regstartProcess; }
        }

        public bool RegCardMode
        {
            get 
            {
                if (identifyDone.WaitOne(1) == false)
                    return true;
                else
                    return false;
            }
        }

        public bool TextOutMode
        {
            get
            {
                if (textDone.WaitOne(1) == false)
                    return true;
                else
                    return false;
            }
        }
        // вывести информацию о текущем депутате в окно свойств
        // MicNum - номер пульта выводимого депутата
        public int ShowDelegateProp(int MicNum)
        {
            if (_IsPopupMnuOpen)
                return -1;

            _MicNum = MicNum;

            if (_SeatDelegatesList.ContainsKey(MicNum))
            {
                _TheDelegate = (SesDelegateObj)_SeatDelegatesList[MicNum];
            }
            else
            {
                _TheDelegate = null;
            }

            ShowDelegateProp(_TheDelegate);

            if (DataModel.TheSettingsHall.IsDelegateSeatViewMode == false && _TheDelegate != null && _TheDelegate.IsCardRegistered == false)
                _TheDelegate = null;

            return 1;
        }

        // пересадить депутат с одного места на другое
        public void MoveDelegate(int SourceSeatNum, int DestSeatNum, bool backgroundmode)
        {
            if (SourceSeatNum == DestSeatNum)
                return;

            SesDelegateObj sourceDelegate = null;
            SesDelegateObj destDelegate = null;

            _nuow = _Delegates.Session.BeginNestedUnitOfWork();

            // вычислить перемещаемого депутата 
            if (_SeatDelegatesList.ContainsKey(SourceSeatNum))
            {
                sourceDelegate = (SesDelegateObj)_nuow.GetNestedObject(_SeatDelegatesList[SourceSeatNum]);
            }

            // а если на перемещаемом месте уже сидит другой депутат? 
            if (_SeatDelegatesList.ContainsKey(DestSeatNum))
            {
                destDelegate = (SesDelegateObj)_nuow.GetNestedObject(_SeatDelegatesList[DestSeatNum]);
            }

            if (destDelegate != null)
            {
                // на новом месте есть депутат - высаживаем его оттедова
                if (destDelegate.IsCardRegistered == true)
                {
                    destDelegate.idSeat = _emptySeat;
                    destDelegate.SeatFixed = false;
                    UnRegisterCard(destDelegate);
                }

                if (destDelegate.IsCardRegistered == false)
                {
                    if (sourceDelegate != null)
                    {
                        DialogResult dr = DialogResult.Yes;
                        if (backgroundmode == false)
                        {
                            string msg = String.Format("Вы намерены поменять местами депутата \"{0}(место № {1})\" и депутата \"{2} (место № {3}\")?", sourceDelegate.idDelegate.FullName, sourceDelegate.idSeat.MicNum, destDelegate.idDelegate.FullName, destDelegate.idSeat.MicNum);
                            dr = _msgForm.ShowWarningQuiestion(msg);
                        }
                        if (dr == DialogResult.Yes)
                        {
                            // ну-с, начнем пересадку
                            SeatObj seat = sourceDelegate.idSeat;
                            sourceDelegate.idSeat = destDelegate.idSeat;
                            destDelegate.idSeat = seat;
                            destDelegate.SeatFixed = false;

                            _nuow.CommitChanges();
                            DataModel.UpdateSesDelegates(); // акуализировать список

                            SesDelegateObj d = _SeatDelegatesList[SourceSeatNum];
                            _SeatDelegatesList[SourceSeatNum] = _SeatDelegatesList[DestSeatNum];
                            _SeatDelegatesList[DestSeatNum] = d;


                            InitSeatsList();

                            if (backgroundmode == false)
                            {
                                UpdateViewSeats(); // обновить изменения на форме
                            }
                        }
                    }
                }
            }
            else // если место не занято никем
            {
                if (sourceDelegate != null)
                {
                    if (_SeatsList.ContainsKey(DestSeatNum))
                    {
                        DialogResult dr = DialogResult.Yes;
                        if (dr == DialogResult.Yes)
                        {
                            SeatObj seat = (SeatObj)_nuow.GetNestedObject(_SeatsList[DestSeatNum]);

                            sourceDelegate.idSeat = seat;
                            sourceDelegate.SeatFixed = false;

                            _nuow.CommitChanges();

                            InitSeatsList();
                            if (backgroundmode == false)
                            {
                                UpdateViewSeats(); // обновить изменения на форме
                            }

                            if (seat.id == ID_TRIBUNE)
                            { 
//                              _TheDelegate = sourceDelegate;
                                Fix_Seat_Ask(); // если депутатика перетаскивают на трибуну, то надо автоматически предложить его там закрепить (на случай если он карточку свою с собой не захватил)
                            }
                        }
                    }

                }
            }

        }


        // метод, определяет, какие же пункты должны быть в контекстном меню
        public void GetPopupMenuStates(int micNum, ref bool SwitcherPhysicalState, ref bool SwitcherLogicalState, ref bool SeatFree_Enable, ref bool SeatSelect_Enable, ref bool AllDisable, ref bool IsRegisteredEnabled, ref bool IsRegistered, ref bool RegisterCard_Enable, ref bool IsCardRegistered)
        {
            // сначала необходимо определить выбранное микрофонное место

            if (micNum < 1)
            {
                _theSeat = null;
            }
            else
            {
                _theSeat = (SeatObj)_SeatsList[micNum];
            }

            if (_theSeat == null) // исключительный случай - выбранное место отстуствует в списке всех мест, т.е. в базе данных
            {
                AllDisable = true;
                return;
            }

            SwitcherPhysicalState = _theSeat.PhysicalState;
            SwitcherLogicalState = _theSeat.LogicalState;

            bool IsSeatViewMode = DataModel.TheSettingsHall.IsDelegateSeatViewMode;
            if (_TheDelegate != null) // место занято?
            {
                SeatFree_Enable = true; // включить возможность освобождения места
                SeatSelect_Enable = false; // выключить возможность выбора депутата для данного места
            }
            else
            {
                SeatFree_Enable = false;
                SeatSelect_Enable = true & IsSeatViewMode;
            }

            if (_TheDelegate == null || _TheDelegate.IsCardRegistered == false || _theSeat.LogicalState == false || _theSeat.PhysicalState == false)
                IsRegisteredEnabled = false;
            else
            {
                IsRegisteredEnabled = true;
                IsRegistered = _TheDelegate.IsRegistered;
            }

            if (_TheDelegate == null || DataModel.TheSettings.IsCardMode == false)
                RegisterCard_Enable = false; // дизаблить активацию и деактивацию карточек если режим карточек выключен
            else
            {
                RegisterCard_Enable = true;
                IsCardRegistered = _TheDelegate.IsCardRegistered;
            }

        }
        public void SetPopupState(bool state)
        {
            _IsPopupMnuOpen = state;
        }

        // пользователь решил включить или выключить (физически) текущий пульт
        public void SetSwitchersValuePhysic(bool OnPhysic)
        {
            if (_theSeat == null)
                return;

            _theSeat.PhysicalState = OnPhysic;

            _theSeat.Save();

            UpdateViewSeats(); // обновить изменения на форме
        }

        // пользователь решил включить или выключить (логически) текущий пульт
        public void SetSwitchersValueLogic(bool OnLogic)
        {
            if (_theSeat == null)
                return;

            _theSeat.LogicalState = OnLogic;

            _theSeat.Save();

            UpdateViewSeats(); // обновить изменения на форме
        }

        // пользователь пытается освободить место (пульт) от депутата
        public void FreeCurrentSeat()
        {
            if (_theSeat == null)
                return;

            if (_TheDelegate == null)
                return;

            if (_TheDelegate.idSeat.id == _theSeat.id)
            {
                string msg = String.Format("Вы намерены освободить место № {0}, занятое  депутатом \"{1}\" ?", _theSeat.MicNum, _TheDelegate.idDelegate.FullName);
                DialogResult dr = _msgForm.ShowWarningQuiestion(msg);
                if (dr == DialogResult.Yes)
                {

                // необходимо найти объект, выполняющий роль пустого места
                    _TheDelegate.idSeat = _emptySeat; // утсановить депутату, что он не занимет никакого места
                    _TheDelegate.SeatFixed = false;
                    _TheDelegate.IsRegistered = false;
                    _TheDelegate.Save();

                    _regQnty = GetRegDelegatesQnty();

                    InitSeatsList(); // обновить изменения на форме
                    UpdateViewSeats(); // обновить изменения на форме
                }
            }
        }

        public void Fix_Seat_Ask()
        {
            if (_TheDelegate != null)
            {
                DialogResult dr = _msgForm.ShowQuestion("Закрепить депутата на выбранном месте?");
                if (dr == DialogResult.Yes)
                {
                    _TheDelegate.SeatFixed = true;
                    _TheDelegate.Save();
                    UpdateViewSeats(); // обновить изменения на форме
                }
            }
        }

        public void RegCurrentSeat()
        {
            if (_theSeat == null)
                return;

            if (_TheDelegate == null)
                return;

            if (_TheDelegate.idSeat.id == _theSeat.id && _TheDelegate.IsCardRegistered == true)
            {
                if (_TheDelegate.IsRegistered == false)
                {
                    _TheDelegate.IsRegistered = true;
                }
                else
                {
                    _TheDelegate.IsRegistered = false;
                }
                _TheDelegate.Save();


                _regQnty = GetRegDelegatesQnty();
                _View.ReqQnty = _regQnty;

                _Isquorum = false;
                if (_regQnty >= _quorumQnty) //_TheSession.Quorum)
                {
                    _Isquorum = true;
                }

                DataModel.TheSession.RegQnty = _regQnty;
                DataModel.TheSession.IsQuorum = _Isquorum;
                DataModel.TheSession.Save();

                UpdateViewSeats(); // обновить изменения на форме

            }
        }

        public void RegCardCurrentSeat()
        {
            if (_theSeat == null)
                return;

            if (_TheDelegate == null)
                return;

            if (_TheDelegate.idSeat.id == _theSeat.id)
            {
                if (_TheDelegate.IsCardRegistered == false)
                {
                    _TheDelegate.IsCardRegistered = true;
                }
                else
                {
                    _TheDelegate.IsCardRegistered = false;
                }

                _TheDelegate.Save();


                _regQnty = GetRegDelegatesQnty();

/*
                _View.ReqQnty = _regQnty;

                _Isquorum = false;
                if (_regQnty >= _quorumQnty) //_TheSession.Quorum)
                {
                    _Isquorum = true;
                }
*/
                UpdateViewSeats(); // обновить изменения на форме

            }
        }

        // пользователь пытется выбрать депутата и посадить его за текущий пульт
        public void Delegate_SetToSeat()
        {
            // определение списка нерассаженных депутатов
            List<int> nonseatDelegates = new List<int>(); 
            foreach (SesDelegateObj o in _Delegates)
            {
                if (o.idSeat.MicNum <= 0)
                    nonseatDelegates.Add(o.idDelegate.id);
            }
            //

            DelegatesCntrler d = (DelegatesCntrler)_mainController.GetSubController("Delegate List Controller");
            d.Mode = DelegatesCntrler.DelegatesFormMode.SeatSelect; // включить режим выбора депутатов без мест

            DelegateObj Delegate = d.GetSelectedItemFromList(nonseatDelegates); // выбрать депутата из списка нерассаженных депутатов

            if (Delegate != null) // если депутат выбран,
            {
                CriteriaOperator criteria1 = CriteriaOperator.Parse("idDelegate.id == ?", Delegate.id);
                _Delegates.Filter = criteria1;
                if (_Delegates.Count != 0)
                {
                    SesDelegateObj seatdelegate = (SesDelegateObj)_Delegates[0];
                    seatdelegate.idSeat = _theSeat; // устанавливаем ему новое место(пульт)
                    seatdelegate.SeatFixed = false;
                    if (DataModel.TheSettings.IsCardMode == false)
                    {
                        seatdelegate.IsCardRegistered = true; // если выключен режим карточек, то при рассадке депутата сразу активировать его
                    }
                    else if (seatdelegate.idDelegate.idType.CodeName != "Delegate_Active")
                    {
                        seatdelegate.IsCardRegistered = true; // если выключен режим карточек, то при рассадке депутата сразу активировать его
                    }

                    seatdelegate.Save();
                    _Delegates.Filter = null;
                    InitSeatsList(); // обновить изменения на форме
                    UpdateViewSeats();
                }
                _Delegates.Filter = null;
            }
        }

        private void Delegate_SetToSeat(SesDelegateObj sourceDelegate, int micnum)
        {
            if (micnum <= 0)
                return;

            SesDelegateObj destDelegate = GetDelegateByMic(micnum);
            if (destDelegate != null)
            {
                if (destDelegate.IsCardRegistered == true)
                {
                    lock (this)
                    {
                        destDelegate.IsCardRegistered = false;
                    }
                }

                if (destDelegate.IsCardRegistered == false)
                {
                    lock (this)
                    {
                        destDelegate.idSeat = _emptySeat;
                        destDelegate.SeatFixed = false;
                    }
//                  destDelegate.Save();
                }
                else
                    return;
            }
            SeatObj seat = GetSeatByMic(micnum);

            if (seat == null)
                return;

            if (sourceDelegate.SeatFixed == false)
            {
                // устанавливаем место под депутат только если он незафиксирован на другом месте
                lock (this)
                {
                    sourceDelegate.idSeat = seat; // здесь может блокироваться!
                }
            }
//          sourceDelegate.Save();

            InitSeatsList();
        }

        public void SetDelegateSeatViewMode(bool IsChecked)
        {
            DataModel.TheSettingsHall.IsDelegateSeatViewMode = IsChecked;
            DataModel.TheSettingsHall.Save();
            UpdateViewSeats();
        }

        public void SetAutoCardMode(bool IsChecked)
        {
            DataModel.TheSettingsHall.IsAutoCardMode = IsChecked;
            DataModel.TheSettingsHall.Save();
        }

        public void SetBackCardMode(bool IsChecked)
        {
            DataModel.TheSettingsHall.IsBackCardMode = IsChecked;
            DataModel.TheSettingsHall.Save();
        }

        public void OnBackCardMode()
        {
            if (DataModel.TheSettingsHall.IsBackCardMode)
            {
//              if (DataModel.IsSpeechApp == false)
                {
                    if (DataModel.TheSettingsHall.BackCardModeInterval > 0)
                    {
                        AutoIdentifyCardsCheck();
                    }
                }
            }
        }

        public void SetBackCardModeInterval(int value)
        {
            if (value > 0)
            {
                DataModel.TheSettingsHall.BackCardModeInterval = value;
                DataModel.TheSettingsHall.Save();
            }
        }

        public void SetAutoNamesMode(bool IsChecked)
        {
            DataModel.TheSettingsHall.IsAutoNamesOutMode = IsChecked;
            DataModel.TheSettingsHall.Save();
        }

#endregion  public View methods

#region public methods

        // подготовить страничку к показу
        public void InitPage()
        {
            _View.InitControls(DataModel.IsSpeechApp, DataModel.TheSettingsHall, DataModel.TheSettings);
            InitSeatsList();

            _regQnty = DataModel.RegDelegateQnty;
            _quorumQnty = CalcQuorumQnty();                

            UpdateViewSeats(); // обновить изменения на форме

            // подготовим вывод на мониторы
            PrepareMonitorRegInfo();
        }


        // очистить итоги регистрации
        public void Registration_Clear()
        {
            foreach (SesDelegateObj d in _Delegates)
            {
                d.IsRegistered = false;
//              d.Save();
            }

            DataModel.RegDelegateQnty = -1; // сбросить счетчик регистраций, чтобы затем его пересчитать
            _regQnty = 0;
            UpdateViewSeats(); // обновить изменения на форме
        }

        public void Registration_Clear_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Отменить итоги регистрации?");
            if (dr == DialogResult.Yes)
            {
                Registration_Clear();
                SaveToDBase();
            }
        }


        public void Seats_Clear_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Освободить все места от депутатов?");
            if (dr == DialogResult.Yes)
            {
                foreach (SesDelegateObj d in _Delegates)
                {
                    if (d.idSeat.MicNum > 0)
                    {
                        if (d.IsCardRegistered == true)
                            d.IsCardRegistered = false;

                        if (d.IsRegistered == true)
                            d.IsRegistered = false;

                        d.idSeat = _emptySeat;
                        d.SeatFixed = false;
                    }
                }

                DataModel.RegDelegateQnty = -1; // сбросить счетчик регистраций, чтобы затем его пересчитать
                _regQnty = DataModel.RegDelegateQnty;
                InitSeatsList();
                UpdateViewSeats(); // обновить изменения на форме

                SaveToDBase();
            }
        }


        public int GetRegDelegatesQnty()
        {
            _Delegates.Reload();
            int iCnt = 0;
            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0 && d.IsRegistered == true && d.IsCardRegistered == true)
                    iCnt++;
            }

            return iCnt;
        }

        public int GetSeatDelegatesQnty()
        {
//          int res = _SeatDelegatesList.Count;


            int iCnt = 0;
            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0 && d.IsCardRegistered == true)
                    iCnt++;
                else if (d.idSeat.MicNum > 0 && d.idDelegate.idType.CodeName != "Delegate_Active")
                    iCnt++;
            }

            return iCnt;

        }

        public int GetCardQnty()
        {
//          _Delegates.Reload();
            int iCnt = 0;
            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0 && d.IsCardRegistered == true && d.idDelegate.idType.IsVoteRight == true)
                    iCnt++;
            }

            return iCnt;
        }

        public int CalcQuorumQnty()
        {
            int totalQnty = -1;
            switch (DataModel.TheSettings.idQuorumQnty.idDelegateQntyType.CodeName)
            {
                case "AssignedQnty":
                    {
                        totalQnty = DataModel.TheSettings.AssignedQnty;
                        break;
                    }

                case "SelectedQnty":
                    {
                        totalQnty = DataModel.TheSettings.SelectedQnty;
                        break;
                    }
                case "RegisteredQnty":
                    {
                        totalQnty = _сardQnty;
                        break;
                    }
            }
            int needProc = DataModel.TheSettings.idQuorumQnty.ProcentValue;

            int calcQnty = (totalQnty * needProc) / 100;

            return calcQnty;
        }

        public void ShowMonitorPreview(string action)
        {
            if (action == "RegPrepare")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.RegPrepare;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "RegStart")
            {
                _mainController.CurrentActivity = MainCntrler.Activities.RegStart;
                _mainController.ShowMonitorPreview();
            }
            else if (action == "RegResults")
            {
                PrepareMonitorRegInfo();
                _mainController.ShowMonitorPreview();
            }
            else
            {
                _mainController.CurrentActivity = MainCntrler.Activities.None;
                _mainController.ShowMonitorPreview();
            }
        }

        public SesDelegateObj GetDelegateByMic(int MicNum)
        {
            if (_SeatDelegatesList.ContainsKey(MicNum) == false)
                return null;

            return _SeatDelegatesList[MicNum];
        }

        Dictionary<string, SesDelegateObj> _delegateByCodeList = new Dictionary<string, SesDelegateObj>();
        public void BuildDelegateByCodeList()
        {
            _Delegates.Filter = null;
            _Delegates.Reload();
            _delegateByCodeList.Clear();

            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idDelegate.CardCode != "" && d.idDelegate.CardCode != null)
                    _delegateByCodeList[d.idDelegate.CardCode] = d;
            }
        }

        public SesDelegateObj GetDelegateByCode(string code)
        {
            if (_delegateByCodeList.ContainsKey(code))
                return _delegateByCodeList[code];//DataModel.SesDelegates_GetByCard(code);

            return null;
        }

        public SeatObj GetSeatByMic(int MicNum)
        {
            if (_SeatsList.ContainsKey(MicNum) == false)
                return null;

            return _SeatsList[MicNum];
        }

        private ManualResetEvent identifyDone =
            new ManualResetEvent(true);

        private ManualResetEvent textDone =
            new ManualResetEvent(true);


        // вызывается по конопке "начать регистрацию" и выводит форму для старта процесса регистрации
        public void Registration_PrepareToStart()
        {
            // сначала прогоняем процедуру считывания карточек
            if (DataModel.TheSettingsHall.IsAutoCardMode)
                IdentifyCards();

            _RegStartForm = new RegStartForm(this);
            int RegTime = DataModel.TheSettings.RegTime; //_TheSession.RegTime;
            _RegStartForm.RegTime = RegTime;

            ConfHelper.SendCommand_RegPrepareStart();

            //           System.Threading.Thread.Sleep(3000);

            DialogResult dr = _RegStartForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                // послать на контроллер команду старта голосования
                ConfHelper.SendCommand_RegStart();

                Registration_Clear();// отменить итоги регистрации
                DataModel.RegDelegateQnty = -1; // сбросить количестиво зарегестрированных депутатов, для дальнейшего перерасчета
                _regQnty = DataModel.RegDelegateQnty;
                _quorumQnty = CalcQuorumQnty();

                RegTime = _RegStartForm.RegTime;
                DataModel.TheSettings.RegTime = RegTime;

                _View.StartRegistration(RegTime);

                _regstartProcess = true;

                _mainController.HistoryAdd("СГ", "Определение кворума");
            }
            else
            {
                ConfHelper.SendCommand_RegPrepareOff();

                ShowMonitorPreview(""); // очистить мониторы от предупреждений
            }
        }

        public void RegCard_Stop()
        {
            identifyDone.Set();

            _View.StopRegCards();

/*
            if (_IsIdentifyCardsSuccess == true && _IsTextOutAutoProcess == true)
                TextOut_auto(); // самое время запускать автовывод текста
*/

            if (_IsIdentifyCardsSuccess == true) 
            {
                SaveToDBase(); // отложенная запись в БД после процедуры идентификации
            }
            else
            {
                foreach (SesDelegateObj d in _Delegates)
                {
                    d.Reload(); // в случае неудачной идентификации - следует восстановить состояние делегатов как было
                }
            }
            
            // обновим рассадку на экране
            InitSeatsList();
            UpdateViewSeats();

                
        }


        public void Registration_Stop()
        {
            DataModel.RegDelegateQnty = -1; // сбросить счетчик регистраций, чтобы затем его пересчитать
            _regQnty = DataModel.RegDelegateQnty; // возьмем кол-во регистраций по факту

            // послать на контроллер команду остановки регистрации
            ConfHelper.SendCommand_RegStop();

            _View.StopRegistration();

            _regstartProcess = false;

            ShowMonitorPreview("RegResults");

            SaveToDBase();
            DataModel.TheSettings.Save();

            DataModel.TheSession.RegQnty = _regQnty;
            DataModel.TheSession.RegDate = DateTime.Now;
            DataModel.TheSession.IsQuorum = _Isquorum;
            DataModel.TheSession.Save();

            MessageBox.Show("Регистрация завершена!");
        }

        public void Registration_Stop_Ask()
        {
            DialogResult dr = _msgForm.ShowQuestion("Завершить регистрацию?");
            if (dr == DialogResult.Yes)
            {
                Registration_Stop();
            }
        }

        // обработчик процесса регистрации
        public void OnRegProcess(DateTime leftTime, int progress)
        {
            if (_regstartProcess == false)
                return;

            ConfHelper.MicState ms = new ConfHelper.MicState();
            bool b = ConfHelper.GetData_Reg(ref ms);

            while (b)
            {
                int micNum = ms.MicNum;
                string btnState = ms.State;

                if (_SeatDelegatesList.ContainsKey(micNum))
                {
                    SesDelegateObj d = _SeatDelegatesList[micNum]; //DataModel.SesDelegates_GetByMicNum(micNum);
                    if (d.id != 0 && d.idSeat.MicNum > 0 && d.idSeat.LogicalState == true && d.IsRegistered == false && d.IsCardRegistered == true)
                    {
                        d.IsRegistered = true;
                        //                      d.Save();
                        _regQnty++;
                        //                  curr_delegate = d;
                    }
                }

                b = ConfHelper.GetData_Reg(ref ms);
            }


            //            _regQnty = GetVoteDelegatesQnty();
            _View.ReqQnty = _regQnty;

            _Isquorum = false;
            if (_regQnty >= _quorumQnty) //_TheSession.Quorum)
            {
                _Isquorum = true;
            }

            UpdateViewSeats(); // обновить изменения на форме

            // здесь следует сохранить текущую информацию о процессе регистрации
            Platform.RegInfoStart info = new Platform.RegInfoStart();
            /*
                        DateTime quorumTime = new DateTime(1900, 12, 13, 1, 1, 1);

                        if (leftTime.Minute == 0 && leftTime.Second == 0 || leftTime.Day < quorumTime.Day)
                        {
                            progress = -1;

                            //                  _Controller.Vote_Stop();
                        }
            */
            //          info.CurrDelegate = curr_delegate.idDelegate.FullName;
            info.DelegateQnty = _сardQnty;
            info.RegQnty = _regQnty;
            info.leftTime = leftTime.ToString("mm:ss");
            info.IsQuorum = _Isquorum;
            info.Progress = progress;

            _mainController._MonitorInfo = info;

            if (_IsregInfoToScreen)
                ShowMonitorPreview("RegStart");

            if (_regQnty == _сardQnty)
            {
                Registration_Stop();
            }
        }


        private bool _IsIdentifyCardsSuccess = false;
        public bool IdentifyCards()
        {
            _IsIdentifyCardsSuccess = false;
            _IsTextOutAutoProcess = false;

            bool bRes = IdentifyCards_inner();

            if (bRes)
            {
                //          _mainController.HistoryAdd("СГ", "Определение карточек");

                Platform.ProgressForm pform = new Platform.ProgressForm();
                pform.Owner = _View.GetView();
                pform.ProgressDone = identifyDone;
                pform.IsNeedFinishEvent = true;
                pform.MaxTime = 8000;
                pform.Text = "Идентификация карточек";
                pform.ProgressText = "Идет процедура определения карточек.";
                pform.ProgressFinishText = "Процедура определения карточек завершена.";
                pform.ProgressFailedText = "Не удалось завершить процедуру определения карточек.";
                pform.IsAutoCloseMode = false;
                DialogResult dr = pform.ShowDialog();
                if (dr == DialogResult.Cancel)
                {
                    //              _IsCardMode = false;
                    //              _IsReadCardsMode = false;

                    _msgForm.ShowWarningMsg("Процедура определения карточек прервана пользователем.");
                }
                
                if (identifyDone.WaitOne(1) == false)
                {
                     identifyDone.Set();
                     _IsIdentifyCardsSuccess = false;

/*
                    foreach (SesDelegateObj d in _Delegates)
                    {
                        d.Reload();
                    }
*/
                 }
            }
            return _IsIdentifyCardsSuccess;
        }


        public void IdentifyCards_auto()
        {
            if (RegCardMode == true || TextOutMode == true) // надлежит проверить, что система не находится в процессе проверки карточек и вывода текста
                return;

            if (_delegateByCodeList.Count == 0)
            {
                _IsTextOutAutoProcess = true; // указываетЮ что запущено автоматичкий режим определения карточек и вывод текста

                bool bRes = IdentifyCards_inner();

                if (bRes)
                {
                    _autoProgressForm.Start();
                }

                return;
            }
            identifyDone.Reset();

            bool IsChanges = false;
            _View.ShowCardChanges(false);

            // взять всю информацию по карточкам и внести изменения в рассадку депутатов
            // сбор пойманных данных. Обязательно провести последний сбор, после получения ообщения об окончании идентификации карт
            ConfHelper.MicState ms = new ConfHelper.MicState();
            bool b = ConfHelper.GetData_Card(ref ms);
            while (b)
            {
                if (ProccessCard(ms))
                    IsChanges = true;
                b = ConfHelper.GetData_Card(ref ms);
            }

            identifyDone.Set();


            if (IsChanges == true)
            {
                SaveToDBase(); // отложенная запись в БД после процедуры идентификации

                // обновим рассадку на экране
                InitSeatsList();
                UpdateViewSeats();

                _View.ShowCardChanges(true);

                _mainController.OnChangeSeating();
            }
         }


        private bool IdentifyCards_inner()
        {
            _IsIdentifyCardsSuccess = false;

            if (RegStartMode == true || RegCardMode == true)
                return false;

            if (TextOutMode == true)
                return false;


            foreach (SesDelegateObj d in _Delegates)
            {
                //              if (/*d.idSeat.MicNum > 9 || */d.idSeat.MicNum < 1)
                {
                        d.IsCardRegistered = false;
                }
            }

            //            DataModel.UpdateSesDelegates();

            BuildDelegateByCodeList();

            identifyDone.Reset();
            ConfHelper.SendCommand_rfid_Start();

            _View.StartRegCards();

            //          _mainController.HistoryAdd("СГ", "Определение карточек");

            return true;
        }

        public void SaveToDBase() // метод для сохранения в базу данных изменений с рассадкой и регистрацией делегатов
        {
            if (DataModel.IsSpeechApp == true)
                return;

            lock (this)
            {
                try
                {
                    foreach (SesDelegateObj d in _Delegates)
                    {
                        d.Save();
                    }
                }
                catch (Exception ex)
                {
                    string err = "Ошибка сохранения в базу данных. Повторите операцию!\n";
                    err = err + ex.Message;
                    _msgForm.ShowError(err);
                }
            }
        }


        // обработчик процесса регистрации карточек
        public void OnRegCardProcess()
        {
            bool stop = false;
            if (RegCardMode == false)
            {
                stop = true;
            }

            bool IsChanges = false;

            // сбор пойманных данных. Обязательно провести последний сбор, после получения ообщения об окончании идентификации карт
            ConfHelper.MicState ms = new ConfHelper.MicState();
            bool b = ConfHelper.GetData_Card(ref ms);

            while (b)
            {
                if (ProccessCard(ms))
                    IsChanges = true;
                b = ConfHelper.GetData_Card(ref ms);
            }

            /*
                        if (iRegCardProc % 2 == 0)
                        {
                            InitSeatsList();
                            UpdateViewSeats();
                        }
            */

            if (stop == true)
            {
                RegCard_Stop();
            }


           _mainController.OnChangeSeating();
        }


        public void AutoIdentifyCardsCheck()
        {
            if (DataModel.TheSettingsHall.IsBackCardMode == false)
                return;

            if (_TextOutAutoProcessMode == 2)
            {
                _TextOutAutoProcessMode = 1;
            }
            else if (_TextOutAutoProcessMode == 1)
            {
                 IdentifyCards_auto();
            }
        }

#endregion public methods

#region private methods

        // подготовим вывод на мониторы
        private void PrepareMonitorRegInfo()
        {
            _mainController.CurrentActivity = MainCntrler.Activities.RegistrationInfo;
            Platform.RegInfoFinish info = new Platform.RegInfoFinish();

            int RegQnty = DataModel.RegDelegateQnty;
            info.IsQuorum = false;
            if (RegQnty >= _quorumQnty) //_TheSession.Quorum)
            {
                info.IsQuorum = true;
            }
            info.RegQnty = RegQnty;
            info.SeatQnty = GetSeatDelegatesQnty();
            info.DelegateQnty = GetCardQnty();

            _mainController._MonitorInfo = info;
        }

        private void LoadSeatsInfo()
        {
            _SeatsList.Clear();
            foreach (SeatObj s in _Seats)
            {
                if (s.MicNum > 0)
                    _SeatsList[s.MicNum] = s;
            }
        }

        // обновить изменения на форме
        private void UpdateViewSeats()
        {
//          DataModel.UpdateSesDelegates();

            UpdateViewRegParams(); // просчитать и вывести данные о регистрации

            bool IsChanged = _View.InitSeats(_SeatDelegatesList, _SeatsList, DataModel.TheSettingsHall.IsDelegateSeatViewMode); // отрисовать картинки
            _View.IsQuorum = _Isquorum;

            if (IsChanged)
            {
                _mainController.HistoryAdd("СГ", "Изменилась рассадка депутатов");
            }
        }

        // обновить списки в случае изменений мест у депутатов
        private void InitSeatsList()
        {
            DataModel.UpdateSesDelegates();

            // обновить списки SeatDelegatesList и SeatsList
            _SeatDelegatesList.Clear();

            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0)
                    _SeatDelegatesList[d.idSeat.MicNum] = d;
            }
        }

        // передать данные о свойствах депутата в основную страничку для вывода в окне свойств
        private void ShowDelegateProp(SesDelegateObj theDelegate)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            if (theDelegate != null)
            {
                properties["FirstName"] = theDelegate.idDelegate.FirstName;
                properties["SecondName"] = theDelegate.idDelegate.SecondName;
                properties["LastName"] = theDelegate.idDelegate.LastName;
                properties["PartyName"] = theDelegate.idDelegate.idParty.Name;
                properties["FractionName"] = theDelegate.idDelegate.idFraction.Name;
                properties["RegionName"] = theDelegate.idDelegate.idRegion.Name;
                properties["RegionNum"] = theDelegate.idDelegate.idRegion.Number;

                properties["MicNum"] = theDelegate.idSeat.MicNum.ToString();
                properties["RowNum"] = theDelegate.idSeat.RowNum.ToString();
                properties["SeatNum"] = theDelegate.idSeat.SeatNum.ToString();
            }
            else
            {
                properties["FirstName"] = "";
                properties["SecondName"] = "";
                properties["LastName"] = "";
                properties["PartyName"] = "";
                properties["FractionName"] = "";
                properties["RegionName"] = "";
                properties["RegionNum"] = "";

                properties["MicNum"] = "";
                properties["RowNum"] = "";
                properties["SeatNum"] = "";
            }

            _View.SetProperties(properties);
        }

        // просчитать и вывести данные о регистрации
        private void UpdateViewRegParams()
        {
            int RegTime = DataModel.TheSettings.RegTime; // _TheSession.RegTime;
            int DelegatesQnty = GetSeatDelegatesQnty();
            _сardQnty = GetCardQnty();

            _View.InitRegParams(RegTime, DelegatesQnty, _сardQnty, _regQnty, _quorumQnty);
        }

        private void OnProccessCard(ConfHelper.MicState ms)
        { // метод вызывается из потока обработки данных - лучше здесь ничего особого не делать, чтобы не вызвать нарушение данных при одновременном доступе потоков
        }

        private bool ProccessCard(ConfHelper.MicState ms)
        {
            bool IsChanges = false;
            if (ms.MicNum < 0)
                return false;

            if (_regstartProcess == true)
                return false;

                SesDelegateObj d = GetDelegateByCode(ms.Code);
                if (d != null)
                {
                    if (d.IsCardRegistered == false)
                    {
                        RegisterCard(d);
                        IsChanges = true;
                    }


                    if (d.idSeat.MicNum != ms.MicNum && d.SeatFixed == false) // депутат сел на новое место
                    {
                        Delegate_SetToSeat(d, ms.MicNum);
                        IsChanges = true;
                    }
                }

                else if (ms.Code == "0")
                {
                    SesDelegateObj d2 = GetDelegateByMic(ms.MicNum);
                    if (d2 != null)
                    {
                        if (d2.SeatFixed == false)
                        {
                            UnRegisterCard(d2);
                            IsChanges = true;
                        }
                    }
                }

                return IsChanges;
         }

        public void TextOut()
        {
//          bool bRes = IdentifyCards();

            TextOut_inner();


//          SaveToDBase(); // отложенная запись в БД после процедуры identifycards

            Platform.ProgressForm pform = new Platform.ProgressForm();
            pform.Owner = _View.GetView();
            pform.IsNeedFinishEvent = true;
            pform.ProgressDone = textDone;
            pform.MaxTime = 20000;
            pform.Text = "Вывод имен на пульты";
            pform.ProgressText = "Идет процедура вывода имен на пульты.";
            pform.ProgressFinishText = "Процедура вывода имен  завершена.";
            pform.ProgressFailedText = "Не удалось завершить процедуру вывода имен.";
            pform.IsAutoCloseMode = false;
            pform.ShowDialog();
        }
/*
        public void TextOut_auto()
        {

            TextOut_inner();

            _autoProgressForm2.Start();
 
        }
*/

        public void TextOut_inner()
        {
            if (RegStartMode == true || RegCardMode == true)
                return;

            textDone.Reset();

            //          _Delegates.Reload();
            ConfHelper.SendCommand_Text_Start();

            //          _mainController.HistoryAdd("СГ", "Вывод имен на пульты");
            foreach (SeatObj seat in _Seats)
            {
                //              if (seat.MicNum > 9)
                {
                    if (_SeatDelegatesList.ContainsKey(seat.MicNum))
                    {
                        SesDelegateObj d = _SeatDelegatesList[seat.MicNum];
                        ConfHelper.SendMicCommand_FIO(seat.MicNum, d.idDelegate.ShortName);
/*
                        if (d.IsCardRegistered == true)
                            ConfHelper.SendMicCommand_FIO(seat.MicNum, d.idDelegate.ShortName);
                        else
                            ConfHelper.SendMicCommand_FIO(seat.MicNum, "");
 */ 
                    }
                    else
                    {
                        ConfHelper.SendMicCommand_FIO(seat.MicNum, "");
                    }
                }

                System.Threading.Thread.Sleep(1);
            }

            ConfHelper.SendCommand_Text_Stop();

            //          _msgForm.ShowWarningMsg("Начата процедура вывода имен на пульты с карточками!");

        }

        public void ActivateAll()
        {
            _Delegates.Reload();

            int icnt = 0;
            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0 && d.idDelegate.idType.CodeName == "Delegate_Active") // активировать следует только тех делегатов, которые рассажены и у которых тип = "Депутат"
                {
                    icnt++;
                    if (d.IsCardRegistered == false)
                    {
                        d.IsCardRegistered = true;
                        d.Save();
                    }
                }
            }

            InitSeatsList();
            UpdateViewSeats();
            string str = string.Format("Активировано {0} карточек согласно рассадке депутатов по местам", icnt);
            _msgForm.ShowWarningMsg(str);
        }


        public void RegistrateAll()
        {
            _Delegates.Reload();

            int icnt = 0;
            foreach (SesDelegateObj d in _Delegates)
            {
                if (d.idSeat.MicNum > 0 && d.idDelegate.idType.CodeName == "Delegate_Active" && d.IsCardRegistered == true) // активировать следует только тех делегатов, которые рассажены и у которых тип = "Депутат"
                {
                    icnt++;
                    if (d.IsRegistered == false)
                    {
                        d.IsRegistered = true;
                        d.Save();
                    }
                }
            }

            DataModel.RegDelegateQnty = -1; // сбросить счетчик регистраций, чтобы затем его пересчитать
            _regQnty = DataModel.RegDelegateQnty;

            InitSeatsList();
            UpdateViewSeats();
            string str = string.Format("Зарегистрировано {0} депутатов согласно имеющимся карточкам", icnt);
            _msgForm.ShowWarningMsg(str);
        }

        public void SetTextOutAutoProcessPause()
        {
            _TextOutAutoProcessMode = 2;
        }

        public void SetTextOutAutoProcessOff()
        {
            // сначала следует прекратить любые операции по определению карточек и выводу текста
            if (RegCardMode == true)
            {
                RegCard_Stop();
            }
            if (TextOutMode == true)
            {
                FinishProccessText();
            }

            _TextOutAutoProcessMode = 0;
        }

        public void SetTextOutAutoProcessOn()
        {
            _TextOutAutoProcessMode = 1;
        }

        private void ProccessCardNumbers(ConfHelper.MicState ms) // процедура для определения номеров карточек
        {
            if (ms.MicNum < 1)
                return;

            if (_regstartProcess == true)
                return;

            if (ms.Code != "ffffffffff")
            {
                if (ms.Code.Length == 8) //надо добавить 0d, поскольку она была съедена
                    ms.Code = ms.Code + "0d";

                SesDelegateObj d = GetDelegateByMic(ms.MicNum); 
                if (d != null)
                {
                    d.idDelegate.CardCode = ms.Code;
                    d.idDelegate.Save();
                }
            }
        }

        private void FinishIdentifyCards()
        {
            _IsIdentifyCardsSuccess = true;
           identifyDone.Set();
//           System.Windows.Forms.MessageBox.Show("процедура определения карточек завершена! \n Идет вывод информации на пульты...");
        }

        private void FinishProccessText()
        {
                textDone.Set();
        }

        private ManualResetEvent registercardDone =
            new ManualResetEvent(false);

        private void RegisterCard(SesDelegateObj d)
        {
            if (d == null)
                return;

            lock (this)
            {
                d.IsCardRegistered = true;
            }
/*
            if (d.IsCardRegistered == false) //  надо активировать карточку, если еще не активирована
            {
                try
                {
                    d.IsCardRegistered = true;
//                  d.Save();
                }

                catch
                {
                    try
                    {
                        d.IsCardRegistered = true;
//                      d.Save();
                    }
                    catch
                    {
                        _msgForm.ShowError("Ошибка при сохранении данных в базу данных. Повторите опреацию!");
                    }
                }
  
            }
 */ 
        }

        private void UnRegisterCard(SesDelegateObj d)
        {
            if (d == null)
                return;

            lock (this)
            {
                d.IsCardRegistered = false;
            }
/*
            if (d.IsCardRegistered == true) //  надо активировать карточку, если еще не активирована
            {

                    d.IsCardRegistered = false;
//              d.Save();
            }
 */ 
        }

#endregion private methods

#region public RegStartForm methods

        public void OnRegStartFormLoad()
        {
            _RegStartForm.RegTime = DataModel.TheSettings.RegTime; // _TheSession.RegTime;
            _RegStartForm.DelegatesQnty = _сardQnty;
            _RegStartForm.QuorumQnty = CalcQuorumQnty(); // _TheSession.Quorum;

            _IsregInfoToScreen = true;

            ShowMonitorPreview("RegPrepare");
        }

        // метод сохраняет настроку по выводу информации о регистрации на мониторы
        public void SetRegInfoToScreen(bool value)
        {
            _IsregInfoToScreen = value;
        }

#endregion public RegStartForm methods

    }
}
