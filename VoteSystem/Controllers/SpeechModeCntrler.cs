﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    /*
     * субконтроллер - координирует работу формы SpeechModeForm - "Режим выступлений по выбранной теме".
     * Управление формой осущестлвляется через интерфейс ISpeechModeForm
     * Дополнителльно координирует работу формы SpeechStartForm
     */
    public class SpeechModeCntrler
    {
#region private секция данных
        // перечень значений, которые предоставляет пульт при голосовании
        public enum SeatStates { Ready, SpeechStart, SpeechReady, SpeechReadyHot, Disabled, None };

        ISpeechModeForm _View; // главный интерфейс, с которым работаем
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController; // главный контроллер
        HallCntrler _hall; // контроллер управления залом

        StatementObj _TheStatement; // тема, по которой производится обсуждение
        DelegateStatementObj _theCurrDelegateStatem; // текущий делегат, который собирается произносить речь 
        DelegateStatementObj _theCurrDelegateSpeeching; // текущий делегат, который собирается произносить речь 
        XPCollection<StatementObj> _TheStatemInCollection; // отфильтрованная коллекция, которая содержит только одну тему - _TheStatement

        XPCollection<DelegateStatementObj> _TheStatemDetails;

        // список, сопоставляет номеру пульта - текущее состояние на этом пульте
        Dictionary<int, SeatStates> _States = new Dictionary<int, SeatStates>();

        XPCollection<SesDelegateObj> _localdelegates;

        int _DelegatesQnty = 0;

        int _SpeechTime = 0; // время отводимое на выступление

        bool _speechMode = false; // признак, указывает, идет ли процесс выступления в данный момент
        bool _speechDirectMode = false; // режим прямого включения микрофона
        bool _hallMode = false;  // режим управления выступлениями через зал
        bool _viewMode = false;   // режим просмотра
        bool _IsActive = true;    // режим открытой темы 

        SpeechTypeObj _speechDefaultType;

#endregion private секция данных

        public bool SpeechMode
        {
            get { return _speechMode; }
        }

        public bool HallMode
        {
            get { return _hallMode; }
        }

        public bool ViewMode
        {
            get { return _viewMode; }
            set { _viewMode = value; }
        }

        public StatementObj CurrentStatement
        {
            get { return _TheStatement; }
        }

#region основные методы

        public SpeechModeCntrler(MainCntrler controller)
        {
            _mainController = controller;
        }

        public void AssignView(ISpeechModeForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void ShowForm()
        {
            _View = (ISpeechModeForm)_mainController.GetSubForm("SpeechModeForm");
            _msgForm = _View.GetShowInterface();

            _View.ShowView();
        }

        // первоначальная инициализация
        public bool Init(StatementObj statem) // инициализировать контроллер темой
        {
            _TheStatement = statem;
            if (_TheStatement.State == 0 || _TheStatement.DateTimeStart == DateTime.MinValue)
            {
                _TheStatement.DateTimeStart = DateTime.Now;
            }
            // необходимо из XPO коллекций извлечь требуемую информацию по указанному вопросу
            _TheStatemDetails = DataModel.GetStatemDetails(_TheStatement);

            _TheStatemInCollection = DataModel.GetStatemFromCollection(_TheStatement);

            _speechMode = false;


            foreach (DelegateStatementObj dst in _TheStatemDetails)
            {
                if (dst.State == 2) // оппа, есть выступающий
                {
                    _speechMode = true;
                    _theCurrDelegateSpeeching = dst;
                }
            }

            if (_viewMode == false)
            {
                foreach (StatementObj st in DataModel.SesStatements)
                {
                    if (st.State == 2 && st.id != statem.id)
                    {
                        DialogResult dr = _msgForm.ShowWarningQuiestion("В данный момент открыта другая тема.\nЗавершить выступления по данной теме?");
                        if (dr != DialogResult.Yes)
                            return false;

                       _mainController.CloseOpenStatements();
                    }
                }


                if (_TheStatement.State == 0)
                {
//                    _TheStatement.DateTimeStart = DateTime.Now;
                }

                if (_TheStatement.State != 2)
                {
                    _TheStatement.State = 2; // переводим в режим обсуждения тему
                    _TheStatement.Save();
                    DataModel.SesStatements.Reload();
                }
                else
                {
                    InitApplyDelegates();
                }

                ConfHelper.SendCommand_SpeechStart();
            }

//            InitSeats();

            // не вызывать окно для старта голосования
            _View.PrepareToStart = false;

            _hall = (HallCntrler)_mainController.GetSubController("HallPage Controller");

            CriteriaOperator crSpeech = CriteriaOperator.Parse("CodeName == ?", "Statement_Speech");
            XPCollection<SpeechTypeObj> speechTypes = new XPCollection<SpeechTypeObj>(crSpeech);
            if (speechTypes.Count > 0)
                _speechDefaultType = speechTypes[0];

            _SpeechTime = _speechDefaultType.Value;

            return true;

        }

        private void InitApplyDelegates()
        {
/*
            foreach (DelegateStatementObj ds in _TheStatemDetails)
            {
                if (ds.State == 0 && ds.idSeat.MicNum > 0)
                {
                    ConfHelper.SendCommand_MicApply(ds.idSeat.MicNum);
                }
            }
 */ 
        }

        public void Reload()
        {
            if (_IsActive == false)
                return;

/*
            DataModel.UpdateSesDelegatesST();

          _View.SetDataSources(null);

          DataModel.Reload();
          _localdelegates = new XPCollection<SesDelegateObj>(DataModel.SesDelegates_st);
          _localdelegates.Load();
          _View.SetDataSources(_localdelegates);
*/
            _View.ShowInfo("Изменилась рассадка депутатов");
            InitSeats();
            UpdateSeats();
        }

        public void ShowWarningInfo(string warning)
        {
            _View.ShowInfo(warning);
        }

#endregion основные методы

#region public methods, which is called from the view

        public void OnLoadForm()
        {
//            DataModel.Reload();
            _localdelegates = new XPCollection<SesDelegateObj>(DataModel.SesDelegates_st);

            _View.InitControls(_TheStatement.Caption, ref _TheStatemDetails, ref _TheStatemInCollection, ref _localdelegates);

            InitSeats();

            _View.UpdateControls();
            UpdatePropData();
//            ShowStatemProperties(_TheStatement); // вывести инфу о вопросе в панель свойств

//          InitSpeechParams(); // получить параметры голосования

//          _View.ShowSpeechResults(_TheStatemDetails); // вывести результаты выступлений на форму (если есть)

            _IsActive = true;
        }

        public bool checkFinishSpeech()
        {
            if (SpeechMode == true)
            {
                bool FinishSpeech = true;
                DialogResult dr = _msgForm.ShowQuestion("В данный момент есть выступающий. Завершить его выступление?");
                if (dr == DialogResult.Yes)
                {
                    FinishSpeech = true;
                }
                else if (dr == DialogResult.Cancel)
                    return false;
                else if (dr == DialogResult.No)
                    FinishSpeech = false;


                int Secs = _View.StopSpeeching();
                StopSpeeching(Secs, true, FinishSpeech); // завершить выступление
            }

            return true;
        }

        public bool OnClosing()
        {
            if (_viewMode == true)
                return true;

            if (!checkFinishSpeech())
                return false;

            if (_TheStatement.State != 1)
            {
                int state = 0;

                foreach (DelegateStatementObj ds in _TheStatemDetails)
                {
                    if (ds.idSeat.MicNum > 0) // если есть хоть один выступивший или записавшийся на выступления, по теме, то тема переносится в открытую
                    {
                        state = 2;
                        break;
                    }
                }

                _TheStatement.State = state;
                _TheStatement.Save();

                if (state != 2)
                {
                    ConfHelper.SendCommand_SpeechStop();
                }
            }

            _IsActive = false;

            return true;
        }


        public void OnFinishStatem()
        {
            if (_viewMode == true)
                return;

            ConfHelper.SendCommand_SpeechStop(); // не регистрировать запись
            finishStatem(_TheStatement);
        }

        public void finishStatem(StatementObj statem)
        {
            CancelSpeeching();
            if (statem != null && statem.State == 2)
            {

                statem.State = 1;
                statem.Save();
            }
        }

        public void StartRegistration()
        {
            MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
//          _View.UpdateControls();
            ConfHelper.ClearAll(); // очистить старую очередь
            ConfHelper.StartDataRecieve();
        }

        public void StopRegistration()
        {
            ConfHelper.ClearAll(); // очистить старую очередь
            ConfHelper.StopDataRecieve();
        }

        // вывести форму для старта голосования по текущему вопросу
        public void PrepareToStart()
        {
            SpeechStartForm _StartForm;

            _StartForm = new SpeechStartForm(this);


            DialogResult dr = _StartForm.ShowDialog();
            if (dr == DialogResult.OK)
            {

                _SpeechTime = _StartForm.SpeechTime;
                bool fromTribune = _StartForm.IsTribune;
                SpeechTypeObj speechtype = _StartForm.SpeechType;

                StartSpeeching(_SpeechTime, speechtype, fromTribune);
            }
        }

        public void StartSpeeching(int speechtime, SpeechTypeObj speechtype, bool fromTribune)
        {
            _lastspeechsecs = 0;

            if (_theCurrDelegateStatem != null)
            {
                DelegateStatementObj ds = Delegate_Apply(_theCurrDelegateStatem.idSeat.MicNum);
                if (ds != null)
                    _theCurrDelegateStatem = ds;
                if (_theCurrDelegateStatem.State != 2) // проверить, что в данный момент депутат не говорит...
                {
                    MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
                    _theCurrDelegateStatem.State = 2;
                    _theCurrDelegateStatem.SpeechDurationLast = 0;
                    //          _theCurrDelegateStatem.SpeechDurationTotal = _SpeechTime;
                    _theCurrDelegateStatem.SpeechTimeStart = DateTime.Now;
                    _theCurrDelegateStatem.idSpeechType = speechtype;
                    _theCurrDelegateStatem.Save();


                    SeatObj seat = _theCurrDelegateStatem.idSeat;

                    if (fromTribune == false)
                        ConfHelper.SendCommand_MicOn(seat.MicNum); // включить микрофон только если доклад идет с места

                    _theCurrDelegateSpeeching = _theCurrDelegateStatem;

                    _speechMode = true;

                    _View.StartSpeeching(speechtime);
                    _View.UpdateControls();
                    UpdateSeats();
                }
            }
        }

        public void StartDirectSpeeching()
        {
            _lastspeechsecs = 0;

            _SpeechTime = 10;

            if (_theCurrDelegateStatem == null)
                return;

            _theCurrDelegateStatem.State = 2;
            _theCurrDelegateStatem.idSpeechType = _speechDefaultType;

            MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
            _theCurrDelegateStatem.SpeechDurationLast = 0;
//          _theCurrDelegateStatem.SpeechDurationTotal = _SpeechTime;
            _theCurrDelegateStatem.SpeechTimeStart = DateTime.Now;
            _theCurrDelegateStatem.Save();


            SeatObj seat = _theCurrDelegateStatem.idSeat;

            ConfHelper.SendCommand_MicOn(seat.MicNum); // включить микрофон

            _theCurrDelegateSpeeching = _theCurrDelegateStatem;

            _speechMode = true;
            _speechDirectMode = true;


            _View.StartSpeeching(_SpeechTime);
            _View.UpdateControls();
            UpdateSeats();

            SetPosition(_theCurrDelegateSpeeching);
        }

        public void SetPosition(DelegateStatementObj ds)
        {
            _TheStatemDetails.Reload();
            int index = _TheStatemDetails.IndexOf(ds);

            _View.NavigatorPosition = index;
            UpdatePropData();
        }

        public void StopSpeeching(int speechSeconds, bool IsMicCommand, bool FinishSpeech)
        {
//          MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
            if (_theCurrDelegateSpeeching == null)
                return;

//          _View.UpdateControls();

            if (FinishSpeech)
            {
                _theCurrDelegateSpeeching.QNum = GetMaxQueueNum(true) + 1;
                _theCurrDelegateSpeeching.State = 1;
                _TheStatement.DelegateQnty = _TheStatement.DelegateQnty + 1;
                _TheStatement.AvrgTime = Convert.ToInt32(_TheStatement.SpeechTimeTotal / _TheStatement.DelegateQnty);
            }
            else
            {
            }

            _theCurrDelegateSpeeching.SpeechDurationTotal = _theCurrDelegateSpeeching.SpeechDurationTotal + speechSeconds;
            _TheStatement.SpeechTimeTotal = _TheStatement.SpeechTimeTotal + speechSeconds;

            _theCurrDelegateSpeeching.Save();
            _TheStatement.Save();

            SeatObj seat = _theCurrDelegateSpeeching.idSeat;// DataModel.SesDelegates_GetByDelegateID(_theDelegateSpeech.idDelegate.id);
            if (IsMicCommand && FinishSpeech)
                ConfHelper.SendCommand_MicOff(seat.MicNum); // выключить микрофон

            _theCurrDelegateSpeeching = null;

            _speechMode = false;
            _speechDirectMode = false;

            _View.UpdateControls();
            UpdateSeats();

            _mainController.CurrentActivity = MainCntrler.Activities.None;
            _mainController.ShowMonitorPreview();
        }

        public void SelectFirstDelegate()
        {
            if (_TheStatemDetails.Count == 0)
                return;

            DelegateStatementObj selDelegate = _TheStatemDetails[0];
            foreach (DelegateStatementObj d in _TheStatemDetails)
            {
                if (d.State == 0 || d.State == 2) // парень готовится к выступлению или выступает
                {
                    selDelegate = d;
                    break;
                }
            }

            if (selDelegate != null)
                SetPosition(selDelegate);
        }

        public void CancelSpeeching()
        {
            if (_theCurrDelegateSpeeching == null)
                return;


            SeatObj seat = _theCurrDelegateSpeeching.idSeat;// DataModel.SesDelegates_GetByDelegateID(_theDelegateSpeech.idDelegate.id);
            ConfHelper.SendCommand_MicOff(seat.MicNum); // включить микрофон

            _theCurrDelegateSpeeching.State = 0;
            _theCurrDelegateSpeeching.Save();

            _speechMode = false;
            _speechDirectMode = false;

            _View.UpdateControls();
            UpdateSeats();

            _theCurrDelegateSpeeching = null;

            _mainController.CurrentActivity = MainCntrler.Activities.None;
            _mainController.ShowMonitorPreview();

        }

        public void OnRegProcess()
        {

            ConfHelper.MicState ms = new ConfHelper.MicState();
            bool b = ConfHelper.GetData_Apply(ref ms);

            while (b)
            {
                int micNum = ms.MicNum;
                string btnState = ms.State; 

                if (micNum > 0)
                {
                    if (btnState == "apply") // запрос на выступление
                    {
                        Delegate_Apply(micNum);
                    }
                    else if (btnState == "off") // запрос на прекращение выступления
                    {
                        foreach (DelegateStatementObj ds in _TheStatemDetails)
                        {
                            if (ds.State == 2 && micNum == ds.idSeat.MicNum)
                            {
                                // делегат выступает прямо сейчас и выключил у себя кнопку, значит надо прекратить его выступления
                                int secs = _View.StopSpeeching(); // иммитируем нажатие стоп кнопки
                                StopSpeeching(secs, false, true);
                                break;
                            }
                        }
                    }
                    else if (btnState == "unapply") // запрос на прекращение выступления
                    {
                        DelegateStatementObj unapplydeleg = null;
                        foreach (DelegateStatementObj ds in _TheStatemDetails)
                        {
                            if (ds.State == 0 && micNum == ds.idSeat.MicNum)
                            {
                                unapplydeleg = ds;
                                break;
                            }
                        }

                        if (unapplydeleg != null)
                            RemoveDelegate(unapplydeleg);
                    }
                }

                UpdateSeats();
                b = ConfHelper.GetData_Apply(ref ms);
            } // while
        }

        private DelegateStatementObj Delegate_Apply(int micNum)
        {
            DelegateStatementObj res = null;
            SesDelegateObj sesd = DataModel.SesDelegatesST_GetByMicNum(micNum);
            DelegateObj theDelegate = null;
            if (sesd != null && sesd.idDelegate.id != 0 && sesd.IsCardRegistered == true)
            {
                theDelegate = DataModel.Delegates.Lookup(sesd.idDelegate.id);
            }
            else if (sesd == null && micNum == HallCntrler.ID_TRIBUNE)
            {
                theDelegate = DataModel.Delegates.Lookup(DataModel._ID_TRIBUNE); // найти "выступающего с трибуны"
            }
            if (theDelegate != null)
            {
                // проверка на отсутствие дублирования
                bool found = false;
                foreach (DelegateStatementObj ds in _TheStatemDetails)
                {
                    if (ds.idDelegate.id == theDelegate.id)
                    {
                        if (ds.State == 0)
                        {
                            res = ds;
                            found = true; // делегат найден среди невыступавших, добавлять не надо
                            break;
                        }
                    }
                }

                if (!found)
                {
                    SeatObj seat = _hall.GetSeatByMic(micNum);
                    if (seat != null)
                    {
                        res = new DelegateStatementObj(Session.DefaultSession);
                        int idMax = _mainController.GetMaxID("DelegateStatements");

                        res.id = idMax + 1;
                        res.idDelegate = theDelegate;
                        res.idSeat = seat;
                        res.idStatement = _TheStatement;
                        res.QNum = GetMaxQueueNum(false) + 1;

                        _TheStatemDetails.Add(res);
                        // записать изменения в базу данных
                        res.Save();
                        _TheStatemDetails.Reload();
                    }
                }
            }

            return res;
        }

        int _lastspeechsecs = 0;
        public void OnSpeechProcess(int speechsecs, int speechtime)
        {
            if (_speechMode == true)
            {
                int speechmin = (Convert.ToInt32(speechsecs / 60));
                _theCurrDelegateSpeeching.SpeechDurationLast = speechsecs;
                _theCurrDelegateSpeeching.Save();

                if (_speechDirectMode == false && ( (speechsecs - _lastspeechsecs) > 60 || _lastspeechsecs == 0) )
                {
                    _lastspeechsecs = speechsecs;
                    // здесь следует сохранить текущую информацию о процессе выступления
                    Platform.SpeechInfoStart info = new Platform.SpeechInfoStart();
                    info.leftMinutes = speechtime - speechmin;
                    info.SpeechTime = speechtime;
                    _mainController._MonitorInfo = info;
                    _mainController.CurrentActivity = MainCntrler.Activities.SpeechStart;
                    _mainController.ShowMonitorPreview();
                }
            }
        }

        public void OnSelHallPage()
        {
            _hallMode = true;
            InitSeats();
            UpdateSeats();
            _View.InitHallPage();
            _View.UpdateControls();
            ShowDelegateProp(null);
        }

        public void OnSelDelegatesPage()
        {
            _hallMode = false;
            _View.InitDelegatesPage();
            _View.UpdateControls();
            _View.SetDelegateProperties2(_TheStatemDetails);
        }

        // выисляет значение результата для пульта с указанным номером
        public SeatStates GetSeatState(int MicNum)
        {

            SeatStates res = SeatStates.None;

            if (_States.ContainsKey(MicNum))
                res =_States[MicNum];


            return res;
        }

        public void DelegateMicOn(int MicNum)
        {
            if (_viewMode == true)
                return;  // в режиме просомтра микрофоны не включаем

            if (MicNum < 1)
                return;

            int curmic = 0;
            if (SpeechMode == true && _theCurrDelegateSpeeching != null)
            {
                DialogResult dr = DialogResult.Yes;
//              DialogResult dr = _msgForm.ShowQuestion("Завершить текущее выступление?");
                if (dr != DialogResult.Yes)
                    return;

                if (_theCurrDelegateSpeeching != null)
                {
                    curmic = _theCurrDelegateSpeeching.idSeat.MicNum;
                    int Secs = _View.StopSpeeching();
                    StopSpeeching(Secs, true, true); // завершить выступление
                }
            }

            if (curmic != MicNum)
            {

                DelegateStatementObj ds = Delegate_Apply(MicNum);
                if (ds != null)
                {
                    _theCurrDelegateStatem = ds;
                    if (_theCurrDelegateStatem.State != 2) // проверить, что в данный момент депутат не говорит...
                    {
                        StartDirectSpeeching();
                    }
                }
            }
            else
                SelectFirstDelegate();

            UpdateSeats();
        }


        public SesDelegateObj GetDelegateByMic(int micNum)
        {
            return DataModel.SesDelegatesST_GetByMicNum(micNum);
        }

        public void PrintThemeReport()
        {
            ReportCntrler c = (ReportCntrler)_mainController.GetSubController("Report Controller");
            c.ShowReport_T(_TheStatement);
        }

#endregion public methods, which is called from the view

#region private methods
        /*
        /// получить максимальный идентификатор вопроса из списка
        /// IsFinishedState - если true, то взять макисмальный номер среди делегатов, завершивших свое выступление
        */ 
        private int GetMaxQueueNum(bool IsFinishedState)
        {
            int maxQNum = 0;
            if (IsFinishedState == false)
            {
                if (_TheStatemDetails.Count > 0)
                    maxQNum = _TheStatemDetails.Max<DelegateStatementObj>(v => v.QNum);
            }
            else
            {
                CriteriaOperator criteria = CriteriaOperator.Parse("State = 1");
                _TheStatemDetails.Filter = criteria;
                if (_TheStatemDetails.Count > 0)
                    maxQNum = _TheStatemDetails.Max<DelegateStatementObj>(v => v.QNum);

                _TheStatemDetails.Filter = null;
            }
            return maxQNum;
        }



        // добавляет депутата в список депутатов сессии  из глобального списка депутатов
        public void AppendDelegate()
        {

            List<int> arr = new List<int>();
            List<int> arr_Max = new List<int>();
            foreach (DelegateStatementObj ds in _TheStatemDetails)
            {
                if (ds.State == 0)
                    arr.Add(ds.idDelegate.id); // убрать из списка для выбора тех депутатов, которые уже записались
            }

            foreach (SesDelegateObj ds in DataModel.SesDelegates_st)
            {
                if (ds.idSeat.id == 0)
                    arr.Add(ds.idDelegate.id); // убрать из списка для выбора тех депутатов, которые не имеют рассадки
            }

            // взять контроллер, отвечающий за глобальный список депутатов
            DelegatesCntrler d = (DelegatesCntrler)_mainController.GetSubController("Delegate List Controller");
            d.Mode = DelegatesCntrler.DelegatesFormMode.SelectStatem;

            bool IsOk = d.SelectItems(arr); // выбрать депутата из глобального списка

            if (IsOk == true)
            {
                DelegateObj Delegate = d.GetFirstSelItem();

//              int idMax = _mainController.GetMaxID("DelegateStatements");

                while (Delegate != null)
                {
                    if (Delegate.isActive == true)
                    {

                        SesDelegateObj sesd = DataModel.SesDelegatesST_GetByDelegateID(Delegate.id);

                        Delegate_Apply(sesd.idSeat.MicNum);
/*
                        DelegateStatementObj ds = new DelegateStatementObj();

                        ds.id = idMax + 1;
                        ds.idSesDelegate = sesd;
                        ds.idStatement = _TheStatement;
                        ds.QNum = GetMaxQueue() + 1;

                        _TheStatemDetails.Add(ds);

                        // записать изменения в базу данных

                        ds.Save();

                        idMax = ds.id;
 */ 
                    }
                    Delegate = d.GetNextSelItem();
                }

                _TheStatemDetails.Reload();
            }
        }

        public void ClearAllDelegates()
        {
            ArrayList arr = new ArrayList();
            foreach (DelegateStatementObj ds in _TheStatemDetails)
            {
                if (ds.State == 0) 
                {
                    arr.Add(ds);
                }
            }

            foreach (DelegateStatementObj ds in arr)
            {
                if (ds.State == 0) 
                {
                    RemoveDelegate(ds);
                }
            }

            UpdatePropData();

            ConfHelper.SendCommand_MicResetAll();
        }

        public void RemoveCurrentItem()
        {
            if (_theCurrDelegateStatem == null)
               return;

            RemoveDelegate(_theCurrDelegateStatem);

            SelectFirstDelegate();
        }

        private void RemoveDelegate( DelegateStatementObj dso)
        {
            if (dso == null)
                return;

            ConfHelper.SendCommand_MicReset(dso.idSeat.MicNum);
            try
            {
                if (dso.State != 2)
                    _TheStatemDetails.Remove(dso);
            }
            catch (Exception Ex)
            {
                
            }
        }

        // вывести информацию о текущем вопросе в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;

            _theCurrDelegateStatem = null;

            bool IsFinished = true;

            if (pos >= 0 && pos < _TheStatemDetails.Count)
            {
                _theCurrDelegateStatem = _TheStatemDetails[pos];
                IsFinished = (_theCurrDelegateStatem.State == 1);
            }

            _View.EnableButtons(IsFinished);
        }

        public SesDelegateObj ShowDelegateProp(int micnum)
        {
            SesDelegateObj Delegate = DataModel.SesDelegatesST_GetByMicNum(micnum);
            ShowDelegateProp(Delegate);
            return Delegate;
        }


        // вывести на форму на панель свойств информацию об указанном депутате
        private void ShowDelegateProp(SesDelegateObj theDelegate)
        {
            int idDelegate = -1;
            if (theDelegate != null && theDelegate.IsCardRegistered == true)
                idDelegate = theDelegate.id;

            CriteriaOperator criteria1 = CriteriaOperator.Parse("id == ?", idDelegate);

            _localdelegates.Filter = criteria1;

            _View.SetDelegateProperties1(_localdelegates);
        }
/*
        public void OnRefreshDelegates()
        {

            if (_viewMode == true)
                return;

            if (_hallMode == true)
            {
//                _localdelegates = new XPCollection<SesDelegateObj>(DataModel.SesDelegates);
//                _View.SetDataSources(ref _localdelegates);

//                InitSeats();
//                UpdateSeats();
            }

//            else
//            {
//                DataModel.CheckDBaseChanges();
//            }
 
        }
*/

        private void InitSeats()
        {
            DataModel.UpdateSesDelegatesST();

            _States.Clear();

            foreach (SeatObj s in DataModel.Seats)
            {
                _States[s.MicNum] = SeatStates.None;
            }

            foreach (SesDelegateObj d in DataModel.SesDelegates_st)
            {
                int micnum = d.idSeat.MicNum;

                if (micnum >= 1)
                {
                    if (d.idSeat.PhysicalState == false)
                        _States[micnum] = SeatStates.Disabled;
                    else if (d.IsCardRegistered == false)
                        _States[micnum] = SeatStates.None;
                    else
                        _States[micnum] = SeatStates.Ready;
                }
            }

//          _View.InitSeats();
        }

        private void UpdateSeats()
        {
            if (_hallMode == false)
                return;

            for (int i = 0; i < DataModel.Seats.Count; i++) 
            {
                if (_States.ContainsKey(i))
                {
                    SeatStates st = _States[i];
                    if (st == SeatStates.SpeechStart || st == SeatStates.SpeechReady)
                        _States[i] = SeatStates.Ready;
                }
            }

            int QueueNum = Int32.MaxValue;
            int hotMicNum = 0;
            foreach (DelegateStatementObj d in _TheStatemDetails)
            {
                if (d.idSeat.MicNum > 0)
                {
                    if (d.State == 0) // готовятся
                    {
                        _States[d.idSeat.MicNum] = SeatStates.SpeechReady;

                        if (d.QNum < QueueNum)
                        {
                            hotMicNum = d.idSeat.MicNum;
                            QueueNum = d.QNum;
                        }
                    }
                    else if (d.State == 2) // выступает
                        _States[d.idSeat.MicNum] = SeatStates.SpeechStart;
                }
            }

            if (hotMicNum > 0)
                _States[hotMicNum] = SeatStates.SpeechReadyHot;

            _View.InitSeats();
        }


#endregion private methods

#region public methods


        // открытие формы для старта голосования
        public void OnSpeechStartFormLoad()
        {
//            _View.EnableMonitor(_mainController.PanelEnabled);
            //            ShowMonitorPreview("VotePrepare");
//            _IsvoteInfoToScreen = true;
        }

#endregion  public methods

    }
}
