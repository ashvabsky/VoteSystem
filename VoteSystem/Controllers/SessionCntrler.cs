﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;


namespace VoteSystem
{
    /* контроллер для MainForm.SessionPage
     * взаимодействует с интерфейсом -ISessionPage
     */
    public class SessionCntrler
    {

        SessionObj _TheSession; // xpo-объект - информация о сессии из БД

        MainCntrler _mainController; // главный контроллер
        ISessionPage _View; // интерфейс основной View
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        public SessionCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(ISessionPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void Init()
        {
            _TheSession = DataModel.TheSession;

            _View.Init();
        }

        public void InitPage()
        {
            _View.InitControls(_TheSession, DataModel.TheSettings, DataModel.IsSpeechApp);

            // подготовка информации для вывода на мониторы
            _mainController.CurrentActivity = MainCntrler.Activities.Session;
            Platform.SessionInfo info = new Platform.SessionInfo();
            info.Caption = DataModel.TheSession.Code;// +". " + DataModel.TheSession.Caption;
//            info.Description = DataModel.TheSession.Content;
            info.DelegateQnty = DataModel.RegDelegateQnty;
            info.QuorumQnty = _mainController.CalcQuorumQnty();
            _mainController._MonitorInfo = info;
        }

        public void SaveSession()
        {
            _TheSession.Save();
        }

        public void SessionFinish()
        {
            _TheSession.IsFinished = true;
            DateTime fd = _View.GetFinishdate();

            if (fd == DateTime.MinValue)
                _TheSession.FinishDate = DateTime.Now;
            else
                _TheSession.FinishDate = fd;

            SaveSession();
            _View.InitControls(_TheSession, DataModel.TheSettings, DataModel.IsSpeechApp);
        }

        public void SessionNew()
        {
            if (_TheSession.IsFinished != true)
                SessionFinish();

            SessionObj oldSession = _TheSession;

            _TheSession = new SessionObj(Session.DefaultSession);

            _TheSession.id = GetMaxID() + 1;

            _TheSession.StartDate = DateTime.Today;
            _TheSession.FinishDate = _TheSession.StartDate;
            _TheSession.Caption = "Новая сессия";
            _TheSession.Code = "0";
            _TheSession.HallName = oldSession.HallName;

            DataModel.Sessions.Add(_TheSession);            

            SaveSession();

            _View.InitControls(_TheSession, DataModel.TheSettings, DataModel.IsSpeechApp);
        }

        public void HistoryAdd(string caption, string msg)
        {
            string histmsg = DateTime.Now.ToString("t") + " " + caption + ": <<" + msg + ">>";
            _View.AddHistoryLine(histmsg);
        }

        // получить максимальный идентификатор вопроса из списка
        private int GetMaxID()
        {
            int maxid = 0;
            if (DataModel.Sessions.Count > 0)
                maxid = DataModel.Sessions.Max<SessionObj>(v => v.id);

            return maxid;
        }
    }
}
