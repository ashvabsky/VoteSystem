﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    /*
     * Контроллер предназначен для управления списком вопросов с результатами голосования
     * на страничке главного окна MainForm.ResultsPage (далее, основная страничка)
     */
    public class ResQuestionsCntrler
    {
#region private секция данных

        SessionObj _TheSession;  // данные о текущей сессии
        IResultsPage _View; // интерфейс для управления основной страничкой 
//      IQuestProperties _questSheetView; // интерфейс для управления панелью свойств на форме

        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        MainCntrler _mainController; // главный контроллер

        QuestionObj _theQuestion; // текущий (выбранный или выделенный в данный момент) вопрос из списка
        NestedUnitOfWork _nuow;

        XPCollection _Questions; // список вопросов с результатами 
        XPCollection<VoteTypeObj> _VoteTypes; // типы голосваний
        XPCollection<VoteKindObj> _VoteKinds; // виды голосваний
/*
        XPCollection<VoteQntyObj> _VoteQntyTypes; // 
        XPCollection<DecisionProcTypeObj> _DecisionProcTypes;
        XPCollection<QuotaProcTypeObj> _QuotaProcTypes;
 */ 
        XPCollection<VoteResultObj> _VoteResults; // список результатов голосований

//      XPCollection<QuestResultType> _ResultValTypes;

#endregion private секция данных

#region основные методы
        public ResQuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IResultsPage viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
//          _questSheetView = _View.GetQuestPropertiesInterface();
        }

        // первоначальная инициализация
        public void Init(ref XPCollection questions)
        {
            _TheSession = DataModel.TheSession;
            _Questions = questions;
            _VoteTypes = DataModel.VoteTypes;
            _VoteKinds = DataModel.VoteKinds;
            _VoteResults = DataModel.VoteResults;

/*
            _VoteQntyTypes = DataModel.VoteQntyTypes;
            _DecisionProcTypes = DataModel.DecisionProcTypes;
            _QuotaProcTypes = DataModel.QuotaProcTypes;
            _ResultValTypes = DataModel.QResTypes;
*/
            CriteriaOperator cr = new BinaryOperator(
            new OperandProperty("idSession"), new OperandValue(_TheSession.id),
            BinaryOperatorType.Equal);

            _Questions.Criteria = cr;
            DataModel.ResQuestions.Criteria = cr;

            DataModel.ResQuestions.Load();

            _View.Init();
        }

        // инициализация странички при ее активации
        public void InitPage()
        {
            _Questions.Reload();
            _VoteResults.Reload();
            _View.InitControls(ref _Questions);

            UpdatePropData();

            _mainController.CurrentActivity = MainCntrler.Activities.None;
        }

#endregion основные методы

#region public methods, which is called from the view

        // вывести информацию о текущем вопросе в окно свойств
        public void UpdatePropData()
        {
            int pos = _View.NavigatorPosition;
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as QuestionObj;

            ShowResults();

        }

        // попытка создать новое чтение существующего вопроса
        public void CreateNewReading()
        {
            DialogResult dr = _msgForm.ShowQuestion("Вы намерены создать новое чтение по выбранному вопросу?");
            if (dr != DialogResult.Yes)
                return;

             SesTaskObj q = new SesTaskObj(_theQuestion);
             q.ReadNum = _theQuestion.idTask.ReadNum + 1;
            bool bRes = _mainController.AddNewVoteQuestion(q);
            if (bRes == false)
            {
                _msgForm.ShowWarningMsg("Не удалось создать новое чтение по данному вопросу.");
            }
        }

        public void EditCurrRecord()
        {
            _nuow = _Questions.Session.BeginNestedUnitOfWork();

            SesTaskObj Q = _nuow.GetNestedObject(_theQuestion.idTask) as SesTaskObj;

            QuestionEditForm qedt = new QuestionEditForm(Q);
            qedt._readMode = true;
            qedt.ShowDialog();

        }

        // просмотреть подробные итоги голосования по выбранному вопросу
        public void VoteDetailsRun()
        {
            _mainController.VoteDetailsRun(CurrentQuestion);
        }

        // выбор вопроса из списка
        public void OnSelectNewPosition()
        {
            _mainController.ClearMonitor();
            UpdatePropData();
        }


#endregion public methods, which is called from the view

#region public methods

        public QuestionObj CurrentQuestion
        {
            get { return _theQuestion; }
        }

        // находит указанные вопрос в списке и выделяет его как текущий
        public void SetPosition(QuestionObj Q)
        {
            _mainController.ClearMonitor();

            _Questions.Reload();
            int index = _Questions.IndexOf(Q);

            _View.NavigatorPosition = index;
            UpdatePropData();
        }
#endregion  public methods

#region private methods


        // передает данные по итогам голосвания по текущему вопросу для отображения на форме
        private void ShowResults()
        {

            VoteResultObj vres = _theQuestion.idResult.idVoteResult;

            _View.ShowVoteResults(vres);
            _VoteResults.Filter = null;

            Platform.VoteInfoResult info = new Platform.VoteInfoResult();
            info.VotedQnty_A = vres.AnsQnty1;
            info.VotedQnty_B = vres.AnsQnty2;
            info.VotedQnty_C = vres.AnsQnty3;
            info.DelegateQnty = vres.AvailableQnty;
            bool isaccept = false;
            if (vres.idResultValue.CodeName == "Parlament_Accept")
                isaccept = true;
            info.IsResult = isaccept;

            _mainController._MonitorInfo = info;
            _mainController.CurrentActivity = MainCntrler.Activities.VoteResults;
        }

#endregion private methods
    }
}
