namespace VoteSystem
{
    partial class VoteStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoteStartForm));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnMonitorVotePrepare = new DevExpress.XtraEditors.CheckButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.chkTotalsToScreen = new DevExpress.XtraEditors.CheckEdit();
            this.chkVoteProccess = new DevExpress.XtraEditors.CheckEdit();
            this.txtDelegateQnty = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoteTime = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtIsQuorum = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTotalsToScreen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoteProccess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsQuorum.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl1.Controls.Add(this.btnMonitorVotePrepare);
            this.groupControl1.Controls.Add(this.chkTotalsToScreen);
            this.groupControl1.Controls.Add(this.chkVoteProccess);
            this.groupControl1.Location = new System.Drawing.Point(19, 137);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(249, 137);
            this.groupControl1.TabIndex = 30;
            this.groupControl1.Text = "����� �� ��������";
            // 
            // btnMonitorVotePrepare
            // 
            this.btnMonitorVotePrepare.ImageIndex = 6;
            this.btnMonitorVotePrepare.ImageList = this.ButtonImages;
            this.btnMonitorVotePrepare.Location = new System.Drawing.Point(5, 90);
            this.btnMonitorVotePrepare.Name = "btnMonitorVotePrepare";
            this.btnMonitorVotePrepare.Size = new System.Drawing.Size(239, 34);
            this.btnMonitorVotePrepare.TabIndex = 11;
            this.btnMonitorVotePrepare.Text = "������� ����������� �� �������";
            this.btnMonitorVotePrepare.CheckedChanged += new System.EventHandler(this.btnMonitorPrepare_CheckedChanged);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // chkTotalsToScreen
            // 
            this.chkTotalsToScreen.EditValue = true;
            this.chkTotalsToScreen.Location = new System.Drawing.Point(5, 59);
            this.chkTotalsToScreen.Name = "chkTotalsToScreen";
            this.chkTotalsToScreen.Properties.Caption = "������� �������� ���������";
            this.chkTotalsToScreen.Size = new System.Drawing.Size(242, 19);
            this.chkTotalsToScreen.TabIndex = 9;
            this.chkTotalsToScreen.CheckedChanged += new System.EventHandler(this.chkTotalsToScreen_CheckedChanged);
            // 
            // chkVoteProccess
            // 
            this.chkVoteProccess.EditValue = true;
            this.chkVoteProccess.Location = new System.Drawing.Point(5, 33);
            this.chkVoteProccess.Name = "chkVoteProccess";
            this.chkVoteProccess.Properties.Caption = "���������� ��� �����������";
            this.chkVoteProccess.Size = new System.Drawing.Size(248, 19);
            this.chkVoteProccess.TabIndex = 8;
            this.chkVoteProccess.CheckedChanged += new System.EventHandler(this.chkVoteProccess_CheckedChanged);
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDelegateQnty.Location = new System.Drawing.Point(189, 53);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDelegateQnty.Properties.IsFloatValue = false;
            this.txtDelegateQnty.Properties.Mask.EditMask = "000";
            this.txtDelegateQnty.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtDelegateQnty.Properties.ReadOnly = true;
            this.txtDelegateQnty.Size = new System.Drawing.Size(61, 20);
            this.txtDelegateQnty.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(28, 96);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(41, 13);
            this.labelControl5.TabIndex = 26;
            this.labelControl5.Text = "������:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(28, 56);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(155, 13);
            this.labelControl3.TabIndex = 25;
            this.labelControl3.Text = "���������� ��������������:";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 2;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(179, 289);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(93, 37);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "��������";
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnStart.ImageIndex = 0;
            this.btnStart.ImageList = this.ButtonImages;
            this.btnStart.Location = new System.Drawing.Point(19, 289);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(148, 37);
            this.btnStart.TabIndex = 23;
            this.btnStart.Text = "������ �����������";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(256, 20);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(22, 13);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "���.";
            // 
            // txtVoteTime
            // 
            this.txtVoteTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtVoteTime.Location = new System.Drawing.Point(189, 13);
            this.txtVoteTime.Name = "txtVoteTime";
            this.txtVoteTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVoteTime.Properties.IsFloatValue = false;
            this.txtVoteTime.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtVoteTime.Size = new System.Drawing.Size(61, 20);
            this.txtVoteTime.TabIndex = 21;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(28, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(116, 13);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "����� �� �����������:";
            // 
            // txtIsQuorum
            // 
            this.txtIsQuorum.EditValue = "����";
            this.txtIsQuorum.Location = new System.Drawing.Point(189, 93);
            this.txtIsQuorum.Name = "txtIsQuorum";
            this.txtIsQuorum.Size = new System.Drawing.Size(56, 20);
            this.txtIsQuorum.TabIndex = 31;
            // 
            // VoteStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 338);
            this.Controls.Add(this.txtIsQuorum);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.txtDelegateQnty);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtVoteTime);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VoteStartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����������";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTotalsToScreen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoteProccess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIsQuorum.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit chkTotalsToScreen;
        private DevExpress.XtraEditors.CheckEdit chkVoteProccess;
        private DevExpress.XtraEditors.SpinEdit txtDelegateQnty;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit txtVoteTime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtIsQuorum;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.CheckButton btnMonitorVotePrepare;
    }
}