namespace VoteSystem
{
    partial class StatementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatementForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridDelegates = new DevExpress.XtraGrid.GridControl();
            this.xpDelegates = new DevExpress.Xpo.XPCollection();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMicNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox30 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox31 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox32 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox33 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox34 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox35 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox36 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox37 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox38 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox39 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox40 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBox41 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox42 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow13 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow14 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow15 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow16 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow17 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow19 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow20 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow18 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow21 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow7 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow22 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow23 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow24 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow25 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.PropertiesControl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnDelegateAcceptMode = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnVoteFinish = new DevExpress.XtraEditors.SimpleButton();
            this.lblVoteTime = new DevExpress.XtraEditors.LabelControl();
            this.btnVoteCancel = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarVoting = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblVoting = new DevExpress.XtraEditors.LabelControl();
            this.picVoteTime = new DevExpress.XtraEditors.PictureEdit();
            this.btnStateStart = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridDelegates);
            this.splitContainerControl1.Panel1.Controls.Add(this.ribbonControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "������ �������";
            this.splitContainerControl1.Size = new System.Drawing.Size(1227, 701);
            this.splitContainerControl1.SplitterPosition = 835;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridDelegates
            // 
            this.gridDelegates.DataSource = this.xpDelegates;
            this.gridDelegates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDelegates.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridDelegates.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridDelegates.EmbeddedNavigator.Name = "";
            this.gridDelegates.Location = new System.Drawing.Point(0, 95);
            this.gridDelegates.MainView = this.gridView1;
            this.gridDelegates.Name = "gridDelegates";
            this.gridDelegates.Size = new System.Drawing.Size(831, 602);
            this.gridDelegates.TabIndex = 8;
            this.gridDelegates.UseEmbeddedNavigator = true;
            this.gridDelegates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // xpDelegates
            // 
            this.xpDelegates.DeleteObjectOnRemove = true;
            this.xpDelegates.ObjectType = typeof(VoteSystem.DelegateStatementObj);
            this.xpDelegates.DisplayableProperties = "This;id;State;StatementTime;Status;idDelegate.idSeat.MicNum;idDelegate.idDelegate" +
                ".FullName;idDelegate.idDelegate.idRegion.Name;idDelegate.idDelegate.idRegion.Num" +
                "ber;idDelegate.idDelegate.idFraction.Name;idDelegate.idDelegate.idParty.Name;QNu" +
                "m";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colFraction,
            this.colPartyName,
            this.colStatus,
            this.colMicNum,
            this.colQNum});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(862, 678, 208, 191);
            this.gridView1.GridControl = this.gridDelegates;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsPrint.PrintPreview = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStatus, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colFullName
            // 
            this.colFullName.Caption = "���";
            this.colFullName.FieldName = "idDelegate.idDelegate.FullName";
            this.colFullName.MinWidth = 100;
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 1;
            this.colFullName.Width = 290;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "������";
            this.colRegionName.FieldName = "idDelegate.idDelegate.idRegion.Name";
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 2;
            this.colRegionName.Width = 154;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "�������";
            this.colFraction.FieldName = "idDelegate.idDelegate.idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Visible = true;
            this.colFraction.VisibleIndex = 3;
            this.colFraction.Width = 113;
            // 
            // colPartyName
            // 
            this.colPartyName.Caption = "������";
            this.colPartyName.FieldName = "idDelegate.idDelegate.idParty.Name";
            this.colPartyName.MinWidth = 100;
            this.colPartyName.Name = "colPartyName";
            this.colPartyName.OptionsColumn.ReadOnly = true;
            this.colPartyName.Visible = true;
            this.colPartyName.VisibleIndex = 4;
            this.colPartyName.Width = 117;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 100;
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsColumn.ShowCaption = false;
            this.colStatus.Width = 100;
            // 
            // colMicNum
            // 
            this.colMicNum.Caption = "���";
            this.colMicNum.FieldName = "idDelegate.idSeat.MicNum";
            this.colMicNum.Name = "colMicNum";
            this.colMicNum.OptionsColumn.ReadOnly = true;
            this.colMicNum.Visible = true;
            this.colMicNum.VisibleIndex = 5;
            this.colMicNum.Width = 56;
            // 
            // colQNum
            // 
            this.colQNum.Caption = "�������";
            this.colQNum.FieldName = "QNum";
            this.colQNum.Name = "colQNum";
            this.colQNum.OptionsColumn.FixedWidth = true;
            this.colQNum.OptionsFilter.AllowAutoFilter = false;
            this.colQNum.OptionsFilter.AllowFilter = false;
            this.colQNum.Visible = true;
            this.colQNum.VisibleIndex = 0;
            this.colQNum.Width = 80;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonKeyTip = "";
            this.ribbonControl1.ApplicationIcon = null;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.btnClose});
            this.ribbonControl1.LargeImages = this.RibbonImages;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 9;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.SelectedPage = this.ribbonPage1;
            this.ribbonControl1.ShowCategoryInCaption = false;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(831, 95);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "���������";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.LargeImageIndex = 0;
            this.barButtonItem1.LargeWidth = 100;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "����� ����������";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.LargeImageIndex = 10;
            this.barButtonItem2.LargeWidth = 100;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "�������� �����";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.LargeImageIndex = 11;
            this.barButtonItem3.LargeWidth = 100;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "����� ��������";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.LargeImageIndex = 9;
            this.barButtonItem4.LargeWidth = 100;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "�������� ���������";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.LargeImageIndex = 10;
            this.barButtonItem5.LargeWidth = 100;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "��������� �� ��������";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.LargeImageIndex = 1;
            this.barButtonItem6.LargeWidth = 100;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "������";
            this.barButtonItem7.Id = 7;
            this.barButtonItem7.LargeImageIndex = 15;
            this.barButtonItem7.LargeWidth = 100;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // btnClose
            // 
            this.btnClose.Caption = "�����";
            this.btnClose.Id = 8;
            this.btnClose.LargeImageIndex = 16;
            this.btnClose.LargeWidth = 100;
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(48, 48);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.KeyTip = "";
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.KeyTip = "";
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "������������";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup2.KeyTip = "";
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "�������� � ����";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup3.KeyTip = "";
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "������";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(382, 697);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            this.xtraTabControl1.Text = "xtraTabControl1";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.vGridControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(373, 666);
            this.xtraTabPage1.Text = "���������� � �����������";
            // 
            // vGridControl1
            // 
            this.vGridControl1.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.vGridControl1.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vGridControl1.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.vGridControl1.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vGridControl1.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.vGridControl1.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.vGridControl1.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.vGridControl1.Location = new System.Drawing.Point(0, 0);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.OptionsView.FixRowHeaderPanelWidth = true;
            this.vGridControl1.RecordWidth = 108;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemGridLookUpEdit5,
            this.repositoryItemComboBox30,
            this.repositoryItemComboBox31,
            this.repositoryItemComboBox32,
            this.repositoryItemComboBox33,
            this.repositoryItemComboBox34,
            this.repositoryItemComboBox35,
            this.repositoryItemDateEdit5,
            this.repositoryItemComboBox36,
            this.repositoryItemComboBox37,
            this.repositoryItemComboBox38,
            this.repositoryItemComboBox39,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemDateEdit7,
            this.repositoryItemMemoEdit5,
            this.repositoryItemComboBox40,
            this.repositoryItemSpinEdit3,
            this.repositoryItemComboBox41,
            this.repositoryItemComboBox42,
            this.repositoryItemMemoEdit6,
            this.repositoryItemMemoEdit7});
            this.vGridControl1.RowHeaderWidth = 92;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow4,
            this.categoryRow5,
            this.categoryRow7,
            this.categoryRow6});
            this.vGridControl1.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vGridControl1.Size = new System.Drawing.Size(373, 510);
            this.vGridControl1.TabIndex = 13;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemGridLookUpEdit5
            // 
            this.repositoryItemGridLookUpEdit5.AutoHeight = false;
            this.repositoryItemGridLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit5.Name = "repositoryItemGridLookUpEdit5";
            this.repositoryItemGridLookUpEdit5.View = this.gridView11;
            // 
            // gridView11
            // 
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox30
            // 
            this.repositoryItemComboBox30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox30.ImmediatePopup = true;
            this.repositoryItemComboBox30.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox30.Name = "repositoryItemComboBox30";
            // 
            // repositoryItemComboBox31
            // 
            this.repositoryItemComboBox31.AutoHeight = false;
            this.repositoryItemComboBox31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox31.ImmediatePopup = true;
            this.repositoryItemComboBox31.Name = "repositoryItemComboBox31";
            this.repositoryItemComboBox31.Sorted = true;
            // 
            // repositoryItemComboBox32
            // 
            this.repositoryItemComboBox32.AutoHeight = false;
            this.repositoryItemComboBox32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox32.Name = "repositoryItemComboBox32";
            // 
            // repositoryItemComboBox33
            // 
            this.repositoryItemComboBox33.AutoHeight = false;
            this.repositoryItemComboBox33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox33.ImmediatePopup = true;
            this.repositoryItemComboBox33.Name = "repositoryItemComboBox33";
            // 
            // repositoryItemComboBox34
            // 
            this.repositoryItemComboBox34.AutoHeight = false;
            this.repositoryItemComboBox34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox34.Name = "repositoryItemComboBox34";
            // 
            // repositoryItemComboBox35
            // 
            this.repositoryItemComboBox35.AutoHeight = false;
            this.repositoryItemComboBox35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox35.Name = "repositoryItemComboBox35";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            this.repositoryItemDateEdit5.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox36
            // 
            this.repositoryItemComboBox36.AutoHeight = false;
            this.repositoryItemComboBox36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox36.Name = "repositoryItemComboBox36";
            // 
            // repositoryItemComboBox37
            // 
            this.repositoryItemComboBox37.AutoHeight = false;
            this.repositoryItemComboBox37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox37.Name = "repositoryItemComboBox37";
            // 
            // repositoryItemComboBox38
            // 
            this.repositoryItemComboBox38.AutoHeight = false;
            this.repositoryItemComboBox38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox38.Name = "repositoryItemComboBox38";
            // 
            // repositoryItemComboBox39
            // 
            this.repositoryItemComboBox39.AutoHeight = false;
            this.repositoryItemComboBox39.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox39.Name = "repositoryItemComboBox39";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            this.repositoryItemDateEdit7.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit5
            // 
            this.repositoryItemMemoEdit5.Name = "repositoryItemMemoEdit5";
            // 
            // repositoryItemComboBox40
            // 
            this.repositoryItemComboBox40.AutoHeight = false;
            this.repositoryItemComboBox40.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox40.Name = "repositoryItemComboBox40";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // repositoryItemComboBox41
            // 
            this.repositoryItemComboBox41.AutoHeight = false;
            this.repositoryItemComboBox41.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox41.Name = "repositoryItemComboBox41";
            // 
            // repositoryItemComboBox42
            // 
            this.repositoryItemComboBox42.AutoHeight = false;
            this.repositoryItemComboBox42.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox42.Name = "repositoryItemComboBox42";
            // 
            // repositoryItemMemoEdit6
            // 
            this.repositoryItemMemoEdit6.Name = "repositoryItemMemoEdit6";
            // 
            // repositoryItemMemoEdit7
            // 
            this.repositoryItemMemoEdit7.Name = "repositoryItemMemoEdit7";
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow13,
            this.editorRow14,
            this.editorRow15,
            this.editorRow16,
            this.editorRow17,
            this.editorRow19});
            this.categoryRow4.Height = 19;
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "��������������";
            // 
            // editorRow13
            // 
            this.editorRow13.Height = 28;
            this.editorRow13.Name = "editorRow13";
            this.editorRow13.Properties.Caption = "� �����������";
            this.editorRow13.Properties.ReadOnly = true;
            this.editorRow13.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow13.Properties.Value = "12";
            // 
            // editorRow14
            // 
            this.editorRow14.Height = 47;
            this.editorRow14.Name = "editorRow14";
            this.editorRow14.Properties.Caption = "���� �����������";
            this.editorRow14.Properties.ReadOnly = true;
            this.editorRow14.Properties.RowEdit = this.repositoryItemMemoEdit7;
            this.editorRow14.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow14.Properties.Value = "���������� ����������� �������";
            // 
            // editorRow15
            // 
            this.editorRow15.Height = 80;
            this.editorRow15.Name = "editorRow15";
            this.editorRow15.Properties.Caption = "��������";
            this.editorRow15.Properties.RowEdit = this.repositoryItemMemoEdit5;
            this.editorRow15.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow15.Properties.Value = "�������� ����: 1) ��������� ������������� � ��������� ������ 2) ���������� ������" +
                "���������� ���� �67";
            // 
            // editorRow16
            // 
            this.editorRow16.Height = 28;
            this.editorRow16.Name = "editorRow16";
            this.editorRow16.Properties.Caption = "����/����� ��������";
            this.editorRow16.Properties.ReadOnly = true;
            this.editorRow16.Properties.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.editorRow16.Properties.Value = new System.DateTime(2010, 6, 14, 12, 38, 0, 0);
            // 
            // editorRow17
            // 
            this.editorRow17.Name = "editorRow17";
            this.editorRow17.Properties.Caption = "��� �����������";
            this.editorRow17.Properties.ReadOnly = true;
            this.editorRow17.Properties.RowEdit = this.repositoryItemComboBox41;
            this.editorRow17.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow17.Properties.Value = "����������";
            // 
            // editorRow19
            // 
            this.editorRow19.Height = 25;
            this.editorRow19.Name = "editorRow19";
            this.editorRow19.Properties.Caption = "������";
            this.editorRow19.Properties.ReadOnly = true;
            this.editorRow19.Properties.RowEdit = this.repositoryItemComboBox42;
            this.editorRow19.Properties.Value = "���������";
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow20,
            this.editorRow18,
            this.editorRow21});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "���������";
            // 
            // editorRow20
            // 
            this.editorRow20.Name = "editorRow20";
            this.editorRow20.Properties.Caption = "����� �� �����������";
            this.editorRow20.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.editorRow20.Properties.Value = "2 ���.";
            // 
            // editorRow18
            // 
            this.editorRow18.Name = "editorRow18";
            this.editorRow18.Properties.Caption = "��������� ��������";
            this.editorRow18.Properties.Value = true;
            // 
            // editorRow21
            // 
            this.editorRow21.Name = "editorRow21";
            this.editorRow21.Properties.Caption = "���-�� ���. max";
            this.editorRow21.Properties.Value = "10";
            // 
            // categoryRow7
            // 
            this.categoryRow7.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow22,
            this.editorRow23,
            this.editorRow24});
            this.categoryRow7.Name = "categoryRow7";
            this.categoryRow7.Properties.Caption = "�����";
            // 
            // editorRow22
            // 
            this.editorRow22.Name = "editorRow22";
            this.editorRow22.Properties.Caption = "���������, ���.";
            this.editorRow22.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.editorRow22.Properties.Value = "7";
            // 
            // editorRow23
            // 
            this.editorRow23.Name = "editorRow23";
            this.editorRow23.Properties.Caption = "����� �����, ���";
            this.editorRow23.Properties.Value = "12";
            // 
            // editorRow24
            // 
            this.editorRow24.Name = "editorRow24";
            this.editorRow24.Properties.Caption = "������� �����, ���";
            this.editorRow24.Properties.Value = "1.75";
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow25});
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "�������";
            // 
            // editorRow25
            // 
            this.editorRow25.Height = 89;
            this.editorRow25.Name = "editorRow25";
            this.editorRow25.Properties.RowEdit = this.repositoryItemMemoEdit6;
            this.editorRow25.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow25.Properties.Value = "� �������� ���������� ��� ������� ������ �� ���������� ��� �143 � ������������ ��" +
                "����� � ����������� ������";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.PropertiesControl);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(373, 666);
            this.xtraTabPage2.Text = "���������� � ��������";
            // 
            // PropertiesControl
            // 
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControl.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControl.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControl.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControl.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControl.Name = "PropertiesControl";
            this.PropertiesControl.OptionsView.AutoScaleBands = true;
            this.PropertiesControl.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControl.OptionsView.ShowEmptyRowImage = true;
            this.PropertiesControl.RecordWidth = 114;
            this.PropertiesControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit9,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6});
            this.PropertiesControl.RowHeaderWidth = 86;
            this.PropertiesControl.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat,
            this.categoryRow1});
            this.PropertiesControl.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControl.Size = new System.Drawing.Size(373, 455);
            this.PropertiesControl.TabIndex = 10;
            this.PropertiesControl.Click += new System.EventHandler(this.PropertiesControl_Click);
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            this.repositoryItemComboBox4.Sorted = true;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.ImmediatePopup = true;
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "���";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "�������";
            this.rowLastName.Properties.FieldName = "LastName";
            this.rowLastName.Properties.ReadOnly = true;
            this.rowLastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName.Properties.Value = "��������";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "���";
            this.rowFirstName.Properties.FieldName = "FirstName";
            this.rowFirstName.Properties.ReadOnly = true;
            this.rowFirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFirstName.Properties.Value = "������";
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "��������";
            this.rowSecondName.Properties.FieldName = "SecondName";
            this.rowSecondName.Properties.ReadOnly = true;
            this.rowSecondName.Properties.Value = "����������";
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "������";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "������";
            this.rowRegionName.Properties.FieldName = "RegionName";
            this.rowRegionName.Properties.ReadOnly = true;
            this.rowRegionName.Properties.RowEdit = this.repositoryItemComboBox4;
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionName.Properties.Value = "������-��������";
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "� �������";
            this.rowRegionNum.Properties.FieldName = "RegionNum";
            this.rowRegionNum.Properties.ReadOnly = true;
            this.rowRegionNum.Properties.RowEdit = this.repositoryItemComboBox5;
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionNum.Properties.Value = "� 25";
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction,
            this.rowPartyName});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "�������������� ����������";
            // 
            // rowFraction
            // 
            this.rowFraction.Height = 25;
            this.rowFraction.Name = "rowFraction";
            this.rowFraction.Properties.Caption = "�������";
            this.rowFraction.Properties.FieldName = "Fraction";
            this.rowFraction.Properties.ReadOnly = true;
            this.rowFraction.Properties.RowEdit = this.repositoryItemComboBox6;
            this.rowFraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFraction.Properties.Value = "�����������������";
            // 
            // rowPartyName
            // 
            this.rowPartyName.Height = 23;
            this.rowPartyName.Name = "rowPartyName";
            this.rowPartyName.Properties.Caption = "������";
            this.rowPartyName.Properties.FieldName = "idParty.Name";
            this.rowPartyName.Properties.ReadOnly = true;
            this.rowPartyName.Properties.RowEdit = this.repositoryItemComboBox2;
            this.rowPartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowPartyName.Properties.Value = "������ 1";
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum});
            this.catSeat.Name = "catSeat";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Height = 16;
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "�������� �";
            this.rowMicNum.Properties.ReadOnly = true;
            this.rowMicNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.rowMicNum.Properties.Value = "73";
            // 
            // rowRowNum
            // 
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "��� �";
            this.rowRowNum.Properties.ReadOnly = true;
            this.rowRowNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.rowRowNum.Properties.Value = "7";
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "����� �";
            this.rowSeatNum.Properties.ReadOnly = true;
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.rowSeatNum.Properties.Value = "3";
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow1,
            this.editorRow2,
            this.editorRow3});
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "����� �����������";
            // 
            // editorRow1
            // 
            this.editorRow1.Height = 31;
            this.editorRow1.Name = "editorRow1";
            this.editorRow1.Properties.Caption = "��������";
            this.editorRow1.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.editorRow1.Properties.Value = true;
            // 
            // editorRow2
            // 
            this.editorRow2.Name = "editorRow2";
            this.editorRow2.Properties.Caption = "����� �����������";
            this.editorRow2.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.editorRow2.Properties.Value = "2 ���.";
            // 
            // editorRow3
            // 
            this.editorRow3.Height = 88;
            this.editorRow3.Name = "editorRow3";
            this.editorRow3.Properties.Caption = "�������";
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.panelControl2.Controls.Add(this.checkBox1);
            this.panelControl2.Controls.Add(this.btnDelegateAcceptMode);
            this.panelControl2.Controls.Add(this.btnVoteFinish);
            this.panelControl2.Controls.Add(this.lblVoteTime);
            this.panelControl2.Controls.Add(this.btnVoteCancel);
            this.panelControl2.Controls.Add(this.progressBarVoting);
            this.panelControl2.Controls.Add(this.lblVoting);
            this.panelControl2.Controls.Add(this.picVoteTime);
            this.panelControl2.Controls.Add(this.btnStateStart);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 701);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1227, 85);
            this.panelControl2.TabIndex = 34;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(87, 57);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(249, 17);
            this.checkBox1.TabIndex = 263;
            this.checkBox1.Text = "������������� ���������� ������������";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnDelegateAcceptMode
            // 
            this.btnDelegateAcceptMode.ImageIndex = 13;
            this.btnDelegateAcceptMode.ImageList = this.ButtonImages;
            this.btnDelegateAcceptMode.Location = new System.Drawing.Point(87, 6);
            this.btnDelegateAcceptMode.Name = "btnDelegateAcceptMode";
            this.btnDelegateAcceptMode.Size = new System.Drawing.Size(249, 38);
            this.btnDelegateAcceptMode.TabIndex = 262;
            this.btnDelegateAcceptMode.Text = "�������� ������ �� �����������";
            this.btnDelegateAcceptMode.Click += new System.EventHandler(this.btnDelegateAcceptMode_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnVoteFinish
            // 
            this.btnVoteFinish.ImageIndex = 8;
            this.btnVoteFinish.ImageList = this.ButtonImages;
            this.btnVoteFinish.Location = new System.Drawing.Point(370, 48);
            this.btnVoteFinish.Name = "btnVoteFinish";
            this.btnVoteFinish.Size = new System.Drawing.Size(172, 31);
            this.btnVoteFinish.TabIndex = 257;
            this.btnVoteFinish.Text = "��������� �����������";
            // 
            // lblVoteTime
            // 
            this.lblVoteTime.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoteTime.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblVoteTime.Appearance.Options.UseFont = true;
            this.lblVoteTime.Appearance.Options.UseForeColor = true;
            this.lblVoteTime.Location = new System.Drawing.Point(610, 37);
            this.lblVoteTime.Name = "lblVoteTime";
            this.lblVoteTime.Size = new System.Drawing.Size(42, 15);
            this.lblVoteTime.TabIndex = 243;
            this.lblVoteTime.Text = "02 : 30";
            // 
            // btnVoteCancel
            // 
            this.btnVoteCancel.ImageIndex = 12;
            this.btnVoteCancel.ImageList = this.ButtonImages;
            this.btnVoteCancel.Location = new System.Drawing.Point(870, 23);
            this.btnVoteCancel.Name = "btnVoteCancel";
            this.btnVoteCancel.Size = new System.Drawing.Size(194, 38);
            this.btnVoteCancel.TabIndex = 241;
            this.btnVoteCancel.Text = "�������� �����������";
            // 
            // progressBarVoting
            // 
            this.progressBarVoting.Location = new System.Drawing.Point(666, 49);
            this.progressBarVoting.Name = "progressBarVoting";
            this.progressBarVoting.Size = new System.Drawing.Size(144, 18);
            this.progressBarVoting.TabIndex = 240;
            // 
            // lblVoting
            // 
            this.lblVoting.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblVoting.Appearance.Options.UseFont = true;
            this.lblVoting.Location = new System.Drawing.Point(666, 23);
            this.lblVoting.Name = "lblVoting";
            this.lblVoting.Size = new System.Drawing.Size(108, 13);
            this.lblVoting.TabIndex = 229;
            this.lblVoting.Text = "���� �����������...";
            // 
            // picVoteTime
            // 
            this.picVoteTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picVoteTime.EditValue = ((object)(resources.GetObject("picVoteTime.EditValue")));
            this.picVoteTime.Location = new System.Drawing.Point(570, 27);
            this.picVoteTime.Name = "picVoteTime";
            this.picVoteTime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picVoteTime.Properties.Appearance.Options.UseBackColor = true;
            this.picVoteTime.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picVoteTime.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picVoteTime.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picVoteTime.Properties.UseParentBackground = true;
            this.picVoteTime.Size = new System.Drawing.Size(35, 47);
            this.picVoteTime.TabIndex = 228;
            // 
            // btnStateStart
            // 
            this.btnStateStart.ImageIndex = 0;
            this.btnStateStart.ImageList = this.ButtonImages;
            this.btnStateStart.Location = new System.Drawing.Point(370, 6);
            this.btnStateStart.Name = "btnStateStart";
            this.btnStateStart.Size = new System.Drawing.Size(172, 38);
            this.btnStateStart.TabIndex = 230;
            this.btnStateStart.Text = "������ �����������";
            this.btnStateStart.Click += new System.EventHandler(this.btnStateStart_Click);
            // 
            // StatementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 786);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StatementForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "����� �����������";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox30;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox31;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox32;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox33;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox34;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox35;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox36;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox37;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox38;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox40;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox41;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox42;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow13;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow14;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow15;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow16;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow17;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow19;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow20;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow18;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow21;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow7;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow22;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow23;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow24;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow25;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraGrid.GridControl gridDelegates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colPartyName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnVoteFinish;
        private DevExpress.XtraEditors.LabelControl lblVoteTime;
        private DevExpress.XtraEditors.SimpleButton btnVoteCancel;
        private DevExpress.XtraEditors.ProgressBarControl progressBarVoting;
        private DevExpress.XtraEditors.LabelControl lblVoting;
        private DevExpress.XtraEditors.PictureEdit picVoteTime;
        private DevExpress.XtraEditors.SimpleButton btnStateStart;
        private DevExpress.XtraEditors.SimpleButton btnDelegateAcceptMode;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow3;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.Xpo.XPCollection xpDelegates;
        private DevExpress.XtraGrid.Columns.GridColumn colMicNum;
        private DevExpress.XtraGrid.Columns.GridColumn colQNum;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private System.Windows.Forms.CheckBox checkBox1;

    }
}