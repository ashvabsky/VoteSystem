﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public interface IQuestProperties
    {
        void SetViewMode();
        void FillDropDowns();
        void PropertiesQuestions_StartEdit();
        void Properties_FinishEdit();

        void SetPropertiesQuestions(Dictionary<string, object> properties);
    }

    partial class MainForm : IQuestProperties
    {
        private void SetViewMode()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControlQuest.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                    foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowinin in rowin.ChildRows)
                    {
                        rowinin.Properties.ReadOnly = true;
                    }
                }
            }

            btnPropQuestApply.Enabled = false;
            btnPropQuestCancel.Enabled = false;
        }


        # region IQuestProperties methods
        void IQuestProperties.SetViewMode()
        {
            this.SetViewMode();
        }

        void IQuestProperties.FillDropDowns()
        {
            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteType"].Properties.RowEdit;
            list.Items.Clear();

            foreach (VoteTypeObj vt in DataModel.VoteTypes)
            {
                if (vt.Name != null)
                    list.Items.Add(vt.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list2 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteKind"].Properties.RowEdit;
            list2.Items.Clear();

            foreach (VoteKindObj vk in xpVoteKinds)
            {
                if (vk.Name != null)
                    list2.Items.Add(vk.Name);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list3 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_VoteQnty"].Properties.RowEdit;
            list3.Items.Clear();

            foreach (VoteQntyObj vq in xpVoteQntyTypes)
            {
                if (vq.Value != null)
                    list3.Items.Add(vq.Value);
            }

            DevExpress.XtraEditors.Repository.RepositoryItemComboBox list4 = (DevExpress.XtraEditors.Repository.RepositoryItemComboBox)PropertiesControlQuest.Rows["row_sq_DecisionProcType"].Properties.RowEdit;
            list4.Items.Clear();

            foreach (DecisionProcTypeObj dpt in xpDecisionProcTypes)
            {
                if (dpt.Value != null)
                    list4.Items.Add(dpt.Value);
            }

        }

        void IQuestProperties.SetPropertiesQuestions(Dictionary<string, object> properties)
        {
            PropertiesControlQuest.Rows["row_sq_Number"].Properties.Value = (string)properties["Number"].ToString();
            PropertiesControlQuest.Rows["row_sq_Caption"].Properties.Value = (string)properties["Caption"];
            PropertiesControlQuest.Rows["row_sq_CrDate"].Properties.Value = properties["CrDate"];

            PropertiesControlQuest.Rows["row_sq_Notes"].Properties.Value = properties["Notes"];
            PropertiesControlQuest.Rows["row_sq_Description"].Properties.Value = properties["Description"];


            PropertiesControlQuest.Rows["row_sq_VoteType"].Properties.Value = properties["VoteType_Value"];
            _selectedVoteType = properties["VoteType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_VoteKind"].Properties.Value = properties["VoteKind_Value"];
            _selectedVoteKind = properties["VoteKind_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_ReadNum"].Properties.Value = properties["ReadNum"];

            PropertiesControlQuest.Rows["row_sq_VoteQnty"].Properties.Value = properties["VoteQntyType_Value"];
            _selectedVoteQnty = properties["VoteQntyType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_DecisionProcValue"].Properties.Value = properties["DecisionProc_Value"];

            PropertiesControlQuest.Rows["row_sq_DecisionProcType"].Properties.Value = properties["DecisionProcType_Value"];
            _selectedDecisionProcType = properties["DecisionProcType_Name"].ToString();

            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;

            if ((string)properties["QuotaProcType_Name"] == "Total_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
            else if ((string)properties["QuotaProcType_Name"] == "Present_Qnty")
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;

            if (((string)properties["Notes"]) == "" && MainTabControl.SelectedTabPage.Name == "pgResQuestions")
                cat_sq_Notes.Expanded = false;
            else
                cat_sq_Notes.Expanded = true;
        }

        void IQuestProperties.PropertiesQuestions_StartEdit()
        {

            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControlQuest.Rows)
            {
                row.Properties.ReadOnly = false;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = false;
                    foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowinin in rowin.ChildRows)
                    {
                        rowinin.Properties.ReadOnly = false;
                    }
                }

            }

            // перечислить поля, которые следует оставить как readonly:
            PropertiesControlQuest.Rows["row_sq_Number"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_Caption"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_CrDate"].Properties.ReadOnly = true;
            PropertiesControlQuest.Rows["row_sq_ReadNum"].Properties.ReadOnly = true;

            btnPropQuestApply.Enabled = true;
            btnPropQuestCancel.Enabled = true;

            SelQuestGrid.Enabled = false;

            PropertiesControlQuest.FocusedRow = PropertiesControlQuest.Rows["row_sq_Notes"];
            PropertiesControlQuest.ShowEditor();

        }

        void IQuestProperties.Properties_FinishEdit()
        {
            SetViewMode();

            SelQuestGrid.Enabled = true;
        }

        private void btnPropQuestApply_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            properties["Notes"] = PropertiesControlQuest.Rows["row_sq_Notes"].Properties.Value;
            properties["VoteType_Name"] = _selectedVoteType;

            properties["VoteKind_Name"] = _selectedVoteKind;

            properties["VoteQntyType_Name"] = _selectedVoteQnty;

            properties["DecisionProc_Value"] = PropertiesControlQuest.Rows["row_sq_DecisionProcValue"].Properties.Value;

            properties["DecisionProcType_Name"] = _selectedDecisionProcType;

            properties["QuotaProcType_Name"] = "";

            if ((bool)PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value == true)
                properties["QuotaProcType_Name"] = "Total_Qnty";
            else if ((bool)PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value == true)
                properties["QuotaProcType_Name"] = "Present_Qnty";

            _selquestController.ApplyChanges(properties);
        }

        private void btnPropQuestCancel_Click(object sender, EventArgs e)
        {
            _selquestController.CancelChanges();

            // обновить поля
            _selquestController.UpdatePropData(navigatorQuestions.Position);

        }

        #endregion


        private void repository_cmb_votetypes_SelectedIndexChanged(object sender, EventArgs e)
        {

            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteTypeObj vto = (VoteTypeObj)xpVoteTypes[cmb.SelectedIndex];

            _selectedVoteType = vto.Name;
        }

        private void repository_cmb_VoteKinds_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteKindObj vko = (VoteKindObj)xpVoteKinds[cmb.SelectedIndex];
            _selectedVoteKind = vko.Name;
        }

        private void repository_cmb_VoteQntyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            VoteQntyObj vqo = (VoteQntyObj)xpVoteQntyTypes[cmb.SelectedIndex];
            _selectedVoteQnty = vqo.Name;

        }

        private void repository_cmb_DecisionProcTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            DecisionProcTypeObj dpt = (DecisionProcTypeObj)xpDecisionProcTypes[cmb.SelectedIndex];
            _selectedDecisionProcType = dpt.Name;

        }

        private void repository_cedt_QuotaProcType_Present_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
        }

        private void repository_cedt_QuotaProcType_Total_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit chkedit = (CheckEdit)sender;
            if ((bool)chkedit.EditValue == true)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = true;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = false;
            }
            else if ((bool)chkedit.EditValue == false)
            {
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Total"].Properties.Value = false;
                PropertiesControlQuest.Rows["row_sq_QuotaProcentType_Present"].Properties.Value = true;
            }
        }


    }
}