﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public interface IResultsPage
    {
        IMsgMethods GetShowInterface();
        IQuestProperties GetQuestPropertiesInterface();
//      void ShowView();

        void InitResQuestPage();
        int NavigatorPosition { get; }

        void ShowVoteResults(VoteResultObj vro);
        void SetNavigatorPos_ResQuestPage(int iPos);

    }

    partial class MainForm : IResultsPage
    {
        ResQuestionsCntrler _resquestController;



        #region реализация интерфейса IResultsPage
        IMsgMethods IResultsPage.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        IQuestProperties IResultsPage.GetQuestPropertiesInterface()
        {
            return (IQuestProperties)this;
        }

        void IResultsPage.InitResQuestPage()
        {
            grpResQuestPropsIn.Controls.Remove(PropertiesControlQuest2);
            grpResQuestPropsIn.Controls.Add(PropertiesControlQuest);
            //            cat_sq_Criteries.Expanded = false;

        }


        int IResultsPage.NavigatorPosition
        {
            get
            {
                return navigatorResQuestions.Position;
            }
        }


        void IResultsPage.ShowVoteResults(VoteResultObj vro)
        {
            if (vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteDateTime.Visible = true;

                lblDecision.Text = vro.idResultValue.Value;

                lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty2.ToString();
                lblVoteAbstain.Text = vro.AnsQnty3.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;
                int votes_proc = (votes * 100) / vro.AvailableQnty;

                int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                int procAgainst = (vro.AnsQnty2 * 100) / vro.AvailableQnty;
                int procAbstain = (vro.AnsQnty3 * 100) / vro.AvailableQnty;

                lblProcAye.Text = procAye.ToString();
                lblProcAgainst.Text = procAgainst.ToString();
                lblProcAbstain.Text = procAbstain.ToString();

                int notvoted = vro.AvailableQnty - votes;
                int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                lblNonVoteQnty.Text = notvoted.ToString();
                lblNonProcQnty.Text = notvoted_proc.ToString();

                lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                lblProcQnty.Text = string.Format("{0}", votes_proc);

            }
            else
            {
                lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }

        void IResultsPage.SetNavigatorPos_ResQuestPage(int iPos)
        {
            ClearMonitor();
            navigatorResQuestions.Position = iPos;
        }

        #endregion

        private void navigatorResQuestions_PositionChanged(object sender, EventArgs e)
        {
            ClearMonitor();

            navigatorResQuestions_PositionChanged();
        }

        private void navigatorResQuestions_PositionChanged()
        {
            ClearMonitor();
            _resquestController.UpdatePropData(navigatorResQuestions.Position);
        }


        private void btnQuestVoteCancel_Click(object sender, EventArgs e)
        {
            _resquestController.ResultCancel();
            navigatorResQuestions_PositionChanged();
        }

        private void btnQuestNewRead_Click(object sender, EventArgs e)
        {
            _resquestController.CreateNewReading();
        }

        private void btnQuestVoteDetail_Click(object sender, EventArgs e)
        {
            _Controller.VoteDetailsRun();
        }


   }
}