using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public partial class StatementForm : DevExpress.XtraEditors.XtraForm
    {
        public StatementForm()
        {
            InitializeComponent();
        }

        public StatementForm(StatementObj st)
        {
            InitializeComponent();

            CriteriaOperator criteria1 = new BinaryOperator(
                new OperandProperty("idStatement.id"), new OperandValue(st.id),
                BinaryOperatorType.Equal);

            xpDelegates.Filter = criteria1;

            this.Text = string.Format("����� ����������� �� ����: {0}", st.Name);
        }

        private void PropertiesControl_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnStateStart_Click(object sender, EventArgs e)
        {
            StateStartForm ssf = new StateStartForm();
            ssf.ShowDialog();
        }

        private void btnDelegateAcceptMode_Click(object sender, EventArgs e)
        {
            MonitorPreviewForm2 mpf2 = new MonitorPreviewForm2();
            mpf2.ShowDelegateAcceptStart();
        }
    }
}