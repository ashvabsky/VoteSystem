﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Windows.Forms;

namespace VoteSystem
{
    public class QuestionsCntrler
    {
        public enum QuestsFormMode
        {
            Manage,
            Select
        }

        QuestObj _theQuestion;
        NestedUnitOfWork _nuow;

        IQuestionsForm _View;
        IMsgMethods _msgForm; // интерфейс, для вывода сообщений на форму

        XPCollection _Questions;
        XPCollection _VoteKinds;
        XPCollection _VoteTypes;

        QuestsFormMode _Mode = QuestsFormMode.Manage;

        List<int> _filterArray;

        MainCntrler _mainController;

        public QuestsFormMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public QuestionsCntrler(MainCntrler _mcontroller)
        {
            _mainController = _mcontroller;
        }

        public void AssignView(IQuestionsForm viewInterface)
        {
            _View = viewInterface;
            _msgForm = _View.GetShowInterface();
        }

        public void Init(ref XPCollection Questions, ref XPCollection VoteTypes, ref XPCollection VoteKinds)
        {
            _Questions = Questions;
            _VoteKinds = VoteKinds;
            _VoteTypes = VoteTypes;
        }

        public void EditRecord(int pos)
        {
            _theQuestion = null;

            if (pos <= -1)
                return;

            _nuow = _Questions.Session.BeginNestedUnitOfWork();

            _theQuestion = _nuow.GetNestedObject(_Questions[pos]) as QuestObj;

            _nuow.BeginTransaction();
            _View.Properties_StartEdit();
        }

        public void DataPositionChanged(int pos)
        {
            _theQuestion = null;

            if (pos < 0)
                return;

            if (pos >= _Questions.Count)
                return;

            _theQuestion = _Questions[pos] as QuestObj;

            ShowRecProperties(_theQuestion); 
        }

        public void ShowRecProperties(QuestObj theQuestion)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            properties["Caption"] = theQuestion.Caption;
            properties["Description"] = theQuestion.Description;
            properties["VoteKind"] = theQuestion.idKind.Name;
            properties["VoteType"] = theQuestion.idType.Name;
            properties["id"] = theQuestion.id.ToString();


            _View.SetProperties(properties);
        }

        public void ApplyChanges(Dictionary<string, string> properties)
        {
            _theQuestion.Caption = properties["Caption"];
            _theQuestion.Description = properties["Description"];

//          CriteriaOperator criteria = CriteriaOperator.Parse("Name != 'PartyName'");

            // партии
            CriteriaOperator criteria1 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(properties["VoteKind"]),
                BinaryOperatorType.Equal);

            _VoteKinds.Filter = criteria1;
            if (_VoteKinds.Count == 0)
                _VoteKinds.Filter = null;

            VoteKindObj vk = _nuow.GetNestedObject(_VoteKinds[0]) as VoteKindObj;

            _theQuestion.idKind = vk;

            // фракции
            CriteriaOperator criteria2 = new BinaryOperator(
                new OperandProperty("Name"), new OperandValue(properties["VoteType"]),
                BinaryOperatorType.Equal);

            _VoteTypes.Filter = criteria2;
            if (_VoteTypes.Count == 0)
                _VoteTypes.Filter = null;

            VoteTypeObj vt = _nuow.GetNestedObject(_VoteTypes[0]) as VoteTypeObj;

            _theQuestion.idType = vt;

            _nuow.CommitChanges();

            _View.Properties_FinishEdit();
        }

        public void CancelChanges()
        {
            _nuow.RollbackTransaction();
            _View.Properties_FinishEdit();
        }

        public int AddNewQuestion()
        {
            QuestObj newQuest = new QuestObj();
            VoteKindObj votekind = (VoteKindObj)_VoteKinds[0];
            VoteTypeObj votetype = (VoteTypeObj)_VoteTypes[0];
            newQuest.idKind = votekind;
            newQuest.idType = votetype;

            newQuest.Caption = "Новый";
            newQuest.Description = "";
            newQuest.CrDate = DateTime.Now;

            int maxid = 0;
//          CriteriaOperator crit = _Questions.Filter;
            _Questions.Filter = null;
            if (_Questions.Count > 0)
            {
                QuestObj last = (QuestObj)_Questions[_Questions.Count - 1];
                maxid = last.id;
            }
//          _Questions.Filter = crit;

              newQuest.id = maxid + 1;
              if (Mode == QuestsFormMode.Select)
              {
                  if (_filterArray != null)
                  {
                      _filterArray.Add(newQuest.id);
                      CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
                      _Questions.Filter = cr;
                  }
              }

            int i = _Questions.Add(newQuest);
            newQuest.Save();
            return i;
        }

        public void RemoveQuestion()
        {
            if (_theQuestion == null)
                return;

            _Questions.Remove(_theQuestion);
        }

        public QuestObj GetSelectedItem(List<int> filterlist)
        {
//          _View = new QuestionsForm(this);

            List<int> list = new List<int>();
            foreach (QuestObj o in _Questions)
            {
                list.Add(o.id);
            }

            _filterArray = new List<int>(list.Except<int>(filterlist).ToArray<int>());

            CriteriaOperator cr = new InOperator("id", _filterArray.ToArray());
            _Questions.Filter = cr;
            _Mode = QuestsFormMode.Select;

            DialogResult dr = _View.ShowView();
            if (dr == DialogResult.OK)
            {
                return _theQuestion;
            }

            return null;
        }

        public void ShowView()
        {
            _View.ShowView();
        }
    }
}
