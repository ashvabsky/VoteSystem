using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public partial class TaskEditForm : DevExpress.XtraEditors.XtraForm
    {

        SesTaskObj _theTask;
        NestedUnitOfWork _nuow;
        public XPCollection<SesTaskObj> _Tasks;
        public XPCollection<FractionObj> _Fractions;

        public bool _readMode = false;

        public TaskEditForm(SesTaskObj question)
        {
            InitializeComponent();
            _theTask = question;
         }

        private void QuestionEditForm_Load(object sender, EventArgs e)
        {
          _theTask.Session.BeginTransaction();

          _Tasks = new XPCollection<SesTaskObj>(_theTask.Session, false);

          _Tasks.LoadingEnabled = false;

          _Tasks.Add(_theTask);


          PropertiesControlQuest.DataSource = _Tasks;


          CriteriaOperator criteriaInUse = CriteriaOperator.Parse("id > 0 AND IsDel = false") ;


            XPCollection<TaskKindObj> Kinds = new XPCollection<TaskKindObj>(_theTask.Session);
            Kinds.Criteria = criteriaInUse;
            Kinds.DisplayableProperties = "Name;idQntyType.Name";

            repositoryItemGridLookUpEdit2.DataSource = Kinds;
            repositoryItemGridLookUpEdit2.DisplayMember = "Name";
            repositoryItemGridLookUpEdit2.PopulateViewColumns();

            repositoryItemGridLookUpEdit2.View.Columns[0].Caption = "������������";
            repositoryItemGridLookUpEdit2.View.Columns[0].Width = 250;

            repositoryItemGridLookUpEdit2.View.Columns[1].Caption = "��� ��������";

            repositoryItemGridLookUpEdit2.View.Columns[1].Width = 250;

            SetReadMode();
        }

        private void SetReadMode()
        {
//          rowidKind_Name.Properties.ReadOnly = _readMode;

//          rowDescription.Properties.ReadOnly = _readMode;

//          rowCaption.Properties.ReadOnly = false;//!_readMode;
        }

        private void QuestionEditForm_Shown(object sender, EventArgs e)
        {

        }

        private void PropertiesControl_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {

        }

        private void PropertiesControl_CellValueChanging(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
/*
            PropertiesControl.UpdateFocusedRecord();
            PropertiesControl.Update();

            rowRowNum.Properties.Value = 10;
 */ 
        }

        private void btnPropCancel_Click(object sender, EventArgs e)
        {

            _theTask.Session.RollbackTransaction();

        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
            _theTask.Session.CommitTransaction();
        }

        private void QuestionEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_theTask.Session.InTransaction)
                _theTask.Session.RollbackTransaction();

        }
    }
}