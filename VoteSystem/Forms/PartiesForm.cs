using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class PartiesForm : DevExpress.XtraEditors.XtraForm
    {
        public PartiesForm()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            gridControl.MainView.ShowEditor();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int i = AddNewParty();
            if (i >= 0)
                dataNavigator1.Position = i;

            gridControl.MainView.ShowEditor();
        }

        public int AddNewParty()
        {
            PartyObj newParty = new PartyObj();
            newParty.Name = "�����";

            PartyObj last = (PartyObj)xpParties[xpParties.Count - 1];
            newParty.id = last.id + 1;
            int i = xpParties.Add(newParty);
            newParty.Save();
            return i;
        }

        public DialogResult ShowQuestion(string Quest)
        {
            return MessageBox.Show(this, Quest, "������", MessageBoxButtons.YesNoCancel);
        }

        public void ShowWarningMsg(string Text)
        {
            MessageBox.Show(this, Text, "��������!", MessageBoxButtons.OK);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult dr = ShowQuestion("�� �������, ��� ������ ������� ���������� ������?");
            if (dr == DialogResult.Yes)
            {
                int i = dataNavigator1.Position;
                if (i >= 0)
                {
                    PartyObj o = (PartyObj)xpParties[i];
                    if (o.Delegates.Count == 0)
                        xpParties.Remove(o); // ToDo: embrace in try...catch
                    else
                    {
                        ShowWarningMsg("���������� ������� ������ ������, ��� ��� �� ��� ��������� ������ ������.");
                    }
                }
            }
        }
     }
}