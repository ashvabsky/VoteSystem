namespace VoteSystem
{
    partial class ReportDelegate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReport = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkShowAllSessions = new DevExpress.XtraEditors.CheckEdit();
            this.chkShowCurrSession = new DevExpress.XtraEditors.CheckEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowAllSessions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowCurrSession.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReport
            // 
            this.btnReport.Location = new System.Drawing.Point(102, 182);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(161, 44);
            this.btnReport.TabIndex = 0;
            this.btnReport.Text = "������� �����";
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.chkShowAllSessions);
            this.groupControl1.Controls.Add(this.chkShowCurrSession);
            this.groupControl1.Location = new System.Drawing.Point(34, 24);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(530, 119);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "���������";
            // 
            // chkShowAllSessions
            // 
            this.chkShowAllSessions.EditValue = true;
            this.chkShowAllSessions.Location = new System.Drawing.Point(29, 79);
            this.chkShowAllSessions.Name = "chkShowAllSessions";
            this.chkShowAllSessions.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkShowAllSessions.Properties.Appearance.Options.UseFont = true;
            this.chkShowAllSessions.Properties.Caption = "�������� ���������� �� ���� �������";
            this.chkShowAllSessions.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style7;
            this.chkShowAllSessions.Properties.RadioGroupIndex = 1;
            this.chkShowAllSessions.Size = new System.Drawing.Size(361, 22);
            this.chkShowAllSessions.TabIndex = 10;
            // 
            // chkShowCurrSession
            // 
            this.chkShowCurrSession.EditValue = true;
            this.chkShowCurrSession.Location = new System.Drawing.Point(29, 39);
            this.chkShowCurrSession.Name = "chkShowCurrSession";
            this.chkShowCurrSession.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkShowCurrSession.Properties.Appearance.Options.UseFont = true;
            this.chkShowCurrSession.Properties.Caption = "�������� ���������� �� ������� ������";
            this.chkShowCurrSession.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style7;
            this.chkShowCurrSession.Properties.RadioGroupIndex = 1;
            this.chkShowCurrSession.Size = new System.Drawing.Size(351, 22);
            this.chkShowCurrSession.TabIndex = 9;
            this.chkShowCurrSession.CheckedChanged += new System.EventHandler(this.chkShowCurrSession_CheckStateChanged);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(334, 182);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(161, 44);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "�������";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ReportDelegate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 250);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportDelegate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "������������ ������ �� ���������";
            this.Load += new System.EventHandler(this.ReportDelegate_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportDelegate_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkShowAllSessions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowCurrSession.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnReport;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit chkShowCurrSession;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.CheckEdit chkShowAllSessions;
    }
}