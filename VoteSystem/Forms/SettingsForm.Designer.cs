namespace VoteSystem
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.edtSelectedQnty = new DevExpress.XtraEditors.SpinEdit();
            this.edtAssignedQnty = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chkCardMode = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtPath_DBase = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtPath_Excel = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtPath_Docs = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.btnSetPortions = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection();
            this.btnChange = new DevExpress.XtraEditors.SimpleButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.xpQKinds = new DevExpress.Xpo.XPCollection();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.gridQuestKinds = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQntyType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridLookUpEdit_QTypes = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.xpQTypes = new DevExpress.Xpo.XPCollection();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pageQuorumSettings = new DevExpress.XtraTab.XtraTabPage();
            this.listDQuantTypes = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.edtProcent = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.listQuantTypes = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.pagePredsedatel = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.chkPredsedatel_ViewAgenda = new DevExpress.XtraEditors.CheckEdit();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtSelectedQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAssignedQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCardMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_DBase.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_Excel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_Docs.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQKinds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestKinds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpEdit_QTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            this.pageQuorumSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDQuantTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtProcent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listQuantTypes)).BeginInit();
            this.pagePredsedatel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPredsedatel_ViewAgenda.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 3);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(696, 411);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.pageQuorumSettings,
            this.pagePredsedatel});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Controls.Add(this.chkCardMode);
            this.xtraTabPage1.Controls.Add(this.groupControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(689, 383);
            this.xtraTabPage1.Text = "�����";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.edtSelectedQnty);
            this.groupControl1.Controls.Add(this.edtAssignedQnty);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Location = new System.Drawing.Point(11, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(673, 123);
            this.groupControl1.TabIndex = 11;
            this.groupControl1.Text = "���������� ���������";
            // 
            // edtSelectedQnty
            // 
            this.edtSelectedQnty.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edtSelectedQnty.Location = new System.Drawing.Point(137, 76);
            this.edtSelectedQnty.Name = "edtSelectedQnty";
            this.edtSelectedQnty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtSelectedQnty.Properties.IsFloatValue = false;
            this.edtSelectedQnty.Properties.Mask.EditMask = "n0";
            this.edtSelectedQnty.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edtSelectedQnty.Size = new System.Drawing.Size(100, 20);
            this.edtSelectedQnty.TabIndex = 7;
            this.edtSelectedQnty.ValueChanged += new System.EventHandler(this.edtSelectedQnty_ValueChanged);
            // 
            // edtAssignedQnty
            // 
            this.edtAssignedQnty.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edtAssignedQnty.Location = new System.Drawing.Point(137, 37);
            this.edtAssignedQnty.Name = "edtAssignedQnty";
            this.edtAssignedQnty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtAssignedQnty.Properties.IsFloatValue = false;
            this.edtAssignedQnty.Properties.Mask.EditMask = "n0";
            this.edtAssignedQnty.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edtAssignedQnty.Size = new System.Drawing.Size(100, 20);
            this.edtAssignedQnty.TabIndex = 6;
            this.edtAssignedQnty.ValueChanged += new System.EventHandler(this.edtAssignedQnty_ValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(15, 38);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(105, 16);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "�������������:";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Location = new System.Drawing.Point(15, 80);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 16);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "���������:";
            // 
            // chkCardMode
            // 
            this.chkCardMode.Location = new System.Drawing.Point(24, 356);
            this.chkCardMode.Name = "chkCardMode";
            this.chkCardMode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkCardMode.Properties.Appearance.Options.UseFont = true;
            this.chkCardMode.Properties.Caption = "������������ ����� ������ � ����������";
            this.chkCardMode.Size = new System.Drawing.Size(315, 21);
            this.chkCardMode.TabIndex = 10;
            this.chkCardMode.ToolTip = "���������� ����� ������ � ���������� ��� �������� ����� ������";
            this.chkCardMode.CheckedChanged += new System.EventHandler(this.chkCardMode_CheckedChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.txtPath_DBase);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.txtPath_Excel);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtPath_Docs);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Location = new System.Drawing.Point(11, 148);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(673, 194);
            this.groupControl2.TabIndex = 12;
            this.groupControl2.Text = "��������� �����";
            // 
            // txtPath_DBase
            // 
            this.txtPath_DBase.Location = new System.Drawing.Point(15, 164);
            this.txtPath_DBase.Name = "txtPath_DBase";
            this.txtPath_DBase.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPath_DBase.Size = new System.Drawing.Size(620, 20);
            this.txtPath_DBase.TabIndex = 20;
            this.txtPath_DBase.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtPath_DBase_ButtonClick);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Location = new System.Drawing.Point(20, 142);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(376, 16);
            this.labelControl8.TabIndex = 19;
            this.labelControl8.Text = "���� � ����� � ���������� ������� ��� ������ �������:";
            // 
            // txtPath_Excel
            // 
            this.txtPath_Excel.Location = new System.Drawing.Point(15, 106);
            this.txtPath_Excel.Name = "txtPath_Excel";
            this.txtPath_Excel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPath_Excel.Size = new System.Drawing.Size(620, 20);
            this.txtPath_Excel.TabIndex = 18;
            this.txtPath_Excel.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtPath_Excel_ButtonClick);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl6.Location = new System.Drawing.Point(20, 84);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(198, 16);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "���� � ����� � excel ��������:";
            // 
            // txtPath_Docs
            // 
            this.txtPath_Docs.Location = new System.Drawing.Point(15, 52);
            this.txtPath_Docs.Name = "txtPath_Docs";
            this.txtPath_Docs.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtPath_Docs.Size = new System.Drawing.Size(620, 20);
            this.txtPath_Docs.TabIndex = 16;
            this.txtPath_Docs.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtPath_Docs_ButtonClick);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl5.Location = new System.Drawing.Point(21, 30);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(203, 16);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "���� � ����� � �������������:";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.btnSetPortions);
            this.xtraTabPage2.Controls.Add(this.btnChange);
            this.xtraTabPage2.Controls.Add(this.dataNavigator1);
            this.xtraTabPage2.Controls.Add(this.btnRemove);
            this.xtraTabPage2.Controls.Add(this.btnAdd);
            this.xtraTabPage2.Controls.Add(this.gridQuestKinds);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(689, 383);
            this.xtraTabPage2.Text = "���� ��������";
            // 
            // btnSetPortions
            // 
            this.btnSetPortions.ImageList = this.ButtonImages;
            this.btnSetPortions.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSetPortions.Location = new System.Drawing.Point(209, 291);
            this.btnSetPortions.Name = "btnSetPortions";
            this.btnSetPortions.Size = new System.Drawing.Size(156, 34);
            this.btnSetPortions.TabIndex = 20;
            this.btnSetPortions.Text = "��������� �����...";
            this.btnSetPortions.Click += new System.EventHandler(this.btnSetPortions_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnChange
            // 
            this.btnChange.Enabled = false;
            this.btnChange.ImageIndex = 9;
            this.btnChange.ImageList = this.ButtonImages;
            this.btnChange.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnChange.Location = new System.Drawing.Point(121, 291);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(40, 34);
            this.btnChange.TabIndex = 18;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.DataSource = this.xpQKinds;
            this.dataNavigator1.Location = new System.Drawing.Point(20, 212);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(192, 24);
            this.dataNavigator1.TabIndex = 19;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.Visible = false;
            // 
            // xpQKinds
            // 
            this.xpQKinds.CriteriaString = "[id] > 0 And [IsDel] = False";
            this.xpQKinds.DeleteObjectOnRemove = true;
            this.xpQKinds.ObjectType = typeof(VoteSystem.TaskKindObj);
            // 
            // btnRemove
            // 
            this.btnRemove.ImageIndex = 2;
            this.btnRemove.ImageList = this.ButtonImages;
            this.btnRemove.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnRemove.Location = new System.Drawing.Point(63, 291);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(40, 34);
            this.btnRemove.TabIndex = 17;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ImageIndex = 3;
            this.btnAdd.ImageList = this.ButtonImages;
            this.btnAdd.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(7, 291);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 34);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gridQuestKinds
            // 
            this.gridQuestKinds.DataSource = this.xpQKinds;
            this.gridQuestKinds.Dock = System.Windows.Forms.DockStyle.Top;
            gridLevelNode1.RelationName = "Level1";
            this.gridQuestKinds.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridQuestKinds.Location = new System.Drawing.Point(0, 0);
            this.gridQuestKinds.MainView = this.gridView3;
            this.gridQuestKinds.Name = "gridQuestKinds";
            this.gridQuestKinds.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.GridLookUpEdit_QTypes});
            this.gridQuestKinds.ShowOnlyPredefinedDetails = true;
            this.gridQuestKinds.Size = new System.Drawing.Size(689, 285);
            this.gridQuestKinds.TabIndex = 15;
            this.gridQuestKinds.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colQntyType});
            this.gridView3.GridControl = this.gridQuestKinds;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // colName
            // 
            this.colName.Caption = "������������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 214;
            // 
            // colQntyType
            // 
            this.colQntyType.Caption = "��� ��������";
            this.colQntyType.ColumnEdit = this.GridLookUpEdit_QTypes;
            this.colQntyType.FieldName = "idQntyType!";
            this.colQntyType.Name = "colQntyType";
            this.colQntyType.Visible = true;
            this.colQntyType.VisibleIndex = 1;
            this.colQntyType.Width = 323;
            // 
            // GridLookUpEdit_QTypes
            // 
            this.GridLookUpEdit_QTypes.AutoHeight = false;
            this.GridLookUpEdit_QTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GridLookUpEdit_QTypes.DataSource = this.xpQTypes;
            this.GridLookUpEdit_QTypes.DisplayMember = "Name";
            this.GridLookUpEdit_QTypes.Name = "GridLookUpEdit_QTypes";
            this.GridLookUpEdit_QTypes.NullText = "";
            this.GridLookUpEdit_QTypes.PopupFormMinSize = new System.Drawing.Size(400, 0);
            this.GridLookUpEdit_QTypes.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // xpQTypes
            // 
            this.xpQTypes.BindingBehavior = DevExpress.Xpo.CollectionBindingBehavior.AllowNone;
            this.xpQTypes.CriteriaString = "[id] > 0";
            this.xpQTypes.DeleteObjectOnRemove = true;
            this.xpQTypes.DisplayableProperties = "This;Name;Details";
            this.xpQTypes.ObjectType = typeof(VoteSystem.QntyTypeObj);
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowIncrementalSearch = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowColumnHeaders = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.PreviewFieldName = "Name";
            this.repositoryItemGridLookUpEdit1View.ViewCaption = "���� ���������";
            // 
            // pageQuorumSettings
            // 
            this.pageQuorumSettings.Controls.Add(this.listDQuantTypes);
            this.pageQuorumSettings.Controls.Add(this.labelControl4);
            this.pageQuorumSettings.Controls.Add(this.edtProcent);
            this.pageQuorumSettings.Controls.Add(this.labelControl7);
            this.pageQuorumSettings.Controls.Add(this.labelControl1);
            this.pageQuorumSettings.Controls.Add(this.listQuantTypes);
            this.pageQuorumSettings.Name = "pageQuorumSettings";
            this.pageQuorumSettings.Size = new System.Drawing.Size(689, 383);
            this.pageQuorumSettings.Text = "������� �������";
            // 
            // listDQuantTypes
            // 
            this.listDQuantTypes.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listDQuantTypes.Appearance.Options.UseFont = true;
            this.listDQuantTypes.DisplayMember = "Name";
            this.listDQuantTypes.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� �������������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ���������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ��������������������")});
            this.listDQuantTypes.Location = new System.Drawing.Point(351, 134);
            this.listDQuantTypes.Name = "listDQuantTypes";
            this.listDQuantTypes.Size = new System.Drawing.Size(232, 83);
            this.listDQuantTypes.TabIndex = 26;
            this.listDQuantTypes.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.listDQuantTypes_ItemCheck);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl4.Location = new System.Drawing.Point(354, 112);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(18, 16);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "��:";
            // 
            // edtProcent
            // 
            this.edtProcent.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.edtProcent.Location = new System.Drawing.Point(434, 74);
            this.edtProcent.Name = "edtProcent";
            this.edtProcent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtProcent.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.edtProcent.Properties.IsFloatValue = false;
            this.edtProcent.Properties.Mask.EditMask = "N00";
            this.edtProcent.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.edtProcent.Size = new System.Drawing.Size(149, 20);
            this.edtProcent.TabIndex = 24;
            this.edtProcent.ValueChanged += new System.EventHandler(this.edtProcent_ValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.labelControl7.Location = new System.Drawing.Point(354, 77);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 16);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "�������:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(64, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(244, 16);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "�������� ��� �������� ��� �������:";
            // 
            // listQuantTypes
            // 
            this.listQuantTypes.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listQuantTypes.Appearance.Options.UseFont = true;
            this.listQuantTypes.DisplayMember = "Name";
            this.listQuantTypes.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� �������������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ���������� ����� ���."),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("�� ��������������������")});
            this.listQuantTypes.Location = new System.Drawing.Point(59, 60);
            this.listQuantTypes.Name = "listQuantTypes";
            this.listQuantTypes.Size = new System.Drawing.Size(270, 184);
            this.listQuantTypes.TabIndex = 6;
            this.listQuantTypes.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.listQuantTypes_ItemCheck);
            this.listQuantTypes.SelectedValueChanged += new System.EventHandler(this.listQuantTypes_SelectedValueChanged);
            // 
            // pagePredsedatel
            // 
            this.pagePredsedatel.Controls.Add(this.groupControl3);
            this.pagePredsedatel.Name = "pagePredsedatel";
            this.pagePredsedatel.Size = new System.Drawing.Size(689, 383);
            this.pagePredsedatel.Text = "��������� ������������";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.chkPredsedatel_ViewAgenda);
            this.groupControl3.Location = new System.Drawing.Point(9, 41);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(673, 123);
            this.groupControl3.TabIndex = 12;
            this.groupControl3.Text = "��������� �������� ���� ��������� ������������";
            // 
            // chkPredsedatel_ViewAgenda
            // 
            this.chkPredsedatel_ViewAgenda.Location = new System.Drawing.Point(31, 55);
            this.chkPredsedatel_ViewAgenda.Name = "chkPredsedatel_ViewAgenda";
            this.chkPredsedatel_ViewAgenda.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkPredsedatel_ViewAgenda.Properties.Appearance.Options.UseFont = true;
            this.chkPredsedatel_ViewAgenda.Properties.Caption = "���������� ���� �������� ���";
            this.chkPredsedatel_ViewAgenda.Size = new System.Drawing.Size(315, 21);
            this.chkPredsedatel_ViewAgenda.TabIndex = 11;
            this.chkPredsedatel_ViewAgenda.ToolTip = "���������� ����� ������ � ���������� ��� �������� ����� ������";
            this.chkPredsedatel_ViewAgenda.CheckedChanged += new System.EventHandler(this.chkPredsedatel_ViewAgenda_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.ImageIndex = 0;
            this.btnOk.ImageList = this.ButtonImages;
            this.btnOk.Location = new System.Drawing.Point(526, 425);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(124, 34);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "�������";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 471);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���������";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtSelectedQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAssignedQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCardMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_DBase.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_Excel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPath_Docs.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQKinds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridQuestKinds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLookUpEdit_QTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpQTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            this.pageQuorumSettings.ResumeLayout(false);
            this.pageQuorumSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listDQuantTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtProcent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listQuantTypes)).EndInit();
            this.pagePredsedatel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkPredsedatel_ViewAgenda.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.CheckEdit chkCardMode;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SpinEdit edtSelectedQnty;
        private DevExpress.XtraEditors.SpinEdit edtAssignedQnty;
        private DevExpress.XtraGrid.GridControl gridQuestKinds;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colQntyType;
        private DevExpress.XtraEditors.SimpleButton btnChange;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.SimpleButton btnSetPortions;
        private DevExpress.XtraTab.XtraTabPage pageQuorumSettings;
        private DevExpress.XtraEditors.CheckedListBoxControl listQuantTypes;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedListBoxControl listDQuantTypes;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit edtProcent;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit GridLookUpEdit_QTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.Xpo.XPCollection xpQTypes;
        public DevExpress.Xpo.XPCollection xpQKinds;
        private DevExpress.XtraEditors.ButtonEdit txtPath_DBase;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ButtonEdit txtPath_Excel;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ButtonEdit txtPath_Docs;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraTab.XtraTabPage pagePredsedatel;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit chkPredsedatel_ViewAgenda;
    }
}