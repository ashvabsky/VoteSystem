﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace VoteSystem
{
    public interface IQuestionsForm
    {
        IMsgMethods GetShowInterface();
        void EnableButtons(bool IsVoted);

        DialogResult ShowView();
    }

    public interface IResultShow
    {
        void ShowVoteResults(VoteResultObj vro);
    }

    public partial class QuestionsForm : DevExpress.XtraEditors.XtraForm, IQuestionsForm, IResultShow
    {
        QuestionsCntrler _Controller;
        IMsgMethods _msgForm;

        bool _init = false;

        public QuestionsForm()
        {
            InitializeComponent();
            _Controller.Init(ref xpQuestions, ref xpVoteTypes, ref xpVoteKinds);
        }

        public QuestionsForm(QuestionsCntrler cntrler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntrler;

            _msgForm = (IMsgMethods) new MsgForm(this);

            _Controller.Init(ref xpQuestions, ref xpVoteTypes, ref xpVoteKinds);

        }


        #region реализация интерфейса IQuestionsForm

        IMsgMethods IQuestionsForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IQuestionsForm.ShowView()
        {
            return ShowDialog();
        }


        void IQuestionsForm.EnableButtons(bool IsVoted)
        {
            btnQuestDetails.Enabled = IsVoted;
        }

#endregion

        #region реализация интерфейса IResultShow
        void IResultShow.ShowVoteResults(VoteResultObj vro)
        {
            if (vro.id != 0)
            {
                grpVoteResults.Visible = true;
                lblVoteQntytxt.Visible = true;

//              lblVoteDateTime.Visible = true;

//              lblDecision.Text = vro.idResultValue.Name;
//              lblVoteDateTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty3.ToString();
                lblVoteAbstain.Text = vro.AnsQnty2.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;

                if (vro.AvailableQnty != 0)
                {
                    int votes_proc = (votes * 100) / vro.AvailableQnty;

                    int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                    int procAgainst = (vro.AnsQnty3 * 100) / vro.AvailableQnty;
                    int procAbstain = (vro.AnsQnty2 * 100) / vro.AvailableQnty;

                    lblProcAye.Text = procAye.ToString();
                    lblProcAgainst.Text = procAgainst.ToString();
                    lblProcAbstain.Text = procAbstain.ToString();

                    int notvoted = vro.AvailableQnty - votes;
                    int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                    lblNonVoteQnty.Text = notvoted.ToString();
                    lblNonProcQnty.Text = notvoted_proc.ToString();

                    lblVoteQnty.Text = string.Format("{0} из {1}", votes, vro.AvailableQnty);
                    lblProcQnty.Text = string.Format("{0}", votes_proc);
                }
                else
                {
                    lblProcAye.Text = "0";
                    lblProcAgainst.Text = "0";
                    lblProcAbstain.Text = "0";

                    lblNonVoteQnty.Text = "0";
                    lblNonProcQnty.Text = "0";

                    lblVoteQnty.Text = "0";
                    lblProcQnty.Text = "0";
                }
            }
            else
            {
//              lblDecision.Text = "Нет решения";
                grpVoteResults.Visible = false;
                lblVoteQntytxt.Visible = false;

//              lblVoteDateTime.Visible = false;

                lblVoteQnty.Text = "";
                lblProcQnty.Text = "";
            }

        }

        #endregion

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.DataPositionChanged(dn.Position);
        }


        private void QuestionsForm_Load(object sender, EventArgs e)
        {
            _init = true;
            dataNavigator1_PositionChanged(dataNavigator1, null); // обновить выводимые свойства
        }



        private void xpCollection1_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_init) // проверка: первоначальная загрузка списка уже осуществлена 
                _Controller.DataPositionChanged(dataNavigator1.Position); // обновить выводимые свойства
        }


        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
/*
            FractionsForm fr = new FractionsForm();
            fr.ShowDialog();
            xpFractions.Reload();
            FillDropDowns();
 */ 
        }

        private void QuestionsForm_Shown(object sender, EventArgs e)
        {
            if (_Controller.Mode == QuestionsCntrler.QuestsFormMode.Manage)
            {
//                btnSelect.Visible = false;
                panelbtnSelect.Visible = false;
            }
            else if (_Controller.Mode == QuestionsCntrler.QuestsFormMode.Select)
            {
//                btnSelect.Visible = true;
                panelbtnSelect.Visible = true;
            }

        }

        private void dataNavigator2_PositionChanged(object sender, EventArgs e)
        {
        }

        private void gridViewSession_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
//          Create an empty list.
             ArrayList rows = new ArrayList();
            // Add the selected rows to the list.
             DevExpress.XtraGrid.Columns.GridColumn GridColumn = gridViewSession.Columns[0];
            for (int i = 0; i < gridViewSession.SelectedRowsCount; i++)
            {
                int handle = gridViewSession.GetSelectedRows()[i];
                if (handle >= 0)
                {
                    object d = gridViewSession.GetRowCellValue(handle, GridColumn);
                    if (d != null)
                        rows.Add((int)d);
                }
            }

            _Controller.FillQuestionList(rows);
            gridViewQuestions.ExpandAllGroups();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

        }

        private void btnQuestDetails_Click(object sender, EventArgs e)
        {
            _Controller.ShowSelQuestionDetails();
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridViewQuestions.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (!hi.InRow) return;

            _Controller.ShowSelQuestionDetails();

        }
    }
}