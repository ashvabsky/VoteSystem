using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class SendTextForm : DevExpress.XtraEditors.XtraForm
    {
        public SendTextForm()
        {
            InitializeComponent();
        }

        private void SendTextForm_Load(object sender, EventArgs e)
        {
            richTextBox1.Focus();
        }
    }
}