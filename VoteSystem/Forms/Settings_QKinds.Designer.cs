namespace VoteSystem
{
    partial class Settings_QKinds
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings_QKinds));
            this.txtQKindName = new DevExpress.XtraEditors.TextEdit();
            this.cmbQKinds = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnPropCancel = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection();
            this.btnPropApply = new DevExpress.XtraEditors.SimpleButton();
            this.lstQntyTypes = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQKindName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQKinds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstQntyTypes.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtQKindName
            // 
            this.txtQKindName.Location = new System.Drawing.Point(33, 53);
            this.txtQKindName.Name = "txtQKindName";
            this.txtQKindName.Size = new System.Drawing.Size(322, 20);
            this.txtQKindName.TabIndex = 0;
            // 
            // cmbQKinds
            // 
            this.cmbQKinds.Location = new System.Drawing.Point(33, 125);
            this.cmbQKinds.Name = "cmbQKinds";
            this.cmbQKinds.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQKinds.Size = new System.Drawing.Size(322, 20);
            this.cmbQKinds.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(46, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "��� �������:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(46, 106);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "����� �������:";
            // 
            // btnPropCancel
            // 
            this.btnPropCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPropCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPropCancel.ImageIndex = 1;
            this.btnPropCancel.ImageList = this.ButtonImages;
            this.btnPropCancel.Location = new System.Drawing.Point(251, 207);
            this.btnPropCancel.Name = "btnPropCancel";
            this.btnPropCancel.Size = new System.Drawing.Size(104, 33);
            this.btnPropCancel.TabIndex = 9;
            this.btnPropCancel.Text = "�������� ";
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnPropApply
            // 
            this.btnPropApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPropApply.ImageIndex = 13;
            this.btnPropApply.ImageList = this.ButtonImages;
            this.btnPropApply.Location = new System.Drawing.Point(33, 207);
            this.btnPropApply.Name = "btnPropApply";
            this.btnPropApply.Size = new System.Drawing.Size(164, 33);
            this.btnPropApply.TabIndex = 8;
            this.btnPropApply.Text = "��������� ���������";
            // 
            // lstQntyTypes
            // 
            this.lstQntyTypes.Location = new System.Drawing.Point(33, 160);
            this.lstQntyTypes.Name = "lstQntyTypes";
            this.lstQntyTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lstQntyTypes.Size = new System.Drawing.Size(322, 20);
            this.lstQntyTypes.TabIndex = 10;
            // 
            // Settings_QKinds
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 271);
            this.Controls.Add(this.lstQntyTypes);
            this.Controls.Add(this.btnPropCancel);
            this.Controls.Add(this.btnPropApply);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.cmbQKinds);
            this.Controls.Add(this.txtQKindName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Settings_QKinds";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���� ��������";
            this.Load += new System.EventHandler(this.Settings_QKinds_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtQKindName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQKinds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstQntyTypes.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtQKindName;
        private DevExpress.XtraEditors.ComboBoxEdit cmbQKinds;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnPropCancel;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.SimpleButton btnPropApply;
        private DevExpress.XtraEditors.LookUpEdit lstQntyTypes;
    }
}