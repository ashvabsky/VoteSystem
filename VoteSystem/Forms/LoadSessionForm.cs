using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using VoteSystem.DataLayer;

namespace VoteSystem
{
    public partial class LoadSessionForm : DevExpress.XtraEditors.XtraForm
    {
        MainCntrler _controller;
        public LoadSessionForm()
        {
            InitializeComponent();
        }

        public LoadSessionForm(MainCntrler c)
        {
            InitializeComponent();
            _controller = c;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            int pos = dataNavigator1.Position;
            if (pos >= 0 && pos < xpSessions.Count)
            {
                SessionObj ses = (SessionObj)xpSessions[dataNavigator1.Position];
                if (ses.id != DataModel.TheSession.id)
                {
                    DataModel.Sessions_SetActive(ses);
                    MessageBox.Show(this, "���������� ������� ������!\n��������� ����� �������������", "��������", MessageBoxButtons.OK);
                    _controller.RestartApplication();
//                  System.Environment.Exit(1);
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int pos = dataNavigator1.Position;
            if (pos >= 0 && pos < xpSessions.Count)
            {
                SessionObj ses = (SessionObj)xpSessions[dataNavigator1.Position];
                DialogResult dr = MessageBox.Show(this, "�� �������, ��� ������ ������� ��������� ������?", "�������� ������", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    if (DataModel.Sessions_CanDelete(ses) == false)
                    {
                        DialogResult dr2 = MessageBox.Show(this, "������ ������ �������� �������, �� ������� ��������� ��������� �����������.\n�� ������� ��� ������ ������� ������ ������ �� ���� ������?", "�������� ������", MessageBoxButtons.YesNoCancel);
                        if (dr2 != DialogResult.Yes)
                            return;

                    }

                    DataModel.Sessions_Delete(ses);
                }
                xpSessions.Reload();

                if (DataModel.TheSession.IsDel == true)
                {
                    MessageBox.Show(this, "������� ������ ���� �������, ��������� ����� �������������", "�������� ������", MessageBoxButtons.OK);
                    _controller.RestartApplication();
                }
            }

        }
    }
}