using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface ISettingsPortionsForm
    {
//      IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(ref  XPCollection<QntyTypeObj> QTypes, ref  XPCollection<DelegateQntyTypeObj> DQTypes);
    }

    public partial class Settings_portion : DevExpress.XtraEditors.XtraForm, ISettingsPortionsForm
    {
        SettingsCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;

        protected int _lastSelectedItem = -1;
//        QntyTypeObj _lastQntyTypeObj = null;

        public Settings_portion(SettingsCntrler cntrler)
        {
            InitializeComponent();

            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;
        }

        #region ���������� ���������� ISettingsForm


        DialogResult ISettingsPortionsForm.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void ISettingsPortionsForm.InitControls(ref  XPCollection<QntyTypeObj> QTypes, ref  XPCollection<DelegateQntyTypeObj> DQTypes)
        {
            listQuantTypes.DataSource = QTypes;
            listDQuantTypes.DataSource = DQTypes;
            listDQuantTypes.SelectionMode = SelectionMode.One;
//          listDQuantTypes.CheckOnClick = true;

        }

        XtraForm ISettingsPortionsForm.GetView()
        {
            return this;
        }

        #endregion ���������� ���������� ISettingsForm

        private void Settings_portion_Load(object sender, EventArgs e)
        {
            _Controller.OnInitPortionsForm();
            listQuantTypes_SelectedValueChanged(null, null);
        }

        private void listQuantTypes_SelectedValueChanged(object sender, EventArgs e)
        {
            QntyTypeObj qto = (QntyTypeObj)listQuantTypes.SelectedItem;
            int currproc = -1;

            DelegateQntyTypeObj dq = null;

            if (listDQuantTypes.CheckedItems.Count > 0)
                dq = (DelegateQntyTypeObj)listDQuantTypes.CheckedItems[0];

          
            currproc = Convert.ToInt32(edtProcent.Text);

            _Controller.PortionsView_CheckDelegateQntyTypeChanged(ref qto, ref dq, currproc);


            listDQuantTypes.SelectedItem = qto.idDelegateQntyType;
            edtProcent.Text = qto.ProcentValue.ToString();
            if (listDQuantTypes.SelectedIndex >= 0)
            {
                listDQuantTypes.SetItemChecked(listDQuantTypes.SelectedIndex, true);
            }
        }

        private void listDQuantTypes_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            if (listDQuantTypes.SelectedIndex >= 0)
            {
                if (_lastSelectedItem != listDQuantTypes.SelectedIndex && _lastSelectedItem >= 0)
                {
                    listDQuantTypes.SetItemChecked(_lastSelectedItem, false);
                }
//                listDQuantTypes.SetItemChecked(listDQuantTypes.SelectedIndex, true);
                _lastSelectedItem = listDQuantTypes.SelectedIndex;

            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        private void Settings_portion_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Controller.PortionsView_CheckDelegateQntyChanged() == false)
                e.Cancel = true;
        }

    }
}