using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface IVoteModeForm
    {
        IMsgMethods GetShowInterface();

        void ShowView();

        XtraForm GetView();

        void InitControls(string Caption);
        void InitSeats();
        void SetDelegateProperties(XPCollection<VoteDetailObj> delegates);
        void SetQuestionProperties(XPCollection<QuestionObj> question);
        void ShowVoteResults(VoteResultObj vro);
        void StartVoting(int VotTime);
        void StopVoting();
        int  VoteQnty { set; }
        void InitParams(int VoteTime, int DelegatesQnty);
        void EnableMonitor(bool IsEnabled);
        void EnableMonitorButtons(bool IsEnabled);
    }

    public partial class VoteModeForm : DevExpress.XtraEditors.XtraForm, IVoteModeForm
    {
        VoteModeCntrler _Controller;
        MainCntrler _mainCntrler;
        MsgForm _msgForm;

        public VoteModeForm()
        {
            InitializeComponent();
        }

        public VoteModeForm(VoteModeCntrler cntler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntler;
            _mainCntrler = mcontr;
            _msgForm = new MsgForm(this);
        }

        IMsgMethods IVoteModeForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        int _CurrentImageNum = 0;

#region ���������� ���������� IVoteModeForm

        public void ShowView()
        {
            ShowDialog();
        }

        DevExpress.XtraEditors.XtraForm IVoteModeForm.GetView()
        {
            return this;
        }

        void IVoteModeForm.InitControls(string Caption)
        {
            _CurrentImageNum = 0;
            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];
            picVoteTime.Enabled = false;
            lblVoting.Enabled = false;
            progressBarVoting.Enabled = false;

            txtDelegateQnty.Properties.ReadOnly = true;

            txtVoteQnty.Properties.ReadOnly = true;

            btnVoteCancel.Text = "�������� �����������";
            btnVoteCancel.Enabled = true;
            btnVoteFinish.Enabled = false;
            btnVoteStart.Enabled = true;
            btnFormClose.Enabled = true;

            grpSeates.Text = "����������� �� �������: " + Caption;
        }

        void IVoteModeForm.InitSeats()
        {
            foreach (Control c in grpSeates.Controls)
            {
                if (c.Tag == null)
                    continue;

                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();
                    int MicNum = Convert.ToInt32(s.Substring(3));
                    DevExpress.XtraEditors.PictureEdit pic = (DevExpress.XtraEditors.PictureEdit)c;
                    VoteModeCntrler.SeatStates seatstate = _Controller.GetSeatState(MicNum);

                    if (seatstate == VoteModeCntrler.SeatStates.None)
                        pic.Image = ManImages.Images[7];
                    else if (seatstate == VoteModeCntrler.SeatStates.Disabled)
                        pic.Image = ManImages.Images[6];
                    else if (seatstate == VoteModeCntrler.SeatStates.NoAnswer)
                        pic.Image = ManImages.Images[5];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer1)
                        pic.Image = ManImages.Images[0];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer2)
                        pic.Image = ManImages.Images[1];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer3)
                        pic.Image = ManImages.Images[2];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer4)
                        pic.Image = ManImages.Images[5];
                    else if (seatstate == VoteModeCntrler.SeatStates.Answer5)
                        pic.Image = ManImages.Images[5];
                }
            }

        }

        void IVoteModeForm.SetDelegateProperties(XPCollection<VoteDetailObj> delegates)
        {
            PropertiesControlDelegate.DataSource = delegates;
        }

        void IVoteModeForm.SetQuestionProperties(XPCollection<QuestionObj> questions)
        {
            PropertiesControlQuest.DataSource = questions;
        }

        void IVoteModeForm.ShowVoteResults(VoteResultObj vro)
        {
            if (vro != null && vro.id != 0)
            {
                grpVoteResultsTable.Visible = true;
                grpVoteResults.Visible = true;

                if (_Controller.VoteMode != true)
                {
                    if (vro.idResultValue.id != 0)
                    {
                        btnVoteCancel.Enabled = false;
                        btnVoteStart.Enabled = false;
                    }
                    else
                    {
                        btnVoteCancel.Enabled = false;
                        btnVoteStart.Enabled = true;
                    }
                }

                lblDecision.Text = vro.idResultValue.Name;

                lblVoteResDate.Text = vro.VoteDateTime.ToString("dd.MM.yy");
                lblVoteResTime.Text = vro.VoteDateTime.ToString("t");

                lblVoteAye.Text = vro.AnsQnty1.ToString();
                lblVoteAgainst.Text = vro.AnsQnty3.ToString();
                lblVoteAbstain.Text = vro.AnsQnty2.ToString();

                int votes = vro.AnsQnty1 + vro.AnsQnty2 + vro.AnsQnty3;
                txtVoteQnty.Text = votes.ToString();

                if (vro.AvailableQnty != 0)
                {

                    int votes_proc = (votes * 100) / vro.AvailableQnty;

                    int procAye = (vro.AnsQnty1 * 100) / vro.AvailableQnty;
                    int procAgainst = (vro.AnsQnty3 * 100) / vro.AvailableQnty;
                    int procAbstain = (vro.AnsQnty2 * 100) / vro.AvailableQnty;

                    lblProcAye.Text = procAye.ToString();
                    lblProcAgainst.Text = procAgainst.ToString();
                    lblProcAbstain.Text = procAbstain.ToString();

                    int notvoted = vro.AvailableQnty - votes;
                    int notvoted_proc = (notvoted * 100) / vro.AvailableQnty;

                    lblNonVoteQnty.Text = notvoted.ToString();
                    lblNonProcQnty.Text = notvoted_proc.ToString();

                    //                  lblVoteQnty.Text = string.Format("{0} �� {1}", votes, vro.AvailableQnty);
                    txtProcQnty.Text = string.Format("{0}", votes_proc);
                }
                else
                {
                    lblProcAye.Text = "0";
                    lblProcAgainst.Text = "0";
                    lblProcAbstain.Text = "0";
                    lblNonVoteQnty.Text = "0";
                    lblNonProcQnty.Text = "0";

                    //                  lblVoteQnty.Text = string.Format("{0} �� {1}", votes, vro.AvailableQnty);
                    txtProcQnty.Text = "0";
                }
            }

            if ((vro == null || vro.id == 0) || (_Controller.VoteMode == false && vro != null && vro.idResultValue.id == 0))
            {
                grpVoteResults.Visible = false;

                //              lblVoteQnty.Text = "";
                txtProcQnty.Text = "";
                txtVoteQnty.Text = "";

                btnVoteStart.Enabled = true;
                btnVoteCancel.Enabled = false;

            }

            if (_Controller.VoteMode == true) // ���� ����������
            {
                btnVoteStart.Enabled = false;
                btnVoteCancel.Enabled = true;
            }

            btnClose.Enabled = !_Controller.VoteMode;
        }

        void IVoteModeForm.StartVoting(int VotTime)
        {
            btnVoteCancel.Enabled = true;
            btnVoteFinish.Enabled = true;
            btnVoteStart.Enabled = false;
            btnFormClose.Enabled = false;

            picVoteTime.Enabled = true;
            lblVoting.Visible = true;
            lblVoting.Text = "���� �����������...";
            lblVoting.Enabled = true;
            progressBarVoting.Enabled = true;
            progressBarVoting.Visible = true;

            //          btnVoteStart.ImageIndex = 9;

            _VoteTime = VotTime;
            _startvotetime = DateTime.Now;

            tmrVoteStart_Tick(null, null); // ��� ����� ��������� ��������� ����������� � �� �������� �������
            tmrVoteStart.Start();

            tmrRotation.Start();
        }

        void IVoteModeForm.StopVoting()
        {
            if (tmrVoteStart.Enabled == true)
            {
                tmrVoteStart.Stop();
                tmrRotation.Stop();
            }

            progressBarVoting.Position = -1;
            progressBarVoting.Enabled = false;

            lblVoting.Enabled = false;

            _CurrentImageNum = 0;
            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];

            picVoteTime.Enabled = false;
            lblVoteTime.Enabled = false;

            btnVoteStart.Text = "������ �����������";
            btnVoteStart.ImageIndex = 0;

            btnVoteCancel.Enabled = true;
            btnVoteFinish.Enabled = false;
            btnVoteStart.Enabled = true;
            btnFormClose.Enabled = true;
        }

        int IVoteModeForm.VoteQnty
        {
            set { txtVoteQnty.Text = value.ToString(); }
        }

        void IVoteModeForm.InitParams(int VoteTime, int DelegatesQnty)
        {
            string stime = VoteTime.ToString();
            if (stime.Length <= 1)
                stime = "0" + stime;

            lblVoteTime.Text = String.Format("{0} ���.", stime);
            txtDelegateQnty.Text = DelegatesQnty.ToString();
        }

        void IVoteModeForm.EnableMonitor(bool IsEnabled)
        {
            chkPanels2.Checked = IsEnabled;
        }

        void IVoteModeForm.EnableMonitorButtons(bool IsEnabled)
        {
            barbtnClearMonitor2.Enabled = IsEnabled;
            barbtnSplashToMonitor2.Enabled = IsEnabled;
            barbtnInfoToMonitor2.Enabled = IsEnabled;
            barbtnMonitorMsg2.Enabled = IsEnabled;

            if (IsEnabled)
            {
                chkPanels2.Caption = "���������";
            }
            else
            {
                chkPanels2.Caption = "��������";
            }
        }

#endregion

#region private methods
        private void VoteModeForm_Load(object sender, EventArgs e)
        {
            _mainCntrler.SetMessageOwner(this);
            _Controller.OnLoadForm();
        }


        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            info_1.Visible = false;

            Control c = (Control)sender;
            if (c.Tag == null)
            {
                _Controller.ShowDelegateProp(-1); // �������� ������ ����
                return;
            }
            SesDelegateObj theDelegate = null;

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));


            theDelegate = _Controller.ShowDelegateProp(MicNum);


            if (MicNum > 0 && theDelegate != null)
            {
                info_1.Text = theDelegate.idDelegate.ShortName;

                int halfwidth = (info_1.Width - c.Width) / 2;
                info_1.Left = c.Left - halfwidth;
                info_1.Top = c.Top - info_1.Height;
                info_1.ForeColor = System.Drawing.Color.Purple;
                info_1.Visible = true;
            }
        }


        private void btnVoteStart_Click(object sender, EventArgs e)
        {
            _Controller.Vote_PrepareToStart();
        }


        DateTime _startvotetime;
        int _VoteTime;
        int _VoteQnty = 0;

        private void tmrVoteStart_Tick(object sender, EventArgs e)
        {

            int sec = _VoteTime;
            int min = 0;
            int our = 0;
            if (sec >= 60)
            {
                sec = (_VoteTime % 60);
                min = Convert.ToInt32(_VoteTime / 60);
                if (min >= 60)
                {
                    min = (_VoteTime % 60);
                    our = Convert.ToInt32(min / 60);
                }
            }

            System.TimeSpan time = DateTime.Now - _startvotetime;
            DateTime voteTime = new DateTime(1900, 12, 13, our, min, sec);

            DateTime t = voteTime.AddTicks(-time.Ticks);

            int VoteSecs = voteTime.Minute * 60 + voteTime.Second;
            DateTime t2 = new DateTime(time.Ticks);
            int Secs = t2.Minute * 60 + t2.Second;

            int pos = 0;
            if (VoteSecs != 0)
                pos = (Secs * (100 + (300/VoteSecs))) / VoteSecs;

            _Controller.OnVoteProcess(t, pos);

            if (t.Minute == 0 && t.Second == 0 || t.Day < voteTime.Day)
            {
                pos = 100;
                lblVoteTime.Text = t.ToString("00:00");
                lblVoting.Text = "���� ������� �����������...";
                progressBarVoting.Position = pos;
                tmrVoteStop.Start();
            }
            else
            {
                lblVoteTime.Text = t.ToString("mm:ss");
                progressBarVoting.Position = pos;
            }



        }

        private void tmrVoteStop_Tick(object sender, EventArgs e)
        {
            tmrVoteStop.Stop();
            _Controller.Vote_Stop();
        }


        private void tmrRotation_Tick(object sender, EventArgs e)
        {
            _CurrentImageNum++;
            if (_CurrentImageNum > TimerImages.Images.Count - 1)
                _CurrentImageNum = 0;

            picVoteTime.Image = TimerImages.Images[_CurrentImageNum];
        }


        private void btnRegCancel_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Clear_Ask();
        }

        private void btnVoteFinish_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Stop_Ask();
        }

        private void btnVoteCancel_Click(object sender, EventArgs e)
        {
            _Controller.Vote_Clear_Ask();
        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        private void VoteModeForm_Shown(object sender, EventArgs e)
        {
            _Controller.OnFormShown();
        }

        private void chkPanels2_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.EnableMonitor(chkPanels2.Checked);
        }


        #endregion

        private void barbtnInfoToMonitor2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview("QuestionInfo");
        }

        private void VoteModeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Controller.OnClose() == false)
            {
                e.Cancel = true;
            }
            else
            {
                _Controller.ShowMonitorPreview("");
            }

            if ((e.Cancel == false))
            {
                _mainCntrler.SetMessageOwner(null);
            }
        }

        private void barbtnSplashToMonitor2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.ShowMonitorPreview("");
        }

        private void barbtnReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.PrintQuestionReport();
        }

        private void grpSeates_MouseMove(object sender, MouseEventArgs e)
        {
            info_1.Visible = false;
        }

        private void btnFormClose_Click(object sender, EventArgs e)
        {

        }

        private void VoteModeForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }


    }
}