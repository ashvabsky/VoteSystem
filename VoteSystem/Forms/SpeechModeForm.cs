using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace VoteSystem
{
    public interface ISpeechModeForm
    {
        IMsgMethods GetShowInterface();

        void ShowView();

        void InitControls(string Caption, ref XPCollection<DelegateStatementObj> d, ref XPCollection<StatementObj> s, ref XPCollection<SesDelegateObj> sesd);
        void SetDataSources(XPCollection<SesDelegateObj> sesd);
        void HallPageInit();
        void InitSeats();
        void UpdateControls(); // �������� �������� �� �����
        void InitHallPage();
        void InitDelegatesPage();

        void SetDelegateProperties1(XPCollection<SesDelegateObj> delegates);
        void SetDelegateProperties2(XPCollection<DelegateStatementObj> delegates);

        void StartSpeeching(int SpeechTime);
        int StopSpeeching();

        bool PrepareToStart { set; }
        void InitParams(int VoteTime, int DelegatesQnty);
        void EnableMonitor(bool IsEnabled);
        void EnableMonitorButtons(bool IsEnabled);

        int NavigatorPosition { get; set; }
        void EnableButtons(bool IsFinished);

        void ShowInfo(string info);
    }


    public partial class SpeechModeForm : DevExpress.XtraEditors.XtraForm, ISpeechModeForm
    {
        SpeechModeCntrler _Controller;
        MainCntrler _mainCntrler;
        MsgForm _msgForm;
             

        bool _prepareToStart = false; // ���� ������� ���������, ������� �� ��������� ���� ��� ���������� ����� ����� �������� �����.

        public SpeechModeForm()
        {
            InitializeComponent();
        }

        public SpeechModeForm(SpeechModeCntrler cntler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntler;
            _mainCntrler = mcontr;
            _msgForm = new MsgForm(this);
        }

/*
        public SpeechForm()
        {
            InitializeComponent();

            CriteriaOperator criteria1 = new BinaryOperator(
                new OperandProperty("idStatement.id"), new OperandValue(st.id),
                BinaryOperatorType.Equal);

            xpDelegates.Filter = criteria1;

            this.Text = string.Format("����� ����������� �� ����: {0}", st.idStatement.Name);
 
        }
*/

        #region ���������� ���������� IVoteModeForm

        void ISpeechModeForm.ShowView()
        {
//          this.TopMost = true;
            ShowDialog();
        }

        IMsgMethods ISpeechModeForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        void ISpeechModeForm.SetDataSources(XPCollection<SesDelegateObj> sesd)
        {
            PropCntrlDelegate2.DataSource = null;
            PropCntrlDelegate2.DataSource = sesd;
        }

        void ISpeechModeForm.InitControls(string Caption, ref XPCollection<DelegateStatementObj> d, ref XPCollection<StatementObj> s, ref XPCollection<SesDelegateObj> sesd)
        {
            mainTabControl.SelectedTabPage = pgDelegateList;
            InfoTabControl.SelectedTabPage = pgDelegateInfo;
            PropertiesControlDelegate.DataSource = d;
            PropertiesControlStatem.DataSource = s;
            gridDelegates.DataSource = d;
            navigatorSpeechDelegates.DataSource = d;

            PropCntrlDelegate2.DataSource = sesd;

            this.Text = "����� ����������� �� ����: " + Caption;
            lblProcess.Text = "";

            if (_Controller.ViewMode == true)
            {
                btnSpeechStart.Enabled = false;
//              btnSpeechDirectStart.Enabled = false;
                btnClearAll.Enabled = false;
                btnspeechFinish.Enabled = false;
//              btnSpeechCancel.Enabled = false;

                _regMode = false;
                chRegMode.Checked = false;
                chRegMode.Text = "�������� ������ �� �����������";
                chRegMode.Enabled = false;

//              btnThemeClose.Text = "�������";

                colSpeech.Visible = false;
            }
            else
            {
                _regMode = false;
                chRegMode.Checked = true;
                chRegMode.Text = "���� ������ �� �����������...";
                chRegMode.Enabled = true;

//              btnThemeClose.Text = "��������� ����";

                tmrRefreshHall.Start();
            }

            lblInfoControls[1] = info_1;
            lblInfoControls[2] = info_2;
            lblInfoControls[3] = info_3;
            lblInfoControls[4] = info_4;
            lblInfoControls[5] = info_5;
            lblInfoControls[6] = info_6;
            lblInfoControls[7] = info_7;
            lblInfoControls[8] = info_8;
            lblInfoControls[9] = info_9;

        }


        void ISpeechModeForm.StartSpeeching(int SpeechTime)
        {
            _startspeechtime = DateTime.Now;
            _speechtime = SpeechTime;

            lblProcess.Text = "���� ����������� �������� �� " + _speechtime.ToString() + " ���.";

            tmrProcessSpeech.Start();
        }

        int ISpeechModeForm.StopSpeeching()
        {
            return StopSpeechingCurrentDelegate();
        }

        bool ISpeechModeForm.PrepareToStart
        {
            set { _prepareToStart = value; }
        }

        void ISpeechModeForm.InitParams(int VoteTime, int DelegatesQnty)
        {
        }

        void ISpeechModeForm.EnableMonitor(bool IsEnabled)
        {
        }

        void ISpeechModeForm.EnableMonitorButtons(bool IsEnabled)
        {
        }

        void ISpeechModeForm.SetDelegateProperties1(XPCollection<SesDelegateObj> delegates)
        {
//            PropertiesControlDelegate.DataSource = delegates;
        }

        void ISpeechModeForm.SetDelegateProperties2(XPCollection<DelegateStatementObj> delegates)
        {
//            PropertiesControlDelegate.DataSource = delegates;
        }

        void ISpeechModeForm.UpdateControls()
        {
            if (_Controller.ViewMode == true)
                return;

            if (_regMode == true) // ���� ������� ����� ������ �� �����������
            {
            }

            if (_Controller.SpeechMode == true)
            {
                btnSpeechStart.Enabled = false;
//              btnSpeechDirectStart.Enabled = false;
                btnClearAll.Enabled = false;
                btnspeechFinish.Enabled = true;
//              lblProcess.Text = "���� ����������� ��������...";
//              btnSpeechCancel.Enabled = true;
            }
            else 
            {
                btnSpeechStart.Enabled = true;
//              btnSpeechDirectStart.Enabled = true;
                btnClearAll.Enabled = true;
                btnspeechFinish.Enabled = false;
                lblProcess.Text = "";
//              btnSpeechCancel.Enabled = false;
            }

            if (_Controller.HallMode == true)
            {
                btnSpeechStart.Enabled = false;
//              btnSpeechDirectStart.Enabled = false;
            }
            else
            {
                pgDelegateInfo.PageVisible = true;
                pgDelegatesHall.PageVisible = false;
            }
        }

        void ISpeechModeForm.InitHallPage()
        {
                btnSpeechStart.Enabled = false;
//              btnSpeechDirectStart.Enabled = false;

                pgDelegatesHall.PageVisible = true;
                pgDelegateInfo.PageVisible = false;

                InfoTabControl.SelectedTabPage = pgDelegatesHall;
        }

        void ISpeechModeForm.InitDelegatesPage()
        {
            pgDelegateInfo.PageVisible = true;
            pgDelegatesHall.PageVisible = false;
            InfoTabControl.SelectedTabPage = pgDelegateInfo;
        }

        void ISpeechModeForm.InitSeats()
        {
            ResetControls();
            foreach (Control c in grpSeates.Controls)
            {
                if (c.Tag == null)
                    continue;

                if (c.Tag.ToString().StartsWith("Mic"))
                {
                    string s = c.Tag.ToString();
                    int MicNum = Convert.ToInt32(s.Substring(3));
                    DevExpress.XtraEditors.PictureEdit pic = (DevExpress.XtraEditors.PictureEdit)c;
                    SpeechModeCntrler.SeatStates seatstate = _Controller.GetSeatState(MicNum);

                   
                    if (seatstate == SpeechModeCntrler.SeatStates.None)
                        pic.Image = ManImages.Images[4];
                    else if (seatstate == SpeechModeCntrler.SeatStates.Disabled)
                        pic.Image = ManImages.Images[5];
                    else if (seatstate == SpeechModeCntrler.SeatStates.Ready)
                        pic.Image = ManImages.Images[2];
                    else if (seatstate == SpeechModeCntrler.SeatStates.SpeechReady || seatstate == SpeechModeCntrler.SeatStates.SpeechReadyHot)
                    {
                        pic.Image = ManImages.Images[1];
                        LabelControl info = GetNextLabelInfo();
                        if (info != null)
                        {
                            SesDelegateObj d = _Controller.GetDelegateByMic(MicNum);
                            if (d != null)
                            {
                                info.Text = d.idDelegate.ShortName;
                                int halfwidth = (info.Width - pic.Width) / 2;
                                info.Left = pic.Left - halfwidth;
                                info.Top = pic.Top - info.Height;
                                info.ForeColor = System.Drawing.Color.Purple;
                                info.Visible = true;

                                if (seatstate == SpeechModeCntrler.SeatStates.SpeechReadyHot)
                                {
                                    info.ForeColor = Color.Red;
                                }
                            }

                        }
                    }
                    else if (seatstate == SpeechModeCntrler.SeatStates.SpeechStart)
                        pic.Image = ManImages.Images[0];
                }
            }

        }

        LabelControl[] lblInfoControls = new LabelControl[10]; 

        int iLastControlNum = 0;
        LabelControl GetNextLabelInfo()
        {
            iLastControlNum++;
            if (iLastControlNum <= 9)
            {
                return (LabelControl)lblInfoControls[iLastControlNum];
            }

            return null;
        }

        private void ResetControls()
        {
            for (int i = 1; i <= 9; i++)
            {
                LabelControl l = (LabelControl)lblInfoControls[i];
                l.Visible = false;
            }
            iLastControlNum = 0;
        }

        int ISpeechModeForm.NavigatorPosition
        {
            get
            {
                return navigatorSpeechDelegates.Position;
            }
            set
            {
                navigatorSpeechDelegates.Position = value;
            }
        }

        void ISpeechModeForm.ShowInfo(string Info)
        {
            barInfoText.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            barInfoText.Caption = Info;
            tmrWarningInfo.Start();
        }

        void ISpeechModeForm.EnableButtons(bool IsFinished)
        {
            if (_Controller.ViewMode == false)
            {
                navControlSpeechDelegates.Buttons.CustomButtons[0].Enabled = true;
                navControlSpeechDelegates.Buttons.CustomButtons[1].Enabled = !IsFinished;
                navControlSpeechDelegates.Buttons.CustomButtons[2].Enabled = true;
                navControlSpeechDelegates.Buttons.CustomButtons[3].Enabled = true;
            }
            else
            {
                navControlSpeechDelegates.Buttons.CustomButtons[0].Enabled = false;
                navControlSpeechDelegates.Buttons.CustomButtons[1].Enabled = false;
                navControlSpeechDelegates.Buttons.CustomButtons[2].Enabled = false;
                navControlSpeechDelegates.Buttons.CustomButtons[3].Enabled = false;
            }

            /*
                        btnspeechFinish.Enabled = !IsFinished;
                        btnSpeechCancel.Enabled = !IsFinished;
             */ 
        }

        void ISpeechModeForm.HallPageInit()
        {
        }

        #endregion ���������� ���������� ISpeechModeForm

        private void PropertiesControl_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        DateTime _startspeechtime;
        int _speechtime = 0;
        bool _regMode = true; // ��������� ������ ������ �� �����������
//      bool _speechMode = false; // ��������� ������ ������ �� �����������

        private void btnStateStart_Click(object sender, EventArgs e)
        {
            _Controller.PrepareToStart(); // ������ �����������
        }



        private void SpeechModeForm_Load(object sender, EventArgs e)
        {
            _mainCntrler.SetMessageOwner(this);
            _Controller.OnLoadForm();
        }


        private void SpeechModeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !_Controller.OnClosing();
            if (e.Cancel == false)
            {
                _mainCntrler.SetMessageOwner(null);
            }
        }

        private void tmrProcess_Tick(object sender, EventArgs e)
        {
            System.TimeSpan time = DateTime.Now - _startspeechtime;


            int sec = 0;
            int min = _speechtime;
            int our = 0;

            if (min >= 60)
            {
                min = (_speechtime % 60);
                our = Convert.ToInt32(_speechtime / 60);
            }

            DateTime speechDTime = new DateTime(1900, 12, 13, our, min, 0);

            DateTime zeroTime = new DateTime(1900, 12, 13, 0, 0, 0);

            DateTime t = zeroTime.AddTicks(time.Ticks);
            lblProcesTime.Text = t.ToString("mm:ss");

            int SpeechSecs = speechDTime.Hour*3600 + speechDTime.Minute * 60 + speechDTime.Second;
            DateTime t2 = new DateTime(time.Ticks);
            int Secs = t2.Minute * 60 + t2.Second;

            int pos = 0;
            if (SpeechSecs != 0)
                pos = (Secs * 100) / SpeechSecs;

            progressBarVoting.Position = pos;

            _Controller.OnSpeechProcess(Secs, _speechtime);

        }

        private void navControlSpeechDelegates_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    _Controller.AppendDelegate();

                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    DialogResult dr = ((IMsgMethods)_msgForm).ShowQuestion("�� �������, ��� ������ ������� �������� �� ������ �����������?");
                    if (dr == DialogResult.Yes)
                    {
                        _Controller.RemoveCurrentItem();
                    }

                }
            }
        }

        private void navigatorSpeechDelegates_PositionChanged(object sender, EventArgs e)
        {
            if (_Controller != null)
                _Controller.UpdatePropData();
        }

        public int StopSpeechingCurrentDelegate()
        {
            if (_Controller.SpeechMode == true)
            {
                tmrProcessSpeech.Stop();
//              _speechMode = false;
                if (_startspeechtime.Year > 1)
                {
                    System.TimeSpan time = DateTime.Now - _startspeechtime;
                    DateTime t2 = new DateTime(time.Ticks);
                    int Secs = t2.Minute * 60 + t2.Second;
                    return Secs;
                }
            }
            return 0;
        }

        private void btnspeechFinish_Click(object sender, EventArgs e)
        {
            int Secs = StopSpeechingCurrentDelegate();
            _Controller.StopSpeeching(Secs, true, true); // ��������� �����������
            _Controller.SelectFirstDelegate();
        }

        private void btnSpeechCancel_Click(object sender, EventArgs e)
        {
            if (_Controller.SpeechMode == true)
            {
                tmrProcessSpeech.Stop();

                _Controller.CancelSpeeching(); // ��������� �����������
            }
        }

        private void RegMode_CheckedChanged(object sender, EventArgs e)
        {
            if (_regMode == false)
            {
                _regMode = true;
                chRegMode.Text = "���� ������ �� �����������...";
                _Controller.StartRegistration(); // ������ ������ �� �����������
                tmrProcessReg.Start();
            }
            else
            {
                _regMode = false;
                tmrProcessReg.Stop();
                _Controller.StopRegistration(); // ��������� ������ �� �����������
                chRegMode.Text = "�������� ������ �� �����������";
            }

        }

        private void tmrProcessReg_Tick(object sender, EventArgs e)
        {
            if (_regMode == true)
                _Controller.OnRegProcess();
        }

        private void gridDelegates_DoubleClick(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.

            GridHitInfo hi = gridviewSpeechDelegates.CalcHitInfo(gridDelegates.PointToClient(MousePosition));
            if (!hi.InRow) return;
/*
            if (btnSpeechDirectStart.Enabled == true)
            {
                btnSpeechDirectStart.Focus();
                btnSpeechDirectStart_Click(null, null);
            }
*/ 
        }

        private void btnSpeechDirectStart_Click(object sender, EventArgs e)
        {
            _Controller.StartDirectSpeeching();
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            _Controller.ClearAllDelegates();

        }

        private void xtraTabControl2_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page.Name == "pgHall")
                _Controller.OnSelHallPage();
            else if (e.Page.Name == "pgDelegateList")
                _Controller.OnSelDelegatesPage();
        }

        private void Mic_MouseEnter(object sender, EventArgs e)
        {
            info_sel.Visible = false;
            Control c = (Control)sender;
            if (c.Tag == null)
            {
                _Controller.ShowDelegateProp(-1); // �������� ������ ����
                return;
            }

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));

            SesDelegateObj theDelegate = null;
            theDelegate = _Controller.ShowDelegateProp(MicNum);

            if (MicNum > 0 && theDelegate != null && theDelegate.IsCardRegistered == true)
            {
                info_sel.Text = theDelegate.idDelegate.ShortName;

                int halfwidth = (info_sel.Width - c.Width) / 2;
                info_sel.Left = c.Left - halfwidth;
                info_sel.Top = c.Top - info_sel.Height;
                info_sel.ForeColor = System.Drawing.Color.Purple;
                info_sel.Visible = true;
            }
        }

        private void Mic_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Control c = (Control)sender;
            if (c.Tag == null)
            {
                return;
            }

            string s = c.Tag.ToString();
            int MicNum = Convert.ToInt32(s.Substring(3));

            _Controller.DelegateMicOn(MicNum);

        }

        private void btnThemeClose_Click(object sender, EventArgs e)
        {
            _Controller.OnFinishStatem();
            Close();
        }

        private void btnThemeClose_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnThemeClose_Click(sender, (EventArgs)e);
        }

        private void tmrRefreshHall_Tick(object sender, EventArgs e)
        {
/*
            tmrRefreshHall.Stop();
            _Controller.OnRefreshDelegates();
            tmrRefreshHall.Start();
*/ 
        }

        private void gridviewSpeechDelegates_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (_Controller.ViewMode == true)
            {
                return;
            }

            if (e.Column.Name == "colSpeech")
            {
                bool value = (bool)e.Value;
                DelegateStatementObj d = (DelegateStatementObj)gridviewSpeechDelegates.GetRow(e.RowHandle);

                int MicNum = d.idSeat.MicNum;

               _Controller.DelegateMicOn(MicNum);
            }
        }

        private void grpSeates_MouseMove(object sender, MouseEventArgs e)
        {
            info_sel.Visible = false;
        }

        private void gridviewSpeechDelegates_MouseEnter(object sender, EventArgs e)
        {
            if (_Controller.ViewMode == true)
            {
                return;
            }

        }

        private int hotTrackRow = DevExpress.XtraGrid.GridControl.InvalidRowHandle;

        private int HotTrackRow
        {
            get
            {
                return hotTrackRow;
            }
            set
            {
                if (hotTrackRow != value)
                {
                    int prevHotTrackRow = hotTrackRow;
                    hotTrackRow = value;

                    gridviewSpeechDelegates.RefreshRow(prevHotTrackRow);
                    gridviewSpeechDelegates.RefreshRow(hotTrackRow);
                }
            }
        }

        private void gridviewSpeechDelegates_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Controller.ViewMode == true)
            {
                return;
            }


            GridHitInfo info = gridviewSpeechDelegates.CalcHitInfo(new Point(e.X, e.Y));
            if (info.InRowCell && info.Column.Name == "colSpeech")
            {
                gridDelegates.Cursor = Cursors.Hand;
            }
            else
            {
                gridDelegates.Cursor = Cursors.Default;
            }

            if (info.InRowCell)
            {
                HotTrackRow = info.RowHandle;
            }
            else
                HotTrackRow = DevExpress.XtraGrid.GridControl.InvalidRowHandle;

        }


        private void gridviewSpeechDelegates_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle == HotTrackRow)
                e.Appearance.BackColor = Color.PaleGoldenrod;
        }

        private void barbtnReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Controller.PrintThemeReport();
        }

        private void tmrWarningInfo_Tick(object sender, EventArgs e)
        {
            tmrWarningInfo.Stop();

            barInfoText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            barInfoText.Caption = "";
        }

    }
}