using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class StartMenuForm : DevExpress.XtraEditors.XtraForm
    {
        MainCntrler _Controller;
        public StartMenuForm()
        {
            InitializeComponent();
        }

        public StartMenuForm(MainCntrler c)
        {
            _Controller = c;
            InitializeComponent();
        }

        private void btnNewSession_Click(object sender, EventArgs e)
        {
            _Controller.CreateNewSession("newSession");
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            _Controller.ShowSettings();
        }

        private void btnViewSession_Click(object sender, EventArgs e)
        {
            _Controller.ShowSessions();

        }

        private void btnNewConvoc_Click(object sender, EventArgs e)
        {
            ConvocationStartNew startNew = new ConvocationStartNew();
            startNew.Text = "���������� �� �������� ���� ������ ������ ������";
            startNew.DocumentFile = _Controller.GetDocFile_NewConvoc();
            try
            {
                startNew.ShowDialog();
            }
            catch (Exception Ex)
            {
                string err = "������ ��� �������� ����� ������������!\n" + Ex.Message;
                MessageBox.Show(this, err, "������");
            }
        }

        private void btnLoadConvoc_Click(object sender, EventArgs e)
        {
            ConvocationStartNew startNew = new ConvocationStartNew();
            startNew.Text = "���������� �� �������� �� ������ ���� ������ ������";
            startNew.DocumentFile = _Controller.GetDocFile_LoadConvoc();

            try
            {
                startNew.ShowDialog();
            }
            catch (Exception Ex)
            {
                string err = "������ ��� �������� ����� ������������!\n" + Ex.Message;
                MessageBox.Show(this, err, "������");
            }
        }

        private void btnDelegates_Click(object sender, EventArgs e)
        {
            _Controller.ShowReportGlobalDelegates();
        }

        private void btnQuestions_Click(object sender, EventArgs e)
        {
            _Controller.ShowGlobalQuestions();
        }


    }
}