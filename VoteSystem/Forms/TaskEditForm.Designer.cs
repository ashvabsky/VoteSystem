namespace VoteSystem
{
    partial class TaskEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskEditForm));
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.PropertiesControlQuest = new DevExpress.XtraVerticalGrid.VGridControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnPropCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnPropApply = new DevExpress.XtraEditors.SimpleButton();
            this.xpTasks = new DevExpress.Xpo.XPCollection();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox9 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox10 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox11 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox12 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox13 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox14 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.catCharacteristics = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowItemNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowCaption = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowidKind_Name = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.editorRow26 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catResult = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowResult = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowResDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.PropertiesControlQuest);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(469, 382);
            this.panelControl1.TabIndex = 6;
            // 
            // PropertiesControlQuest
            // 
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlQuest.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlQuest.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlQuest.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlQuest.DataSource = this.xpTasks;
            this.PropertiesControlQuest.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlQuest.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlQuest.Location = new System.Drawing.Point(2, 2);
            this.PropertiesControlQuest.Name = "PropertiesControlQuest";
            this.PropertiesControlQuest.OptionsBehavior.UseEnterAsTab = true;
            this.PropertiesControlQuest.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlQuest.RecordWidth = 145;
            this.PropertiesControlQuest.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemComboBox9,
            this.repositoryItemComboBox10,
            this.repositoryItemComboBox11,
            this.repositoryItemComboBox12,
            this.repositoryItemComboBox13,
            this.repositoryItemComboBox14,
            this.repositoryItemMemoEdit2,
            this.repositoryItemMemoEdit3,
            this.repositoryItemTextEdit7,
            this.repositoryItemTextEdit2});
            this.PropertiesControlQuest.RowHeaderWidth = 55;
            this.PropertiesControlQuest.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics,
            this.catNotes,
            this.catResult,
            this.categoryRow1});
            this.PropertiesControlQuest.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlQuest.Size = new System.Drawing.Size(465, 328);
            this.PropertiesControlQuest.TabIndex = 18;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnPropCancel);
            this.panelControl2.Controls.Add(this.btnPropApply);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 330);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(469, 52);
            this.panelControl2.TabIndex = 7;
            // 
            // btnPropCancel
            // 
            this.btnPropCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPropCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPropCancel.ImageIndex = 1;
            this.btnPropCancel.ImageList = this.ButtonImages;
            this.btnPropCancel.Location = new System.Drawing.Point(284, 9);
            this.btnPropCancel.Name = "btnPropCancel";
            this.btnPropCancel.Size = new System.Drawing.Size(154, 33);
            this.btnPropCancel.TabIndex = 7;
            this.btnPropCancel.Text = "�������� ���������";
            this.btnPropCancel.Click += new System.EventHandler(this.btnPropCancel_Click);
            // 
            // btnPropApply
            // 
            this.btnPropApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPropApply.ImageIndex = 13;
            this.btnPropApply.ImageList = this.ButtonImages;
            this.btnPropApply.Location = new System.Drawing.Point(37, 8);
            this.btnPropApply.Name = "btnPropApply";
            this.btnPropApply.Size = new System.Drawing.Size(164, 33);
            this.btnPropApply.TabIndex = 6;
            this.btnPropApply.Text = "��������� � �������";
            this.btnPropApply.Click += new System.EventHandler(this.btnPropApply_Click);
            // 
            // xpTasks
            // 
            this.xpTasks.CriteriaString = "[id] > 0";
            this.xpTasks.DeleteObjectOnRemove = true;
            this.xpTasks.DisplayableProperties = "This;id;idQuestion.Caption;idKind!";
            this.xpTasks.ObjectType = typeof(VoteSystem.SesTaskObj);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.ImmediatePopup = true;
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupFormMinSize = new System.Drawing.Size(500, 250);
            this.repositoryItemGridLookUpEdit2.PopupFormSize = new System.Drawing.Size(500, 250);
            this.repositoryItemGridLookUpEdit2.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox9
            // 
            this.repositoryItemComboBox9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox9.ImmediatePopup = true;
            this.repositoryItemComboBox9.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox9.Name = "repositoryItemComboBox9";
            // 
            // repositoryItemComboBox10
            // 
            this.repositoryItemComboBox10.AutoHeight = false;
            this.repositoryItemComboBox10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox10.ImmediatePopup = true;
            this.repositoryItemComboBox10.Name = "repositoryItemComboBox10";
            this.repositoryItemComboBox10.Sorted = true;
            // 
            // repositoryItemComboBox11
            // 
            this.repositoryItemComboBox11.AutoHeight = false;
            this.repositoryItemComboBox11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox11.Name = "repositoryItemComboBox11";
            // 
            // repositoryItemComboBox12
            // 
            this.repositoryItemComboBox12.AutoHeight = false;
            this.repositoryItemComboBox12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox12.ImmediatePopup = true;
            this.repositoryItemComboBox12.Name = "repositoryItemComboBox12";
            // 
            // repositoryItemComboBox13
            // 
            this.repositoryItemComboBox13.AutoHeight = false;
            this.repositoryItemComboBox13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox13.Name = "repositoryItemComboBox13";
            // 
            // repositoryItemComboBox14
            // 
            this.repositoryItemComboBox14.AutoHeight = false;
            this.repositoryItemComboBox14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox14.Name = "repositoryItemComboBox14";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.repositoryItemTextEdit7.Appearance.Options.UseForeColor = true;
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 20;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // catCharacteristics
            // 
            this.catCharacteristics.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowItemNum,
            this.rowCaption,
            this.rowidKind_Name,
            this.rowDescription});
            this.catCharacteristics.Height = 19;
            this.catCharacteristics.Name = "catCharacteristics";
            this.catCharacteristics.Properties.Caption = "��������������";
            // 
            // rowItemNum
            // 
            this.rowItemNum.Height = 28;
            this.rowItemNum.Name = "rowItemNum";
            this.rowItemNum.Properties.Caption = "� ������";
            this.rowItemNum.Properties.FieldName = "ItemNum";
            this.rowItemNum.Properties.RowEdit = this.repositoryItemTextEdit2;
            // 
            // rowCaption
            // 
            this.rowCaption.Height = 127;
            this.rowCaption.Name = "rowCaption";
            this.rowCaption.Properties.Caption = "���������";
            this.rowCaption.Properties.FieldName = "Caption";
            this.rowCaption.Properties.RowEdit = this.repositoryItemMemoEdit3;
            this.rowCaption.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // rowidKind_Name
            // 
            this.rowidKind_Name.Height = 28;
            this.rowidKind_Name.Name = "rowidKind_Name";
            this.rowidKind_Name.Properties.Caption = "���";
            this.rowidKind_Name.Properties.FieldName = "idKind!";
            this.rowidKind_Name.Properties.RowEdit = this.repositoryItemGridLookUpEdit2;
            // 
            // rowDescription
            // 
            this.rowDescription.Height = 95;
            this.rowDescription.Name = "rowDescription";
            this.rowDescription.Properties.Caption = "��������";
            this.rowDescription.Properties.FieldName = "Description";
            this.rowDescription.Properties.RowEdit = this.repositoryItemMemoEdit2;
            // 
            // catNotes
            // 
            this.catNotes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.editorRow26});
            this.catNotes.Height = 19;
            this.catNotes.Name = "catNotes";
            this.catNotes.Properties.Caption = "�������";
            this.catNotes.Visible = false;
            // 
            // editorRow26
            // 
            this.editorRow26.Height = 62;
            this.editorRow26.Name = "editorRow26";
            this.editorRow26.Properties.RowEdit = this.repositoryItemMemoEdit2;
            // 
            // catResult
            // 
            this.catResult.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catResult.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowResult,
            this.rowResDate});
            this.catResult.Height = 20;
            this.catResult.Name = "catResult";
            this.catResult.Properties.Caption = "���������";
            this.catResult.Visible = false;
            // 
            // rowResult
            // 
            this.rowResult.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResult.Appearance.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResult.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResult.Appearance.Options.UseBackColor = true;
            this.rowResult.Appearance.Options.UseFont = true;
            this.rowResult.Appearance.Options.UseForeColor = true;
            this.rowResult.Height = 36;
            this.rowResult.Name = "rowResult";
            this.rowResult.Properties.Caption = "�������";
            this.rowResult.Properties.ReadOnly = true;
            this.rowResult.Properties.RowEdit = this.repositoryItemTextEdit7;
            // 
            // rowResDate
            // 
            this.rowResDate.Appearance.BackColor = System.Drawing.Color.OldLace;
            this.rowResDate.Appearance.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowResDate.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.rowResDate.Appearance.Options.UseBackColor = true;
            this.rowResDate.Appearance.Options.UseFont = true;
            this.rowResDate.Appearance.Options.UseForeColor = true;
            this.rowResDate.Height = 28;
            this.rowResDate.Name = "rowResDate";
            this.rowResDate.Properties.Caption = "����/�����";
            this.rowResDate.Properties.ReadOnly = true;
            this.rowResDate.Properties.Value = "111";
            // 
            // categoryRow1
            // 
            this.categoryRow1.Name = "categoryRow1";
            // 
            // TaskEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 382);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "TaskEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "�������� �������";
            this.Load += new System.EventHandler(this.QuestionEditForm_Load);
            this.Shown += new System.EventHandler(this.QuestionEditForm_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuestionEditForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlQuest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xpTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnPropCancel;
        private DevExpress.XtraEditors.SimpleButton btnPropApply;
        private DevExpress.Xpo.XPCollection xpTasks;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlQuest;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox9;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox10;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox12;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox13;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowCaption;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowidKind_Name;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow26;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResult;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowResDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowItemNum;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;

    }
}