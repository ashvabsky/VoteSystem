﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class ConvocationStartNew : DevExpress.XtraEditors.XtraForm
    {
        string _Filename = "";
        public ConvocationStartNew()
        {
            InitializeComponent();
        }

        public string DocumentFile
        {
            set
            {
                _Filename = value;
            }
        }

        private void richEditControl1_DocumentLoaded(object sender, EventArgs e)
        {
            this.Focus();
            richEditControl1.Focus();
            bool f= richEditControl1.Focused;

            richEditControl1.UseWaitCursor = false;
        }

        private void ConvocationStartNew_Load(object sender, EventArgs e)
        {
        }

        private void ConvocationStartNew_Shown(object sender, EventArgs e)
        {
                if (_Filename != "")
                    richEditControl1.LoadDocument(_Filename);
        }
    }
}