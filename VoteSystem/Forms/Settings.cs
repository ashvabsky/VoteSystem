using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class Settings : DevExpress.XtraEditors.XtraForm
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void btnSetPortions_Click(object sender, EventArgs e)
        {
            Settings_portion sp = new Settings_portion();
            sp.ShowDialog();
        }
    }
}