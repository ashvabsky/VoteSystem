using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public partial class ThemeEditForm : DevExpress.XtraEditors.XtraForm
    {

        StatementObj _theTheme;
        NestedUnitOfWork _nuow;
        public XPCollection<StatementObj> _Themes;
        public XPCollection<SesTaskObj> _Tasks;

        public bool _readMode = false;


        public ThemeEditForm(StatementObj theme)
        {
            InitializeComponent();
            _theTheme = theme;
        }

        private void ThemeEditForm_Load(object sender, EventArgs e)
        {
            _theTheme.Session.BeginTransaction();
            _Themes = new XPCollection<StatementObj>(_theTheme.Session, false);
            _Themes.LoadingEnabled = false;
            _Themes.Add(_theTheme);


//            _theTheme.idTask.Session.BeginTransaction();
            _Tasks = new XPCollection<SesTaskObj>(_theTheme.Session);

            CriteriaOperator crTasks = CriteriaOperator.Parse("id = 0 || (idSession.id = ? AND IsDeleted = false)", _theTheme.idSession.id);

            PropertiesControlQuest.DataSource = _Themes;

            _Tasks.Criteria = crTasks;
            _Tasks.DisplayableProperties = "ItemNumStr;Caption;idKind.Name;CaptionItem;QueueNum";

            //          repositoryItemGridLookUpEdit1.PopupFormMinSize = 175;

            repositoryItemGridLookUpEdit1.DataSource = _Tasks;
            repositoryItemGridLookUpEdit1.DisplayMember = "CaptionItem";

            repositoryItemGridLookUpEdit1.PopulateViewColumns();

            repositoryItemGridLookUpEdit1.View.Columns[0].Caption = "� ������";
            repositoryItemGridLookUpEdit1.View.Columns[0].Width = 50;
//          repositoryItemGridLookUpEdit1.View.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            repositoryItemGridLookUpEdit1.View.Columns[1].Caption = "���������";
            repositoryItemGridLookUpEdit1.View.Columns[1].Width = 200;
            repositoryItemGridLookUpEdit1.View.Columns[2].Caption = "���";
            repositoryItemGridLookUpEdit1.View.Columns[2].Width = 100;

            repositoryItemGridLookUpEdit1.View.Columns[3].Visible = false;
            repositoryItemGridLookUpEdit1.View.Columns[4].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            repositoryItemGridLookUpEdit1.View.Columns[4].Visible = false;

            SetReadMode();
        }

        private void SetReadMode()
        {
            rowDescription.Properties.ReadOnly = _readMode;
            rowCaption.Properties.ReadOnly = false;//!_readMode;
        }

        private void ThemeEditForm_Shown(object sender, EventArgs e)
        {
            if (_theTheme.idTask.id == 0)
                rowDescription.Visible = false;
        }



        private void btnPropCancel_Click(object sender, EventArgs e)
        {
            _theTheme.Session.RollbackTransaction();
//            _theTheme.idTask.Session.RollbackTransaction();
        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
            _theTheme.Session.CommitTransaction();
//            _theTheme.idTask.Session.CommitTransaction();
        }

        private void ThemeEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_theTheme.Session.InTransaction)
                _theTheme.Session.RollbackTransaction();
        }

        private void PropertiesControlQuest_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
        }

        private void PropertiesControlQuest_CellValueChanging(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            if (e.Value is SesTaskObj)
            {
                SesTaskObj task = (SesTaskObj)e.Value;
                _theTheme.Caption = task.Caption;
                _theTheme.idTask = task;

                PropertiesControlQuest.DataSource = null;
                PropertiesControlQuest.DataSource = _Themes;
                rowDescription.Visible = true;
            }
        }

        private void PropertiesControlQuest_Click(object sender, EventArgs e)
        {

        }
    }
}