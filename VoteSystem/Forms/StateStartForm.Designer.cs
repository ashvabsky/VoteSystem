namespace VoteSystem
{
    partial class StateStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StateStartForm));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.btnMonitorStatePrepare = new DevExpress.XtraEditors.CheckButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoteTime = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.groupControl1.Controls.Add(this.checkBox2);
            this.groupControl1.Controls.Add(this.btnMonitorStatePrepare);
            this.groupControl1.Location = new System.Drawing.Point(19, 137);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(333, 137);
            this.groupControl1.TabIndex = 30;
            this.groupControl1.Text = "��������";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(31, 94);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(285, 17);
            this.checkBox2.TabIndex = 12;
            this.checkBox2.Text = "�������� �������������� �� ��������� �������";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // btnMonitorStatePrepare
            // 
            this.btnMonitorStatePrepare.ImageIndex = 6;
            this.btnMonitorStatePrepare.ImageList = this.ButtonImages;
            this.btnMonitorStatePrepare.Location = new System.Drawing.Point(16, 40);
            this.btnMonitorStatePrepare.Name = "btnMonitorStatePrepare";
            this.btnMonitorStatePrepare.Size = new System.Drawing.Size(285, 34);
            this.btnMonitorStatePrepare.TabIndex = 11;
            this.btnMonitorStatePrepare.Text = "������� ���������� �� ��������";
            this.btnMonitorStatePrepare.CheckedChanged += new System.EventHandler(this.btnMonitorStatePrepare_CheckedChanged);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 2;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(258, 363);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(93, 37);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "��������";
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnStart.ImageIndex = 0;
            this.btnStart.ImageList = this.ButtonImages;
            this.btnStart.Location = new System.Drawing.Point(19, 363);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(148, 37);
            this.btnStart.TabIndex = 23;
            this.btnStart.Text = "������ �����������";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(247, 80);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(22, 13);
            this.labelControl2.TabIndex = 22;
            this.labelControl2.Text = "���.";
            // 
            // txtVoteTime
            // 
            this.txtVoteTime.EditValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtVoteTime.Location = new System.Drawing.Point(180, 73);
            this.txtVoteTime.Name = "txtVoteTime";
            this.txtVoteTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtVoteTime.Properties.IsFloatValue = false;
            this.txtVoteTime.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtVoteTime.Size = new System.Drawing.Size(61, 20);
            this.txtVoteTime.TabIndex = 21;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(19, 76);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(119, 13);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "����� �� �����������:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(19, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(59, 13);
            this.labelControl3.TabIndex = 31;
            this.labelControl3.Text = "���������:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(35, 29);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(317, 32);
            this.richTextBox1.TabIndex = 32;
            this.richTextBox1.Text = "�����������, ������ ������������, ��� � 7";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(19, 106);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(255, 17);
            this.checkBox1.TabIndex = 33;
            this.checkBox1.Text = "��������� �������� �� ��������� �������";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(35, 309);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(317, 32);
            this.richTextBox2.TabIndex = 35;
            this.richTextBox2.Text = "����������, ������ ���������, ���. � 16";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(19, 292);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 13);
            this.labelControl4.TabIndex = 34;
            this.labelControl4.Text = "���������:";
            // 
            // StateStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 424);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtVoteTime);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StateStartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����������";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoteTime.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit txtVoteTime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.CheckButton btnMonitorStatePrepare;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
    }
}