using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace VoteSystem
{
    public partial class QuestionEditForm : DevExpress.XtraEditors.XtraForm
    {

        QuestionObj _theQuestion;
        NestedUnitOfWork _nuow;
        public XPCollection<QuestionObj> _Questions;
        public XPCollection<FractionObj> _Fractions;

        public bool _readMode = false;

        public QuestionEditForm(QuestionObj question)
        {
            InitializeComponent();
            _theQuestion = question;
         }

        private void QuestionEditForm_Load(object sender, EventArgs e)
        {
          _theQuestion.Session.BeginTransaction();

          _Questions = new XPCollection<QuestionObj>(_theQuestion.Session, false);

          _Questions.LoadingEnabled = false;

          _Questions.Add(_theQuestion);


          PropertiesControlQuest.DataSource = _Questions;


          CriteriaOperator criteriaInUse = CriteriaOperator.Parse("id > 0 AND IsDel = false");


            XPCollection<TaskKindObj> Kinds = new XPCollection<TaskKindObj>(_theQuestion.Session);
            Kinds.Criteria = criteriaInUse;
            Kinds.DisplayableProperties = "Name;idQntyType.Name";

            repositoryItemGridLookUpEdit2.DataSource = Kinds;
            repositoryItemGridLookUpEdit2.DisplayMember = "Name";
            repositoryItemGridLookUpEdit2.PopulateViewColumns();


            repositoryItemGridLookUpEdit2.View.Columns[0].Caption = "������������";
            repositoryItemGridLookUpEdit2.View.Columns[1].Caption = "��� ��������";

//---------------------
            CriteriaOperator crTasks = CriteriaOperator.Parse("idSession.id = ? AND IsDeleted = false", _theQuestion.idSession.id);


            XPCollection<SesTaskObj> Tasks = new XPCollection<SesTaskObj>(_theQuestion.Session);
            Tasks.Criteria = crTasks;
            Tasks.DisplayableProperties = "ItemNum;Caption;idKind.Name;CaptionItem";

//          repositoryItemGridLookUpEdit1.PopupFormMinSize = 175;

            repositoryItemGridLookUpEdit1.DataSource = Tasks;
            repositoryItemGridLookUpEdit1.DisplayMember = "CaptionItem";
            repositoryItemGridLookUpEdit1.PopulateViewColumns();

            repositoryItemGridLookUpEdit1.View.Columns[0].Caption = "� ������";
            repositoryItemGridLookUpEdit1.View.Columns[0].Width = 50;
            repositoryItemGridLookUpEdit1.View.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            repositoryItemGridLookUpEdit1.View.Columns[1].Caption = "���������";
            repositoryItemGridLookUpEdit1.View.Columns[1].Width = 200;
            repositoryItemGridLookUpEdit1.View.Columns[2].Caption = "���";
            repositoryItemGridLookUpEdit1.View.Columns[2].Width = 100;

            repositoryItemGridLookUpEdit1.View.Columns[3].Visible = false;

            SetReadMode();
        }

        private void SetReadMode()
        {
            rowidKind_Name.Properties.ReadOnly = _readMode;

            rowDescription.Properties.ReadOnly = _readMode;

//          rowCaption.Properties.ReadOnly = _readMode;
        }

        private void QuestionEditForm_Shown(object sender, EventArgs e)
        {

        }


        private void btnPropCancel_Click(object sender, EventArgs e)
        {

            _theQuestion.Session.RollbackTransaction();

        }

        private void btnPropApply_Click(object sender, EventArgs e)
        {
            _theQuestion.Session.CommitTransaction();
        }

        private void QuestionEditForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_theQuestion.Session.InTransaction)
                _theQuestion.Session.RollbackTransaction();

        }
    }
}