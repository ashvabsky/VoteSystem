namespace VoteSystem
{
    partial class RegStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegStartForm));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtRegTime = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.chkRegProccessToMonitor = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtDelegateQnty = new DevExpress.XtraEditors.TextEdit();
            this.txtQuorumQnty = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegProccessToMonitor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(31, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(152, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "����� �� �����������:";
            // 
            // txtRegTime
            // 
            this.txtRegTime.EditValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtRegTime.Location = new System.Drawing.Point(220, 17);
            this.txtRegTime.Name = "txtRegTime";
            this.txtRegTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtRegTime.Properties.IsFloatValue = false;
            this.txtRegTime.Properties.MaxValue = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.txtRegTime.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtRegTime.Size = new System.Drawing.Size(61, 20);
            this.txtRegTime.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(287, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(21, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "���.";
            // 
            // btnStart
            // 
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnStart.ImageIndex = 5;
            this.btnStart.ImageList = this.ButtonImages;
            this.btnStart.Location = new System.Drawing.Point(22, 217);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(154, 37);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "������ �����������";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageIndex = 2;
            this.btnClose.ImageList = this.ButtonImages;
            this.btnClose.Location = new System.Drawing.Point(196, 217);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 37);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "��������";
            // 
            // chkRegProccessToMonitor
            // 
            this.chkRegProccessToMonitor.EditValue = true;
            this.chkRegProccessToMonitor.Location = new System.Drawing.Point(29, 170);
            this.chkRegProccessToMonitor.Name = "chkRegProccessToMonitor";
            this.chkRegProccessToMonitor.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkRegProccessToMonitor.Properties.Appearance.Options.UseFont = true;
            this.chkRegProccessToMonitor.Properties.Caption = "�������� ���������� �� ��������";
            this.chkRegProccessToMonitor.Size = new System.Drawing.Size(280, 21);
            this.chkRegProccessToMonitor.TabIndex = 8;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(31, 63);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(111, 16);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "�������� � ����:";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(31, 108);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(166, 16);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "���������� ��� �������:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(287, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(22, 13);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "���.";
            // 
            // txtDelegateQnty
            // 
            this.txtDelegateQnty.Location = new System.Drawing.Point(220, 60);
            this.txtDelegateQnty.Name = "txtDelegateQnty";
            this.txtDelegateQnty.Size = new System.Drawing.Size(61, 20);
            this.txtDelegateQnty.TabIndex = 20;
            // 
            // txtQuorumQnty
            // 
            this.txtQuorumQnty.Location = new System.Drawing.Point(220, 104);
            this.txtQuorumQnty.Name = "txtQuorumQnty";
            this.txtQuorumQnty.Size = new System.Drawing.Size(61, 20);
            this.txtQuorumQnty.TabIndex = 21;
            // 
            // RegStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 266);
            this.Controls.Add(this.txtQuorumQnty);
            this.Controls.Add(this.chkRegProccessToMonitor);
            this.Controls.Add(this.txtDelegateQnty);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtRegTime);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RegStartForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� �����������";
            this.Load += new System.EventHandler(this.RegStartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtRegTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRegProccessToMonitor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDelegateQnty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuorumQnty.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit txtRegTime;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.CheckEdit chkRegProccessToMonitor;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.XtraEditors.TextEdit txtDelegateQnty;
        private DevExpress.XtraEditors.TextEdit txtQuorumQnty;
    }
}