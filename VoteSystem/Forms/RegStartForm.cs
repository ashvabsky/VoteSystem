using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VoteSystem
{
    public partial class RegStartForm : DevExpress.XtraEditors.XtraForm
    {
        HallCntrler _Controller;
        public RegStartForm(HallCntrler controller)
        {
            InitializeComponent();
            _Controller = controller;
        }

        public int RegTime
        {
            set { txtRegTime.Text = value.ToString(); }
            get { return Int32.Parse(txtRegTime.Text); }
        }

        public int DelegatesQnty
        {
            set { txtDelegateQnty.Text = value.ToString(); }
        }

        public int QuorumQnty
        {
            set { txtQuorumQnty.Text = value.ToString(); }
            get { return Int32.Parse(txtQuorumQnty.Text); }
        }

        public bool IsRegProccess
        {
            set { chkRegProccessToMonitor.Checked = value; }
            get { return chkRegProccessToMonitor.Checked; }
        }

        public bool IsRegInfoToScreen
        {
            set 
            { 
                chkRegProccessToMonitor.Checked = value;
                _Controller.SetRegInfoToScreen(value);
            }
            get { return chkRegProccessToMonitor.Checked; }
        }

        private void RegStartForm_Load(object sender, EventArgs e)
        {
            _Controller.OnRegStartFormLoad();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }
    }
}