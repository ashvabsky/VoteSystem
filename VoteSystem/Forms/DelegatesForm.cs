using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data.Filtering;
using DevExpress.Utils;

namespace VoteSystem
{
    public interface IDelegatesForm
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        void Properties_StartEdit();
        void Properties_FinishEdit();

        void SetSelectMode();
        int[] GetSelectedItems();

        IMsgMethods GetMessages();
        void EnableButtons(bool IsActiveDelegate, bool IsDeleted);
        XtraForm GetView();
    }

    public partial class DelegatesForm : DevExpress.XtraEditors.XtraForm, IDelegatesForm
    {
        DelegatesCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;

        bool _init = false;
        int[] _handles; // ������ ���������� ������� ����� �����


        NavigatorCustomButton btnAdd = null;
        NavigatorCustomButton btnRemove = null;
        NavigatorCustomButton btnChange = null;
        NavigatorCustomButton btnShowAll = null;
        NavigatorCustomButton btnRestore = null;

        public DelegatesForm()
        {
            InitializeComponent();
        }

        public DelegatesForm(DelegatesCntrler cntrler, MainCntrler mcontr, MainForm mform)
        {
            InitializeComponent();
            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;

            _Controller.Init(ref xpDelegates, ref xpParties, ref xpFractions, ref xpRegions, ref xpSeats);

        }

#region ���������� ���������� IDelegatesForm

        IMsgMethods IDelegatesForm.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IDelegatesForm.ShowView()
        {
            _Controller.InitShow();

            ConditionsAdjustment();

            DialogResult dr = (DialogResult)this.ShowDialog();
            gridView1.OptionsSelection.MultiSelect = false;
            return dr;
        }

        void IDelegatesForm.SetSelectMode()
        {
            gridView1.OptionsSelection.MultiSelect = true;
        }

        int[] IDelegatesForm.GetSelectedItems()
        {
            if (_handles == null)
                return new int[0];

            List<int> IDs = new List<int>();
            int c = _handles.Length;
//          int[] handles = gridView1.GetSelectedRows();

            for (int i = 0; i < c ; i++)
            {
                int handle = _handles[i];
                if (handle >= 0)
                {
                    DelegateObj dr = (DelegateObj)gridView1.GetRow(handle);
                    IDs.Add(dr.id);
                }
                else
                {
                    DelegateObj dr = (DelegateObj)gridView1.GetRow(handle); // �������� ������ �� ������� ������
                    FractionObj fraction = dr.idFraction;
                    _Controller.GetDelegateIDsByFraction(fraction.id, ref IDs);
                }
            }
            return IDs.ToArray();
        }

        DevExpress.XtraEditors.XtraForm IDelegatesForm.GetView()
        {
            return this;
        }

        void IDelegatesForm.Properties_StartEdit()
        {

            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = false;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = false;
                }

            }

            // ����������� ����, ������� ������� �������� ��� readonly:
            PropertiesControl.Rows["rowRegionNum"].Properties.ReadOnly = true;


            splitContainerControl1.Panel1.Enabled = false;

            PropertiesControl.FocusedRow = PropertiesControl.Rows["rowLastName"];
            PropertiesControl.ShowEditor();

        }

        void IDelegatesForm.Properties_FinishEdit()
        {
            foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow row in PropertiesControl.Rows)
            {
                row.Properties.ReadOnly = true;
                foreach (DevExpress.XtraVerticalGrid.Rows.BaseRow rowin in row.ChildRows)
                {
                    rowin.Properties.ReadOnly = true;
                }
            }

            splitContainerControl1.Panel1.Enabled = true;

            _Controller.DataPositionChanged(dataNavigator1.Position);

        }

        void IDelegatesForm.EnableButtons(bool IsActiveDelegate, bool IsDeleted)
        {
            if (_Controller.Mode != DelegatesCntrler.DelegatesFormMode.Manage)
                btnSelect.Enabled = IsActiveDelegate;
            else
            {
                btnRestore.Enabled = IsDeleted;
            }
        }

        IMsgMethods IDelegatesForm.GetMessages()
        {
            return _messages;
        }
#endregion

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            DataNavigator dn = (DataNavigator)sender;
            if (dn == null)
                return;

            _Controller.DataPositionChanged(dn.Position);
        }

        private void StartEdit()
        {

            int Pos = dataNavigator1.Position;
            _Controller.EditRecord(Pos);
        }


        private void DelegatesForm_Load(object sender, EventArgs e)
        {
//            _Controller.Init(ref xpDelegates, ref xpParties, ref xpFractions, ref xpRegions);
            btnAdd = navigatorDelegates.Buttons.CustomButtons[0];
            btnRemove = navigatorDelegates.Buttons.CustomButtons[1];
            btnChange = navigatorDelegates.Buttons.CustomButtons[2];
            btnShowAll = navigatorDelegates.Buttons.CustomButtons[3];
            btnRestore = navigatorDelegates.Buttons.CustomButtons[4];


            _init = true;
            dataNavigator1_PositionChanged(dataNavigator1, null); // �������� ��������� ��������
        }



        private void gridControl1_DoubleClick_1(object sender, EventArgs e)
        {
            // Checks whether an end-used has double clicked the row indicator.
            GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (!hi.InRow) return;

            StartEdit();
        }

        private void gridControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                StartEdit();
            }
        }


        private void xpCollection1_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_init) // ��������: �������������� �������� ������ ��� ������������ 
                _Controller.DataPositionChanged(dataNavigator1.Position); // �������� ��������� ��������
        }


        private void barLargeButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FractionsForm fr = new FractionsForm();
            fr.ShowDialog();
            xpFractions.Reload();
        }

        private void barLargeButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PartiesForm pr = new PartiesForm();
            pr.ShowDialog();
            xpParties.Reload();
        }

        private void barLargeButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RegionsForm rg = new RegionsForm();
            rg.ShowDialog();
            xpRegions.Reload();
        }

        private void DelegatesForm_Shown(object sender, EventArgs e)
        {
            if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.Manage)
            {
                btnSelect.Text = "���������� �����������...";
                btnSelect.Visible = true;

                btnAdd.Visible = true;
                btnRemove.Visible = true;
                btnChange.Visible = true;
                btnShowAll.Visible = true;
                btnRestore.Visible = false;
                catSeat.Visible = true;

            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.Select)
            {
                btnSelect.Text = "������� ��� ������";
                btnSelect.Visible = true;

                btnAdd.Visible = true;
                btnRemove.Visible = true;
                btnChange.Visible = true;
                catSeat.Visible = true;
                btnShowAll.Visible = false;
                btnRestore.Visible = false;

            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.ReportSelect)
            {
                btnSelect.Text = "����� �� ���������...";
                btnSelect.Visible = true;

                btnAdd.Visible = false;
                btnRemove.Visible = false;
                btnChange.Visible = false;
                btnShowAll.Visible = true;
                btnRestore.Visible = false;

                catSeat.Visible = true;
            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.SelectStatem)
            {
                btnSelect.Text = "������� ��� �����������";
                btnSelect.Visible = true;

                btnAdd.Visible = true;
                btnRemove.Visible = true;
                btnChange.Visible = true;
                catSeat.Visible = false;
            }
            else if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.SeatSelect)
            {
                btnSelect.Text = "�������";
                btnSelect.Visible = true;
                bar1.Visible = false;

                btnAdd.Visible = false;
                btnRemove.Visible = false;
                btnChange.Visible = false;
                catSeat.Visible = true;
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            _handles = gridView1.GetSelectedRows();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (_Controller.Mode == DelegatesCntrler.DelegatesFormMode.Manage || _Controller.Mode == DelegatesCntrler.DelegatesFormMode.ReportSelect) // � ������ ���������� ��� ������ ���������� ���������� �� ���������� ��������
            {
                _Controller.ShowDetails();
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "colInUse")
            {
                bool value = (bool)e.Value;
                DelegateObj d = (DelegateObj)gridView1.GetRow(e.RowHandle);

                if (d.isActive)
                {
                    _Controller.AddSesDelegate(d);
                }
            }
        }

        private void navigatorDelegates_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button.Tag.ToString() == "Append")
                {
                    int i = _Controller.AddNewDelegate();
                    if (i >= 0)
                        dataNavigator1.Position = i;
                }
                else if (e.Button.Tag.ToString() == "Remove")
                {
                    _Controller.HideOrRemoveDelegate();
                    btnRestore.Visible = true;
                }
                else if (e.Button.Tag.ToString() == "Edit")
                {
                    StartEdit();
                }
                else if (e.Button.Tag.ToString() == "ShowAll")
                {
                    _Controller.ShowAll();
                    btnRestore.Visible = true;
                }
                else if (e.Button.Tag.ToString() == "Restore")
                {
                    _Controller.RestoreDeletedItem();
                }
            }

        }

        private void btnSyncDBase_Click(object sender, EventArgs e)
        {
            _Controller.SyncCardDBase();
        }

        private void ConditionsAdjustment()
        {
            StyleFormatCondition cn;

            cn = new StyleFormatCondition(FormatConditionEnum.Equal, gridView1.Columns["IsDel"], null, true);
            cn.ApplyToRow = true;
            cn.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Strikeout);
            cn.Appearance.ForeColor = SystemColors.ControlDark;
            gridView1.FormatConditions.Add(cn);
            //<gridControl1>
            //          gridView1.BestFitColumns();
        }
    }
}