using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Shell32;
using DevExpress.Xpo;

namespace VoteSystem
{
    public interface IReportThemes
    {
        IMsgMethods GetShowInterface();

        DialogResult ShowView();

        XtraForm GetView();

        void InitControls(SettingsReportObj settings);
    }

    public partial class ReportThemes : DevExpress.XtraEditors.XtraForm, IReportThemes
    {
        ReportCntrler _Controller;
        MsgForm _msgForm;
        IMsgMethods _messages;


        public ReportThemes(ReportCntrler cntrler)
        {
            InitializeComponent();

            _Controller = cntrler;
            _msgForm = new MsgForm(this);
            _messages = (IMsgMethods)_msgForm;
        }

        #region ���������� ���������� ISettingsForm

        IMsgMethods IReportThemes.GetShowInterface()
        {
            return (IMsgMethods)_msgForm;
        }

        DialogResult IReportThemes.ShowView()
        {
            DialogResult dr = (DialogResult)this.ShowDialog();
            return dr;
        }

        void IReportThemes.InitControls(SettingsReportObj settings)
        {
            txtTemplateFile.Text = settings.TemplateFile_T;
            txtResultFile.Text = _Controller.GenerateResultFile_T();
            txtResultPath.Text = settings.ReportPath;

            chkShowDetails.EditValue = settings.Report_ShowDetails_T;
            chkShowNonSpeech.EditValue = settings.Report_ShowNonSpeeches;

        }

        XtraForm IReportThemes.GetView()
        {
            return this;
        }


        #endregion ���������� ���������� ISettingsForm

        private void btnReport_Click(object sender, EventArgs e)
        {
            _Controller.T_SaveParams(txtTemplateFile.Text, txtResultPath.Text, (bool)chkShowDetails.EditValue, (bool)chkShowNonSpeech.EditValue, txtResultFile.Text);
            Cursor = Cursors.WaitCursor;
//            _Controller.T_PrintReport();
            Cursor = Cursors.Default;
        }

        private void ReportThemes_FormClosing(object sender, FormClosingEventArgs e)
        {
            _Controller.OnClosing();
        }

        private void ReportThemes_Load(object sender, EventArgs e)
        {
            _Controller.T_OnInitForm();
        }

        private void txtTemplateFile_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string fname = "";
            
            openFileDialog.Filter = "template files (*.xls)|*.xls;|All files(*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fname = openFileDialog.FileName;
                txtTemplateFile.Text = fname;
//              m_Controller.AssignFileName(fname, null);
            }
            
        }


        private void txtResultPath_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Shell shell = new Shell();
            Folder folder = shell.BrowseForFolder(0, "�������� ����� ��� ���������� �����������", 0, ShellSpecialFolderConstants.ssfDESKTOP);
//          Folder Parent = folder.ParentFolder;
            txtResultPath.Text = "";
            string result = "";
            string title = "";
            string disk = "";
            while (folder != null)
            {
                title = folder.Title;
                folder = folder.ParentFolder;
                if (folder != null && folder.Title != "������� ����")
                {
                    result = title + "\\" + result;
                    disk = title;
                }
                else
                    break;
//              title = Parent.Title;
            }

            int b = disk.IndexOf("(");
            int c = disk.IndexOf(")", b);
            string diskname = "";
            if (b>=0 && c > b)
                diskname = disk.Substring(b + 1, c - b-1);
            txtResultPath.Text = result.Replace(disk, diskname);

            string resultfile = txtResultPath.Text + txtResultFile.Text;
            System.IO.FileInfo f = new System.IO.FileInfo(resultfile);

        }

        private void chkShowDetails_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkShowDetails.CheckState == CheckState.Checked)
                chkShowNonSpeech.Enabled = true;
            else
                chkShowNonSpeech.Enabled = false;

        }

    }
}