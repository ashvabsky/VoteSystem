namespace VoteSystem
{
    partial class SpeechModeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeechModeForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.mainTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.pgHall = new DevExpress.XtraTab.XtraTabPage();
            this.grpSeates = new DevExpress.XtraEditors.GroupControl();
            this.info_sel = new DevExpress.XtraEditors.LabelControl();
            this.info_9 = new DevExpress.XtraEditors.LabelControl();
            this.info_8 = new DevExpress.XtraEditors.LabelControl();
            this.info_7 = new DevExpress.XtraEditors.LabelControl();
            this.info_6 = new DevExpress.XtraEditors.LabelControl();
            this.info_5 = new DevExpress.XtraEditors.LabelControl();
            this.info_4 = new DevExpress.XtraEditors.LabelControl();
            this.info_3 = new DevExpress.XtraEditors.LabelControl();
            this.info_2 = new DevExpress.XtraEditors.LabelControl();
            this.info_1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit12 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit13 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit14 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit15 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit11 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.labelControl97 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit75 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit76 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl99 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit77 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit78 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit79 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit80 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit81 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit82 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl105 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit83 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl106 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit84 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit85 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit86 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit87 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit88 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit89 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit90 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit91 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit92 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit93 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit94 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit95 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl118 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit96 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl119 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit97 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl120 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit98 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl121 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit99 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl122 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit100 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit101 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit102 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit103 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit104 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit105 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit106 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit107 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl130 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit108 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit109 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit110 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit111 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl134 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit112 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit113 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl136 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit114 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl137 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit115 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl138 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit116 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl139 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit117 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl140 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit118 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl141 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit119 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl142 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit120 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit121 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit122 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit59 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit60 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit61 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit62 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit63 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit64 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit65 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit66 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit67 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit68 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit69 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit70 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit71 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl94 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit72 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl95 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit73 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl96 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit74 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit43 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit44 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit45 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit46 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit47 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit48 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit49 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit50 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit51 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit52 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit53 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit54 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit55 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit56 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit57 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit58 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit27 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit19 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit28 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit20 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit29 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit21 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit30 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit22 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit31 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit23 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit32 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit24 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit33 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit25 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit34 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit26 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit35 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit36 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit37 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit38 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit39 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit40 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit41 = new DevExpress.XtraEditors.PictureEdit();
            this.Mic_2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit42 = new DevExpress.XtraEditors.PictureEdit();
            this.Mic_1 = new DevExpress.XtraEditors.PictureEdit();
            this.pgDelegateList = new DevExpress.XtraTab.XtraTabPage();
            this.navControlSpeechDelegates = new DevExpress.XtraEditors.ControlNavigator();
            this.ButtonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.gridDelegates = new DevExpress.XtraGrid.GridControl();
            this.gridviewSpeechDelegates = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFraction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPartyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMicNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQNum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeech = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barbtnReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.btnCloseTheme = new DevExpress.XtraBars.BarButtonItem();
            this.barInfoText = new DevExpress.XtraBars.BarStaticItem();
            this.RibbonImages = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.InfoTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.pgStatemInfo = new DevExpress.XtraTab.XtraTabPage();
            this.navigatorSpeechDelegates = new DevExpress.XtraEditors.DataNavigator();
            this.PropertiesControlStatem = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox30 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox31 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox32 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox33 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox34 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox35 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemDateEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemComboBox36 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox37 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox38 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox39 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemComboBox40 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemComboBox41 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox42 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemMemoEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemMemoEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.catCharacteristics_st = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowTask_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowName_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStartDate_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowDescription_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStatus_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catTotals_st = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowDelegateQnty_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSpeechTimeTotal_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowAvrgTime_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catNotes = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowNotes_st = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.pgDelegateInfo = new DevExpress.XtraTab.XtraTabPage();
            this.PropertiesControlDelegate = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.catFIO = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowPartyName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catResults = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowSpeechType_std = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowState_std = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowStartTime_std = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLastDuration = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowNotes_std = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.pgDelegatesHall = new DevExpress.XtraTab.XtraTabPage();
            this.PropCntrlDelegate2 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox7 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox8 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.catFIO_H = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowLastName_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowFirstName_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSecondName_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catRegion_H = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowRegionName_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRegionNum_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catInfo_H = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowFraction_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowParty_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.catSeat_H = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowMicNum_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowRowNum_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSeatNum_H = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnClearAll = new DevExpress.XtraEditors.SimpleButton();
            this.chRegMode = new DevExpress.XtraEditors.CheckEdit();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnspeechFinish = new DevExpress.XtraEditors.SimpleButton();
            this.lblProcesTime = new DevExpress.XtraEditors.LabelControl();
            this.progressBarVoting = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblProcess = new DevExpress.XtraEditors.LabelControl();
            this.picVoteTime = new DevExpress.XtraEditors.PictureEdit();
            this.btnSpeechStart = new DevExpress.XtraEditors.SimpleButton();
            this.tmrProcessSpeech = new System.Windows.Forms.Timer(this.components);
            this.tmrProcessReg = new System.Windows.Forms.Timer(this.components);
            this.ManImages = new DevExpress.Utils.ImageCollection(this.components);
            this.tmrRefreshHall = new System.Windows.Forms.Timer(this.components);
            this.tmrWarningInfo = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).BeginInit();
            this.mainTabControl.SuspendLayout();
            this.pgHall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpSeates)).BeginInit();
            this.grpSeates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).BeginInit();
            this.pgDelegateList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewSpeechDelegates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoTabControl)).BeginInit();
            this.InfoTabControl.SuspendLayout();
            this.pgStatemInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlStatem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.pgDelegateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlDelegate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.pgDelegatesHall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PropCntrlDelegate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chRegMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManImages)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.mainTabControl);
            this.splitContainerControl1.Panel1.Controls.Add(this.ribbonControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.InfoTabControl);
            this.splitContainerControl1.Panel2.Text = "������ �������";
            this.splitContainerControl1.Size = new System.Drawing.Size(1394, 750);
            this.splitContainerControl1.SplitterPosition = 956;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // mainTabControl
            // 
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 94);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedTabPage = this.pgHall;
            this.mainTabControl.Size = new System.Drawing.Size(956, 656);
            this.mainTabControl.TabIndex = 242;
            this.mainTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgDelegateList,
            this.pgHall});
            this.mainTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl2_SelectedPageChanged);
            // 
            // pgHall
            // 
            this.pgHall.Controls.Add(this.grpSeates);
            this.pgHall.Name = "pgHall";
            this.pgHall.Size = new System.Drawing.Size(949, 628);
            this.pgHall.Text = "������������ � ����";
            // 
            // grpSeates
            // 
            this.grpSeates.AppearanceCaption.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpSeates.AppearanceCaption.Options.UseFont = true;
            this.grpSeates.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.grpSeates.ContentImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.grpSeates.Controls.Add(this.info_sel);
            this.grpSeates.Controls.Add(this.info_9);
            this.grpSeates.Controls.Add(this.info_8);
            this.grpSeates.Controls.Add(this.info_7);
            this.grpSeates.Controls.Add(this.info_6);
            this.grpSeates.Controls.Add(this.info_5);
            this.grpSeates.Controls.Add(this.info_4);
            this.grpSeates.Controls.Add(this.info_3);
            this.grpSeates.Controls.Add(this.info_2);
            this.grpSeates.Controls.Add(this.info_1);
            this.grpSeates.Controls.Add(this.labelControl29);
            this.grpSeates.Controls.Add(this.pictureEdit12);
            this.grpSeates.Controls.Add(this.labelControl30);
            this.grpSeates.Controls.Add(this.pictureEdit13);
            this.grpSeates.Controls.Add(this.labelControl31);
            this.grpSeates.Controls.Add(this.pictureEdit14);
            this.grpSeates.Controls.Add(this.labelControl32);
            this.grpSeates.Controls.Add(this.pictureEdit15);
            this.grpSeates.Controls.Add(this.labelControl2);
            this.grpSeates.Controls.Add(this.pictureEdit8);
            this.grpSeates.Controls.Add(this.labelControl26);
            this.grpSeates.Controls.Add(this.pictureEdit9);
            this.grpSeates.Controls.Add(this.labelControl27);
            this.grpSeates.Controls.Add(this.pictureEdit10);
            this.grpSeates.Controls.Add(this.labelControl28);
            this.grpSeates.Controls.Add(this.pictureEdit11);
            this.grpSeates.Controls.Add(this.labelControl3);
            this.grpSeates.Controls.Add(this.pictureEdit7);
            this.grpSeates.Controls.Add(this.label3);
            this.grpSeates.Controls.Add(this.label2);
            this.grpSeates.Controls.Add(this.label92);
            this.grpSeates.Controls.Add(this.label15);
            this.grpSeates.Controls.Add(this.label24);
            this.grpSeates.Controls.Add(this.label27);
            this.grpSeates.Controls.Add(this.label36);
            this.grpSeates.Controls.Add(this.label37);
            this.grpSeates.Controls.Add(this.label38);
            this.grpSeates.Controls.Add(this.labelControl97);
            this.grpSeates.Controls.Add(this.pictureEdit75);
            this.grpSeates.Controls.Add(this.labelControl98);
            this.grpSeates.Controls.Add(this.pictureEdit76);
            this.grpSeates.Controls.Add(this.labelControl99);
            this.grpSeates.Controls.Add(this.pictureEdit77);
            this.grpSeates.Controls.Add(this.labelControl100);
            this.grpSeates.Controls.Add(this.pictureEdit78);
            this.grpSeates.Controls.Add(this.labelControl101);
            this.grpSeates.Controls.Add(this.pictureEdit79);
            this.grpSeates.Controls.Add(this.labelControl102);
            this.grpSeates.Controls.Add(this.pictureEdit80);
            this.grpSeates.Controls.Add(this.labelControl103);
            this.grpSeates.Controls.Add(this.pictureEdit81);
            this.grpSeates.Controls.Add(this.labelControl104);
            this.grpSeates.Controls.Add(this.pictureEdit82);
            this.grpSeates.Controls.Add(this.labelControl105);
            this.grpSeates.Controls.Add(this.pictureEdit83);
            this.grpSeates.Controls.Add(this.labelControl106);
            this.grpSeates.Controls.Add(this.pictureEdit84);
            this.grpSeates.Controls.Add(this.labelControl107);
            this.grpSeates.Controls.Add(this.pictureEdit85);
            this.grpSeates.Controls.Add(this.labelControl108);
            this.grpSeates.Controls.Add(this.pictureEdit86);
            this.grpSeates.Controls.Add(this.labelControl109);
            this.grpSeates.Controls.Add(this.pictureEdit87);
            this.grpSeates.Controls.Add(this.labelControl110);
            this.grpSeates.Controls.Add(this.pictureEdit88);
            this.grpSeates.Controls.Add(this.labelControl111);
            this.grpSeates.Controls.Add(this.pictureEdit89);
            this.grpSeates.Controls.Add(this.labelControl112);
            this.grpSeates.Controls.Add(this.pictureEdit90);
            this.grpSeates.Controls.Add(this.labelControl113);
            this.grpSeates.Controls.Add(this.pictureEdit91);
            this.grpSeates.Controls.Add(this.labelControl114);
            this.grpSeates.Controls.Add(this.pictureEdit92);
            this.grpSeates.Controls.Add(this.labelControl115);
            this.grpSeates.Controls.Add(this.pictureEdit93);
            this.grpSeates.Controls.Add(this.labelControl116);
            this.grpSeates.Controls.Add(this.pictureEdit94);
            this.grpSeates.Controls.Add(this.labelControl117);
            this.grpSeates.Controls.Add(this.pictureEdit95);
            this.grpSeates.Controls.Add(this.labelControl118);
            this.grpSeates.Controls.Add(this.pictureEdit96);
            this.grpSeates.Controls.Add(this.labelControl119);
            this.grpSeates.Controls.Add(this.pictureEdit97);
            this.grpSeates.Controls.Add(this.labelControl120);
            this.grpSeates.Controls.Add(this.pictureEdit98);
            this.grpSeates.Controls.Add(this.labelControl121);
            this.grpSeates.Controls.Add(this.pictureEdit99);
            this.grpSeates.Controls.Add(this.labelControl122);
            this.grpSeates.Controls.Add(this.pictureEdit100);
            this.grpSeates.Controls.Add(this.labelControl123);
            this.grpSeates.Controls.Add(this.pictureEdit101);
            this.grpSeates.Controls.Add(this.labelControl124);
            this.grpSeates.Controls.Add(this.pictureEdit102);
            this.grpSeates.Controls.Add(this.labelControl125);
            this.grpSeates.Controls.Add(this.pictureEdit103);
            this.grpSeates.Controls.Add(this.labelControl126);
            this.grpSeates.Controls.Add(this.pictureEdit104);
            this.grpSeates.Controls.Add(this.labelControl127);
            this.grpSeates.Controls.Add(this.pictureEdit105);
            this.grpSeates.Controls.Add(this.labelControl128);
            this.grpSeates.Controls.Add(this.pictureEdit106);
            this.grpSeates.Controls.Add(this.labelControl129);
            this.grpSeates.Controls.Add(this.pictureEdit107);
            this.grpSeates.Controls.Add(this.labelControl130);
            this.grpSeates.Controls.Add(this.pictureEdit108);
            this.grpSeates.Controls.Add(this.labelControl131);
            this.grpSeates.Controls.Add(this.pictureEdit109);
            this.grpSeates.Controls.Add(this.labelControl132);
            this.grpSeates.Controls.Add(this.pictureEdit110);
            this.grpSeates.Controls.Add(this.labelControl133);
            this.grpSeates.Controls.Add(this.pictureEdit111);
            this.grpSeates.Controls.Add(this.labelControl134);
            this.grpSeates.Controls.Add(this.pictureEdit112);
            this.grpSeates.Controls.Add(this.labelControl135);
            this.grpSeates.Controls.Add(this.pictureEdit113);
            this.grpSeates.Controls.Add(this.labelControl136);
            this.grpSeates.Controls.Add(this.pictureEdit114);
            this.grpSeates.Controls.Add(this.labelControl137);
            this.grpSeates.Controls.Add(this.pictureEdit115);
            this.grpSeates.Controls.Add(this.labelControl138);
            this.grpSeates.Controls.Add(this.pictureEdit116);
            this.grpSeates.Controls.Add(this.labelControl139);
            this.grpSeates.Controls.Add(this.pictureEdit117);
            this.grpSeates.Controls.Add(this.labelControl140);
            this.grpSeates.Controls.Add(this.pictureEdit118);
            this.grpSeates.Controls.Add(this.labelControl141);
            this.grpSeates.Controls.Add(this.pictureEdit119);
            this.grpSeates.Controls.Add(this.labelControl142);
            this.grpSeates.Controls.Add(this.pictureEdit120);
            this.grpSeates.Controls.Add(this.labelControl143);
            this.grpSeates.Controls.Add(this.pictureEdit121);
            this.grpSeates.Controls.Add(this.labelControl144);
            this.grpSeates.Controls.Add(this.pictureEdit122);
            this.grpSeates.Controls.Add(this.labelControl81);
            this.grpSeates.Controls.Add(this.pictureEdit59);
            this.grpSeates.Controls.Add(this.labelControl82);
            this.grpSeates.Controls.Add(this.pictureEdit60);
            this.grpSeates.Controls.Add(this.labelControl83);
            this.grpSeates.Controls.Add(this.pictureEdit61);
            this.grpSeates.Controls.Add(this.labelControl84);
            this.grpSeates.Controls.Add(this.pictureEdit62);
            this.grpSeates.Controls.Add(this.labelControl85);
            this.grpSeates.Controls.Add(this.pictureEdit63);
            this.grpSeates.Controls.Add(this.labelControl86);
            this.grpSeates.Controls.Add(this.pictureEdit64);
            this.grpSeates.Controls.Add(this.labelControl87);
            this.grpSeates.Controls.Add(this.pictureEdit65);
            this.grpSeates.Controls.Add(this.labelControl88);
            this.grpSeates.Controls.Add(this.pictureEdit66);
            this.grpSeates.Controls.Add(this.labelControl89);
            this.grpSeates.Controls.Add(this.pictureEdit67);
            this.grpSeates.Controls.Add(this.labelControl90);
            this.grpSeates.Controls.Add(this.pictureEdit68);
            this.grpSeates.Controls.Add(this.labelControl91);
            this.grpSeates.Controls.Add(this.pictureEdit69);
            this.grpSeates.Controls.Add(this.labelControl92);
            this.grpSeates.Controls.Add(this.pictureEdit70);
            this.grpSeates.Controls.Add(this.labelControl93);
            this.grpSeates.Controls.Add(this.pictureEdit71);
            this.grpSeates.Controls.Add(this.labelControl94);
            this.grpSeates.Controls.Add(this.pictureEdit72);
            this.grpSeates.Controls.Add(this.labelControl95);
            this.grpSeates.Controls.Add(this.pictureEdit73);
            this.grpSeates.Controls.Add(this.labelControl96);
            this.grpSeates.Controls.Add(this.pictureEdit74);
            this.grpSeates.Controls.Add(this.labelControl65);
            this.grpSeates.Controls.Add(this.pictureEdit43);
            this.grpSeates.Controls.Add(this.labelControl66);
            this.grpSeates.Controls.Add(this.pictureEdit44);
            this.grpSeates.Controls.Add(this.labelControl67);
            this.grpSeates.Controls.Add(this.pictureEdit45);
            this.grpSeates.Controls.Add(this.labelControl68);
            this.grpSeates.Controls.Add(this.pictureEdit46);
            this.grpSeates.Controls.Add(this.labelControl69);
            this.grpSeates.Controls.Add(this.pictureEdit47);
            this.grpSeates.Controls.Add(this.labelControl70);
            this.grpSeates.Controls.Add(this.pictureEdit48);
            this.grpSeates.Controls.Add(this.labelControl71);
            this.grpSeates.Controls.Add(this.pictureEdit49);
            this.grpSeates.Controls.Add(this.labelControl72);
            this.grpSeates.Controls.Add(this.pictureEdit50);
            this.grpSeates.Controls.Add(this.labelControl73);
            this.grpSeates.Controls.Add(this.pictureEdit51);
            this.grpSeates.Controls.Add(this.labelControl74);
            this.grpSeates.Controls.Add(this.pictureEdit52);
            this.grpSeates.Controls.Add(this.labelControl75);
            this.grpSeates.Controls.Add(this.pictureEdit53);
            this.grpSeates.Controls.Add(this.labelControl76);
            this.grpSeates.Controls.Add(this.pictureEdit54);
            this.grpSeates.Controls.Add(this.labelControl77);
            this.grpSeates.Controls.Add(this.pictureEdit55);
            this.grpSeates.Controls.Add(this.labelControl78);
            this.grpSeates.Controls.Add(this.pictureEdit56);
            this.grpSeates.Controls.Add(this.labelControl79);
            this.grpSeates.Controls.Add(this.pictureEdit57);
            this.grpSeates.Controls.Add(this.labelControl80);
            this.grpSeates.Controls.Add(this.pictureEdit58);
            this.grpSeates.Controls.Add(this.labelControl49);
            this.grpSeates.Controls.Add(this.labelControl41);
            this.grpSeates.Controls.Add(this.pictureEdit27);
            this.grpSeates.Controls.Add(this.pictureEdit19);
            this.grpSeates.Controls.Add(this.labelControl50);
            this.grpSeates.Controls.Add(this.labelControl42);
            this.grpSeates.Controls.Add(this.pictureEdit28);
            this.grpSeates.Controls.Add(this.pictureEdit20);
            this.grpSeates.Controls.Add(this.labelControl51);
            this.grpSeates.Controls.Add(this.labelControl43);
            this.grpSeates.Controls.Add(this.pictureEdit29);
            this.grpSeates.Controls.Add(this.pictureEdit21);
            this.grpSeates.Controls.Add(this.labelControl52);
            this.grpSeates.Controls.Add(this.labelControl44);
            this.grpSeates.Controls.Add(this.pictureEdit30);
            this.grpSeates.Controls.Add(this.pictureEdit22);
            this.grpSeates.Controls.Add(this.labelControl53);
            this.grpSeates.Controls.Add(this.labelControl45);
            this.grpSeates.Controls.Add(this.pictureEdit31);
            this.grpSeates.Controls.Add(this.pictureEdit23);
            this.grpSeates.Controls.Add(this.labelControl54);
            this.grpSeates.Controls.Add(this.labelControl46);
            this.grpSeates.Controls.Add(this.pictureEdit32);
            this.grpSeates.Controls.Add(this.pictureEdit24);
            this.grpSeates.Controls.Add(this.labelControl55);
            this.grpSeates.Controls.Add(this.labelControl47);
            this.grpSeates.Controls.Add(this.pictureEdit33);
            this.grpSeates.Controls.Add(this.pictureEdit25);
            this.grpSeates.Controls.Add(this.labelControl56);
            this.grpSeates.Controls.Add(this.labelControl48);
            this.grpSeates.Controls.Add(this.pictureEdit34);
            this.grpSeates.Controls.Add(this.pictureEdit26);
            this.grpSeates.Controls.Add(this.labelControl57);
            this.grpSeates.Controls.Add(this.labelControl8);
            this.grpSeates.Controls.Add(this.pictureEdit35);
            this.grpSeates.Controls.Add(this.pictureEdit1);
            this.grpSeates.Controls.Add(this.labelControl58);
            this.grpSeates.Controls.Add(this.labelControl9);
            this.grpSeates.Controls.Add(this.pictureEdit36);
            this.grpSeates.Controls.Add(this.pictureEdit2);
            this.grpSeates.Controls.Add(this.labelControl59);
            this.grpSeates.Controls.Add(this.labelControl10);
            this.grpSeates.Controls.Add(this.pictureEdit37);
            this.grpSeates.Controls.Add(this.pictureEdit3);
            this.grpSeates.Controls.Add(this.labelControl60);
            this.grpSeates.Controls.Add(this.labelControl11);
            this.grpSeates.Controls.Add(this.pictureEdit38);
            this.grpSeates.Controls.Add(this.pictureEdit4);
            this.grpSeates.Controls.Add(this.labelControl61);
            this.grpSeates.Controls.Add(this.labelControl12);
            this.grpSeates.Controls.Add(this.pictureEdit39);
            this.grpSeates.Controls.Add(this.pictureEdit5);
            this.grpSeates.Controls.Add(this.labelControl62);
            this.grpSeates.Controls.Add(this.labelControl13);
            this.grpSeates.Controls.Add(this.pictureEdit40);
            this.grpSeates.Controls.Add(this.pictureEdit6);
            this.grpSeates.Controls.Add(this.labelControl63);
            this.grpSeates.Controls.Add(this.labelControl14);
            this.grpSeates.Controls.Add(this.pictureEdit41);
            this.grpSeates.Controls.Add(this.Mic_2);
            this.grpSeates.Controls.Add(this.labelControl64);
            this.grpSeates.Controls.Add(this.labelControl15);
            this.grpSeates.Controls.Add(this.pictureEdit42);
            this.grpSeates.Controls.Add(this.Mic_1);
            this.grpSeates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpSeates.Location = new System.Drawing.Point(0, 25);
            this.grpSeates.Name = "grpSeates";
            this.grpSeates.Size = new System.Drawing.Size(949, 603);
            this.grpSeates.TabIndex = 1;
            this.grpSeates.Text = "�����������";
            this.grpSeates.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            this.grpSeates.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grpSeates_MouseMove);
            // 
            // info_sel
            // 
            this.info_sel.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_sel.Location = new System.Drawing.Point(238, 35);
            this.info_sel.Name = "info_sel";
            this.info_sel.Size = new System.Drawing.Size(20, 16);
            this.info_sel.TabIndex = 558;
            this.info_sel.Text = "info";
            // 
            // info_9
            // 
            this.info_9.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_9.Location = new System.Drawing.Point(192, 35);
            this.info_9.Name = "info_9";
            this.info_9.Size = new System.Drawing.Size(7, 16);
            this.info_9.TabIndex = 557;
            this.info_9.Text = "9";
            // 
            // info_8
            // 
            this.info_8.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_8.Location = new System.Drawing.Point(176, 35);
            this.info_8.Name = "info_8";
            this.info_8.Size = new System.Drawing.Size(7, 16);
            this.info_8.TabIndex = 556;
            this.info_8.Text = "8";
            // 
            // info_7
            // 
            this.info_7.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_7.Location = new System.Drawing.Point(154, 35);
            this.info_7.Name = "info_7";
            this.info_7.Size = new System.Drawing.Size(7, 16);
            this.info_7.TabIndex = 555;
            this.info_7.Text = "7";
            // 
            // info_6
            // 
            this.info_6.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_6.Location = new System.Drawing.Point(128, 35);
            this.info_6.Name = "info_6";
            this.info_6.Size = new System.Drawing.Size(7, 16);
            this.info_6.TabIndex = 554;
            this.info_6.Text = "6";
            // 
            // info_5
            // 
            this.info_5.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_5.Location = new System.Drawing.Point(106, 35);
            this.info_5.Name = "info_5";
            this.info_5.Size = new System.Drawing.Size(7, 16);
            this.info_5.TabIndex = 553;
            this.info_5.Text = "5";
            // 
            // info_4
            // 
            this.info_4.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_4.Location = new System.Drawing.Point(82, 35);
            this.info_4.Name = "info_4";
            this.info_4.Size = new System.Drawing.Size(7, 16);
            this.info_4.TabIndex = 552;
            this.info_4.Text = "4";
            // 
            // info_3
            // 
            this.info_3.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_3.Location = new System.Drawing.Point(60, 35);
            this.info_3.Name = "info_3";
            this.info_3.Size = new System.Drawing.Size(7, 16);
            this.info_3.TabIndex = 551;
            this.info_3.Text = "3";
            // 
            // info_2
            // 
            this.info_2.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_2.Location = new System.Drawing.Point(34, 35);
            this.info_2.Name = "info_2";
            this.info_2.Size = new System.Drawing.Size(7, 16);
            this.info_2.TabIndex = 550;
            this.info_2.Text = "2";
            // 
            // info_1
            // 
            this.info_1.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info_1.Location = new System.Drawing.Point(12, 35);
            this.info_1.Name = "info_1";
            this.info_1.Size = new System.Drawing.Size(7, 16);
            this.info_1.TabIndex = 549;
            this.info_1.Text = "1";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl29.Location = new System.Drawing.Point(747, 570);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(18, 13);
            this.labelControl29.TabIndex = 548;
            this.labelControl29.Text = "110";
            this.labelControl29.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl29.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl29.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit12
            // 
            this.pictureEdit12.AllowDrop = true;
            this.pictureEdit12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit12.Location = new System.Drawing.Point(736, 536);
            this.pictureEdit12.Name = "pictureEdit12";
            this.pictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit12.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit12.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit12.Properties.UseParentBackground = true;
            this.pictureEdit12.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit12.TabIndex = 547;
            this.pictureEdit12.Tag = "Mic110";
            this.pictureEdit12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit12.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit12.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl30.Location = new System.Drawing.Point(708, 570);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(18, 13);
            this.labelControl30.TabIndex = 546;
            this.labelControl30.Text = "111";
            this.labelControl30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl30.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl30.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit13
            // 
            this.pictureEdit13.AllowDrop = true;
            this.pictureEdit13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit13.Location = new System.Drawing.Point(697, 536);
            this.pictureEdit13.Name = "pictureEdit13";
            this.pictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit13.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit13.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit13.Properties.UseParentBackground = true;
            this.pictureEdit13.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit13.TabIndex = 545;
            this.pictureEdit13.Tag = "Mic111";
            this.pictureEdit13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit13.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit13.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl31.Location = new System.Drawing.Point(668, 570);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(18, 13);
            this.labelControl31.TabIndex = 544;
            this.labelControl31.Text = "112";
            this.labelControl31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl31.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl31.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit14
            // 
            this.pictureEdit14.AllowDrop = true;
            this.pictureEdit14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit14.Location = new System.Drawing.Point(658, 536);
            this.pictureEdit14.Name = "pictureEdit14";
            this.pictureEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit14.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit14.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit14.Properties.UseParentBackground = true;
            this.pictureEdit14.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit14.TabIndex = 543;
            this.pictureEdit14.Tag = "Mic112";
            this.pictureEdit14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit14.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit14.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl32.Location = new System.Drawing.Point(630, 569);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(18, 13);
            this.labelControl32.TabIndex = 542;
            this.labelControl32.Text = "113";
            this.labelControl32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl32.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl32.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit15
            // 
            this.pictureEdit15.AllowDrop = true;
            this.pictureEdit15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit15.Location = new System.Drawing.Point(619, 536);
            this.pictureEdit15.Name = "pictureEdit15";
            this.pictureEdit15.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit15.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit15.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit15.Properties.UseParentBackground = true;
            this.pictureEdit15.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit15.TabIndex = 541;
            this.pictureEdit15.Tag = "Mic113";
            this.pictureEdit15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit15.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit15.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Location = new System.Drawing.Point(565, 570);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(18, 13);
            this.labelControl2.TabIndex = 540;
            this.labelControl2.Text = "114";
            this.labelControl2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.AllowDrop = true;
            this.pictureEdit8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit8.Location = new System.Drawing.Point(554, 536);
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit8.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit8.Properties.UseParentBackground = true;
            this.pictureEdit8.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit8.TabIndex = 539;
            this.pictureEdit8.Tag = "Mic114";
            this.pictureEdit8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit8.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit8.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl26.Location = new System.Drawing.Point(526, 570);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(18, 13);
            this.labelControl26.TabIndex = 538;
            this.labelControl26.Text = "115";
            this.labelControl26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl26.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl26.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.AllowDrop = true;
            this.pictureEdit9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit9.Location = new System.Drawing.Point(515, 536);
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit9.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit9.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit9.Properties.UseParentBackground = true;
            this.pictureEdit9.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit9.TabIndex = 537;
            this.pictureEdit9.Tag = "Mic115";
            this.pictureEdit9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit9.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit9.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl27.Location = new System.Drawing.Point(486, 570);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(18, 13);
            this.labelControl27.TabIndex = 536;
            this.labelControl27.Text = "116";
            this.labelControl27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl27.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl27.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.AllowDrop = true;
            this.pictureEdit10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit10.Location = new System.Drawing.Point(476, 536);
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit10.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit10.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit10.Properties.UseParentBackground = true;
            this.pictureEdit10.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit10.TabIndex = 535;
            this.pictureEdit10.Tag = "Mic116";
            this.pictureEdit10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit10.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit10.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl28.Location = new System.Drawing.Point(448, 569);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(18, 13);
            this.labelControl28.TabIndex = 534;
            this.labelControl28.Text = "117";
            this.labelControl28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl28.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl28.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit11
            // 
            this.pictureEdit11.AllowDrop = true;
            this.pictureEdit11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit11.Location = new System.Drawing.Point(437, 536);
            this.pictureEdit11.Name = "pictureEdit11";
            this.pictureEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit11.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit11.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit11.Properties.UseParentBackground = true;
            this.pictureEdit11.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit11.TabIndex = 533;
            this.pictureEdit11.Tag = "Mic117";
            this.pictureEdit11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit11.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit11.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Location = new System.Drawing.Point(388, 570);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(18, 13);
            this.labelControl3.TabIndex = 532;
            this.labelControl3.Text = "118";
            this.labelControl3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl3.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.AllowDrop = true;
            this.pictureEdit7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit7.Location = new System.Drawing.Point(377, 536);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit7.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit7.Properties.UseParentBackground = true;
            this.pictureEdit7.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit7.TabIndex = 531;
            this.pictureEdit7.Tag = "Mic118";
            this.pictureEdit7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit7.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit7.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(22, 551);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 530;
            this.label3.Text = "��� 8";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(22, 495);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 529;
            this.label2.Text = "��� 7";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(22, 439);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(43, 16);
            this.label92.TabIndex = 528;
            this.label92.Text = "��� 6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(22, 383);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 16);
            this.label15.TabIndex = 527;
            this.label15.Text = "��� 5";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(22, 327);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 16);
            this.label24.TabIndex = 526;
            this.label24.Text = "��� 4";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(22, 271);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 525;
            this.label27.Text = "��� 3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(21, 215);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 16);
            this.label36.TabIndex = 524;
            this.label36.Text = "��� 2";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(22, 159);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 523;
            this.label37.Text = "��� 1";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(9, 60);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(81, 16);
            this.label38.TabIndex = 522;
            this.label38.Text = "���������";
            // 
            // labelControl97
            // 
            this.labelControl97.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl97.Location = new System.Drawing.Point(349, 570);
            this.labelControl97.Name = "labelControl97";
            this.labelControl97.Size = new System.Drawing.Size(18, 13);
            this.labelControl97.TabIndex = 521;
            this.labelControl97.Text = "119";
            this.labelControl97.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl97.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl97.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit75
            // 
            this.pictureEdit75.AllowDrop = true;
            this.pictureEdit75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit75.Location = new System.Drawing.Point(338, 536);
            this.pictureEdit75.Name = "pictureEdit75";
            this.pictureEdit75.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit75.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit75.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit75.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit75.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit75.Properties.UseParentBackground = true;
            this.pictureEdit75.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit75.TabIndex = 520;
            this.pictureEdit75.Tag = "Mic119";
            this.pictureEdit75.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit75.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit75.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl98
            // 
            this.labelControl98.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl98.Location = new System.Drawing.Point(309, 570);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(18, 13);
            this.labelControl98.TabIndex = 519;
            this.labelControl98.Text = "120";
            this.labelControl98.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl98.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl98.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit76
            // 
            this.pictureEdit76.AllowDrop = true;
            this.pictureEdit76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit76.Location = new System.Drawing.Point(299, 536);
            this.pictureEdit76.Name = "pictureEdit76";
            this.pictureEdit76.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit76.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit76.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit76.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit76.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit76.Properties.UseParentBackground = true;
            this.pictureEdit76.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit76.TabIndex = 518;
            this.pictureEdit76.Tag = "Mic120";
            this.pictureEdit76.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit76.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit76.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl99
            // 
            this.labelControl99.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl99.Location = new System.Drawing.Point(271, 569);
            this.labelControl99.Name = "labelControl99";
            this.labelControl99.Size = new System.Drawing.Size(18, 13);
            this.labelControl99.TabIndex = 517;
            this.labelControl99.Text = "121";
            this.labelControl99.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl99.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl99.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit77
            // 
            this.pictureEdit77.AllowDrop = true;
            this.pictureEdit77.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit77.Location = new System.Drawing.Point(260, 536);
            this.pictureEdit77.Name = "pictureEdit77";
            this.pictureEdit77.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit77.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit77.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit77.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit77.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit77.Properties.UseParentBackground = true;
            this.pictureEdit77.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit77.TabIndex = 516;
            this.pictureEdit77.Tag = "Mic121";
            this.pictureEdit77.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit77.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit77.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl100
            // 
            this.labelControl100.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl100.Location = new System.Drawing.Point(749, 514);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(18, 13);
            this.labelControl100.TabIndex = 515;
            this.labelControl100.Text = "109";
            this.labelControl100.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl100.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl100.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit78
            // 
            this.pictureEdit78.AllowDrop = true;
            this.pictureEdit78.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit78.Location = new System.Drawing.Point(738, 480);
            this.pictureEdit78.Name = "pictureEdit78";
            this.pictureEdit78.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit78.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit78.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit78.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit78.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit78.Properties.UseParentBackground = true;
            this.pictureEdit78.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit78.TabIndex = 514;
            this.pictureEdit78.Tag = "Mic109";
            this.pictureEdit78.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit78.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit78.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl101
            // 
            this.labelControl101.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl101.Location = new System.Drawing.Point(708, 513);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(18, 13);
            this.labelControl101.TabIndex = 513;
            this.labelControl101.Text = "108";
            this.labelControl101.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl101.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl101.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit79
            // 
            this.pictureEdit79.AllowDrop = true;
            this.pictureEdit79.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit79.Location = new System.Drawing.Point(697, 480);
            this.pictureEdit79.Name = "pictureEdit79";
            this.pictureEdit79.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit79.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit79.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit79.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit79.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit79.Properties.UseParentBackground = true;
            this.pictureEdit79.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit79.TabIndex = 512;
            this.pictureEdit79.Tag = "Mic108";
            this.pictureEdit79.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit79.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit79.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl102
            // 
            this.labelControl102.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl102.Location = new System.Drawing.Point(668, 514);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(18, 13);
            this.labelControl102.TabIndex = 511;
            this.labelControl102.Text = "107";
            this.labelControl102.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl102.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl102.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit80
            // 
            this.pictureEdit80.AllowDrop = true;
            this.pictureEdit80.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit80.Location = new System.Drawing.Point(658, 480);
            this.pictureEdit80.Name = "pictureEdit80";
            this.pictureEdit80.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit80.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit80.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit80.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit80.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit80.Properties.UseParentBackground = true;
            this.pictureEdit80.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit80.TabIndex = 510;
            this.pictureEdit80.Tag = "Mic107";
            this.pictureEdit80.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit80.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit80.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl103
            // 
            this.labelControl103.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl103.Location = new System.Drawing.Point(630, 514);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(18, 13);
            this.labelControl103.TabIndex = 509;
            this.labelControl103.Text = "106";
            this.labelControl103.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl103.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl103.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit81
            // 
            this.pictureEdit81.AllowDrop = true;
            this.pictureEdit81.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit81.Location = new System.Drawing.Point(619, 480);
            this.pictureEdit81.Name = "pictureEdit81";
            this.pictureEdit81.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit81.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit81.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit81.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit81.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit81.Properties.UseParentBackground = true;
            this.pictureEdit81.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit81.TabIndex = 508;
            this.pictureEdit81.Tag = "Mic106";
            this.pictureEdit81.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit81.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit81.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl104
            // 
            this.labelControl104.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl104.Location = new System.Drawing.Point(565, 513);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(18, 13);
            this.labelControl104.TabIndex = 507;
            this.labelControl104.Text = "105";
            this.labelControl104.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl104.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl104.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit82
            // 
            this.pictureEdit82.AllowDrop = true;
            this.pictureEdit82.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit82.Location = new System.Drawing.Point(554, 480);
            this.pictureEdit82.Name = "pictureEdit82";
            this.pictureEdit82.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit82.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit82.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit82.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit82.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit82.Properties.UseParentBackground = true;
            this.pictureEdit82.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit82.TabIndex = 506;
            this.pictureEdit82.Tag = "Mic105";
            this.pictureEdit82.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit82.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit82.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl105
            // 
            this.labelControl105.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl105.Location = new System.Drawing.Point(526, 514);
            this.labelControl105.Name = "labelControl105";
            this.labelControl105.Size = new System.Drawing.Size(18, 13);
            this.labelControl105.TabIndex = 505;
            this.labelControl105.Text = "104";
            this.labelControl105.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl105.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl105.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit83
            // 
            this.pictureEdit83.AllowDrop = true;
            this.pictureEdit83.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit83.Location = new System.Drawing.Point(515, 480);
            this.pictureEdit83.Name = "pictureEdit83";
            this.pictureEdit83.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit83.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit83.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit83.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit83.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit83.Properties.UseParentBackground = true;
            this.pictureEdit83.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit83.TabIndex = 504;
            this.pictureEdit83.Tag = "Mic104";
            this.pictureEdit83.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit83.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit83.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl106
            // 
            this.labelControl106.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl106.Location = new System.Drawing.Point(486, 514);
            this.labelControl106.Name = "labelControl106";
            this.labelControl106.Size = new System.Drawing.Size(18, 13);
            this.labelControl106.TabIndex = 503;
            this.labelControl106.Text = "103";
            this.labelControl106.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl106.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl106.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit84
            // 
            this.pictureEdit84.AllowDrop = true;
            this.pictureEdit84.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit84.Location = new System.Drawing.Point(476, 480);
            this.pictureEdit84.Name = "pictureEdit84";
            this.pictureEdit84.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit84.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit84.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit84.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit84.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit84.Properties.UseParentBackground = true;
            this.pictureEdit84.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit84.TabIndex = 502;
            this.pictureEdit84.Tag = "Mic103";
            this.pictureEdit84.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit84.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit84.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl107
            // 
            this.labelControl107.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl107.Location = new System.Drawing.Point(448, 513);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(18, 13);
            this.labelControl107.TabIndex = 501;
            this.labelControl107.Text = "102";
            this.labelControl107.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl107.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl107.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit85
            // 
            this.pictureEdit85.AllowDrop = true;
            this.pictureEdit85.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit85.Location = new System.Drawing.Point(437, 480);
            this.pictureEdit85.Name = "pictureEdit85";
            this.pictureEdit85.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit85.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit85.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit85.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit85.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit85.Properties.UseParentBackground = true;
            this.pictureEdit85.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit85.TabIndex = 500;
            this.pictureEdit85.Tag = "Mic102";
            this.pictureEdit85.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit85.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit85.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl108
            // 
            this.labelControl108.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl108.Location = new System.Drawing.Point(388, 514);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(18, 13);
            this.labelControl108.TabIndex = 499;
            this.labelControl108.Text = "101";
            this.labelControl108.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl108.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl108.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit86
            // 
            this.pictureEdit86.AllowDrop = true;
            this.pictureEdit86.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit86.Location = new System.Drawing.Point(377, 480);
            this.pictureEdit86.Name = "pictureEdit86";
            this.pictureEdit86.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit86.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit86.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit86.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit86.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit86.Properties.UseParentBackground = true;
            this.pictureEdit86.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit86.TabIndex = 498;
            this.pictureEdit86.Tag = "Mic101";
            this.pictureEdit86.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit86.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit86.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl109
            // 
            this.labelControl109.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl109.Location = new System.Drawing.Point(349, 513);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(18, 13);
            this.labelControl109.TabIndex = 497;
            this.labelControl109.Text = "100";
            this.labelControl109.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl109.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl109.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit87
            // 
            this.pictureEdit87.AllowDrop = true;
            this.pictureEdit87.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit87.Location = new System.Drawing.Point(338, 480);
            this.pictureEdit87.Name = "pictureEdit87";
            this.pictureEdit87.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit87.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit87.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit87.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit87.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit87.Properties.UseParentBackground = true;
            this.pictureEdit87.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit87.TabIndex = 496;
            this.pictureEdit87.Tag = "Mic100";
            this.pictureEdit87.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit87.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit87.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl110.Location = new System.Drawing.Point(270, 458);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(12, 13);
            this.labelControl110.TabIndex = 495;
            this.labelControl110.Text = "97";
            this.labelControl110.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl110.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl110.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit88
            // 
            this.pictureEdit88.AllowDrop = true;
            this.pictureEdit88.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit88.Location = new System.Drawing.Point(260, 424);
            this.pictureEdit88.Name = "pictureEdit88";
            this.pictureEdit88.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit88.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit88.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit88.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit88.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit88.Properties.UseParentBackground = true;
            this.pictureEdit88.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit88.TabIndex = 494;
            this.pictureEdit88.Tag = "Mic097";
            this.pictureEdit88.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit88.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit88.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl111
            // 
            this.labelControl111.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl111.Location = new System.Drawing.Point(310, 458);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(12, 13);
            this.labelControl111.TabIndex = 493;
            this.labelControl111.Text = "96";
            this.labelControl111.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl111.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl111.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit89
            // 
            this.pictureEdit89.AllowDrop = true;
            this.pictureEdit89.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit89.Location = new System.Drawing.Point(299, 424);
            this.pictureEdit89.Name = "pictureEdit89";
            this.pictureEdit89.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit89.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit89.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit89.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit89.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit89.Properties.UseParentBackground = true;
            this.pictureEdit89.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit89.TabIndex = 492;
            this.pictureEdit89.Tag = "Mic096";
            this.pictureEdit89.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit89.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit89.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl112.Location = new System.Drawing.Point(349, 457);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(12, 13);
            this.labelControl112.TabIndex = 491;
            this.labelControl112.Text = "95";
            this.labelControl112.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl112.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl112.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit90
            // 
            this.pictureEdit90.AllowDrop = true;
            this.pictureEdit90.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit90.Location = new System.Drawing.Point(338, 424);
            this.pictureEdit90.Name = "pictureEdit90";
            this.pictureEdit90.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit90.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit90.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit90.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit90.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit90.Properties.UseParentBackground = true;
            this.pictureEdit90.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit90.TabIndex = 490;
            this.pictureEdit90.Tag = "Mic095";
            this.pictureEdit90.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit90.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit90.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl113.Location = new System.Drawing.Point(388, 458);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(12, 13);
            this.labelControl113.TabIndex = 489;
            this.labelControl113.Text = "94";
            this.labelControl113.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl113.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl113.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit91
            // 
            this.pictureEdit91.AllowDrop = true;
            this.pictureEdit91.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit91.Location = new System.Drawing.Point(377, 424);
            this.pictureEdit91.Name = "pictureEdit91";
            this.pictureEdit91.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit91.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit91.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit91.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit91.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit91.Properties.UseParentBackground = true;
            this.pictureEdit91.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit91.TabIndex = 488;
            this.pictureEdit91.Tag = "Mic094";
            this.pictureEdit91.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit91.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit91.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl114
            // 
            this.labelControl114.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl114.Location = new System.Drawing.Point(878, 402);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(12, 13);
            this.labelControl114.TabIndex = 487;
            this.labelControl114.Text = "85";
            this.labelControl114.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl114.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl114.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit92
            // 
            this.pictureEdit92.AllowDrop = true;
            this.pictureEdit92.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit92.Location = new System.Drawing.Point(868, 368);
            this.pictureEdit92.Name = "pictureEdit92";
            this.pictureEdit92.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit92.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit92.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit92.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit92.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit92.Properties.UseParentBackground = true;
            this.pictureEdit92.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit92.TabIndex = 486;
            this.pictureEdit92.Tag = "Mic085";
            this.pictureEdit92.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit92.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit92.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl115
            // 
            this.labelControl115.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl115.Location = new System.Drawing.Point(217, 401);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(12, 13);
            this.labelControl115.TabIndex = 485;
            this.labelControl115.Text = "70";
            this.labelControl115.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl115.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl115.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit93
            // 
            this.pictureEdit93.AllowDrop = true;
            this.pictureEdit93.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit93.Location = new System.Drawing.Point(206, 368);
            this.pictureEdit93.Name = "pictureEdit93";
            this.pictureEdit93.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit93.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit93.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit93.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit93.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit93.Properties.UseParentBackground = true;
            this.pictureEdit93.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit93.TabIndex = 484;
            this.pictureEdit93.Tag = "Mic070";
            this.pictureEdit93.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit93.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit93.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl116
            // 
            this.labelControl116.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl116.Location = new System.Drawing.Point(178, 402);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(12, 13);
            this.labelControl116.TabIndex = 483;
            this.labelControl116.Text = "69";
            this.labelControl116.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl116.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl116.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit94
            // 
            this.pictureEdit94.AllowDrop = true;
            this.pictureEdit94.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit94.Location = new System.Drawing.Point(167, 368);
            this.pictureEdit94.Name = "pictureEdit94";
            this.pictureEdit94.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit94.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit94.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit94.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit94.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit94.Properties.UseParentBackground = true;
            this.pictureEdit94.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit94.TabIndex = 482;
            this.pictureEdit94.Tag = "Mic069";
            this.pictureEdit94.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit94.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit94.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl117
            // 
            this.labelControl117.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl117.Location = new System.Drawing.Point(139, 401);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(12, 13);
            this.labelControl117.TabIndex = 481;
            this.labelControl117.Text = "68";
            this.labelControl117.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl117.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl117.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit95
            // 
            this.pictureEdit95.AllowDrop = true;
            this.pictureEdit95.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit95.Location = new System.Drawing.Point(128, 368);
            this.pictureEdit95.Name = "pictureEdit95";
            this.pictureEdit95.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit95.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit95.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit95.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit95.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit95.Properties.UseParentBackground = true;
            this.pictureEdit95.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit95.TabIndex = 480;
            this.pictureEdit95.Tag = "Mic068";
            this.pictureEdit95.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit95.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit95.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl118
            // 
            this.labelControl118.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl118.Location = new System.Drawing.Point(447, 458);
            this.labelControl118.Name = "labelControl118";
            this.labelControl118.Size = new System.Drawing.Size(12, 13);
            this.labelControl118.TabIndex = 479;
            this.labelControl118.Text = "93";
            this.labelControl118.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl118.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl118.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit96
            // 
            this.pictureEdit96.AllowDrop = true;
            this.pictureEdit96.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit96.Location = new System.Drawing.Point(437, 424);
            this.pictureEdit96.Name = "pictureEdit96";
            this.pictureEdit96.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit96.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit96.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit96.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit96.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit96.Properties.UseParentBackground = true;
            this.pictureEdit96.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit96.TabIndex = 478;
            this.pictureEdit96.Tag = "Mic093";
            this.pictureEdit96.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit96.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit96.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl119
            // 
            this.labelControl119.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl119.Location = new System.Drawing.Point(487, 458);
            this.labelControl119.Name = "labelControl119";
            this.labelControl119.Size = new System.Drawing.Size(12, 13);
            this.labelControl119.TabIndex = 477;
            this.labelControl119.Text = "92";
            this.labelControl119.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl119.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl119.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit97
            // 
            this.pictureEdit97.AllowDrop = true;
            this.pictureEdit97.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit97.Location = new System.Drawing.Point(476, 424);
            this.pictureEdit97.Name = "pictureEdit97";
            this.pictureEdit97.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit97.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit97.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit97.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit97.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit97.Properties.UseParentBackground = true;
            this.pictureEdit97.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit97.TabIndex = 476;
            this.pictureEdit97.Tag = "Mic092";
            this.pictureEdit97.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit97.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit97.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl120
            // 
            this.labelControl120.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl120.Location = new System.Drawing.Point(526, 457);
            this.labelControl120.Name = "labelControl120";
            this.labelControl120.Size = new System.Drawing.Size(12, 13);
            this.labelControl120.TabIndex = 475;
            this.labelControl120.Text = "91";
            this.labelControl120.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl120.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl120.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit98
            // 
            this.pictureEdit98.AllowDrop = true;
            this.pictureEdit98.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit98.Location = new System.Drawing.Point(515, 424);
            this.pictureEdit98.Name = "pictureEdit98";
            this.pictureEdit98.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit98.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit98.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit98.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit98.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit98.Properties.UseParentBackground = true;
            this.pictureEdit98.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit98.TabIndex = 474;
            this.pictureEdit98.Tag = "Mic091";
            this.pictureEdit98.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit98.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit98.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl121
            // 
            this.labelControl121.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl121.Location = new System.Drawing.Point(565, 458);
            this.labelControl121.Name = "labelControl121";
            this.labelControl121.Size = new System.Drawing.Size(12, 13);
            this.labelControl121.TabIndex = 473;
            this.labelControl121.Text = "90";
            this.labelControl121.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl121.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl121.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit99
            // 
            this.pictureEdit99.AllowDrop = true;
            this.pictureEdit99.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit99.Location = new System.Drawing.Point(554, 424);
            this.pictureEdit99.Name = "pictureEdit99";
            this.pictureEdit99.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit99.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit99.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit99.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit99.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit99.Properties.UseParentBackground = true;
            this.pictureEdit99.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit99.TabIndex = 472;
            this.pictureEdit99.Tag = "Mic090";
            this.pictureEdit99.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit99.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit99.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl122
            // 
            this.labelControl122.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl122.Location = new System.Drawing.Point(629, 458);
            this.labelControl122.Name = "labelControl122";
            this.labelControl122.Size = new System.Drawing.Size(12, 13);
            this.labelControl122.TabIndex = 471;
            this.labelControl122.Text = "89";
            this.labelControl122.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl122.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl122.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit100
            // 
            this.pictureEdit100.AllowDrop = true;
            this.pictureEdit100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit100.Location = new System.Drawing.Point(619, 424);
            this.pictureEdit100.Name = "pictureEdit100";
            this.pictureEdit100.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit100.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit100.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit100.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit100.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit100.Properties.UseParentBackground = true;
            this.pictureEdit100.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit100.TabIndex = 470;
            this.pictureEdit100.Tag = "Mic089";
            this.pictureEdit100.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit100.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit100.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl123
            // 
            this.labelControl123.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl123.Location = new System.Drawing.Point(669, 457);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(12, 13);
            this.labelControl123.TabIndex = 469;
            this.labelControl123.Text = "88";
            this.labelControl123.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl123.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl123.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit101
            // 
            this.pictureEdit101.AllowDrop = true;
            this.pictureEdit101.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit101.Location = new System.Drawing.Point(658, 424);
            this.pictureEdit101.Name = "pictureEdit101";
            this.pictureEdit101.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit101.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit101.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit101.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit101.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit101.Properties.UseParentBackground = true;
            this.pictureEdit101.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit101.TabIndex = 468;
            this.pictureEdit101.Tag = "Mic088";
            this.pictureEdit101.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit101.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit101.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl124
            // 
            this.labelControl124.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl124.Location = new System.Drawing.Point(708, 458);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(12, 13);
            this.labelControl124.TabIndex = 467;
            this.labelControl124.Text = "87";
            this.labelControl124.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl124.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl124.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit102
            // 
            this.pictureEdit102.AllowDrop = true;
            this.pictureEdit102.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit102.Location = new System.Drawing.Point(697, 424);
            this.pictureEdit102.Name = "pictureEdit102";
            this.pictureEdit102.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit102.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit102.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit102.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit102.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit102.Properties.UseParentBackground = true;
            this.pictureEdit102.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit102.TabIndex = 466;
            this.pictureEdit102.Tag = "Mic087";
            this.pictureEdit102.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit102.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit102.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl125
            // 
            this.labelControl125.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl125.Location = new System.Drawing.Point(747, 457);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(12, 13);
            this.labelControl125.TabIndex = 465;
            this.labelControl125.Text = "86";
            this.labelControl125.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl125.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl125.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit103
            // 
            this.pictureEdit103.AllowDrop = true;
            this.pictureEdit103.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit103.Location = new System.Drawing.Point(736, 424);
            this.pictureEdit103.Name = "pictureEdit103";
            this.pictureEdit103.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit103.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit103.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit103.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit103.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit103.Properties.UseParentBackground = true;
            this.pictureEdit103.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit103.TabIndex = 464;
            this.pictureEdit103.Tag = "Mic086";
            this.pictureEdit103.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit103.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit103.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl126
            // 
            this.labelControl126.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl126.Location = new System.Drawing.Point(270, 514);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(12, 13);
            this.labelControl126.TabIndex = 463;
            this.labelControl126.Text = "98";
            this.labelControl126.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl126.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl126.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit104
            // 
            this.pictureEdit104.AllowDrop = true;
            this.pictureEdit104.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit104.Location = new System.Drawing.Point(260, 480);
            this.pictureEdit104.Name = "pictureEdit104";
            this.pictureEdit104.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit104.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit104.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit104.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit104.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit104.Properties.UseParentBackground = true;
            this.pictureEdit104.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit104.TabIndex = 462;
            this.pictureEdit104.Tag = "Mic098";
            this.pictureEdit104.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit104.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit104.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl127
            // 
            this.labelControl127.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl127.Location = new System.Drawing.Point(310, 514);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(12, 13);
            this.labelControl127.TabIndex = 461;
            this.labelControl127.Text = "99";
            this.labelControl127.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl127.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl127.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit105
            // 
            this.pictureEdit105.AllowDrop = true;
            this.pictureEdit105.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit105.Location = new System.Drawing.Point(299, 480);
            this.pictureEdit105.Name = "pictureEdit105";
            this.pictureEdit105.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit105.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit105.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit105.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit105.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit105.Properties.UseParentBackground = true;
            this.pictureEdit105.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit105.TabIndex = 460;
            this.pictureEdit105.Tag = "Mic099";
            this.pictureEdit105.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit105.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit105.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl128
            // 
            this.labelControl128.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl128.Location = new System.Drawing.Point(840, 401);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(12, 13);
            this.labelControl128.TabIndex = 459;
            this.labelControl128.Text = "84";
            this.labelControl128.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl128.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl128.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit106
            // 
            this.pictureEdit106.AllowDrop = true;
            this.pictureEdit106.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit106.Location = new System.Drawing.Point(831, 368);
            this.pictureEdit106.Name = "pictureEdit106";
            this.pictureEdit106.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit106.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit106.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit106.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit106.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit106.Properties.UseParentBackground = true;
            this.pictureEdit106.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit106.TabIndex = 458;
            this.pictureEdit106.Tag = "Mic084";
            this.pictureEdit106.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit106.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit106.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl129
            // 
            this.labelControl129.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl129.Location = new System.Drawing.Point(801, 402);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(12, 13);
            this.labelControl129.TabIndex = 457;
            this.labelControl129.Text = "83";
            this.labelControl129.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl129.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl129.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit107
            // 
            this.pictureEdit107.AllowDrop = true;
            this.pictureEdit107.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit107.Location = new System.Drawing.Point(792, 368);
            this.pictureEdit107.Name = "pictureEdit107";
            this.pictureEdit107.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit107.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit107.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit107.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit107.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit107.Properties.UseParentBackground = true;
            this.pictureEdit107.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit107.TabIndex = 456;
            this.pictureEdit107.Tag = "Mic083";
            this.pictureEdit107.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit107.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit107.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl130
            // 
            this.labelControl130.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl130.Location = new System.Drawing.Point(746, 402);
            this.labelControl130.Name = "labelControl130";
            this.labelControl130.Size = new System.Drawing.Size(12, 13);
            this.labelControl130.TabIndex = 455;
            this.labelControl130.Text = "82";
            this.labelControl130.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl130.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl130.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit108
            // 
            this.pictureEdit108.AllowDrop = true;
            this.pictureEdit108.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit108.Location = new System.Drawing.Point(738, 368);
            this.pictureEdit108.Name = "pictureEdit108";
            this.pictureEdit108.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit108.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit108.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit108.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit108.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit108.Properties.UseParentBackground = true;
            this.pictureEdit108.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit108.TabIndex = 454;
            this.pictureEdit108.Tag = "Mic082";
            this.pictureEdit108.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit108.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit108.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl131
            // 
            this.labelControl131.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl131.Location = new System.Drawing.Point(706, 401);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(12, 13);
            this.labelControl131.TabIndex = 453;
            this.labelControl131.Text = "81";
            this.labelControl131.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl131.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl131.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit109
            // 
            this.pictureEdit109.AllowDrop = true;
            this.pictureEdit109.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit109.Location = new System.Drawing.Point(697, 368);
            this.pictureEdit109.Name = "pictureEdit109";
            this.pictureEdit109.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit109.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit109.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit109.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit109.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit109.Properties.UseParentBackground = true;
            this.pictureEdit109.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit109.TabIndex = 452;
            this.pictureEdit109.Tag = "Mic081";
            this.pictureEdit109.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit109.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit109.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl132
            // 
            this.labelControl132.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl132.Location = new System.Drawing.Point(667, 402);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(12, 13);
            this.labelControl132.TabIndex = 451;
            this.labelControl132.Text = "80";
            this.labelControl132.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl132.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl132.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit110
            // 
            this.pictureEdit110.AllowDrop = true;
            this.pictureEdit110.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit110.Location = new System.Drawing.Point(658, 368);
            this.pictureEdit110.Name = "pictureEdit110";
            this.pictureEdit110.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit110.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit110.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit110.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit110.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit110.Properties.UseParentBackground = true;
            this.pictureEdit110.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit110.TabIndex = 450;
            this.pictureEdit110.Tag = "Mic080";
            this.pictureEdit110.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit110.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit110.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl133
            // 
            this.labelControl133.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl133.Location = new System.Drawing.Point(628, 401);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(12, 13);
            this.labelControl133.TabIndex = 449;
            this.labelControl133.Text = "79";
            this.labelControl133.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl133.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl133.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit111
            // 
            this.pictureEdit111.AllowDrop = true;
            this.pictureEdit111.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit111.Location = new System.Drawing.Point(619, 368);
            this.pictureEdit111.Name = "pictureEdit111";
            this.pictureEdit111.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit111.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit111.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit111.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit111.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit111.Properties.UseParentBackground = true;
            this.pictureEdit111.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit111.TabIndex = 448;
            this.pictureEdit111.Tag = "Mic079";
            this.pictureEdit111.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit111.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit111.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl134
            // 
            this.labelControl134.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl134.Location = new System.Drawing.Point(562, 401);
            this.labelControl134.Name = "labelControl134";
            this.labelControl134.Size = new System.Drawing.Size(12, 13);
            this.labelControl134.TabIndex = 447;
            this.labelControl134.Text = "78";
            this.labelControl134.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl134.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl134.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit112
            // 
            this.pictureEdit112.AllowDrop = true;
            this.pictureEdit112.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit112.Location = new System.Drawing.Point(554, 368);
            this.pictureEdit112.Name = "pictureEdit112";
            this.pictureEdit112.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit112.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit112.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit112.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit112.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit112.Properties.UseParentBackground = true;
            this.pictureEdit112.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit112.TabIndex = 446;
            this.pictureEdit112.Tag = "Mic078";
            this.pictureEdit112.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit112.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit112.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl135
            // 
            this.labelControl135.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl135.Location = new System.Drawing.Point(524, 401);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(12, 13);
            this.labelControl135.TabIndex = 445;
            this.labelControl135.Text = "77";
            this.labelControl135.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl135.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl135.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit113
            // 
            this.pictureEdit113.AllowDrop = true;
            this.pictureEdit113.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit113.Location = new System.Drawing.Point(515, 368);
            this.pictureEdit113.Name = "pictureEdit113";
            this.pictureEdit113.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit113.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit113.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit113.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit113.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit113.Properties.UseParentBackground = true;
            this.pictureEdit113.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit113.TabIndex = 444;
            this.pictureEdit113.Tag = "Mic077";
            this.pictureEdit113.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit113.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit113.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl136
            // 
            this.labelControl136.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl136.Location = new System.Drawing.Point(485, 400);
            this.labelControl136.Name = "labelControl136";
            this.labelControl136.Size = new System.Drawing.Size(12, 13);
            this.labelControl136.TabIndex = 443;
            this.labelControl136.Text = "76";
            this.labelControl136.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl136.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl136.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit114
            // 
            this.pictureEdit114.AllowDrop = true;
            this.pictureEdit114.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit114.Location = new System.Drawing.Point(476, 368);
            this.pictureEdit114.Name = "pictureEdit114";
            this.pictureEdit114.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit114.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit114.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit114.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit114.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit114.Properties.UseParentBackground = true;
            this.pictureEdit114.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit114.TabIndex = 442;
            this.pictureEdit114.Tag = "Mic076";
            this.pictureEdit114.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit114.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit114.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl137
            // 
            this.labelControl137.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl137.Location = new System.Drawing.Point(446, 401);
            this.labelControl137.Name = "labelControl137";
            this.labelControl137.Size = new System.Drawing.Size(12, 13);
            this.labelControl137.TabIndex = 441;
            this.labelControl137.Text = "75";
            this.labelControl137.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl137.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl137.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit115
            // 
            this.pictureEdit115.AllowDrop = true;
            this.pictureEdit115.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit115.Location = new System.Drawing.Point(437, 368);
            this.pictureEdit115.Name = "pictureEdit115";
            this.pictureEdit115.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit115.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit115.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit115.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit115.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit115.Properties.UseParentBackground = true;
            this.pictureEdit115.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit115.TabIndex = 440;
            this.pictureEdit115.Tag = "Mic075";
            this.pictureEdit115.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit115.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit115.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl138
            // 
            this.labelControl138.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl138.Location = new System.Drawing.Point(385, 401);
            this.labelControl138.Name = "labelControl138";
            this.labelControl138.Size = new System.Drawing.Size(12, 13);
            this.labelControl138.TabIndex = 439;
            this.labelControl138.Text = "74";
            this.labelControl138.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl138.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl138.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit116
            // 
            this.pictureEdit116.AllowDrop = true;
            this.pictureEdit116.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit116.Location = new System.Drawing.Point(377, 368);
            this.pictureEdit116.Name = "pictureEdit116";
            this.pictureEdit116.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit116.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit116.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit116.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit116.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit116.Properties.UseParentBackground = true;
            this.pictureEdit116.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit116.TabIndex = 438;
            this.pictureEdit116.Tag = "Mic074";
            this.pictureEdit116.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit116.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit116.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl139
            // 
            this.labelControl139.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl139.Location = new System.Drawing.Point(347, 400);
            this.labelControl139.Name = "labelControl139";
            this.labelControl139.Size = new System.Drawing.Size(12, 13);
            this.labelControl139.TabIndex = 437;
            this.labelControl139.Text = "73";
            this.labelControl139.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl139.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl139.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit117
            // 
            this.pictureEdit117.AllowDrop = true;
            this.pictureEdit117.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit117.Location = new System.Drawing.Point(338, 368);
            this.pictureEdit117.Name = "pictureEdit117";
            this.pictureEdit117.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit117.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit117.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit117.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit117.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit117.Properties.UseParentBackground = true;
            this.pictureEdit117.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit117.TabIndex = 436;
            this.pictureEdit117.Tag = "Mic073";
            this.pictureEdit117.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit117.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit117.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl140
            // 
            this.labelControl140.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl140.Location = new System.Drawing.Point(308, 401);
            this.labelControl140.Name = "labelControl140";
            this.labelControl140.Size = new System.Drawing.Size(12, 13);
            this.labelControl140.TabIndex = 435;
            this.labelControl140.Text = "72";
            this.labelControl140.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl140.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl140.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit118
            // 
            this.pictureEdit118.AllowDrop = true;
            this.pictureEdit118.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit118.Location = new System.Drawing.Point(299, 368);
            this.pictureEdit118.Name = "pictureEdit118";
            this.pictureEdit118.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit118.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit118.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit118.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit118.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit118.Properties.UseParentBackground = true;
            this.pictureEdit118.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit118.TabIndex = 434;
            this.pictureEdit118.Tag = "Mic072";
            this.pictureEdit118.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit118.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit118.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl141
            // 
            this.labelControl141.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl141.Location = new System.Drawing.Point(269, 400);
            this.labelControl141.Name = "labelControl141";
            this.labelControl141.Size = new System.Drawing.Size(12, 13);
            this.labelControl141.TabIndex = 433;
            this.labelControl141.Text = "71";
            this.labelControl141.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl141.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl141.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit119
            // 
            this.pictureEdit119.AllowDrop = true;
            this.pictureEdit119.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit119.Location = new System.Drawing.Point(260, 368);
            this.pictureEdit119.Name = "pictureEdit119";
            this.pictureEdit119.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit119.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit119.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit119.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit119.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit119.Properties.UseParentBackground = true;
            this.pictureEdit119.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit119.TabIndex = 432;
            this.pictureEdit119.Tag = "Mic071";
            this.pictureEdit119.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit119.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit119.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl142
            // 
            this.labelControl142.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl142.Location = new System.Drawing.Point(876, 345);
            this.labelControl142.Name = "labelControl142";
            this.labelControl142.Size = new System.Drawing.Size(12, 13);
            this.labelControl142.TabIndex = 431;
            this.labelControl142.Text = "50";
            this.labelControl142.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl142.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl142.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit120
            // 
            this.pictureEdit120.AllowDrop = true;
            this.pictureEdit120.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit120.Location = new System.Drawing.Point(868, 312);
            this.pictureEdit120.Name = "pictureEdit120";
            this.pictureEdit120.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit120.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit120.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit120.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit120.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit120.Properties.UseParentBackground = true;
            this.pictureEdit120.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit120.TabIndex = 430;
            this.pictureEdit120.Tag = "Mic050";
            this.pictureEdit120.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit120.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit120.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl143
            // 
            this.labelControl143.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl143.Location = new System.Drawing.Point(137, 345);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(12, 13);
            this.labelControl143.TabIndex = 429;
            this.labelControl143.Text = "67";
            this.labelControl143.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl143.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl143.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit121
            // 
            this.pictureEdit121.AllowDrop = true;
            this.pictureEdit121.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit121.Location = new System.Drawing.Point(128, 312);
            this.pictureEdit121.Name = "pictureEdit121";
            this.pictureEdit121.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit121.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit121.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit121.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit121.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit121.Properties.UseParentBackground = true;
            this.pictureEdit121.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit121.TabIndex = 428;
            this.pictureEdit121.Tag = "Mic067";
            this.pictureEdit121.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit121.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit121.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl144
            // 
            this.labelControl144.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl144.Location = new System.Drawing.Point(174, 345);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(12, 13);
            this.labelControl144.TabIndex = 427;
            this.labelControl144.Text = "66";
            this.labelControl144.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl144.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl144.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit122
            // 
            this.pictureEdit122.AllowDrop = true;
            this.pictureEdit122.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit122.Location = new System.Drawing.Point(165, 312);
            this.pictureEdit122.Name = "pictureEdit122";
            this.pictureEdit122.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit122.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit122.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit122.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit122.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit122.Properties.UseParentBackground = true;
            this.pictureEdit122.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit122.TabIndex = 426;
            this.pictureEdit122.Tag = "Mic066";
            this.pictureEdit122.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit122.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit122.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl81
            // 
            this.labelControl81.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl81.Location = new System.Drawing.Point(214, 346);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(12, 13);
            this.labelControl81.TabIndex = 425;
            this.labelControl81.Text = "65";
            this.labelControl81.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl81.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl81.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit59
            // 
            this.pictureEdit59.AllowDrop = true;
            this.pictureEdit59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit59.Location = new System.Drawing.Point(208, 312);
            this.pictureEdit59.Name = "pictureEdit59";
            this.pictureEdit59.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit59.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit59.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit59.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit59.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit59.Properties.UseParentBackground = true;
            this.pictureEdit59.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit59.TabIndex = 424;
            this.pictureEdit59.Tag = "Mic065";
            this.pictureEdit59.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit59.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit59.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl82
            // 
            this.labelControl82.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl82.Location = new System.Drawing.Point(265, 346);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(12, 13);
            this.labelControl82.TabIndex = 423;
            this.labelControl82.Text = "64";
            this.labelControl82.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl82.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl82.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit60
            // 
            this.pictureEdit60.AllowDrop = true;
            this.pictureEdit60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit60.Location = new System.Drawing.Point(260, 312);
            this.pictureEdit60.Name = "pictureEdit60";
            this.pictureEdit60.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit60.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit60.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit60.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit60.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit60.Properties.UseParentBackground = true;
            this.pictureEdit60.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit60.TabIndex = 422;
            this.pictureEdit60.Tag = "Mic064";
            this.pictureEdit60.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit60.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit60.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl83
            // 
            this.labelControl83.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl83.Location = new System.Drawing.Point(305, 345);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(12, 13);
            this.labelControl83.TabIndex = 421;
            this.labelControl83.Text = "63";
            this.labelControl83.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl83.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl83.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit61
            // 
            this.pictureEdit61.AllowDrop = true;
            this.pictureEdit61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit61.Location = new System.Drawing.Point(299, 312);
            this.pictureEdit61.Name = "pictureEdit61";
            this.pictureEdit61.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit61.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit61.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit61.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit61.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit61.Properties.UseParentBackground = true;
            this.pictureEdit61.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit61.TabIndex = 420;
            this.pictureEdit61.Tag = "Mic063";
            this.pictureEdit61.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit61.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit61.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl84
            // 
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl84.Location = new System.Drawing.Point(344, 346);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(12, 13);
            this.labelControl84.TabIndex = 419;
            this.labelControl84.Text = "62";
            this.labelControl84.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl84.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl84.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit62
            // 
            this.pictureEdit62.AllowDrop = true;
            this.pictureEdit62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit62.Location = new System.Drawing.Point(338, 312);
            this.pictureEdit62.Name = "pictureEdit62";
            this.pictureEdit62.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit62.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit62.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit62.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit62.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit62.Properties.UseParentBackground = true;
            this.pictureEdit62.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit62.TabIndex = 418;
            this.pictureEdit62.Tag = "Mic062";
            this.pictureEdit62.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit62.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit62.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl85
            // 
            this.labelControl85.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl85.Location = new System.Drawing.Point(383, 345);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(12, 13);
            this.labelControl85.TabIndex = 417;
            this.labelControl85.Text = "61";
            this.labelControl85.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl85.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl85.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit63
            // 
            this.pictureEdit63.AllowDrop = true;
            this.pictureEdit63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit63.Location = new System.Drawing.Point(377, 312);
            this.pictureEdit63.Name = "pictureEdit63";
            this.pictureEdit63.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit63.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit63.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit63.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit63.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit63.Properties.UseParentBackground = true;
            this.pictureEdit63.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit63.TabIndex = 416;
            this.pictureEdit63.Tag = "Mic061";
            this.pictureEdit63.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit63.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit63.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl86
            // 
            this.labelControl86.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl86.Location = new System.Drawing.Point(445, 346);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(12, 13);
            this.labelControl86.TabIndex = 415;
            this.labelControl86.Text = "60";
            this.labelControl86.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl86.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl86.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit64
            // 
            this.pictureEdit64.AllowDrop = true;
            this.pictureEdit64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit64.Location = new System.Drawing.Point(437, 312);
            this.pictureEdit64.Name = "pictureEdit64";
            this.pictureEdit64.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit64.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit64.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit64.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit64.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit64.Properties.UseParentBackground = true;
            this.pictureEdit64.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit64.TabIndex = 414;
            this.pictureEdit64.Tag = "Mic060";
            this.pictureEdit64.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit64.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit64.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl87
            // 
            this.labelControl87.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl87.Location = new System.Drawing.Point(485, 346);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(12, 13);
            this.labelControl87.TabIndex = 413;
            this.labelControl87.Text = "59";
            this.labelControl87.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl87.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl87.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit65
            // 
            this.pictureEdit65.AllowDrop = true;
            this.pictureEdit65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit65.Location = new System.Drawing.Point(476, 312);
            this.pictureEdit65.Name = "pictureEdit65";
            this.pictureEdit65.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit65.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit65.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit65.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit65.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit65.Properties.UseParentBackground = true;
            this.pictureEdit65.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit65.TabIndex = 412;
            this.pictureEdit65.Tag = "Mic059";
            this.pictureEdit65.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit65.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit65.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl88
            // 
            this.labelControl88.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl88.Location = new System.Drawing.Point(524, 345);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(12, 13);
            this.labelControl88.TabIndex = 411;
            this.labelControl88.Text = "58";
            this.labelControl88.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl88.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl88.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit66
            // 
            this.pictureEdit66.AllowDrop = true;
            this.pictureEdit66.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit66.Location = new System.Drawing.Point(515, 312);
            this.pictureEdit66.Name = "pictureEdit66";
            this.pictureEdit66.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit66.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit66.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit66.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit66.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit66.Properties.UseParentBackground = true;
            this.pictureEdit66.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit66.TabIndex = 410;
            this.pictureEdit66.Tag = "Mic058";
            this.pictureEdit66.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit66.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit66.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl89
            // 
            this.labelControl89.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl89.Location = new System.Drawing.Point(563, 346);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(12, 13);
            this.labelControl89.TabIndex = 409;
            this.labelControl89.Text = "57";
            this.labelControl89.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl89.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl89.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit67
            // 
            this.pictureEdit67.AllowDrop = true;
            this.pictureEdit67.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit67.Location = new System.Drawing.Point(554, 312);
            this.pictureEdit67.Name = "pictureEdit67";
            this.pictureEdit67.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit67.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit67.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit67.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit67.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit67.Properties.UseParentBackground = true;
            this.pictureEdit67.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit67.TabIndex = 408;
            this.pictureEdit67.Tag = "Mic057";
            this.pictureEdit67.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit67.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit67.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl90
            // 
            this.labelControl90.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl90.Location = new System.Drawing.Point(627, 346);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(12, 13);
            this.labelControl90.TabIndex = 407;
            this.labelControl90.Text = "56";
            this.labelControl90.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl90.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl90.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit68
            // 
            this.pictureEdit68.AllowDrop = true;
            this.pictureEdit68.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit68.Location = new System.Drawing.Point(619, 312);
            this.pictureEdit68.Name = "pictureEdit68";
            this.pictureEdit68.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit68.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit68.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit68.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit68.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit68.Properties.UseParentBackground = true;
            this.pictureEdit68.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit68.TabIndex = 406;
            this.pictureEdit68.Tag = "Mic056";
            this.pictureEdit68.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit68.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit68.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl91
            // 
            this.labelControl91.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl91.Location = new System.Drawing.Point(667, 345);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(12, 13);
            this.labelControl91.TabIndex = 405;
            this.labelControl91.Text = "55";
            this.labelControl91.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl91.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl91.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit69
            // 
            this.pictureEdit69.AllowDrop = true;
            this.pictureEdit69.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit69.Location = new System.Drawing.Point(658, 312);
            this.pictureEdit69.Name = "pictureEdit69";
            this.pictureEdit69.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit69.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit69.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit69.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit69.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit69.Properties.UseParentBackground = true;
            this.pictureEdit69.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit69.TabIndex = 404;
            this.pictureEdit69.Tag = "Mic055";
            this.pictureEdit69.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit69.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit69.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl92
            // 
            this.labelControl92.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl92.Location = new System.Drawing.Point(706, 346);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(12, 13);
            this.labelControl92.TabIndex = 403;
            this.labelControl92.Text = "54";
            this.labelControl92.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl92.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl92.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit70
            // 
            this.pictureEdit70.AllowDrop = true;
            this.pictureEdit70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit70.Location = new System.Drawing.Point(697, 312);
            this.pictureEdit70.Name = "pictureEdit70";
            this.pictureEdit70.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit70.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit70.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit70.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit70.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit70.Properties.UseParentBackground = true;
            this.pictureEdit70.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit70.TabIndex = 402;
            this.pictureEdit70.Tag = "Mic054";
            this.pictureEdit70.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit70.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit70.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl93
            // 
            this.labelControl93.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl93.Location = new System.Drawing.Point(747, 345);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(12, 13);
            this.labelControl93.TabIndex = 401;
            this.labelControl93.Text = "53";
            this.labelControl93.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl93.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl93.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit71
            // 
            this.pictureEdit71.AllowDrop = true;
            this.pictureEdit71.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit71.Location = new System.Drawing.Point(738, 312);
            this.pictureEdit71.Name = "pictureEdit71";
            this.pictureEdit71.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit71.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit71.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit71.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit71.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit71.Properties.UseParentBackground = true;
            this.pictureEdit71.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit71.TabIndex = 400;
            this.pictureEdit71.Tag = "Mic053";
            this.pictureEdit71.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit71.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit71.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl94
            // 
            this.labelControl94.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl94.Location = new System.Drawing.Point(800, 345);
            this.labelControl94.Name = "labelControl94";
            this.labelControl94.Size = new System.Drawing.Size(12, 13);
            this.labelControl94.TabIndex = 399;
            this.labelControl94.Text = "52";
            this.labelControl94.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl94.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl94.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit72
            // 
            this.pictureEdit72.AllowDrop = true;
            this.pictureEdit72.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit72.Location = new System.Drawing.Point(792, 312);
            this.pictureEdit72.Name = "pictureEdit72";
            this.pictureEdit72.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit72.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit72.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit72.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit72.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit72.Properties.UseParentBackground = true;
            this.pictureEdit72.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit72.TabIndex = 398;
            this.pictureEdit72.Tag = "Mic052";
            this.pictureEdit72.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit72.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit72.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl95
            // 
            this.labelControl95.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl95.Location = new System.Drawing.Point(840, 345);
            this.labelControl95.Name = "labelControl95";
            this.labelControl95.Size = new System.Drawing.Size(12, 13);
            this.labelControl95.TabIndex = 397;
            this.labelControl95.Text = "51";
            this.labelControl95.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl95.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl95.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit73
            // 
            this.pictureEdit73.AllowDrop = true;
            this.pictureEdit73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit73.Location = new System.Drawing.Point(831, 312);
            this.pictureEdit73.Name = "pictureEdit73";
            this.pictureEdit73.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit73.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit73.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit73.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit73.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit73.Properties.UseParentBackground = true;
            this.pictureEdit73.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit73.TabIndex = 396;
            this.pictureEdit73.Tag = "Mic051";
            this.pictureEdit73.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit73.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit73.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl96
            // 
            this.labelControl96.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl96.Location = new System.Drawing.Point(840, 289);
            this.labelControl96.Name = "labelControl96";
            this.labelControl96.Size = new System.Drawing.Size(12, 13);
            this.labelControl96.TabIndex = 395;
            this.labelControl96.Text = "49";
            this.labelControl96.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl96.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl96.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit74
            // 
            this.pictureEdit74.AllowDrop = true;
            this.pictureEdit74.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit74.Location = new System.Drawing.Point(831, 256);
            this.pictureEdit74.Name = "pictureEdit74";
            this.pictureEdit74.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit74.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit74.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit74.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit74.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit74.Properties.UseParentBackground = true;
            this.pictureEdit74.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit74.TabIndex = 394;
            this.pictureEdit74.Tag = "Mic049";
            this.pictureEdit74.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit74.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit74.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl65.Location = new System.Drawing.Point(801, 290);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(12, 13);
            this.labelControl65.TabIndex = 393;
            this.labelControl65.Text = "48";
            this.labelControl65.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl65.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl65.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit43
            // 
            this.pictureEdit43.AllowDrop = true;
            this.pictureEdit43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit43.Location = new System.Drawing.Point(792, 256);
            this.pictureEdit43.Name = "pictureEdit43";
            this.pictureEdit43.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit43.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit43.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit43.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit43.Properties.UseParentBackground = true;
            this.pictureEdit43.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit43.TabIndex = 392;
            this.pictureEdit43.Tag = "Mic048";
            this.pictureEdit43.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit43.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit43.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl66.Location = new System.Drawing.Point(748, 290);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(12, 13);
            this.labelControl66.TabIndex = 391;
            this.labelControl66.Text = "47";
            this.labelControl66.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl66.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl66.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit44
            // 
            this.pictureEdit44.AllowDrop = true;
            this.pictureEdit44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit44.Location = new System.Drawing.Point(738, 256);
            this.pictureEdit44.Name = "pictureEdit44";
            this.pictureEdit44.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit44.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit44.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit44.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit44.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit44.Properties.UseParentBackground = true;
            this.pictureEdit44.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit44.TabIndex = 390;
            this.pictureEdit44.Tag = "Mic047";
            this.pictureEdit44.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit44.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit44.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl67.Location = new System.Drawing.Point(708, 290);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(12, 13);
            this.labelControl67.TabIndex = 389;
            this.labelControl67.Text = "46";
            this.labelControl67.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl67.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl67.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit45
            // 
            this.pictureEdit45.AllowDrop = true;
            this.pictureEdit45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit45.Location = new System.Drawing.Point(697, 256);
            this.pictureEdit45.Name = "pictureEdit45";
            this.pictureEdit45.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit45.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit45.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit45.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit45.Properties.UseParentBackground = true;
            this.pictureEdit45.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit45.TabIndex = 388;
            this.pictureEdit45.Tag = "Mic046";
            this.pictureEdit45.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit45.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit45.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl68
            // 
            this.labelControl68.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl68.Location = new System.Drawing.Point(669, 290);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(12, 13);
            this.labelControl68.TabIndex = 387;
            this.labelControl68.Text = "45";
            this.labelControl68.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl68.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl68.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit46
            // 
            this.pictureEdit46.AllowDrop = true;
            this.pictureEdit46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit46.Location = new System.Drawing.Point(658, 256);
            this.pictureEdit46.Name = "pictureEdit46";
            this.pictureEdit46.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit46.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit46.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit46.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit46.Properties.UseParentBackground = true;
            this.pictureEdit46.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit46.TabIndex = 386;
            this.pictureEdit46.Tag = "Mic045";
            this.pictureEdit46.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit46.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit46.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl69
            // 
            this.labelControl69.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl69.Location = new System.Drawing.Point(628, 290);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(12, 13);
            this.labelControl69.TabIndex = 385;
            this.labelControl69.Text = "44";
            this.labelControl69.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl69.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl69.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit47
            // 
            this.pictureEdit47.AllowDrop = true;
            this.pictureEdit47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit47.Location = new System.Drawing.Point(619, 256);
            this.pictureEdit47.Name = "pictureEdit47";
            this.pictureEdit47.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit47.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit47.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit47.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit47.Properties.UseParentBackground = true;
            this.pictureEdit47.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit47.TabIndex = 384;
            this.pictureEdit47.Tag = "Mic044";
            this.pictureEdit47.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit47.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit47.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl70
            // 
            this.labelControl70.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl70.Location = new System.Drawing.Point(562, 290);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(12, 13);
            this.labelControl70.TabIndex = 383;
            this.labelControl70.Text = "43";
            this.labelControl70.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl70.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl70.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit48
            // 
            this.pictureEdit48.AllowDrop = true;
            this.pictureEdit48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit48.Location = new System.Drawing.Point(554, 256);
            this.pictureEdit48.Name = "pictureEdit48";
            this.pictureEdit48.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit48.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit48.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit48.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit48.Properties.UseParentBackground = true;
            this.pictureEdit48.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit48.TabIndex = 382;
            this.pictureEdit48.Tag = "Mic043";
            this.pictureEdit48.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit48.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit48.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl71
            // 
            this.labelControl71.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl71.Location = new System.Drawing.Point(524, 290);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(12, 13);
            this.labelControl71.TabIndex = 381;
            this.labelControl71.Text = "42";
            this.labelControl71.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl71.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl71.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit49
            // 
            this.pictureEdit49.AllowDrop = true;
            this.pictureEdit49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit49.Location = new System.Drawing.Point(515, 256);
            this.pictureEdit49.Name = "pictureEdit49";
            this.pictureEdit49.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit49.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit49.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit49.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit49.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit49.Properties.UseParentBackground = true;
            this.pictureEdit49.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit49.TabIndex = 380;
            this.pictureEdit49.Tag = "Mic042";
            this.pictureEdit49.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit49.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit49.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl72
            // 
            this.labelControl72.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl72.Location = new System.Drawing.Point(485, 289);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(12, 13);
            this.labelControl72.TabIndex = 379;
            this.labelControl72.Text = "41";
            this.labelControl72.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl72.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl72.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit50
            // 
            this.pictureEdit50.AllowDrop = true;
            this.pictureEdit50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit50.Location = new System.Drawing.Point(476, 256);
            this.pictureEdit50.Name = "pictureEdit50";
            this.pictureEdit50.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit50.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit50.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit50.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit50.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit50.Properties.UseParentBackground = true;
            this.pictureEdit50.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit50.TabIndex = 378;
            this.pictureEdit50.Tag = "Mic041";
            this.pictureEdit50.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit50.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit50.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl73
            // 
            this.labelControl73.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl73.Location = new System.Drawing.Point(446, 290);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(12, 13);
            this.labelControl73.TabIndex = 377;
            this.labelControl73.Text = "40";
            this.labelControl73.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl73.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl73.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit51
            // 
            this.pictureEdit51.AllowDrop = true;
            this.pictureEdit51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit51.Location = new System.Drawing.Point(437, 256);
            this.pictureEdit51.Name = "pictureEdit51";
            this.pictureEdit51.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit51.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit51.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit51.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit51.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit51.Properties.UseParentBackground = true;
            this.pictureEdit51.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit51.TabIndex = 376;
            this.pictureEdit51.Tag = "Mic040";
            this.pictureEdit51.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit51.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit51.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl74
            // 
            this.labelControl74.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl74.Location = new System.Drawing.Point(385, 290);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(12, 13);
            this.labelControl74.TabIndex = 375;
            this.labelControl74.Text = "39";
            this.labelControl74.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl74.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl74.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit52
            // 
            this.pictureEdit52.AllowDrop = true;
            this.pictureEdit52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit52.Location = new System.Drawing.Point(377, 256);
            this.pictureEdit52.Name = "pictureEdit52";
            this.pictureEdit52.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit52.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit52.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit52.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit52.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit52.Properties.UseParentBackground = true;
            this.pictureEdit52.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit52.TabIndex = 374;
            this.pictureEdit52.Tag = "Mic039";
            this.pictureEdit52.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit52.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit52.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl75
            // 
            this.labelControl75.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl75.Location = new System.Drawing.Point(343, 290);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(12, 13);
            this.labelControl75.TabIndex = 373;
            this.labelControl75.Text = "38";
            this.labelControl75.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl75.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl75.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit53
            // 
            this.pictureEdit53.AllowDrop = true;
            this.pictureEdit53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit53.Location = new System.Drawing.Point(338, 256);
            this.pictureEdit53.Name = "pictureEdit53";
            this.pictureEdit53.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit53.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit53.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit53.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit53.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit53.Properties.UseParentBackground = true;
            this.pictureEdit53.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit53.TabIndex = 372;
            this.pictureEdit53.Tag = "Mic038";
            this.pictureEdit53.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit53.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit53.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl76
            // 
            this.labelControl76.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl76.Location = new System.Drawing.Point(308, 290);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(12, 13);
            this.labelControl76.TabIndex = 371;
            this.labelControl76.Text = "37";
            this.labelControl76.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl76.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl76.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit54
            // 
            this.pictureEdit54.AllowDrop = true;
            this.pictureEdit54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit54.Location = new System.Drawing.Point(299, 256);
            this.pictureEdit54.Name = "pictureEdit54";
            this.pictureEdit54.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit54.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit54.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit54.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit54.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit54.Properties.UseParentBackground = true;
            this.pictureEdit54.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit54.TabIndex = 370;
            this.pictureEdit54.Tag = "Mic037";
            this.pictureEdit54.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit54.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit54.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl77
            // 
            this.labelControl77.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl77.Location = new System.Drawing.Point(269, 289);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(12, 13);
            this.labelControl77.TabIndex = 369;
            this.labelControl77.Text = "36";
            this.labelControl77.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl77.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl77.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit55
            // 
            this.pictureEdit55.AllowDrop = true;
            this.pictureEdit55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit55.Location = new System.Drawing.Point(260, 256);
            this.pictureEdit55.Name = "pictureEdit55";
            this.pictureEdit55.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit55.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit55.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit55.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit55.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit55.Properties.UseParentBackground = true;
            this.pictureEdit55.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit55.TabIndex = 368;
            this.pictureEdit55.Tag = "Mic036";
            this.pictureEdit55.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit55.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit55.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl78
            // 
            this.labelControl78.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl78.Location = new System.Drawing.Point(216, 290);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(12, 13);
            this.labelControl78.TabIndex = 367;
            this.labelControl78.Text = "35";
            this.labelControl78.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl78.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl78.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit56
            // 
            this.pictureEdit56.AllowDrop = true;
            this.pictureEdit56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit56.Location = new System.Drawing.Point(208, 256);
            this.pictureEdit56.Name = "pictureEdit56";
            this.pictureEdit56.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit56.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit56.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit56.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit56.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit56.Properties.UseParentBackground = true;
            this.pictureEdit56.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit56.TabIndex = 366;
            this.pictureEdit56.Tag = "Mic035";
            this.pictureEdit56.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit56.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit56.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl79
            // 
            this.labelControl79.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl79.Location = new System.Drawing.Point(174, 290);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(12, 13);
            this.labelControl79.TabIndex = 365;
            this.labelControl79.Text = "34";
            this.labelControl79.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl79.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl79.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit57
            // 
            this.pictureEdit57.AllowDrop = true;
            this.pictureEdit57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit57.Location = new System.Drawing.Point(165, 256);
            this.pictureEdit57.Name = "pictureEdit57";
            this.pictureEdit57.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit57.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit57.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit57.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit57.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit57.Properties.UseParentBackground = true;
            this.pictureEdit57.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit57.TabIndex = 364;
            this.pictureEdit57.Tag = "Mic034";
            this.pictureEdit57.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit57.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit57.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl80
            // 
            this.labelControl80.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl80.Location = new System.Drawing.Point(840, 233);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(12, 13);
            this.labelControl80.TabIndex = 363;
            this.labelControl80.Text = "18";
            this.labelControl80.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl80.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl80.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit58
            // 
            this.pictureEdit58.AllowDrop = true;
            this.pictureEdit58.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit58.Location = new System.Drawing.Point(831, 200);
            this.pictureEdit58.Name = "pictureEdit58";
            this.pictureEdit58.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit58.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit58.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit58.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit58.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit58.Properties.UseParentBackground = true;
            this.pictureEdit58.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit58.TabIndex = 362;
            this.pictureEdit58.Tag = "Mic018";
            this.pictureEdit58.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit58.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit58.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl49.Location = new System.Drawing.Point(801, 234);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(12, 13);
            this.labelControl49.TabIndex = 361;
            this.labelControl49.Text = "19";
            this.labelControl49.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl49.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl49.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl41.Location = new System.Drawing.Point(706, 178);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(12, 13);
            this.labelControl41.TabIndex = 329;
            this.labelControl41.Text = "16";
            this.labelControl41.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl41.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl41.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit27
            // 
            this.pictureEdit27.AllowDrop = true;
            this.pictureEdit27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit27.Location = new System.Drawing.Point(792, 200);
            this.pictureEdit27.Name = "pictureEdit27";
            this.pictureEdit27.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit27.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit27.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit27.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit27.Properties.UseParentBackground = true;
            this.pictureEdit27.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit27.TabIndex = 360;
            this.pictureEdit27.Tag = "Mic019";
            this.pictureEdit27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit27.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit27.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit19
            // 
            this.pictureEdit19.AllowDrop = true;
            this.pictureEdit19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit19.Location = new System.Drawing.Point(697, 144);
            this.pictureEdit19.Name = "pictureEdit19";
            this.pictureEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit19.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit19.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit19.Properties.UseParentBackground = true;
            this.pictureEdit19.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit19.TabIndex = 328;
            this.pictureEdit19.Tag = "Mic016";
            this.pictureEdit19.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit19.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit19.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl50.Location = new System.Drawing.Point(744, 234);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(12, 13);
            this.labelControl50.TabIndex = 359;
            this.labelControl50.Text = "20";
            this.labelControl50.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl50.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl50.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl42.Location = new System.Drawing.Point(667, 178);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(12, 13);
            this.labelControl42.TabIndex = 327;
            this.labelControl42.Text = "15";
            this.labelControl42.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl42.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl42.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit28
            // 
            this.pictureEdit28.AllowDrop = true;
            this.pictureEdit28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit28.Location = new System.Drawing.Point(736, 200);
            this.pictureEdit28.Name = "pictureEdit28";
            this.pictureEdit28.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit28.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit28.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit28.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit28.Properties.UseParentBackground = true;
            this.pictureEdit28.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit28.TabIndex = 358;
            this.pictureEdit28.Tag = "Mic020";
            this.pictureEdit28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit28.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit28.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit20
            // 
            this.pictureEdit20.AllowDrop = true;
            this.pictureEdit20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit20.Location = new System.Drawing.Point(658, 144);
            this.pictureEdit20.Name = "pictureEdit20";
            this.pictureEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit20.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit20.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit20.Properties.UseParentBackground = true;
            this.pictureEdit20.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit20.TabIndex = 326;
            this.pictureEdit20.Tag = "Mic015";
            this.pictureEdit20.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit20.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit20.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl51.Location = new System.Drawing.Point(706, 233);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(12, 13);
            this.labelControl51.TabIndex = 357;
            this.labelControl51.Text = "21";
            this.labelControl51.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl51.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl51.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl43.Location = new System.Drawing.Point(628, 177);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(12, 13);
            this.labelControl43.TabIndex = 325;
            this.labelControl43.Text = "14";
            this.labelControl43.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl43.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl43.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit29
            // 
            this.pictureEdit29.AllowDrop = true;
            this.pictureEdit29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit29.Location = new System.Drawing.Point(697, 200);
            this.pictureEdit29.Name = "pictureEdit29";
            this.pictureEdit29.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit29.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit29.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit29.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit29.Properties.UseParentBackground = true;
            this.pictureEdit29.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit29.TabIndex = 356;
            this.pictureEdit29.Tag = "Mic021";
            this.pictureEdit29.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit29.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit29.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit21
            // 
            this.pictureEdit21.AllowDrop = true;
            this.pictureEdit21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit21.Location = new System.Drawing.Point(619, 144);
            this.pictureEdit21.Name = "pictureEdit21";
            this.pictureEdit21.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit21.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit21.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit21.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit21.Properties.UseParentBackground = true;
            this.pictureEdit21.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit21.TabIndex = 324;
            this.pictureEdit21.Tag = "Mic014";
            this.pictureEdit21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit21.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit21.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl52.Location = new System.Drawing.Point(667, 234);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(12, 13);
            this.labelControl52.TabIndex = 355;
            this.labelControl52.Text = "22";
            this.labelControl52.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl52.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl52.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl44.Location = new System.Drawing.Point(386, 178);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(12, 13);
            this.labelControl44.TabIndex = 323;
            this.labelControl44.Text = "13";
            this.labelControl44.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl44.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl44.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit30
            // 
            this.pictureEdit30.AllowDrop = true;
            this.pictureEdit30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit30.Location = new System.Drawing.Point(658, 200);
            this.pictureEdit30.Name = "pictureEdit30";
            this.pictureEdit30.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit30.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit30.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit30.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit30.Properties.UseParentBackground = true;
            this.pictureEdit30.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit30.TabIndex = 354;
            this.pictureEdit30.Tag = "Mic022";
            this.pictureEdit30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit30.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit30.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit22
            // 
            this.pictureEdit22.AllowDrop = true;
            this.pictureEdit22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit22.Location = new System.Drawing.Point(377, 144);
            this.pictureEdit22.Name = "pictureEdit22";
            this.pictureEdit22.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit22.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit22.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit22.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit22.Properties.UseParentBackground = true;
            this.pictureEdit22.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit22.TabIndex = 322;
            this.pictureEdit22.Tag = "Mic013";
            this.pictureEdit22.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit22.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit22.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl53.Location = new System.Drawing.Point(628, 233);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(12, 13);
            this.labelControl53.TabIndex = 353;
            this.labelControl53.Text = "23";
            this.labelControl53.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl53.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl53.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl45.Location = new System.Drawing.Point(347, 177);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(12, 13);
            this.labelControl45.TabIndex = 321;
            this.labelControl45.Text = "12";
            this.labelControl45.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl45.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl45.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit31
            // 
            this.pictureEdit31.AllowDrop = true;
            this.pictureEdit31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit31.Location = new System.Drawing.Point(619, 200);
            this.pictureEdit31.Name = "pictureEdit31";
            this.pictureEdit31.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit31.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit31.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit31.Properties.UseParentBackground = true;
            this.pictureEdit31.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit31.TabIndex = 352;
            this.pictureEdit31.Tag = "Mic023";
            this.pictureEdit31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit31.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit31.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit23
            // 
            this.pictureEdit23.AllowDrop = true;
            this.pictureEdit23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit23.Location = new System.Drawing.Point(338, 144);
            this.pictureEdit23.Name = "pictureEdit23";
            this.pictureEdit23.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit23.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit23.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit23.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit23.Properties.UseParentBackground = true;
            this.pictureEdit23.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit23.TabIndex = 320;
            this.pictureEdit23.Tag = "Mic012";
            this.pictureEdit23.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit23.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit23.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl54.Location = new System.Drawing.Point(560, 234);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(12, 13);
            this.labelControl54.TabIndex = 351;
            this.labelControl54.Text = "24";
            this.labelControl54.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl54.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl54.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl46.Location = new System.Drawing.Point(307, 178);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(12, 13);
            this.labelControl46.TabIndex = 319;
            this.labelControl46.Text = "11";
            this.labelControl46.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl46.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl46.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit32
            // 
            this.pictureEdit32.AllowDrop = true;
            this.pictureEdit32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit32.Location = new System.Drawing.Point(554, 200);
            this.pictureEdit32.Name = "pictureEdit32";
            this.pictureEdit32.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit32.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit32.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit32.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit32.Properties.UseParentBackground = true;
            this.pictureEdit32.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit32.TabIndex = 350;
            this.pictureEdit32.Tag = "Mic024";
            this.pictureEdit32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit32.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit32.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit24
            // 
            this.pictureEdit24.AllowDrop = true;
            this.pictureEdit24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit24.Location = new System.Drawing.Point(299, 144);
            this.pictureEdit24.Name = "pictureEdit24";
            this.pictureEdit24.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit24.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit24.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit24.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit24.Properties.UseParentBackground = true;
            this.pictureEdit24.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit24.TabIndex = 318;
            this.pictureEdit24.Tag = "Mic011";
            this.pictureEdit24.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit24.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit24.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl55.Location = new System.Drawing.Point(522, 234);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(12, 13);
            this.labelControl55.TabIndex = 349;
            this.labelControl55.Text = "25";
            this.labelControl55.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl55.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl55.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl47.Location = new System.Drawing.Point(269, 178);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(12, 13);
            this.labelControl47.TabIndex = 317;
            this.labelControl47.Text = "10";
            this.labelControl47.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl47.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl47.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit33
            // 
            this.pictureEdit33.AllowDrop = true;
            this.pictureEdit33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit33.Location = new System.Drawing.Point(515, 200);
            this.pictureEdit33.Name = "pictureEdit33";
            this.pictureEdit33.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit33.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit33.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit33.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit33.Properties.UseParentBackground = true;
            this.pictureEdit33.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit33.TabIndex = 348;
            this.pictureEdit33.Tag = "Mic025";
            this.pictureEdit33.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit33.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit33.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit25
            // 
            this.pictureEdit25.AllowDrop = true;
            this.pictureEdit25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit25.Location = new System.Drawing.Point(260, 144);
            this.pictureEdit25.Name = "pictureEdit25";
            this.pictureEdit25.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit25.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit25.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit25.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit25.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit25.Properties.UseParentBackground = true;
            this.pictureEdit25.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit25.TabIndex = 316;
            this.pictureEdit25.Tag = "Mic010";
            this.pictureEdit25.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit25.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit25.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl56.Location = new System.Drawing.Point(484, 233);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(12, 13);
            this.labelControl56.TabIndex = 347;
            this.labelControl56.Text = "26";
            this.labelControl56.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl56.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl56.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl48.Location = new System.Drawing.Point(509, 115);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(6, 13);
            this.labelControl48.TabIndex = 315;
            this.labelControl48.Text = "6";
            this.labelControl48.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl48.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl48.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit34
            // 
            this.pictureEdit34.AllowDrop = true;
            this.pictureEdit34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit34.Location = new System.Drawing.Point(476, 200);
            this.pictureEdit34.Name = "pictureEdit34";
            this.pictureEdit34.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit34.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit34.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit34.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit34.Properties.UseParentBackground = true;
            this.pictureEdit34.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit34.TabIndex = 346;
            this.pictureEdit34.Tag = "Mic026";
            this.pictureEdit34.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit34.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit34.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit26
            // 
            this.pictureEdit26.AllowDrop = true;
            this.pictureEdit26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit26.Location = new System.Drawing.Point(498, 82);
            this.pictureEdit26.Name = "pictureEdit26";
            this.pictureEdit26.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit26.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit26.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit26.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit26.Properties.UseParentBackground = true;
            this.pictureEdit26.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit26.TabIndex = 314;
            this.pictureEdit26.Tag = "Mic006";
            this.pictureEdit26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit26.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit26.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl57.Location = new System.Drawing.Point(446, 234);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(12, 13);
            this.labelControl57.TabIndex = 345;
            this.labelControl57.Text = "27";
            this.labelControl57.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl57.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl57.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl8.Location = new System.Drawing.Point(316, 119);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(6, 13);
            this.labelControl8.TabIndex = 313;
            this.labelControl8.Text = "7";
            this.labelControl8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl8.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl8.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit35
            // 
            this.pictureEdit35.AllowDrop = true;
            this.pictureEdit35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit35.Location = new System.Drawing.Point(437, 200);
            this.pictureEdit35.Name = "pictureEdit35";
            this.pictureEdit35.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit35.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit35.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit35.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit35.Properties.UseParentBackground = true;
            this.pictureEdit35.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit35.TabIndex = 344;
            this.pictureEdit35.Tag = "Mic027";
            this.pictureEdit35.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit35.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit35.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.AllowDrop = true;
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit1.Location = new System.Drawing.Point(307, 82);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit1.Properties.UseParentBackground = true;
            this.pictureEdit1.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit1.TabIndex = 312;
            this.pictureEdit1.Tag = "Mic007";
            this.pictureEdit1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl58.Location = new System.Drawing.Point(386, 234);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(12, 13);
            this.labelControl58.TabIndex = 343;
            this.labelControl58.Text = "28";
            this.labelControl58.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl58.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl58.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl9.Location = new System.Drawing.Point(276, 119);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(6, 13);
            this.labelControl9.TabIndex = 311;
            this.labelControl9.Text = "8";
            this.labelControl9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl9.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl9.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit36
            // 
            this.pictureEdit36.AllowDrop = true;
            this.pictureEdit36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit36.Location = new System.Drawing.Point(377, 200);
            this.pictureEdit36.Name = "pictureEdit36";
            this.pictureEdit36.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit36.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit36.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit36.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit36.Properties.UseParentBackground = true;
            this.pictureEdit36.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit36.TabIndex = 342;
            this.pictureEdit36.Tag = "Mic028";
            this.pictureEdit36.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit36.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit36.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.AllowDrop = true;
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit2.Location = new System.Drawing.Point(268, 82);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Properties.UseParentBackground = true;
            this.pictureEdit2.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit2.TabIndex = 310;
            this.pictureEdit2.Tag = "Mic008";
            this.pictureEdit2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl59.Location = new System.Drawing.Point(347, 233);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(12, 13);
            this.labelControl59.TabIndex = 341;
            this.labelControl59.Text = "29";
            this.labelControl59.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl59.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl59.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl10.Location = new System.Drawing.Point(238, 118);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(6, 13);
            this.labelControl10.TabIndex = 309;
            this.labelControl10.Text = "9";
            this.labelControl10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl10.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl10.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit37
            // 
            this.pictureEdit37.AllowDrop = true;
            this.pictureEdit37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit37.Location = new System.Drawing.Point(338, 200);
            this.pictureEdit37.Name = "pictureEdit37";
            this.pictureEdit37.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit37.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit37.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit37.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit37.Properties.UseParentBackground = true;
            this.pictureEdit37.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit37.TabIndex = 340;
            this.pictureEdit37.Tag = "Mic029";
            this.pictureEdit37.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit37.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit37.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.AllowDrop = true;
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit3.Location = new System.Drawing.Point(229, 82);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit3.Properties.UseParentBackground = true;
            this.pictureEdit3.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit3.TabIndex = 308;
            this.pictureEdit3.Tag = "Mic009";
            this.pictureEdit3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit3.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl60.Location = new System.Drawing.Point(308, 234);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(12, 13);
            this.labelControl60.TabIndex = 339;
            this.labelControl60.Text = "30";
            this.labelControl60.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl60.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl60.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl11.Location = new System.Drawing.Point(748, 64);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 307;
            this.labelControl11.Text = "5";
            this.labelControl11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl11.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl11.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit38
            // 
            this.pictureEdit38.AllowDrop = true;
            this.pictureEdit38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit38.Location = new System.Drawing.Point(299, 200);
            this.pictureEdit38.Name = "pictureEdit38";
            this.pictureEdit38.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit38.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit38.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit38.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit38.Properties.UseParentBackground = true;
            this.pictureEdit38.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit38.TabIndex = 338;
            this.pictureEdit38.Tag = "Mic030";
            this.pictureEdit38.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit38.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit38.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.AllowDrop = true;
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit4.Location = new System.Drawing.Point(738, 28);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit4.Properties.UseParentBackground = true;
            this.pictureEdit4.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit4.TabIndex = 306;
            this.pictureEdit4.Tag = "Mic005";
            this.pictureEdit4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit4.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit4.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl61.Location = new System.Drawing.Point(269, 233);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(12, 13);
            this.labelControl61.TabIndex = 337;
            this.labelControl61.Text = "31";
            this.labelControl61.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl61.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl61.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl12.Location = new System.Drawing.Point(594, 64);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 13);
            this.labelControl12.TabIndex = 305;
            this.labelControl12.Text = "4";
            this.labelControl12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl12.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl12.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit39
            // 
            this.pictureEdit39.AllowDrop = true;
            this.pictureEdit39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit39.Location = new System.Drawing.Point(260, 200);
            this.pictureEdit39.Name = "pictureEdit39";
            this.pictureEdit39.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit39.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit39.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit39.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit39.Properties.UseParentBackground = true;
            this.pictureEdit39.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit39.TabIndex = 336;
            this.pictureEdit39.Tag = "Mic031";
            this.pictureEdit39.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit39.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit39.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.AllowDrop = true;
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit5.Location = new System.Drawing.Point(583, 28);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit5.Properties.UseParentBackground = true;
            this.pictureEdit5.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit5.TabIndex = 304;
            this.pictureEdit5.Tag = "Mic004";
            this.pictureEdit5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit5.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit5.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl62.Location = new System.Drawing.Point(212, 234);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(12, 13);
            this.labelControl62.TabIndex = 335;
            this.labelControl62.Text = "32";
            this.labelControl62.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl62.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl62.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl13.Location = new System.Drawing.Point(533, 64);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(6, 13);
            this.labelControl13.TabIndex = 303;
            this.labelControl13.Text = "3";
            this.labelControl13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl13.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl13.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit40
            // 
            this.pictureEdit40.AllowDrop = true;
            this.pictureEdit40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit40.Location = new System.Drawing.Point(204, 200);
            this.pictureEdit40.Name = "pictureEdit40";
            this.pictureEdit40.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit40.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit40.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit40.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit40.Properties.UseParentBackground = true;
            this.pictureEdit40.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit40.TabIndex = 334;
            this.pictureEdit40.Tag = "Mic032";
            this.pictureEdit40.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit40.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit40.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.AllowDrop = true;
            this.pictureEdit6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit6.Location = new System.Drawing.Point(522, 28);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit6.Properties.UseParentBackground = true;
            this.pictureEdit6.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit6.TabIndex = 302;
            this.pictureEdit6.Tag = "Mic003";
            this.pictureEdit6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit6.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit6.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl63.Location = new System.Drawing.Point(174, 234);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(12, 13);
            this.labelControl63.TabIndex = 333;
            this.labelControl63.Text = "33";
            this.labelControl63.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl63.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl63.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl14.Location = new System.Drawing.Point(472, 64);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(6, 13);
            this.labelControl14.TabIndex = 301;
            this.labelControl14.Text = "2";
            this.labelControl14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl14.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            // 
            // pictureEdit41
            // 
            this.pictureEdit41.AllowDrop = true;
            this.pictureEdit41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit41.Location = new System.Drawing.Point(165, 200);
            this.pictureEdit41.Name = "pictureEdit41";
            this.pictureEdit41.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit41.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit41.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit41.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit41.Properties.UseParentBackground = true;
            this.pictureEdit41.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit41.TabIndex = 332;
            this.pictureEdit41.Tag = "Mic033";
            this.pictureEdit41.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit41.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit41.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // Mic_2
            // 
            this.Mic_2.AllowDrop = true;
            this.Mic_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_2.Location = new System.Drawing.Point(461, 28);
            this.Mic_2.Name = "Mic_2";
            this.Mic_2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_2.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_2.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_2.Properties.UseParentBackground = true;
            this.Mic_2.Size = new System.Drawing.Size(28, 32);
            this.Mic_2.TabIndex = 300;
            this.Mic_2.Tag = "Mic002";
            this.Mic_2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.Mic_2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.Mic_2.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl64.Location = new System.Drawing.Point(745, 177);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(12, 13);
            this.labelControl64.TabIndex = 331;
            this.labelControl64.Text = "17";
            this.labelControl64.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl64.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl64.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl15.Location = new System.Drawing.Point(411, 64);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 13);
            this.labelControl15.TabIndex = 299;
            this.labelControl15.Text = "1";
            this.labelControl15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.labelControl15.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            // 
            // pictureEdit42
            // 
            this.pictureEdit42.AllowDrop = true;
            this.pictureEdit42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEdit42.Location = new System.Drawing.Point(736, 144);
            this.pictureEdit42.Name = "pictureEdit42";
            this.pictureEdit42.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit42.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit42.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.pictureEdit42.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit42.Properties.UseParentBackground = true;
            this.pictureEdit42.Size = new System.Drawing.Size(28, 32);
            this.pictureEdit42.TabIndex = 330;
            this.pictureEdit42.Tag = "Mic017";
            this.pictureEdit42.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit42.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.pictureEdit42.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // Mic_1
            // 
            this.Mic_1.AllowDrop = true;
            this.Mic_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Mic_1.EditValue = ((object)(resources.GetObject("Mic_1.EditValue")));
            this.Mic_1.Location = new System.Drawing.Point(400, 28);
            this.Mic_1.Name = "Mic_1";
            this.Mic_1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Mic_1.Properties.Appearance.Options.UseBackColor = true;
            this.Mic_1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Mic_1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.Mic_1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.Mic_1.Properties.UseParentBackground = true;
            this.Mic_1.Size = new System.Drawing.Size(28, 32);
            this.Mic_1.TabIndex = 298;
            this.Mic_1.Tag = "Mic001";
            this.Mic_1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.Mic_1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Mic_MouseDoubleClick);
            this.Mic_1.MouseEnter += new System.EventHandler(this.Mic_MouseEnter);
            // 
            // pgDelegateList
            // 
            this.pgDelegateList.Controls.Add(this.navControlSpeechDelegates);
            this.pgDelegateList.Controls.Add(this.gridDelegates);
            this.pgDelegateList.Name = "pgDelegateList";
            this.pgDelegateList.Size = new System.Drawing.Size(949, 628);
            this.pgDelegateList.Text = "������ �����������";
            // 
            // navControlSpeechDelegates
            // 
            this.navControlSpeechDelegates.Buttons.Append.Visible = false;
            this.navControlSpeechDelegates.Buttons.CancelEdit.Visible = false;
            this.navControlSpeechDelegates.Buttons.Edit.Visible = false;
            this.navControlSpeechDelegates.Buttons.EndEdit.Visible = false;
            this.navControlSpeechDelegates.Buttons.ImageList = this.ButtonImages;
            this.navControlSpeechDelegates.Buttons.Remove.Visible = false;
            this.navControlSpeechDelegates.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "�������� ������", "Append"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "������� �� ������", "Remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 14, false, true, "����� �� ������", "Queue_Up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, false, true, "���� �� ������", "Queue_Down")});
            this.navControlSpeechDelegates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.navControlSpeechDelegates.Location = new System.Drawing.Point(0, 596);
            this.navControlSpeechDelegates.Name = "navControlSpeechDelegates";
            this.navControlSpeechDelegates.Size = new System.Drawing.Size(949, 32);
            this.navControlSpeechDelegates.TabIndex = 241;
            this.navControlSpeechDelegates.Text = "controlNavigator1";
            this.navControlSpeechDelegates.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.navControlSpeechDelegates_ButtonClick);
            // 
            // ButtonImages
            // 
            this.ButtonImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ButtonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ButtonImages.ImageStream")));
            // 
            // gridDelegates
            // 
            this.gridDelegates.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridDelegates.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridDelegates.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridDelegates.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridDelegates.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridDelegates.Location = new System.Drawing.Point(0, 0);
            this.gridDelegates.MainView = this.gridviewSpeechDelegates;
            this.gridDelegates.Name = "gridDelegates";
            this.gridDelegates.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.gridDelegates.Size = new System.Drawing.Size(949, 571);
            this.gridDelegates.TabIndex = 9;
            this.gridDelegates.UseEmbeddedNavigator = true;
            this.gridDelegates.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridviewSpeechDelegates});
            this.gridDelegates.DoubleClick += new System.EventHandler(this.gridDelegates_DoubleClick);
            // 
            // gridviewSpeechDelegates
            // 
            this.gridviewSpeechDelegates.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridviewSpeechDelegates.Appearance.Row.Options.UseFont = true;
            this.gridviewSpeechDelegates.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colRegionName,
            this.colFraction,
            this.colPartyName,
            this.colStatus,
            this.colMicNum,
            this.colQNum,
            this.colSpeech});
            this.gridviewSpeechDelegates.CustomizationFormBounds = new System.Drawing.Rectangle(862, 678, 208, 191);
            this.gridviewSpeechDelegates.GridControl = this.gridDelegates;
            this.gridviewSpeechDelegates.GroupCount = 1;
            this.gridviewSpeechDelegates.Name = "gridviewSpeechDelegates";
            this.gridviewSpeechDelegates.OptionsBehavior.AllowIncrementalSearch = true;
            this.gridviewSpeechDelegates.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridviewSpeechDelegates.OptionsDetail.EnableMasterViewMode = false;
            this.gridviewSpeechDelegates.OptionsPrint.PrintPreview = true;
            this.gridviewSpeechDelegates.OptionsView.ShowGroupPanel = false;
            this.gridviewSpeechDelegates.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStatus, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQNum, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridviewSpeechDelegates.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridviewSpeechDelegates_RowCellStyle);
            this.gridviewSpeechDelegates.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridviewSpeechDelegates_CellValueChanging);
            this.gridviewSpeechDelegates.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridviewSpeechDelegates_MouseMove);
            this.gridviewSpeechDelegates.MouseEnter += new System.EventHandler(this.gridviewSpeechDelegates_MouseEnter);
            // 
            // colFullName
            // 
            this.colFullName.Caption = "���";
            this.colFullName.FieldName = "idDelegate.FullName";
            this.colFullName.MinWidth = 100;
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 2;
            this.colFullName.Width = 290;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "������";
            this.colRegionName.FieldName = "idDelegate.idRegion.Name";
            this.colRegionName.MinWidth = 100;
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 4;
            this.colRegionName.Width = 154;
            // 
            // colFraction
            // 
            this.colFraction.Caption = "�������";
            this.colFraction.FieldName = "idDelegate.idFraction.Name";
            this.colFraction.MinWidth = 100;
            this.colFraction.Name = "colFraction";
            this.colFraction.OptionsColumn.AllowEdit = false;
            this.colFraction.OptionsColumn.ReadOnly = true;
            this.colFraction.Visible = true;
            this.colFraction.VisibleIndex = 5;
            this.colFraction.Width = 113;
            // 
            // colPartyName
            // 
            this.colPartyName.Caption = "������";
            this.colPartyName.FieldName = "idDelegate.idParty.Name";
            this.colPartyName.MinWidth = 100;
            this.colPartyName.Name = "colPartyName";
            this.colPartyName.OptionsColumn.AllowEdit = false;
            this.colPartyName.OptionsColumn.ReadOnly = true;
            this.colPartyName.Width = 117;
            // 
            // colStatus
            // 
            this.colStatus.Caption = " ";
            this.colStatus.FieldName = "Status";
            this.colStatus.MinWidth = 100;
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsColumn.ShowCaption = false;
            this.colStatus.Width = 100;
            // 
            // colMicNum
            // 
            this.colMicNum.Caption = "���";
            this.colMicNum.FieldName = "idSeat.MicNum";
            this.colMicNum.Name = "colMicNum";
            this.colMicNum.OptionsColumn.AllowEdit = false;
            this.colMicNum.OptionsColumn.ReadOnly = true;
            this.colMicNum.Visible = true;
            this.colMicNum.VisibleIndex = 3;
            this.colMicNum.Width = 56;
            // 
            // colQNum
            // 
            this.colQNum.Caption = "�������";
            this.colQNum.FieldName = "QNum";
            this.colQNum.Name = "colQNum";
            this.colQNum.OptionsColumn.FixedWidth = true;
            this.colQNum.OptionsFilter.AllowAutoFilter = false;
            this.colQNum.OptionsFilter.AllowFilter = false;
            this.colQNum.Visible = true;
            this.colQNum.VisibleIndex = 0;
            this.colQNum.Width = 80;
            // 
            // colSpeech
            // 
            this.colSpeech.Caption = "���/����";
            this.colSpeech.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSpeech.FieldName = "IsSpeech";
            this.colSpeech.Name = "colSpeech";
            this.colSpeech.Visible = true;
            this.colSpeech.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.PictureChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit3.PictureChecked")));
            this.repositoryItemCheckEdit3.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit3.PictureGrayed")));
            this.repositoryItemCheckEdit3.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit3.PictureUnchecked")));
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonText = null;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barbtnReport,
            this.btnExit,
            this.btnCloseTheme,
            this.barInfoText});
            this.ribbonControl1.LargeImages = this.RibbonImages;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 18;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowCategoryInCaption = false;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(956, 94);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "���������";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.LargeImageIndex = 0;
            this.barButtonItem1.LargeWidth = 150;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonItem2.Caption = "���/���� ����� �� ��������";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.LargeImageIndex = 1;
            this.barButtonItem2.LargeWidth = 190;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barbtnReport
            // 
            this.barbtnReport.Caption = "�������� �� ������������";
            this.barbtnReport.Id = 7;
            this.barbtnReport.LargeImageIndex = 8;
            this.barbtnReport.LargeWidth = 190;
            this.barbtnReport.Name = "barbtnReport";
            this.barbtnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnReport_ItemClick);
            // 
            // btnExit
            // 
            this.btnExit.Caption = "�����";
            this.btnExit.Id = 8;
            this.btnExit.LargeImageIndex = 16;
            this.btnExit.LargeWidth = 195;
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnCloseTheme
            // 
            this.btnCloseTheme.Caption = "��������� �����������";
            this.btnCloseTheme.Hint = "����� � ��������� �����������";
            this.btnCloseTheme.Id = 9;
            this.btnCloseTheme.LargeImageIndex = 14;
            this.btnCloseTheme.LargeWidth = 200;
            this.btnCloseTheme.Name = "btnCloseTheme";
            this.btnCloseTheme.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThemeClose_Click);
            // 
            // barInfoText
            // 
            this.barInfoText.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barInfoText.Caption = "barStaticItem1";
            this.barInfoText.Id = 17;
            this.barInfoText.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barInfoText.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red;
            this.barInfoText.ItemAppearance.Normal.Options.UseFont = true;
            this.barInfoText.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barInfoText.Name = "barInfoText";
            this.barInfoText.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barInfoText.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barInfoText.Width = 350;
            // 
            // RibbonImages
            // 
            this.RibbonImages.ImageSize = new System.Drawing.Size(48, 48);
            this.RibbonImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("RibbonImages.ImageStream")));
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "������������";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "�������� � ����";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barbtnReport);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "�����";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnExit);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnCloseTheme);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "������";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.barInfoText);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(2, 78);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1390, 24);
            // 
            // InfoTabControl
            // 
            this.InfoTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoTabControl.Location = new System.Drawing.Point(0, 0);
            this.InfoTabControl.Name = "InfoTabControl";
            this.InfoTabControl.SelectedTabPage = this.pgStatemInfo;
            this.InfoTabControl.Size = new System.Drawing.Size(432, 750);
            this.InfoTabControl.TabIndex = 0;
            this.InfoTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgStatemInfo,
            this.pgDelegateInfo,
            this.pgDelegatesHall});
            // 
            // pgStatemInfo
            // 
            this.pgStatemInfo.Controls.Add(this.navigatorSpeechDelegates);
            this.pgStatemInfo.Controls.Add(this.PropertiesControlStatem);
            this.pgStatemInfo.Name = "pgStatemInfo";
            this.pgStatemInfo.Size = new System.Drawing.Size(425, 722);
            this.pgStatemInfo.Text = "���������� � �����������";
            // 
            // navigatorSpeechDelegates
            // 
            this.navigatorSpeechDelegates.Buttons.Append.Hint = "�����";
            this.navigatorSpeechDelegates.Buttons.Append.Visible = false;
            this.navigatorSpeechDelegates.Buttons.CancelEdit.Visible = false;
            this.navigatorSpeechDelegates.Buttons.EndEdit.Visible = false;
            this.navigatorSpeechDelegates.Buttons.ImageList = this.ButtonImages;
            this.navigatorSpeechDelegates.Buttons.Next.Hint = "�����";
            this.navigatorSpeechDelegates.Buttons.Remove.Visible = false;
            this.navigatorSpeechDelegates.Location = new System.Drawing.Point(199, 565);
            this.navigatorSpeechDelegates.Name = "navigatorSpeechDelegates";
            this.navigatorSpeechDelegates.Size = new System.Drawing.Size(117, 31);
            this.navigatorSpeechDelegates.TabIndex = 264;
            this.navigatorSpeechDelegates.Text = "dataNavigator1";
            this.navigatorSpeechDelegates.Visible = false;
            this.navigatorSpeechDelegates.PositionChanged += new System.EventHandler(this.navigatorSpeechDelegates_PositionChanged);
            // 
            // PropertiesControlStatem
            // 
            this.PropertiesControlStatem.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlStatem.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlStatem.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlStatem.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlStatem.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlStatem.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlStatem.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlStatem.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlStatem.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControlStatem.Name = "PropertiesControlStatem";
            this.PropertiesControlStatem.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlStatem.RecordWidth = 124;
            this.PropertiesControlStatem.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit5,
            this.repositoryItemGridLookUpEdit5,
            this.repositoryItemComboBox30,
            this.repositoryItemComboBox31,
            this.repositoryItemComboBox32,
            this.repositoryItemComboBox33,
            this.repositoryItemComboBox34,
            this.repositoryItemComboBox35,
            this.repositoryItemDateEdit5,
            this.repositoryItemComboBox36,
            this.repositoryItemComboBox37,
            this.repositoryItemComboBox38,
            this.repositoryItemComboBox39,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemDateEdit7,
            this.repositoryItemMemoEdit5,
            this.repositoryItemComboBox40,
            this.repositoryItemSpinEdit3,
            this.repositoryItemComboBox41,
            this.repositoryItemComboBox42,
            this.repositoryItemMemoEdit6,
            this.repositoryItemMemoEdit7,
            this.repositoryItemCheckEdit4});
            this.PropertiesControlStatem.RowHeaderWidth = 76;
            this.PropertiesControlStatem.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catCharacteristics_st,
            this.catTotals_st,
            this.catNotes});
            this.PropertiesControlStatem.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlStatem.Size = new System.Drawing.Size(425, 510);
            this.PropertiesControlStatem.TabIndex = 13;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemGridLookUpEdit5
            // 
            this.repositoryItemGridLookUpEdit5.AutoHeight = false;
            this.repositoryItemGridLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit5.Name = "repositoryItemGridLookUpEdit5";
            this.repositoryItemGridLookUpEdit5.View = this.gridView11;
            // 
            // gridView11
            // 
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox30
            // 
            this.repositoryItemComboBox30.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox30.ImmediatePopup = true;
            this.repositoryItemComboBox30.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox30.Name = "repositoryItemComboBox30";
            // 
            // repositoryItemComboBox31
            // 
            this.repositoryItemComboBox31.AutoHeight = false;
            this.repositoryItemComboBox31.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox31.ImmediatePopup = true;
            this.repositoryItemComboBox31.Name = "repositoryItemComboBox31";
            this.repositoryItemComboBox31.Sorted = true;
            // 
            // repositoryItemComboBox32
            // 
            this.repositoryItemComboBox32.AutoHeight = false;
            this.repositoryItemComboBox32.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox32.Name = "repositoryItemComboBox32";
            // 
            // repositoryItemComboBox33
            // 
            this.repositoryItemComboBox33.AutoHeight = false;
            this.repositoryItemComboBox33.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox33.ImmediatePopup = true;
            this.repositoryItemComboBox33.Name = "repositoryItemComboBox33";
            // 
            // repositoryItemComboBox34
            // 
            this.repositoryItemComboBox34.AutoHeight = false;
            this.repositoryItemComboBox34.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox34.Name = "repositoryItemComboBox34";
            // 
            // repositoryItemComboBox35
            // 
            this.repositoryItemComboBox35.AutoHeight = false;
            this.repositoryItemComboBox35.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox35.Name = "repositoryItemComboBox35";
            // 
            // repositoryItemDateEdit5
            // 
            this.repositoryItemDateEdit5.AutoHeight = false;
            this.repositoryItemDateEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit5.Name = "repositoryItemDateEdit5";
            this.repositoryItemDateEdit5.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemComboBox36
            // 
            this.repositoryItemComboBox36.AutoHeight = false;
            this.repositoryItemComboBox36.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox36.Name = "repositoryItemComboBox36";
            // 
            // repositoryItemComboBox37
            // 
            this.repositoryItemComboBox37.AutoHeight = false;
            this.repositoryItemComboBox37.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox37.Name = "repositoryItemComboBox37";
            // 
            // repositoryItemComboBox38
            // 
            this.repositoryItemComboBox38.AutoHeight = false;
            this.repositoryItemComboBox38.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox38.Name = "repositoryItemComboBox38";
            // 
            // repositoryItemComboBox39
            // 
            this.repositoryItemComboBox39.AutoHeight = false;
            this.repositoryItemComboBox39.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox39.Name = "repositoryItemComboBox39";
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemDateEdit7
            // 
            this.repositoryItemDateEdit7.AutoHeight = false;
            this.repositoryItemDateEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit7.Name = "repositoryItemDateEdit7";
            this.repositoryItemDateEdit7.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // repositoryItemMemoEdit5
            // 
            this.repositoryItemMemoEdit5.Name = "repositoryItemMemoEdit5";
            // 
            // repositoryItemComboBox40
            // 
            this.repositoryItemComboBox40.AutoHeight = false;
            this.repositoryItemComboBox40.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox40.Name = "repositoryItemComboBox40";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // repositoryItemComboBox41
            // 
            this.repositoryItemComboBox41.AutoHeight = false;
            this.repositoryItemComboBox41.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox41.Name = "repositoryItemComboBox41";
            // 
            // repositoryItemComboBox42
            // 
            this.repositoryItemComboBox42.AutoHeight = false;
            this.repositoryItemComboBox42.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox42.Name = "repositoryItemComboBox42";
            // 
            // repositoryItemMemoEdit6
            // 
            this.repositoryItemMemoEdit6.Name = "repositoryItemMemoEdit6";
            // 
            // repositoryItemMemoEdit7
            // 
            this.repositoryItemMemoEdit7.Name = "repositoryItemMemoEdit7";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.AutoWidth = true;
            this.repositoryItemCheckEdit4.Caption = " ��������� �������� �� ��������� �������";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // catCharacteristics_st
            // 
            this.catCharacteristics_st.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowTask_st,
            this.rowName_st,
            this.rowStartDate_st,
            this.rowDescription_st,
            this.rowStatus_st});
            this.catCharacteristics_st.Height = 19;
            this.catCharacteristics_st.Name = "catCharacteristics_st";
            this.catCharacteristics_st.Properties.Caption = "��������������";
            // 
            // rowTask_st
            // 
            this.rowTask_st.Height = 30;
            this.rowTask_st.Name = "rowTask_st";
            this.rowTask_st.Properties.Caption = "����� �������� ���";
            this.rowTask_st.Properties.FieldName = "idTask.CaptionItem";
            // 
            // rowName_st
            // 
            this.rowName_st.Height = 31;
            this.rowName_st.Name = "rowName_st";
            this.rowName_st.Properties.Caption = "�������� ����";
            this.rowName_st.Properties.FieldName = "idTask.Caption";
            // 
            // rowStartDate_st
            // 
            this.rowStartDate_st.Height = 32;
            this.rowStartDate_st.Name = "rowStartDate_st";
            this.rowStartDate_st.Properties.Caption = "������ ����������";
            this.rowStartDate_st.Properties.FieldName = "DateTimeStart";
            this.rowStartDate_st.Properties.Format.FormatString = "H:mm (dd/MM/yy)";
            this.rowStartDate_st.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // rowDescription_st
            // 
            this.rowDescription_st.Height = 58;
            this.rowDescription_st.Name = "rowDescription_st";
            this.rowDescription_st.Properties.Caption = "��������";
            this.rowDescription_st.Properties.FieldName = "idTask.Description";
            // 
            // rowStatus_st
            // 
            this.rowStatus_st.Height = 25;
            this.rowStatus_st.Name = "rowStatus_st";
            this.rowStatus_st.Properties.Caption = "���������";
            this.rowStatus_st.Properties.FieldName = "Status";
            this.rowStatus_st.Properties.ReadOnly = true;
            // 
            // catTotals_st
            // 
            this.catTotals_st.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowDelegateQnty_st,
            this.rowSpeechTimeTotal_st,
            this.rowAvrgTime_st});
            this.catTotals_st.Name = "catTotals_st";
            this.catTotals_st.Properties.Caption = "�����";
            // 
            // rowDelegateQnty_st
            // 
            this.rowDelegateQnty_st.Height = 22;
            this.rowDelegateQnty_st.Name = "rowDelegateQnty_st";
            this.rowDelegateQnty_st.Properties.Caption = "���������";
            this.rowDelegateQnty_st.Properties.FieldName = "DelegateQnty_F";
            this.rowDelegateQnty_st.Properties.Value = "7 ���";
            // 
            // rowSpeechTimeTotal_st
            // 
            this.rowSpeechTimeTotal_st.Height = 21;
            this.rowSpeechTimeTotal_st.Name = "rowSpeechTimeTotal_st";
            this.rowSpeechTimeTotal_st.Properties.Caption = "����� �����, ���";
            this.rowSpeechTimeTotal_st.Properties.FieldName = "SpeechTimeTotal_F";
            this.rowSpeechTimeTotal_st.Properties.Value = "12";
            // 
            // rowAvrgTime_st
            // 
            this.rowAvrgTime_st.Height = 21;
            this.rowAvrgTime_st.Name = "rowAvrgTime_st";
            this.rowAvrgTime_st.Properties.Caption = "������� �����, ���";
            this.rowAvrgTime_st.Properties.FieldName = "AvrgTime_F";
            this.rowAvrgTime_st.Properties.Value = "";
            // 
            // catNotes
            // 
            this.catNotes.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowNotes_st});
            this.catNotes.Name = "catNotes";
            this.catNotes.Properties.Caption = "�������";
            // 
            // rowNotes_st
            // 
            this.rowNotes_st.Height = 80;
            this.rowNotes_st.Name = "rowNotes_st";
            this.rowNotes_st.Properties.FieldName = "Notes";
            this.rowNotes_st.Properties.RowEdit = this.repositoryItemMemoEdit6;
            this.rowNotes_st.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowNotes_st.Properties.Value = "";
            // 
            // pgDelegateInfo
            // 
            this.pgDelegateInfo.Controls.Add(this.PropertiesControlDelegate);
            this.pgDelegateInfo.Name = "pgDelegateInfo";
            this.pgDelegateInfo.Size = new System.Drawing.Size(425, 722);
            this.pgDelegateInfo.Text = "���������� � ��������";
            // 
            // PropertiesControlDelegate
            // 
            this.PropertiesControlDelegate.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropertiesControlDelegate.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropertiesControlDelegate.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropertiesControlDelegate.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropertiesControlDelegate.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropertiesControlDelegate.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropertiesControlDelegate.Location = new System.Drawing.Point(0, 0);
            this.PropertiesControlDelegate.Name = "PropertiesControlDelegate";
            this.PropertiesControlDelegate.OptionsView.AutoScaleBands = true;
            this.PropertiesControlDelegate.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropertiesControlDelegate.OptionsView.ShowEmptyRowImage = true;
            this.PropertiesControlDelegate.RecordWidth = 114;
            this.PropertiesControlDelegate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit9,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemComboBox2,
            this.repositoryItemComboBox4,
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6,
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoEdit1});
            this.PropertiesControlDelegate.RowHeaderWidth = 86;
            this.PropertiesControlDelegate.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO,
            this.catRegion,
            this.catInfo,
            this.catSeat,
            this.catResults,
            this.categoryRow1});
            this.PropertiesControlDelegate.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropertiesControlDelegate.Size = new System.Drawing.Size(425, 509);
            this.PropertiesControlDelegate.TabIndex = 10;
            this.PropertiesControlDelegate.Click += new System.EventHandler(this.PropertiesControl_Click);
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.ImmediatePopup = true;
            this.repositoryItemComboBox2.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.AutoHeight = false;
            this.repositoryItemComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox4.ImmediatePopup = true;
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            this.repositoryItemComboBox4.Sorted = true;
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.AutoHeight = false;
            this.repositoryItemComboBox5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.AutoHeight = false;
            this.repositoryItemComboBox6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox6.ImmediatePopup = true;
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "";
            this.repositoryItemCheckEdit1.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // catFIO
            // 
            this.catFIO.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName,
            this.rowFirstName,
            this.rowSecondName});
            this.catFIO.Height = 19;
            this.catFIO.Name = "catFIO";
            this.catFIO.Properties.Caption = "���";
            // 
            // rowLastName
            // 
            this.rowLastName.Height = 27;
            this.rowLastName.Name = "rowLastName";
            this.rowLastName.Properties.Caption = "�������";
            this.rowLastName.Properties.FieldName = "idDelegate.LastName";
            this.rowLastName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName.Properties.Value = "��������";
            // 
            // rowFirstName
            // 
            this.rowFirstName.Height = 20;
            this.rowFirstName.Name = "rowFirstName";
            this.rowFirstName.Properties.Caption = "���";
            this.rowFirstName.Properties.FieldName = "idDelegate.FirstName";
            this.rowFirstName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFirstName.Properties.Value = "������";
            // 
            // rowSecondName
            // 
            this.rowSecondName.Height = 20;
            this.rowSecondName.Name = "rowSecondName";
            this.rowSecondName.Properties.Caption = "��������";
            this.rowSecondName.Properties.FieldName = "idDelegate.SecondName";
            this.rowSecondName.Properties.Value = "����������";
            // 
            // catRegion
            // 
            this.catRegion.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName,
            this.rowRegionNum});
            this.catRegion.Name = "catRegion";
            this.catRegion.Properties.Caption = "������";
            // 
            // rowRegionName
            // 
            this.rowRegionName.Height = 20;
            this.rowRegionName.Name = "rowRegionName";
            this.rowRegionName.Properties.Caption = "������";
            this.rowRegionName.Properties.FieldName = "idDelegate.idRegion.Name";
            this.rowRegionName.Properties.RowEdit = this.repositoryItemComboBox4;
            this.rowRegionName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionName.Properties.Value = "������-��������";
            // 
            // rowRegionNum
            // 
            this.rowRegionNum.Height = 20;
            this.rowRegionNum.Name = "rowRegionNum";
            this.rowRegionNum.Properties.Caption = "� �������";
            this.rowRegionNum.Properties.FieldName = "idDelegate.idRegion.Number";
            this.rowRegionNum.Properties.RowEdit = this.repositoryItemComboBox5;
            this.rowRegionNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionNum.Properties.Value = "� 25";
            // 
            // catInfo
            // 
            this.catInfo.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction,
            this.rowPartyName});
            this.catInfo.Name = "catInfo";
            this.catInfo.Properties.Caption = "�������������� ����������";
            // 
            // rowFraction
            // 
            this.rowFraction.Height = 25;
            this.rowFraction.Name = "rowFraction";
            this.rowFraction.Properties.Caption = "�������";
            this.rowFraction.Properties.FieldName = "idDelegate.idFraction.Name";
            this.rowFraction.Properties.RowEdit = this.repositoryItemComboBox6;
            this.rowFraction.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFraction.Properties.Value = "�����������������";
            // 
            // rowPartyName
            // 
            this.rowPartyName.Height = 23;
            this.rowPartyName.Name = "rowPartyName";
            this.rowPartyName.Properties.Caption = "������";
            this.rowPartyName.Properties.FieldName = "idDelegate.idParty.Name";
            this.rowPartyName.Properties.RowEdit = this.repositoryItemComboBox2;
            this.rowPartyName.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowPartyName.Properties.Value = "������ 1";
            // 
            // catSeat
            // 
            this.catSeat.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum,
            this.rowRowNum,
            this.rowSeatNum});
            this.catSeat.Name = "catSeat";
            // 
            // rowMicNum
            // 
            this.rowMicNum.Height = 16;
            this.rowMicNum.Name = "rowMicNum";
            this.rowMicNum.Properties.Caption = "�������� �";
            this.rowMicNum.Properties.FieldName = "idSeat.MicNum";
            this.rowMicNum.Properties.Value = "73";
            // 
            // rowRowNum
            // 
            this.rowRowNum.Name = "rowRowNum";
            this.rowRowNum.Properties.Caption = "��� �";
            this.rowRowNum.Properties.FieldName = "idSeat.RowNum";
            this.rowRowNum.Properties.Value = "7";
            // 
            // rowSeatNum
            // 
            this.rowSeatNum.Name = "rowSeatNum";
            this.rowSeatNum.Properties.Caption = "����� �";
            this.rowSeatNum.Properties.FieldName = "idSeat.SeatNum";
            this.rowSeatNum.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowSeatNum.Properties.Value = "3";
            // 
            // catResults
            // 
            this.catResults.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowSpeechType_std,
            this.rowState_std,
            this.rowStartTime_std,
            this.rowLastDuration,
            this.rowNotes_std});
            this.catResults.Name = "catResults";
            this.catResults.Properties.Caption = "����� �����������";
            // 
            // rowSpeechType_std
            // 
            this.rowSpeechType_std.Height = 23;
            this.rowSpeechType_std.Name = "rowSpeechType_std";
            this.rowSpeechType_std.Properties.Caption = "��� �����������";
            this.rowSpeechType_std.Properties.FieldName = "idSpeechType.Name";
            this.rowSpeechType_std.Properties.ReadOnly = true;
            this.rowSpeechType_std.Properties.Value = "������";
            // 
            // rowState_std
            // 
            this.rowState_std.Height = 31;
            this.rowState_std.Name = "rowState_std";
            this.rowState_std.Properties.Caption = "�������� �����������";
            this.rowState_std.Properties.FieldName = "State_F";
            this.rowState_std.Properties.RowEdit = this.repositoryItemCheckEdit1;
            this.rowState_std.Properties.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.rowState_std.Properties.Value = true;
            // 
            // rowStartTime_std
            // 
            this.rowStartTime_std.Height = 24;
            this.rowStartTime_std.Name = "rowStartTime_std";
            this.rowStartTime_std.Properties.Caption = "����� ����������� �";
            this.rowStartTime_std.Properties.FieldName = "SpeechTimeStart";
            this.rowStartTime_std.Properties.Format.FormatString = "H:mm (dd/MM/yy)";
            this.rowStartTime_std.Properties.Format.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.rowStartTime_std.Properties.ReadOnly = true;
            // 
            // rowLastDuration
            // 
            this.rowLastDuration.Height = 22;
            this.rowLastDuration.Name = "rowLastDuration";
            this.rowLastDuration.Properties.Caption = "������������ �����������";
            this.rowLastDuration.Properties.FieldName = "SpeechDurationLast_F";
            // 
            // rowNotes_std
            // 
            this.rowNotes_std.Height = 88;
            this.rowNotes_std.Name = "rowNotes_std";
            this.rowNotes_std.Properties.Caption = "�������";
            this.rowNotes_std.Properties.FieldName = "Notes";
            this.rowNotes_std.Properties.RowEdit = this.repositoryItemMemoEdit1;
            // 
            // categoryRow1
            // 
            this.categoryRow1.Name = "categoryRow1";
            // 
            // pgDelegatesHall
            // 
            this.pgDelegatesHall.Controls.Add(this.PropCntrlDelegate2);
            this.pgDelegatesHall.Name = "pgDelegatesHall";
            this.pgDelegatesHall.PageVisible = false;
            this.pgDelegatesHall.Size = new System.Drawing.Size(425, 722);
            this.pgDelegatesHall.Text = "���������� � ��������";
            // 
            // PropCntrlDelegate2
            // 
            this.PropCntrlDelegate2.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.PropCntrlDelegate2.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.PropCntrlDelegate2.Appearance.ReadOnlyRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PropCntrlDelegate2.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.PropCntrlDelegate2.Appearance.RecordValue.ForeColor = System.Drawing.Color.Navy;
            this.PropCntrlDelegate2.Appearance.RecordValue.Options.UseForeColor = true;
            this.PropCntrlDelegate2.Dock = System.Windows.Forms.DockStyle.Top;
            this.PropCntrlDelegate2.LayoutStyle = DevExpress.XtraVerticalGrid.LayoutViewStyle.SingleRecordView;
            this.PropCntrlDelegate2.Location = new System.Drawing.Point(0, 0);
            this.PropCntrlDelegate2.Name = "PropCntrlDelegate2";
            this.PropCntrlDelegate2.OptionsView.AutoScaleBands = true;
            this.PropCntrlDelegate2.OptionsView.FixRowHeaderPanelWidth = true;
            this.PropCntrlDelegate2.OptionsView.ShowEmptyRowImage = true;
            this.PropCntrlDelegate2.RecordWidth = 114;
            this.PropCntrlDelegate2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox7,
            this.repositoryItemComboBox8,
            this.repositoryItemCheckEdit2,
            this.repositoryItemMemoEdit2});
            this.PropCntrlDelegate2.RowHeaderWidth = 86;
            this.PropCntrlDelegate2.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.catFIO_H,
            this.catRegion_H,
            this.catInfo_H,
            this.catSeat_H});
            this.PropCntrlDelegate2.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.PropCntrlDelegate2.Size = new System.Drawing.Size(425, 455);
            this.PropCntrlDelegate2.TabIndex = 11;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.ImmediatePopup = true;
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "������ 3",
            "������ 4"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.AutoHeight = false;
            this.repositoryItemComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox3.ImmediatePopup = true;
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            this.repositoryItemComboBox3.Sorted = true;
            // 
            // repositoryItemComboBox7
            // 
            this.repositoryItemComboBox7.AutoHeight = false;
            this.repositoryItemComboBox7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.repositoryItemComboBox7.Name = "repositoryItemComboBox7";
            // 
            // repositoryItemComboBox8
            // 
            this.repositoryItemComboBox8.AutoHeight = false;
            this.repositoryItemComboBox8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox8.ImmediatePopup = true;
            this.repositoryItemComboBox8.Name = "repositoryItemComboBox8";
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "";
            this.repositoryItemCheckEdit2.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // catFIO_H
            // 
            this.catFIO_H.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowLastName_H,
            this.rowFirstName_H,
            this.rowSecondName_H});
            this.catFIO_H.Height = 19;
            this.catFIO_H.Name = "catFIO_H";
            this.catFIO_H.Properties.Caption = "���";
            // 
            // rowLastName_H
            // 
            this.rowLastName_H.Height = 27;
            this.rowLastName_H.Name = "rowLastName_H";
            this.rowLastName_H.Properties.Caption = "�������";
            this.rowLastName_H.Properties.FieldName = "idDelegate.LastName";
            this.rowLastName_H.Properties.ReadOnly = true;
            this.rowLastName_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowLastName_H.Properties.Value = "��������";
            // 
            // rowFirstName_H
            // 
            this.rowFirstName_H.Height = 20;
            this.rowFirstName_H.Name = "rowFirstName_H";
            this.rowFirstName_H.Properties.Caption = "���";
            this.rowFirstName_H.Properties.FieldName = "idDelegate.FirstName";
            this.rowFirstName_H.Properties.ReadOnly = true;
            this.rowFirstName_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFirstName_H.Properties.Value = "������";
            // 
            // rowSecondName_H
            // 
            this.rowSecondName_H.Height = 20;
            this.rowSecondName_H.Name = "rowSecondName_H";
            this.rowSecondName_H.Properties.Caption = "��������";
            this.rowSecondName_H.Properties.FieldName = "idDelegate.SecondName";
            this.rowSecondName_H.Properties.ReadOnly = true;
            this.rowSecondName_H.Properties.Value = "����������";
            // 
            // catRegion_H
            // 
            this.catRegion_H.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowRegionName_H,
            this.rowRegionNum_H});
            this.catRegion_H.Name = "catRegion_H";
            this.catRegion_H.Properties.Caption = "������";
            // 
            // rowRegionName_H
            // 
            this.rowRegionName_H.Height = 20;
            this.rowRegionName_H.Name = "rowRegionName_H";
            this.rowRegionName_H.Properties.Caption = "������";
            this.rowRegionName_H.Properties.FieldName = "idDelegate.idRegion.Name";
            this.rowRegionName_H.Properties.RowEdit = this.repositoryItemComboBox3;
            this.rowRegionName_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionName_H.Properties.Value = "������-��������";
            // 
            // rowRegionNum_H
            // 
            this.rowRegionNum_H.Height = 20;
            this.rowRegionNum_H.Name = "rowRegionNum_H";
            this.rowRegionNum_H.Properties.Caption = "� �������";
            this.rowRegionNum_H.Properties.FieldName = "idDelegate.idRegion.Number";
            this.rowRegionNum_H.Properties.RowEdit = this.repositoryItemComboBox7;
            this.rowRegionNum_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowRegionNum_H.Properties.Value = "� 25";
            // 
            // catInfo_H
            // 
            this.catInfo_H.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowFraction_H,
            this.rowParty_H});
            this.catInfo_H.Name = "catInfo_H";
            this.catInfo_H.Properties.Caption = "�������������� ����������";
            // 
            // rowFraction_H
            // 
            this.rowFraction_H.Height = 25;
            this.rowFraction_H.Name = "rowFraction_H";
            this.rowFraction_H.Properties.Caption = "�������";
            this.rowFraction_H.Properties.FieldName = "idDelegate.idFraction.Name";
            this.rowFraction_H.Properties.RowEdit = this.repositoryItemComboBox8;
            this.rowFraction_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowFraction_H.Properties.Value = "�����������������";
            // 
            // rowParty_H
            // 
            this.rowParty_H.Height = 23;
            this.rowParty_H.Name = "rowParty_H";
            this.rowParty_H.Properties.Caption = "������";
            this.rowParty_H.Properties.FieldName = "idDelegate.idParty.Name";
            this.rowParty_H.Properties.ReadOnly = true;
            this.rowParty_H.Properties.RowEdit = this.repositoryItemComboBox1;
            this.rowParty_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowParty_H.Properties.Value = "������ 1";
            // 
            // catSeat_H
            // 
            this.catSeat_H.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowMicNum_H,
            this.rowRowNum_H,
            this.rowSeatNum_H});
            this.catSeat_H.Name = "catSeat_H";
            // 
            // rowMicNum_H
            // 
            this.rowMicNum_H.Height = 16;
            this.rowMicNum_H.Name = "rowMicNum_H";
            this.rowMicNum_H.Properties.Caption = "�������� �";
            this.rowMicNum_H.Properties.FieldName = "MicNum";
            this.rowMicNum_H.Properties.ReadOnly = true;
            this.rowMicNum_H.Properties.Value = "73";
            // 
            // rowRowNum_H
            // 
            this.rowRowNum_H.Name = "rowRowNum_H";
            this.rowRowNum_H.Properties.Caption = "��� �";
            this.rowRowNum_H.Properties.FieldName = "RowNum";
            this.rowRowNum_H.Properties.ReadOnly = true;
            this.rowRowNum_H.Properties.Value = "7";
            // 
            // rowSeatNum_H
            // 
            this.rowSeatNum_H.Name = "rowSeatNum_H";
            this.rowSeatNum_H.Properties.Caption = "����� �";
            this.rowSeatNum_H.Properties.FieldName = "SeatNum";
            this.rowSeatNum_H.Properties.ReadOnly = true;
            this.rowSeatNum_H.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.rowSeatNum_H.Properties.Value = "3";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Black;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Controls.Add(this.ribbonStatusBar1);
            this.panelControl2.Controls.Add(this.btnClearAll);
            this.panelControl2.Controls.Add(this.chRegMode);
            this.panelControl2.Controls.Add(this.checkBox1);
            this.panelControl2.Controls.Add(this.btnspeechFinish);
            this.panelControl2.Controls.Add(this.lblProcesTime);
            this.panelControl2.Controls.Add(this.progressBarVoting);
            this.panelControl2.Controls.Add(this.lblProcess);
            this.panelControl2.Controls.Add(this.picVoteTime);
            this.panelControl2.Controls.Add(this.btnSpeechStart);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 751);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1394, 104);
            this.panelControl2.TabIndex = 34;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearAll.Appearance.Options.UseFont = true;
            this.btnClearAll.ImageIndex = 7;
            this.btnClearAll.ImageList = this.ButtonImages;
            this.btnClearAll.Location = new System.Drawing.Point(1148, 22);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(223, 44);
            this.btnClearAll.TabIndex = 267;
            this.btnClearAll.Text = "�������� ������";
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // chRegMode
            // 
            this.chRegMode.Location = new System.Drawing.Point(39, 30);
            this.chRegMode.Name = "chRegMode";
            this.chRegMode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chRegMode.Properties.Appearance.Options.UseFont = true;
            this.chRegMode.Properties.Caption = "���� ������ �� �����������...";
            this.chRegMode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style15;
            this.chRegMode.Size = new System.Drawing.Size(237, 22);
            this.chRegMode.TabIndex = 265;
            this.chRegMode.CheckedChanged += new System.EventHandler(this.RegMode_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(41, 52);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(249, 17);
            this.checkBox1.TabIndex = 263;
            this.checkBox1.Text = "������������� ���������� ������������";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.Visible = false;
            // 
            // btnspeechFinish
            // 
            this.btnspeechFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnspeechFinish.Appearance.Options.UseFont = true;
            this.btnspeechFinish.ImageIndex = 8;
            this.btnspeechFinish.ImageList = this.ButtonImages;
            this.btnspeechFinish.Location = new System.Drawing.Point(870, 22);
            this.btnspeechFinish.Name = "btnspeechFinish";
            this.btnspeechFinish.Size = new System.Drawing.Size(223, 44);
            this.btnspeechFinish.TabIndex = 257;
            this.btnspeechFinish.Text = "��������� �����������";
            this.btnspeechFinish.Click += new System.EventHandler(this.btnspeechFinish_Click);
            // 
            // lblProcesTime
            // 
            this.lblProcesTime.Appearance.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcesTime.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblProcesTime.Location = new System.Drawing.Point(601, 32);
            this.lblProcesTime.Name = "lblProcesTime";
            this.lblProcesTime.Size = new System.Drawing.Size(42, 15);
            this.lblProcesTime.TabIndex = 243;
            this.lblProcesTime.Text = "02 : 30";
            // 
            // progressBarVoting
            // 
            this.progressBarVoting.Location = new System.Drawing.Point(657, 44);
            this.progressBarVoting.Name = "progressBarVoting";
            this.progressBarVoting.Size = new System.Drawing.Size(144, 18);
            this.progressBarVoting.TabIndex = 240;
            // 
            // lblProcess
            // 
            this.lblProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcess.Location = new System.Drawing.Point(657, 18);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(108, 13);
            this.lblProcess.TabIndex = 229;
            this.lblProcess.Text = "���� �����������...";
            // 
            // picVoteTime
            // 
            this.picVoteTime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picVoteTime.EditValue = ((object)(resources.GetObject("picVoteTime.EditValue")));
            this.picVoteTime.Location = new System.Drawing.Point(561, 22);
            this.picVoteTime.Name = "picVoteTime";
            this.picVoteTime.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picVoteTime.Properties.Appearance.Options.UseBackColor = true;
            this.picVoteTime.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picVoteTime.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picVoteTime.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picVoteTime.Properties.UseParentBackground = true;
            this.picVoteTime.Size = new System.Drawing.Size(35, 47);
            this.picVoteTime.TabIndex = 228;
            this.picVoteTime.Visible = false;
            // 
            // btnSpeechStart
            // 
            this.btnSpeechStart.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSpeechStart.Appearance.Options.UseFont = true;
            this.btnSpeechStart.ImageIndex = 0;
            this.btnSpeechStart.ImageList = this.ButtonImages;
            this.btnSpeechStart.Location = new System.Drawing.Point(304, 22);
            this.btnSpeechStart.Name = "btnSpeechStart";
            this.btnSpeechStart.Size = new System.Drawing.Size(223, 44);
            this.btnSpeechStart.TabIndex = 230;
            this.btnSpeechStart.Text = "������ ������";
            this.btnSpeechStart.Click += new System.EventHandler(this.btnStateStart_Click);
            // 
            // tmrProcessSpeech
            // 
            this.tmrProcessSpeech.Interval = 1000;
            this.tmrProcessSpeech.Tick += new System.EventHandler(this.tmrProcess_Tick);
            // 
            // tmrProcessReg
            // 
            this.tmrProcessReg.Interval = 1000;
            this.tmrProcessReg.Tick += new System.EventHandler(this.tmrProcessReg_Tick);
            // 
            // ManImages
            // 
            this.ManImages.ImageSize = new System.Drawing.Size(24, 24);
            this.ManImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ManImages.ImageStream")));
            // 
            // tmrRefreshHall
            // 
            this.tmrRefreshHall.Interval = 10000;
            this.tmrRefreshHall.Tick += new System.EventHandler(this.tmrRefreshHall_Tick);
            // 
            // tmrWarningInfo
            // 
            this.tmrWarningInfo.Interval = 10000;
            this.tmrWarningInfo.Tick += new System.EventHandler(this.tmrWarningInfo_Tick);
            // 
            // SpeechModeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1394, 855);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SpeechModeForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "����� �����������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpeechModeForm_FormClosing);
            this.Load += new System.EventHandler(this.SpeechModeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainTabControl)).EndInit();
            this.mainTabControl.ResumeLayout(false);
            this.pgHall.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpSeates)).EndInit();
            this.grpSeates.ResumeLayout(false);
            this.grpSeates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit81.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit90.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit97.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit98.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit99.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit100.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit101.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit102.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit103.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit104.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit105.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit106.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit107.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit108.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit109.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit110.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit111.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit112.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit113.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit114.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit115.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit116.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit117.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit118.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit119.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit120.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit121.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit122.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit72.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Mic_1.Properties)).EndInit();
            this.pgDelegateList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridviewSpeechDelegates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoTabControl)).EndInit();
            this.InfoTabControl.ResumeLayout(false);
            this.pgStatemInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlStatem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.pgDelegateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropertiesControlDelegate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.pgDelegatesHall.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PropCntrlDelegate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chRegMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarVoting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVoteTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManImages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl InfoTabControl;
        private DevExpress.XtraTab.XtraTabPage pgStatemInfo;
        private DevExpress.XtraTab.XtraTabPage pgDelegateInfo;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlDelegate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowPartyName;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnspeechFinish;
        private DevExpress.XtraEditors.LabelControl lblProcesTime;
        private DevExpress.XtraEditors.ProgressBarControl progressBarVoting;
        private DevExpress.XtraEditors.LabelControl lblProcess;
        private DevExpress.XtraEditors.PictureEdit picVoteTime;
        private DevExpress.XtraEditors.SimpleButton btnSpeechStart;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catResults;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowState_std;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSpeechType_std;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes_std;
        private DevExpress.Utils.ImageCollection ButtonImages;
        private DevExpress.Utils.ImageCollection RibbonImages;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barbtnReport;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private System.Windows.Forms.CheckBox checkBox1;
        private DevExpress.XtraVerticalGrid.VGridControl PropertiesControlStatem;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox30;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox31;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox32;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox33;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox34;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox35;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox36;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox37;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox38;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox40;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox41;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox42;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catCharacteristics_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTask_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowName_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStartDate_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDescription_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStatus_st;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catTotals_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowDelegateQnty_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSpeechTimeTotal_st;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowAvrgTime_st;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catNotes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNotes_st;
        private DevExpress.XtraEditors.DataNavigator navigatorSpeechDelegates;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.Timer tmrProcessSpeech;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.CheckEdit chRegMode;
        private System.Windows.Forms.Timer tmrProcessReg;
        private DevExpress.XtraEditors.SimpleButton btnClearAll;
        private DevExpress.XtraTab.XtraTabControl mainTabControl;
        private DevExpress.XtraTab.XtraTabPage pgHall;
        private DevExpress.XtraTab.XtraTabPage pgDelegateList;
        private DevExpress.XtraGrid.GridControl gridDelegates;
        private DevExpress.XtraGrid.Views.Grid.GridView gridviewSpeechDelegates;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colFraction;
        private DevExpress.XtraGrid.Columns.GridColumn colPartyName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMicNum;
        private DevExpress.XtraGrid.Columns.GridColumn colQNum;
        private DevExpress.XtraEditors.ControlNavigator navControlSpeechDelegates;
        private DevExpress.XtraEditors.GroupControl grpSeates;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.PictureEdit pictureEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.PictureEdit pictureEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.PictureEdit pictureEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.PictureEdit pictureEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.PictureEdit pictureEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.PictureEdit pictureEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LabelControl labelControl97;
        private DevExpress.XtraEditors.PictureEdit pictureEdit75;
        private DevExpress.XtraEditors.LabelControl labelControl98;
        private DevExpress.XtraEditors.PictureEdit pictureEdit76;
        private DevExpress.XtraEditors.LabelControl labelControl99;
        private DevExpress.XtraEditors.PictureEdit pictureEdit77;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.PictureEdit pictureEdit78;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.PictureEdit pictureEdit79;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.PictureEdit pictureEdit80;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.PictureEdit pictureEdit81;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.PictureEdit pictureEdit82;
        private DevExpress.XtraEditors.LabelControl labelControl105;
        private DevExpress.XtraEditors.PictureEdit pictureEdit83;
        private DevExpress.XtraEditors.LabelControl labelControl106;
        private DevExpress.XtraEditors.PictureEdit pictureEdit84;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraEditors.PictureEdit pictureEdit85;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.PictureEdit pictureEdit86;
        private DevExpress.XtraEditors.LabelControl labelControl109;
        private DevExpress.XtraEditors.PictureEdit pictureEdit87;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.PictureEdit pictureEdit88;
        private DevExpress.XtraEditors.LabelControl labelControl111;
        private DevExpress.XtraEditors.PictureEdit pictureEdit89;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.PictureEdit pictureEdit90;
        private DevExpress.XtraEditors.LabelControl labelControl113;
        private DevExpress.XtraEditors.PictureEdit pictureEdit91;
        private DevExpress.XtraEditors.LabelControl labelControl114;
        private DevExpress.XtraEditors.PictureEdit pictureEdit92;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.PictureEdit pictureEdit93;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.PictureEdit pictureEdit94;
        private DevExpress.XtraEditors.LabelControl labelControl117;
        private DevExpress.XtraEditors.PictureEdit pictureEdit95;
        private DevExpress.XtraEditors.LabelControl labelControl118;
        private DevExpress.XtraEditors.PictureEdit pictureEdit96;
        private DevExpress.XtraEditors.LabelControl labelControl119;
        private DevExpress.XtraEditors.PictureEdit pictureEdit97;
        private DevExpress.XtraEditors.LabelControl labelControl120;
        private DevExpress.XtraEditors.PictureEdit pictureEdit98;
        private DevExpress.XtraEditors.LabelControl labelControl121;
        private DevExpress.XtraEditors.PictureEdit pictureEdit99;
        private DevExpress.XtraEditors.LabelControl labelControl122;
        private DevExpress.XtraEditors.PictureEdit pictureEdit100;
        private DevExpress.XtraEditors.LabelControl labelControl123;
        private DevExpress.XtraEditors.PictureEdit pictureEdit101;
        private DevExpress.XtraEditors.LabelControl labelControl124;
        private DevExpress.XtraEditors.PictureEdit pictureEdit102;
        private DevExpress.XtraEditors.LabelControl labelControl125;
        private DevExpress.XtraEditors.PictureEdit pictureEdit103;
        private DevExpress.XtraEditors.LabelControl labelControl126;
        private DevExpress.XtraEditors.PictureEdit pictureEdit104;
        private DevExpress.XtraEditors.LabelControl labelControl127;
        private DevExpress.XtraEditors.PictureEdit pictureEdit105;
        private DevExpress.XtraEditors.LabelControl labelControl128;
        private DevExpress.XtraEditors.PictureEdit pictureEdit106;
        private DevExpress.XtraEditors.LabelControl labelControl129;
        private DevExpress.XtraEditors.PictureEdit pictureEdit107;
        private DevExpress.XtraEditors.LabelControl labelControl130;
        private DevExpress.XtraEditors.PictureEdit pictureEdit108;
        private DevExpress.XtraEditors.LabelControl labelControl131;
        private DevExpress.XtraEditors.PictureEdit pictureEdit109;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.PictureEdit pictureEdit110;
        private DevExpress.XtraEditors.LabelControl labelControl133;
        private DevExpress.XtraEditors.PictureEdit pictureEdit111;
        private DevExpress.XtraEditors.LabelControl labelControl134;
        private DevExpress.XtraEditors.PictureEdit pictureEdit112;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.PictureEdit pictureEdit113;
        private DevExpress.XtraEditors.LabelControl labelControl136;
        private DevExpress.XtraEditors.PictureEdit pictureEdit114;
        private DevExpress.XtraEditors.LabelControl labelControl137;
        private DevExpress.XtraEditors.PictureEdit pictureEdit115;
        private DevExpress.XtraEditors.LabelControl labelControl138;
        private DevExpress.XtraEditors.PictureEdit pictureEdit116;
        private DevExpress.XtraEditors.LabelControl labelControl139;
        private DevExpress.XtraEditors.PictureEdit pictureEdit117;
        private DevExpress.XtraEditors.LabelControl labelControl140;
        private DevExpress.XtraEditors.PictureEdit pictureEdit118;
        private DevExpress.XtraEditors.LabelControl labelControl141;
        private DevExpress.XtraEditors.PictureEdit pictureEdit119;
        private DevExpress.XtraEditors.LabelControl labelControl142;
        private DevExpress.XtraEditors.PictureEdit pictureEdit120;
        private DevExpress.XtraEditors.LabelControl labelControl143;
        private DevExpress.XtraEditors.PictureEdit pictureEdit121;
        private DevExpress.XtraEditors.LabelControl labelControl144;
        private DevExpress.XtraEditors.PictureEdit pictureEdit122;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.PictureEdit pictureEdit59;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.PictureEdit pictureEdit60;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.PictureEdit pictureEdit61;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.PictureEdit pictureEdit62;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.PictureEdit pictureEdit63;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.PictureEdit pictureEdit64;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.PictureEdit pictureEdit65;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.PictureEdit pictureEdit66;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.PictureEdit pictureEdit67;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.PictureEdit pictureEdit68;
        private DevExpress.XtraEditors.LabelControl labelControl91;
        private DevExpress.XtraEditors.PictureEdit pictureEdit69;
        private DevExpress.XtraEditors.LabelControl labelControl92;
        private DevExpress.XtraEditors.PictureEdit pictureEdit70;
        private DevExpress.XtraEditors.LabelControl labelControl93;
        private DevExpress.XtraEditors.PictureEdit pictureEdit71;
        private DevExpress.XtraEditors.LabelControl labelControl94;
        private DevExpress.XtraEditors.PictureEdit pictureEdit72;
        private DevExpress.XtraEditors.LabelControl labelControl95;
        private DevExpress.XtraEditors.PictureEdit pictureEdit73;
        private DevExpress.XtraEditors.LabelControl labelControl96;
        private DevExpress.XtraEditors.PictureEdit pictureEdit74;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.PictureEdit pictureEdit43;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.PictureEdit pictureEdit44;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.PictureEdit pictureEdit45;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.PictureEdit pictureEdit46;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.PictureEdit pictureEdit47;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.PictureEdit pictureEdit48;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.PictureEdit pictureEdit49;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.PictureEdit pictureEdit50;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.PictureEdit pictureEdit51;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.PictureEdit pictureEdit52;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.PictureEdit pictureEdit53;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.PictureEdit pictureEdit54;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.PictureEdit pictureEdit55;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.PictureEdit pictureEdit56;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.PictureEdit pictureEdit57;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.PictureEdit pictureEdit58;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.PictureEdit pictureEdit27;
        private DevExpress.XtraEditors.PictureEdit pictureEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.PictureEdit pictureEdit28;
        private DevExpress.XtraEditors.PictureEdit pictureEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.PictureEdit pictureEdit29;
        private DevExpress.XtraEditors.PictureEdit pictureEdit21;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.PictureEdit pictureEdit30;
        private DevExpress.XtraEditors.PictureEdit pictureEdit22;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.PictureEdit pictureEdit31;
        private DevExpress.XtraEditors.PictureEdit pictureEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.PictureEdit pictureEdit32;
        private DevExpress.XtraEditors.PictureEdit pictureEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.PictureEdit pictureEdit33;
        private DevExpress.XtraEditors.PictureEdit pictureEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.PictureEdit pictureEdit34;
        private DevExpress.XtraEditors.PictureEdit pictureEdit26;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit35;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit36;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit37;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit38;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit39;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PictureEdit pictureEdit40;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PictureEdit pictureEdit41;
        private DevExpress.XtraEditors.PictureEdit Mic_2;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PictureEdit pictureEdit42;
        private DevExpress.XtraEditors.PictureEdit Mic_1;
        private DevExpress.Utils.ImageCollection ManImages;
        private DevExpress.XtraTab.XtraTabPage pgDelegatesHall;
        private DevExpress.XtraVerticalGrid.VGridControl PropCntrlDelegate2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox7;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catFIO_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastName_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFirstName_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSecondName_H;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catRegion_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionName_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRegionNum_H;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catInfo_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowFraction_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowParty_H;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow catSeat_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowMicNum_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowRowNum_H;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSeatNum_H;
        private System.Windows.Forms.Timer tmrRefreshHall;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowStartTime_std;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLastDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeech;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraBars.BarButtonItem btnCloseTheme;
        private DevExpress.XtraEditors.LabelControl info_2;
        private DevExpress.XtraEditors.LabelControl info_1;
        private DevExpress.XtraEditors.LabelControl info_9;
        private DevExpress.XtraEditors.LabelControl info_8;
        private DevExpress.XtraEditors.LabelControl info_7;
        private DevExpress.XtraEditors.LabelControl info_6;
        private DevExpress.XtraEditors.LabelControl info_5;
        private DevExpress.XtraEditors.LabelControl info_4;
        private DevExpress.XtraEditors.LabelControl info_3;
        private DevExpress.XtraEditors.LabelControl info_sel;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.Timer tmrWarningInfo;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem barInfoText;

    }
}