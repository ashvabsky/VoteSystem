namespace VoteSystem
{
    partial class ReportThemes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReport = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTemplateFile = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtResultFile = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.chkShowNonSpeech = new DevExpress.XtraEditors.CheckEdit();
            this.chkShowDetails = new DevExpress.XtraEditors.CheckEdit();
            this.txtResultPath = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtTemplateFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowNonSpeech.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDetails.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultPath.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReport
            // 
            this.btnReport.Location = new System.Drawing.Point(110, 401);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(161, 44);
            this.btnReport.TabIndex = 0;
            this.btnReport.Text = "������� �����";
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl1.Location = new System.Drawing.Point(43, 24);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(98, 16);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "���� �������:";
            // 
            // txtTemplateFile
            // 
            this.txtTemplateFile.Location = new System.Drawing.Point(42, 46);
            this.txtTemplateFile.Name = "txtTemplateFile";
            this.txtTemplateFile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtTemplateFile.Size = new System.Drawing.Size(530, 20);
            this.txtTemplateFile.TabIndex = 2;
            this.txtTemplateFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtTemplateFile_ButtonClick);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl2.Location = new System.Drawing.Point(43, 97);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(193, 16);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "��������� ��������� � ����:";
            // 
            // txtResultFile
            // 
            this.txtResultFile.Location = new System.Drawing.Point(253, 96);
            this.txtResultFile.Name = "txtResultFile";
            this.txtResultFile.Size = new System.Drawing.Size(319, 20);
            this.txtResultFile.TabIndex = 5;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Controls.Add(this.chkShowNonSpeech);
            this.groupControl1.Controls.Add(this.chkShowDetails);
            this.groupControl1.Location = new System.Drawing.Point(42, 202);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(530, 149);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "���������";
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(29, 34);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Caption = "�������� �������� ���";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style7;
            this.checkEdit1.Size = new System.Drawing.Size(280, 22);
            this.checkEdit1.TabIndex = 13;
            // 
            // chkShowNonSpeech
            // 
            this.chkShowNonSpeech.EditValue = true;
            this.chkShowNonSpeech.Location = new System.Drawing.Point(58, 97);
            this.chkShowNonSpeech.Name = "chkShowNonSpeech";
            this.chkShowNonSpeech.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkShowNonSpeech.Properties.Appearance.Options.UseFont = true;
            this.chkShowNonSpeech.Properties.Caption = "��������, ������� �������������";
            this.chkShowNonSpeech.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style8;
            this.chkShowNonSpeech.Size = new System.Drawing.Size(280, 22);
            this.chkShowNonSpeech.TabIndex = 10;
            // 
            // chkShowDetails
            // 
            this.chkShowDetails.EditValue = true;
            this.chkShowDetails.Location = new System.Drawing.Point(29, 70);
            this.chkShowDetails.Name = "chkShowDetails";
            this.chkShowDetails.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkShowDetails.Properties.Appearance.Options.UseFont = true;
            this.chkShowDetails.Properties.Caption = "�������� ���������� �� ���������";
            this.chkShowDetails.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style7;
            this.chkShowDetails.Size = new System.Drawing.Size(280, 22);
            this.chkShowDetails.TabIndex = 9;
            this.chkShowDetails.CheckStateChanged += new System.EventHandler(this.chkShowDetails_CheckStateChanged);
            // 
            // txtResultPath
            // 
            this.txtResultPath.Location = new System.Drawing.Point(42, 162);
            this.txtResultPath.Name = "txtResultPath";
            this.txtResultPath.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtResultPath.Size = new System.Drawing.Size(530, 20);
            this.txtResultPath.TabIndex = 14;
            this.txtResultPath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtResultPath_ButtonClick);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl3.Location = new System.Drawing.Point(42, 140);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(194, 16);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "���� � ����� � ������������:";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(342, 401);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(161, 44);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "�������";
            // 
            // ReportThemes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 479);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtResultPath);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.txtResultFile);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtTemplateFile);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportThemes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "������ ��������� �����������";
            this.Load += new System.EventHandler(this.ReportThemes_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportThemes_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.txtTemplateFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowNonSpeech.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDetails.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtResultPath.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnReport;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ButtonEdit txtTemplateFile;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtResultFile;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit chkShowNonSpeech;
        private DevExpress.XtraEditors.CheckEdit chkShowDetails;
        private DevExpress.XtraEditors.ButtonEdit txtResultPath;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
    }
}