﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoteSystem
{
    public partial class SeatComponent : UserControl
    {
        public SeatComponent()
        {
            InitializeComponent();
        }

        private void Picture_DragEnter(object sender, DragEventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        }

        private void picture_DragDrop(object sender, DragEventArgs e)
        {
            if (MyDragDrop != null)
            {
                MyDragDrop(this, e);
            }
        }

        private void picture_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void picture_DragLeave(object sender, EventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        private void SeatComponent_DragEnter(object sender, DragEventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        }

        private void SeatComponent_DragLeave(object sender, EventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        private void SeatComponent_DragDrop(object sender, DragEventArgs e)
        {
            MyMouseEnter(sender, e);
        }

        private void SeatComponent_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {

                Control c = (Control)sender;
                string s = c.Tag.ToString();

                c.DoDragDrop(s, DragDropEffects.Move);
            }
        }

        private void picture_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Control c = (Control)sender;
                string s = this.Tag.ToString();

                c.DoDragDrop(s, DragDropEffects.Move);
            }
        }

        private void SeatComponent_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void picture_MouseEnter(object sender, EventArgs e)
        {
            if (this.MyMouseEnter != null)
            {
                MyMouseEnter(this, e);
            }
        }

        private void SeatComponent_MouseEnter(object sender, EventArgs e)
        {
            if (this.MyMouseEnter != null)
            {
                MyMouseEnter(this, e);
            }
        }
    }
}
