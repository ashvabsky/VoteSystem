﻿namespace Screen
{
    partial class RegResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegResultForm));
            this.label2 = new System.Windows.Forms.Label();
            this.lblQntyReg = new System.Windows.Forms.Label();
            this.lblQntyDelegates = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblIsQuorum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(49, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(328, 34);
            this.label2.TabIndex = 4;
            this.label2.Text = "Зарегистрировалось:";
            // 
            // lblQntyReg
            // 
            this.lblQntyReg.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyReg.AutoSize = true;
            this.lblQntyReg.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyReg.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyReg.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyReg.Location = new System.Drawing.Point(389, 172);
            this.lblQntyReg.Name = "lblQntyReg";
            this.lblQntyReg.Size = new System.Drawing.Size(51, 34);
            this.lblQntyReg.TabIndex = 10;
            this.lblQntyReg.Text = "50";
            // 
            // lblQntyDelegates
            // 
            this.lblQntyDelegates.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblQntyDelegates.AutoSize = true;
            this.lblQntyDelegates.BackColor = System.Drawing.Color.Transparent;
            this.lblQntyDelegates.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblQntyDelegates.ForeColor = System.Drawing.Color.Gold;
            this.lblQntyDelegates.Location = new System.Drawing.Point(389, 230);
            this.lblQntyDelegates.Name = "lblQntyDelegates";
            this.lblQntyDelegates.Size = new System.Drawing.Size(51, 34);
            this.lblQntyDelegates.TabIndex = 16;
            this.lblQntyDelegates.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Cornsilk;
            this.label6.Location = new System.Drawing.Point(49, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(263, 34);
            this.label6.TabIndex = 15;
            this.label6.Text = "Карт на пультах:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(32, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(421, 39);
            this.label3.TabIndex = 21;
            this.label3.Text = "Регистрация завершена";
            // 
            // lblIsQuorum
            // 
            this.lblIsQuorum.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblIsQuorum.AutoSize = true;
            this.lblIsQuorum.BackColor = System.Drawing.Color.Transparent;
            this.lblIsQuorum.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIsQuorum.ForeColor = System.Drawing.Color.Gold;
            this.lblIsQuorum.Location = new System.Drawing.Point(146, 289);
            this.lblIsQuorum.Name = "lblIsQuorum";
            this.lblIsQuorum.Size = new System.Drawing.Size(189, 25);
            this.lblIsQuorum.TabIndex = 22;
            this.lblIsQuorum.Text = "Кворум имеется";
            // 
            // RegResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(480, 390);
            this.Controls.Add(this.lblIsQuorum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblQntyDelegates);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblQntyReg);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RegResultForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.RegResultForm_Load);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblQntyReg, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.lblQntyDelegates, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.lblIsQuorum, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQntyReg;
        private System.Windows.Forms.Label lblQntyDelegates;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblIsQuorum;

    }
}

