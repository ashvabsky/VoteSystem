﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.WebSockets;

namespace Platform
{
    // сервер для взаимодействия с вебклиентами конференцсистемы через вебсокет
    // по специальному протоколу команд

    public class ClientState
    {
        public WebSocket socket;
        public int userId;
        public int micNum;
        public bool isValid()
        {
            return micNum > 0 && socket != null;
        }
    }

    //## The Server class        
    public class ConfServer
    {
        const string WEB_PREFIX = "web:";
        const string WEB_INIT_STRING = "web:init";

        private int _count = 0;
        private HttpListener _listener;

        public delegate void OnReceiveDataDelegate(string data);
        private OnReceiveDataDelegate _onReceiveData;
        private Thread _clientThread;
        private List<ClientState> _clients;

        //### Starting the server        
        public void Start(OnReceiveDataDelegate processMethod)
        {
            _onReceiveData = processMethod;

            string listenerPrefix = "http://+:10501/";
            _listener = new HttpListener();
            _listener.Prefixes.Add(listenerPrefix);
            _listener.Start();
            Console.WriteLine("Listening...");

            _clients = new List<ClientState>();
            _clientThread = new Thread(new ThreadStart(this.ReceiveClient));
            _clientThread.Start();
        }

        private async void ReceiveClient()
        {
            while (true)
            {
                try
                {
                    HttpListenerContext listenerContext = await _listener.GetContextAsync();
                    if (listenerContext.Request.IsWebSocketRequest)
                    {
                        ProcessClient(listenerContext);
                    }
                    else
                    {
                        listenerContext.Response.StatusCode = 400;
                        listenerContext.Response.Close();
                    }
                } catch (System.Net.HttpListenerException e)
                {
                    Console.WriteLine("Exception: {0}", e);
                    return;
                }
            }
        }

        //### Accepting WebSocket connections
        private async void ProcessClient(HttpListenerContext listenerContext)
        {

            WebSocket webSocket;
            try
            {
                WebSocketContext webSocketContext = await listenerContext.AcceptWebSocketAsync(subProtocol: null);
                Interlocked.Increment(ref _count);
                Console.WriteLine("Processed: {0}", _count);

                webSocket = webSocketContext.WebSocket;
            }
            catch (Exception e)
            {
                // The upgrade process failed somehow. For simplicity lets assume it was a failure on the part of the server and indicate this using 500.
                listenerContext.Response.StatusCode = 500;
                listenerContext.Response.Close();
                Console.WriteLine("Exception: {0}", e);
                return;
            }
            
            if (webSocket == null) return;

            try
            {
                //### Receiving
                byte[] receiveBuffer = new byte[1024];

                // While the WebSocket connection remains open run a simple loop that receives data and sends it back.
                while (webSocket.State == WebSocketState.Open)
                {
                    WebSocketReceiveResult receiveResult = await webSocket.ReceiveAsync(new ArraySegment<byte>(receiveBuffer), CancellationToken.None);
                    if (receiveResult.MessageType == WebSocketMessageType.Close)
                    {
                        Console.WriteLine("Websocket: closing");
                        foreach (ClientState state in _clients) 
                        {
                            if (state.socket == webSocket)
                            {
                                _clients.Remove(state);
                            }
                        }

                        break;
                    }
                    // This echo server can't handle text frames so if we receive any we close the connection with an appropriate status code and message.
                    else if (receiveResult.MessageType == WebSocketMessageType.Text)
                    {
                        if (receiveResult.Count > 0)
                        {
                            // There might be more data, so store the data received so far.
                            string s = Encoding.ASCII.GetString(receiveBuffer, 0, receiveResult.Count);
                            Console.WriteLine("Webcocket received: {0}", s);

                            // string[] stringSeparators = new string[] { "0d\r", "0d", "\r" };
                            string[] stringSeparators = new string[] { "\r" };
                            string[] result = s.Split(stringSeparators, 1000, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string response in result)
                            {
                                if (response.Contains(WEB_INIT_STRING))
                                {
                                    ClientState clientState = createWebClientState(webSocket, response);
                                    if (clientState != null && clientState.isValid())
                                    {
                                        ClientState st = webClientState(clientState.micNum);

                                        if (st?.socket == webSocket) // check for dublicate user init
                                        {
                                            continue;
                                        }
                                        _clients.Add(clientState);
                                    }
                                    continue;
                                }

                                int ind_dv = response.IndexOf(WEB_PREFIX);
                                if (ind_dv > -1)
                                {
                                    string r = response.Substring(ind_dv + WEB_PREFIX.Length);
                                    _onReceiveData(r);
                                }
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("WebSocket: Non text message received.");
                    }
                }
            }
            catch (Exception e)
            {
                String msg = String.Format("Web socket is closed with exception {0}", e);
                Console.WriteLine(msg);
                if (webSocket.State == WebSocketState.Open)
                {
//                    await webSocket.CloseAsync(WebSocketCloseStatus.InvalidMessageType, msg, CancellationToken.None);
                }
            }
        
            finally
            {
                if (webSocket.State == WebSocketState.Open)
                {
//                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                }
                if (webSocket.State == WebSocketState.Closed)
                {
                    webSocket.Dispose();
                }
                Interlocked.Decrement(ref _count);
            }
        }

        private ClientState createWebClientState(WebSocket socket, string initString)
        {
            if (!initString.Contains(WEB_INIT_STRING)) return null;

            string[] parts = initString.Split('_');

            bool success = int.TryParse(parts[1], out int micNum);
            if (!success) return null;

            ClientState res = new ClientState();
            res.micNum = micNum;
            res.userId = micNum;
            res.socket = socket;

            return res;
        }

        private ClientState webClientState(int micNum)
        {
            foreach (ClientState state in _clients)
            {
                if (state.micNum == micNum)
                {
                    return state;
                }
            }

            return null;
        }

        public async void Send(int micNum, string cmd)
        {
            ClientState st = webClientState(micNum);
            if (st == null) return;

            byte[] byteData = Encoding.ASCII.GetBytes(cmd);
            var sendBuffer = new ArraySegment<byte>(byteData);
            try
            {
                await st.socket.SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
                return;
            }
        }

        public async void SendAll(string cmd)
        {
            var tasks = new List<Task>();
            foreach (ClientState state in _clients)
            {
                byte[] byteData = Encoding.ASCII.GetBytes(cmd);
                var sendBuffer = new ArraySegment<byte>(byteData);

                Task task = state.socket.SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                tasks.Add(task);
            }

            foreach (Task task in tasks)
            {
                try
                {
                    if (task.Status == TaskStatus.Created || task.Status == TaskStatus.Running)
                    {
                        await task;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: {0}", e);
                    return;
                }
            }

            return;
        }

        public void ShutDown()
        {
            _clientThread?.Abort();
            _listener.Stop();
        }
    }

    // This extension method wraps the BeginGetContext / EndGetContext methods on HttpListener as a Task, using a helper function from the Task Parallel Library (TPL).
    // This makes it easy to use HttpListener with the C# 5 asynchrony features.
    public static class HelperExtensions
    {
        public static Task GetContextAsync(this HttpListener listener)
        {
            return Task.Factory.FromAsync<HttpListenerContext>(listener.BeginGetContext, listener.EndGetContext, TaskCreationOptions.None);
        }
    }
}
