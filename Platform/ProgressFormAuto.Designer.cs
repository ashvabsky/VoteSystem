namespace Platform
{
    partial class ProgressFormAuto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.lblText = new DevExpress.XtraEditors.LabelControl();
            this.bntOk = new DevExpress.XtraEditors.SimpleButton();
            this.tmrProgress = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(28, 37);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Properties.Step = 1;
            this.progressBarControl1.Size = new System.Drawing.Size(257, 23);
            this.progressBarControl1.TabIndex = 2;
            // 
            // lblText
            // 
            this.lblText.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblText.Location = new System.Drawing.Point(28, 14);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(257, 16);
            this.lblText.TabIndex = 4;
            this.lblText.Text = "���� ������� ����������� ��������...";
            // 
            // bntOk
            // 
            this.bntOk.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bntOk.Appearance.Options.UseFont = true;
            this.bntOk.Location = new System.Drawing.Point(109, 66);
            this.bntOk.Name = "bntOk";
            this.bntOk.Size = new System.Drawing.Size(89, 23);
            this.bntOk.TabIndex = 5;
            this.bntOk.Text = "��";
            this.bntOk.Click += new System.EventHandler(this.bntOk_Click);
            // 
            // tmrProgress
            // 
            this.tmrProgress.Interval = 500;
            this.tmrProgress.Tick += new System.EventHandler(this.tmrProgress_Tick);
            // 
            // ProgressFormAuto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 98);
            this.Controls.Add(this.bntOk);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.progressBarControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressFormAuto";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProgressFormAuto";
            this.Load += new System.EventHandler(this.ProgressFormAuto_Load);
            this.Shown += new System.EventHandler(this.ProgressFormAuto_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProgressFormAuto_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProgressFormAuto_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.LabelControl lblText;
        private DevExpress.XtraEditors.SimpleButton bntOk;
        private System.Windows.Forms.Timer tmrProgress;
    }
}