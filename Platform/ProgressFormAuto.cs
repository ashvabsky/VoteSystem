using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;

namespace Platform
{
    public partial class ProgressFormAuto : DevExpress.XtraEditors.XtraForm
    {
        public ManualResetEvent ProgressDone =
            new ManualResetEvent(false);

        public int _maxTime = 0;

        private int step = 10;

        public string ProgressText = "";
//      public string ProgressFinishText = "";
        public string ProgressFailedText = "";

//      public bool IsNeedFinishEvent = false;
//      public bool IsAutoCloseMode = false;

        public bool IsSuccess = false;
        private int _ticks = 0;
        public ProgressFormAuto()
        {
            InitializeComponent();
        }

        public int MaxTime
        {
            set
            {
                _maxTime = value;
                step = (tmrProgress.Interval * 100) / (value);
                step = step * 2;
            }
        }

        private void tmrProgress_Tick(object sender, EventArgs e)
        {
            _ticks++;
            if (progressBarControl1.Position < 100 && ((_ticks % 2) == 0))
            {
                //progressBarControl1.Position = progressBarControl1.Position + progressBarControl1.Properties.Step;
                progressBarControl1.Increment(step);
//              Hide();
            }


            if (ProgressDone.WaitOne(1))
            {
                tmrProgress.Stop();
                progressBarControl1.Position = 100;
                lblText.Text = "Завершено!";
                IsSuccess = true;
//               bntOk.Enabled = true;

//                this.Close();
            }
            else if (progressBarControl1.Position == 100)
            {
                bntOk.Enabled = true;
                lblText.Text = ProgressFailedText;
                IsSuccess = false;
                Show();
            }
        }

        private void ProgressFormAuto_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.Manual;
            this.Top = this.Owner.Bottom - this.Height;
            this.Left = this.Owner.Right - this.Width;
        }

        private void ProgressFormAuto_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ProgressDone.WaitOne(1) == false)
            {
                IsSuccess = false;
                ProgressDone.Set();
            }
        }

        private void ProgressFormAuto_FormClosing(object sender, FormClosingEventArgs e)
        {
              e.Cancel = true;

              Finish();
        }

        private void ProgressFormAuto_Shown(object sender, EventArgs e)
        {

            Hide();

/*
            progressBarControl1.Position = 0;
            tmrProgress.Start();

            lblText.Text = ProgressText;
            bntOk.Enabled = false;
            IsSuccess = false;
 */ 
        }

        public void Start()
        {

            progressBarControl1.Position = 0;
            tmrProgress.Start();

            lblText.Text = ProgressText;
            bntOk.Enabled = false;
            IsSuccess = false;
        }

        private void bntOk_Click(object sender, EventArgs e)
        {
            Finish();
//          this.Close();
        }

        private void Finish()
        {
            if (ProgressDone.WaitOne(1) == false)
            {
                IsSuccess = false;
                ProgressDone.Set();
            }

            Hide();
        }
    }
}