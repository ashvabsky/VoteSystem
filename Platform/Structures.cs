﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Platform
{
    [Serializable]
    public class SessionInfo
    {
        public int id;
        public string Caption;
        public string Description;
        public int DelegateQnty;
        public int QuorumQnty;
    }

    [Serializable]
    public class DelegateInfo
    {
        public string FIO;
        public string RegionName;
        public string PartyName;
        public string FractionName;
        public int RowNum;
        public int SeatNum;
        public int MicNum;
    }

    [Serializable]
    public class RegInfoStart
    {
        public string CurrDelegate;
        public int DelegateQnty;
        public int RegQnty;
        public int leftSeconds;
        public int leftMinutes;
        public string leftTime;
        public bool IsQuorum;
        public int Progress;
    }

    [Serializable]
    public class RegInfoFinish
    {
        public int DelegateQnty;
        public int RegQnty;
        public int SeatQnty;
        public bool IsQuorum;
    }

    [Serializable]
    public class QuestionInfo
    {
        public string Caption;
        public string Description;
        public string Type;
        public int VoteTime;

    }

    [Serializable]
    public class VoteInfoStart
    {
        public int DelegateQnty;
        public int VotedQnty;
        public int leftSeconds;
        public int leftMinutes;
        public string leftTime;
        public int Progress;
    }

    [Serializable]
    public class VoteInfoResult
    {
        public int DelegateQnty;
        public int VotedQnty_A;
        public int VotedQnty_B;
        public int VotedQnty_C;
        public bool IsResult;
    }

    [Serializable]
    public class SpeechInfoStart
    {
        public int leftMinutes; // в минутах
        public int SpeechTime; // в минутах
    }

}
