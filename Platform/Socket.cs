﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace Platform
{
    // State object for receiving data from remote device.
    public class StateObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    public class AsynchronousClient
    {
        // The port number for the remote device.
        private const int port = 10500;

        // ManualResetEvent instances signal completion.
        private static ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private static ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private static ManualResetEvent receiveDone =
            new ManualResetEvent(false);


        private static Socket _client;
//        public static Queue _commandStack = new Queue();

        public static void StartClient(string sysname)
        {
            // Connect to a remote device.
            try
            {
                // Establish the remote endpoint for the socket.
                // The name of the 
                // remote device is "host.contoso.com".
                //                IPHostEntry ipHostInfo = Dns.Resolve("host.contoso.com");
                IPAddress ipAddress = IPAddress.Parse("192.168.1.80");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.
                _client = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
/*
                // Connect to the remote endpoint.
                _client.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), _client);
 */ 
//              connectDone.WaitOne();

                // Send test data to the remote device.
                string initstring = "ms:name=" + sysname;
                Send(initstring);
//              sendDone.WaitOne();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void ShutDownClient()
        {
            // Connect to a remote device.
            try
            {
                // Release the socket.
                _client.Shutdown(SocketShutdown.Both);
                _client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void RecieveClient()
        {
            // Connect to a remote device.
            try
            {
                Receive(_client);
                //                receiveDone.WaitOne();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.
                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.
                int bytesRead = client.EndReceive(ar);
                receiveDone.Set();

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    string s = Encoding.ASCII.GetString(state.buffer, 0, bytesRead);

                    string[] stringSeparators = new string[] { "0d\r", "0d", "\r" };
                    string[] result;

                    result = s.Split(stringSeparators, 10, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string response in result)
                    {
                        int ind_dv = response.IndexOf(":");
                        if (ind_dv > -1)
                        {
                            string r = response.Substring(ind_dv+1);
                            NetHelper._commandStack.Enqueue(r);
                        }
/*
                        int ind = s.IndexOf("0d");
                        if (ind > 0)
                        {
                            state.sb.Append(s.Substring(0, ind));
                            // The response from the remote device.
                            String response = String.Empty;

                            // All the data has arrived; put it in response.
                            if (state.sb.Length >= 1)
                            {
                                response = state.sb.ToString();
                                if (response.StartsWith(":"))
                                    response = response.Substring(1);
                                NetHelper._commandStack.Enqueue(response);
                            }
                            // Signal that all bytes have been received.


                        }
 */ 
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void Send(String data)
        {
            // Convert the string data to byte data using ASCII encoding.
/*
            string s = "\r";
            if (!data.Contains(":"))
            {
                data = "creator_big:" + data;
            }

            data = data + s; // добавить перевод каретки

            byte[] byteData = Encoding.ASCII.GetBytes(data);


            // Begin sending the data to the remote device.
            _client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), _client);
 */ 
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                //              Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}
